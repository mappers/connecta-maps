
const path = require('path');
const fs = require('fs');
const { exec } = require('child_process');
let paths = [];
let ConfigAll = JSON.parse(
    fs.readFileSync(path.resolve(__dirname, 'Config.json')).toString()
);

let getPaths = (currentPath) => {

    if (!fs.lstatSync(currentPath).isDirectory()) {
        return;
    }

    let directories = fs.readdirSync(currentPath);
    let notWillBeIncluded = /server\/custom\/.+/.test(currentPath);
    let parts = currentPath.split('/');
    let customIndex = parts.indexOf('custom');

    if (ConfigAll.serverInject && ConfigAll.serverInject.module) {
        for (let moduleName in ConfigAll.serverInject.module) {
            if (parts[customIndex + 1] === moduleName) {
                notWillBeIncluded = false;
                break;
            }
        }
    }

    if (notWillBeIncluded || /node_modules/.test(currentPath)) {
        return;
    }

    for (let fileName of directories) {
        if (!(/\../.test(fileName))) {
            getPaths(path.resolve(currentPath, fileName));
        } else if (/package\.json/.test(fileName)) {
            paths.push(currentPath);
        }
    }
};

getPaths(__dirname);

for (let _path of paths) {
    console.log('Installing dependencies into: ' + _path);

    exec(`cd ${_path} && npm i`, (error, stdout, stderr) => {

        let print = (type, message) => {
            let nodeColor;
            if (type === 'info') {
                nodeColor = "\x1b[34m%s\x1b[0m";//Azul
            } else {
                nodeColor = '\x1b[33m%s\x1b[0m';//Amarelo
            }
            console.log(nodeColor, message);
        };

        let strRunning = ['\x1b[1m%s\x1b[0m', 'Running into ' + _path];

        if (error) {
            console.log(strRunning[0], strRunning[1]);
            console.error(`exec error: ${error}`);
        }

        if (stdout) {
            console.log(strRunning[0], strRunning[1]);
            print('info', `stdout: ${stdout}`);
        }

        if (stderr) {
            console.log(strRunning[0], strRunning[1]);
            print('warn', `stderr: ${stderr}`);
        }
    });
}