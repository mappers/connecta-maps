import FieldNames from "../constant/FieldNames";
import axios from 'axios';

export class FindFieldOptions {

    static _fields = Object.values(FieldNames());

    static getDistinctValues () {
        let promises = [];

        FindFieldOptions._fields.forEach((field) => {
            promises.push(
                axios.get(
                    // ConnectaMaps.APP_CONFIG.serviceDomain + "/valec/ocorrencias",
                    "http://172.16.0.43:4100" + "/valec/ocorrencias",
                    {
                        params : {
                            filter : JSON.stringify([
                                {
                                    field,
                                    operator : "distinct"
                                }
                            ])
                        }
                    }
                )
            );
        });

        return Promise.all(promises)
            .then((results) => {
                let distinctValues = {};

                FindFieldOptions._fields.forEach((field, index) => {
                    results[index].data.result.forEach((row) => {
                        if (!distinctValues[field]) {
                            distinctValues[field] =  [];
                        }

                        distinctValues[field] = distinctValues[field].concat(Object.values(row));
                    });
                });

                return distinctValues;
            });
    }
}