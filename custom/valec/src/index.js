import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";

let ConnectaMaps = window['ConnectaMaps'];

const theme = {
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: '#004575',
    },
    menuItem: {
        selectedTextColor: '#004575',
    }
};

class VALEC {

    appInstance;

    constructor(node) {
        ReactDOM.render(
            <MuiThemeProvider muiTheme={getMuiTheme(theme)}>
                <App ref={el => this.appInstance = el } />
            </MuiThemeProvider>,
            node
        );
    }

    getExtension() {
        return {
            Icon : <div>F.O.</div>,
            title : 'Filtrar Ocorrências',
            onClick : ($parent) => {
                this.appInstance.onExtensionClick($parent);
            }
        };
    }

}

window['VALEC'] = VALEC;

let pattern = /\#\/maps\-viewer\/([\d]+)\/domain\/([\d]+)/;

if (window.DEV) {
    let locationHash = window.location.hash;
    let viewerId = '8336'; // Será pego dinamicamente pelo hash: /#/map-viewer/:viewerId/domain/:domainId
    let domainId = '64';// Será pego dinamicamente pelo hash: /#/map-viewer/:viewerId/domain/:domainId
    let connectaUrl = ConnectaMaps.APP_CONFIG.serviceDomain + '/proxy/' + ConnectaMaps.APP_CONFIG.hostApplicationDomain + '/connecta-presenter';

    if (pattern.test(locationHash)) {
        viewerId = window.location.hash.replace(pattern, '$1');
        domainId = Number(window.location.hash.replace(pattern, '$2'));
    }

    if (!viewerId || !domainId) {
        window.location = '#/maps-viewer/{viewerID}/domain/{domainId}';
    } else {
        let instance = new ConnectaMaps(document.getElementById('root'));
        instance.requestHeader['c-maps-domain-id'] = domainId;
        let connectaService = new ConnectaMaps.ConnectaService(instance);

        let options = {
            domainId : domainId,
            connectaUrl
        };

        connectaService.setup(viewerId, options)
            .then((viewer) => {
                instance.viewerId = viewer._id;
                ConnectaMaps.MapsDataLoader.set(viewer._id, viewer);
                instance.DEV = true;

                //VALEC
                ConnectaMaps.themeConfig.palette.primary1Color = '#004575';
                let valecInstance = new VALEC(document.getElementById('root2'));
                instance.putExtension(valecInstance.getExtension());

                instance.render();
                instance.changeCSSColors();
            });
    }
}

/*if (window.DEV) {
    this.instanceMaps = new ConnectaMaps(document.getElementById('root'));

    this.instanceMaps.requestHeader['c-maps-domain-id'] = 72;
    let viewerId = '77150';
    let connectaService = new ConnectaMaps.ConnectaService(this.instanceMaps);

    let options = {
        domainId : 72,
        connectaUrl : 'http://jboss.connectaap.cds.com.br/connecta-presenter'
    };

    connectaService.setup(viewerId, options)
        .then((viewer) => {
            this.instanceMaps.viewerId = viewer._id;
            ConnectaMaps.MapsDataLoader.set(viewer._id, viewer);

            //VALEC
            ConnectaMaps.themeConfig.palette.primary1Color = '#004575';
            let valecInstance = new VALEC(document.getElementById('root2'));
            this.instanceMaps.putExtension(valecInstance.getExtension());

            this.instanceMaps.render();
            this.instanceMaps.changeCSSColors();
        });
}*/


