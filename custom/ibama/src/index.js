import React from "react";
import ReactDOM from "react-dom";
import IBAMAComponent from "./component/index";
import "./style/index.scss";

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import {grey700, indigo900} from "material-ui/styles/colors";
import reactTapEventPlugin from "react-tap-event-plugin";

reactTapEventPlugin();

const configTheme = {
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: indigo900,
        textColor: grey700,

    },
    menuItem: {
        selectedTextColor: indigo900,
    }
};

ReactDOM.render(
    <MuiThemeProvider muiTheme={getMuiTheme(configTheme)}>
        <IBAMAComponent />
    </MuiThemeProvider>,
    document.getElementById('root')
);
