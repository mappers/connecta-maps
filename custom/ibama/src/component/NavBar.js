import React, {Component} from "react";
import {FlatButton, Tab, Tabs} from "material-ui";
import SearchIcon from "material-ui/svg-icons/action/search";
import IbamaService from "./Service";
import CustomDialog from "./CustomDialog";
import FormularioBuscarProcessos from "./NavBar/FormularioBuscarProcessos";
import FormularioBuscarTipologia from "./NavBar/FormularioBuscarTipologia";

const locale = window['mapsLocale'];

export default class IbamaNavBar extends Component {

    _baseClass = "cmaps-ibama-nav-bar-component";

    _maxTabHeight;

    _processosSelecionados = [];

    _tipologiasSelecionadas = [];

    _pesquisaAnterior = null;

    state = {};

    handleSeeMapClick = () => {
        let processNumbers = this._processosSelecionados.map((processo) => {
            return processo.NU_PROCESSO_IBAMA;
        });

        this._pesquisaAnterior = this._processosSelecionados;

        if(processNumbers.length > 0) {
            this.setMessage('Carregando...');

            IbamaService.createViewer(processNumbers)
                .then(data => {
                    if (this.props.onLoad) {
                        data.processosSelecionados = this._processosSelecionados;
                        this.props.onLoad(data);
                    }
                    this.setState({
                        searchDialogOpen : false
                    });
                    this.setMessage('');
                })
                .catch(({response}) => {
                    this.setMessage(
                        (response.data.details ? response.data.details.message : response.data.message)
                    );
                });
        } else {
           this.setMessage('Escolha um processo!');
        }
    };

    setMessage(message) {
        window['ConnectaMaps'].MapsDataLoader.set('IBAMA_MESSAGE', message);
    }

    onChangePesquisaProcessos = (processos) => {
        this._processosSelecionados = processos;
    };

    onChangePesquisaTipologia = (tipologias) => {
        this._tipologiasSelecionadas = tipologias;
    };

    onCancelSearch = () => {
        if(this._pesquisaAnterior) {
            this._processosSelecionados = this._processosSelecionados.filter((item) => {
                let result = false;

                for(let index in this._pesquisaAnterior){
                    if(this._pesquisaAnterior[index].NU_PROCESSO_IBAMA === item.NU_PROCESSO_IBAMA){
                        result = true;
                        break;
                    }
                }

                return result;
            });
        }
        this.setState({searchDialogOpen: false});
    };

    render() {
        return (
            <div className={this._baseClass}>
                <div style={{background: '#fff', height: '48px'}}>
                    <div style={{   display: 'flex', height: '48px', flexDirection: 'row-reverse', alignItems: 'center'}}>
                        <FlatButton
                            label="Buscar processos"
                            labelPosition="after"
                            labelStyle={{color: '#1c1b4f'}}
                            onTouchTap={() => this.setState({searchDialogOpen: true})}
                            icon={<SearchIcon  color="#1c1b4f" />} />
                    </div>
                </div>

                <CustomDialog ref={(element) => {this._maxTabHeight = this._maxTabHeight || element.state.height}}
                              open={this.state.searchDialogOpen}
                              actions={[
                                  <FlatButton key="1"
                                              label={locale.getLabel('L0037')}
                                              onTouchTap={this.onCancelSearch}
                                              primary={true}>
                                  </FlatButton>,
                                  <FlatButton key="2"
                                              label={"Ver no mapa"}
                                              primary={true}
                                              onTouchTap={() => {
                                                  this.handleSeeMapClick();
                                              }}>
                                  </FlatButton>
                              ]}>
                    <Tabs>
                        <Tab label="Buscar processos">
                            <FormularioBuscarProcessos maxHeight={this._maxTabHeight}
                                                       processosSelecionados={this._processosSelecionados}
                                                       onChange={this.onChangePesquisaProcessos}/>
                        </Tab>

                        <Tab label="Empreendimentos">
                            <FormularioBuscarTipologia maxHeight={this._maxTabHeight}
                                                       onChange={this.onChangePesquisaTipologia}/>
                        </Tab>
                    </Tabs>
                </CustomDialog>
            </div>
        );
    }
}