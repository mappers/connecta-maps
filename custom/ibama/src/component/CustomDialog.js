import React from 'react';

class CustomDialog extends React.Component {

    _baseClass = 'cmaps-custom-dialog-component';

    static PropTypes = {
        autoWidth : React.PropTypes.bool,
        height : React.PropTypes.number,
        title : React.PropTypes.string,
        actions : React.PropTypes.arrayOf(React.PropTypes.element)
    };

    static RefTypes = {
        dialogNode : React.PropTypes.element
    };

    constructor(props) {
        super(props);
        let size = this.getSize();
        this.state = {
            height : size.height,
            width : size.width,
            message : ''
        };
    }

    getSize() {
        let windowHeight = window.innerHeight;
        let windowWidth = window.innerWidth;
        let autoWidth = this.props.autoWidth !== undefined ? this.props.autoWidth : false;

        if (!this.props.width) {
            autoWidth = true;
        }

        return {
            height : this.props.height || (windowHeight - 70),
            width : autoWidth ? (windowWidth - 70) : this.props.width
        };
    }

    componentDidMount () {
        let body = document.body;
        body.style.margin = 0;
        body.insertBefore(this.refs.dialogNode, body.childNodes[body.childNodes.length - 1]);

        window.onresize = () => {
            let size = this.getSize();
            this.setState({
                height : size.height,
                width : size.width
            });
        };

        window['ConnectaMaps'].MapsDataLoader.watch('IBAMA_MESSAGE', (message) => {
            this.setState({message});
        });
    }

    render () {
        let contentStyle ={
            overflowY : 'auto',
            overflowX : 'hidden',
            height : this.props.title ? '80%' : '90%'
        };

        return (
            <div ref="dialogNode">
                {this.props.open && (
                    <div className={this._baseClass} style={{height : window.innerHeight}}>
                        <div className={this._baseClass + "-dialog"}
                             style={{height : this.state.height, width : this.state.width}}>

                            {this.props.title && (
                                <div className={this._baseClass + "-dialog-header"}>
                                    <h3 className={this._baseClass + "-dialog-header-title"}>{this.props.title}</h3>
                                </div>
                            )}

                            {this.props.title && (
                                <hr style={{width : '100%', opacity : '0.3'}}/>
                            )}

                            <div className={this._baseClass + "-dialog-content"}
                                 style={contentStyle}>
                                {this.props.children}
                            </div>

                            <hr style={{width : '100%', opacity : '0.3'}}/>

                            <div className={this._baseClass + "-dialog-footer"}>
                                <div className={this._baseClass + '-messages'}
                                     style={{
                                         position : 'absolute',
                                         left : 0,
                                         top : 10,
                                         color : '#900'
                                     }}>{this.state.message}</div>

                                {this.props.actions}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default CustomDialog;