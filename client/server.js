const express = require('express');
const fs = require('fs');
const PATH = require('path');
let ConfigAll = JSON.parse(
    fs.readFileSync(PATH.resolve(__dirname, '../', 'Config.json')).toString()
);
const PORT = ConfigAll.client.port;
const HOST = ConfigAll.client.host || '';//IP da máquina (opcional)
const server = express();

server.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", req.header("Origin"));
    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
    res.header("Access-Control-Max-Age", "3600");
    res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});
server.use("/ibama", express.static(PATH.resolve(__dirname, '../', 'custom', 'ibama', 'build')));
server.use("/valec", express.static(PATH.resolve(__dirname, '../', 'custom', 'valec', 'build')));
server.use(express.static(PATH.resolve(__dirname, 'build')));

server.listen(PORT, HOST, () => {
    console.log('Server ready on port: %d', PORT);
});
