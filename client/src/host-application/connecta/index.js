import HostApplicationBase from "../../_base/abstract/HostApplicationBase";
import {scope} from "../../scope";
import {appConfig} from "../../app.config";

export default class Connecta extends HostApplicationBase {

    $getResultSetsMetadata() {
        let requestHelper = this.$parent.inject(scope.RequestHelper);
        let headers = { Domain : this.$parent.requestHeader['c-maps-domain-id'] };
        return requestHelper
            .get(appConfig.hostApplicationDomain + '/connecta-presenter/analysis',
                null,
                headers
            )
            .then(({data}) => {
                let promises = [];
                
                for (let analysis of data) {
                    promises.push(
                        requestHelper.get(
                            appConfig.hostApplicationDomain + '/connecta-presenter/analysis/' + analysis.id,
                            null,
                            headers
                        )
                    );
                }
                
                return Promise.all(promises)
                    .then((results) => {
                        let index = 0;
                        let result = [];
                        for (let analysis of results) {
                            result.push({
                                id : analysis.data.id,
                                name : analysis.data.name,
                                outFields : analysis.data.analysisColumns.map(({name, label}) => ({
                                    name,
                                    alias : label,
                                    valueType : 'raw'
                                }))
                            });
                            index += 1;
                        }
                        return { data : result };
                    });                
            });
    }

    build() {
        let parts = document.cookie.match(/user.domain.name\=(\d+)/);

        if (parts && parts.length) {
            this.$parent.requestHeader = {
                'c-maps-domain-id' : parts[1]
            };
        }
    }

}