import {cyan700, grey700, indigo900} from 'material-ui/styles/colors';

export default {
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: '#222',
        primary2Color: '#444',
        accent1Color: '#2a4966',
        textColor: grey700
    },
    menuItem: {
        selectedTextColor: '#004575',
    },
    tabs: {
        textColor: 'rgba(255,255,255, 0.4)'
    },
}