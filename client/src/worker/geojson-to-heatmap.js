/*global self*/

export const geoJSONToHeatMap = function geoJSONToHeatMap () {

    self.onmessage = function (event) {
        let config = event.data.config || {};
        let colors = config.colors || ['blue', 'green', 'yellow', 'red'];
        let geoJSON = event.data.geoJSON;
        let features = geoJSON.features || [];
        let legendInfo = {
            colors: colors,
            min : 1,
            max : features.length
        };
        let heatmapOptions = {
            radius: config.blurRadius,
            max : 1,
            gradient : {0.1: colors[0], 1: colors[1]}
        };
        let intensityField = config.field;
        let max = 0;
        let min = 0;

        try {
            let data = [];

            if (!(/point/i.test(geoJSON.geometryType))) {
                throw new Error('Apenas camadas pontuais suportam mapa de calor');
            }

            if (intensityField) {
                min = features[0].businessData[intensityField];
                features.forEach(({businessData}) => {
                    if (businessData[intensityField] > max) {
                        max = businessData[intensityField];
                    }
                    if (businessData[intensityField] < min) {
                        min = businessData[intensityField];
                    }
                });

                legendInfo.max = max;
                legendInfo.min = min;
                heatmapOptions.max = 1;
                heatmapOptions._intensityValues = {};

            } else {
                heatmapOptions.minOpacity = 0.6;
            }

            features.forEach(({businessData, geometry}) => {
                let coordinates = [].concat(geometry.coordinates).reverse();

                if (intensityField) {
                    let proportionValue = Number(
                        (
                            ((100 / (max - min)) * (parseInt(businessData[intensityField]) - min)) / 100
                        ).toFixed(2)
                    );
                    coordinates.push(proportionValue);
                    // heatmapOptions._intensityValues[proportionValue] = true;
                }

                data.push(coordinates);
            });
            self.postMessage({data, legendInfo, heatmapOptions});

        } catch (error) {
            console.log('ERROR: worker/geojson-to-heatmap', error.message);
            throw error;
        }

    };

    function getGradient2() {
        return  '#' + Math.floor(Math.random()*16777215).toString(16);
    }

};