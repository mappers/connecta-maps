import L from "leaflet";
import Q from "q";
import "leaflet/dist/leaflet.css";
import "./style/index.scss";
import {locale} from "../../i18n/Locale";
import ThematicLayer from "./ThematicLayer";
import ChartLayer from "./ChartLayer";
import ClusterLayer from "./ClusterLayer";
import HeatLayer from "./HeatLayer";
import SimpleRendererLayer from "./SimpleRendererLayer";
import {scopeSingletonFactory} from "../../helper/scopeSingletonFactory";
import {ANALYSIS_TYPE} from "../../constant/analysis";
import StaticLayer from "./StaticLayer";
import {VIEWER} from "../../constant/viewer";
import "Leaflet.MultiOptionsPolyline";
import "leaflet-measure/dist/leaflet-measure";
import "leaflet-graphicscale/dist/Leaflet.GraphicScale.min";
import "leaflet-graphicscale/dist/Leaflet.GraphicScale.min.css";
import "leaflet-mouse-position/src/L.Control.MousePosition";
import "leaflet-mouse-position/src/L.Control.MousePosition.css";
import {MAP_CONSTANT} from "../../constant/map";

L.Icon.Default.imagePath = '%MEDIA%/leaflet/';

export class MapAPI {

    /**
     * @type {ConnectaMaps}
     */
    $parent;

    map;

    _onResizeCallbacks = [];

    _analysisLayers = {};

    _staticLayers = {};

    /**
     * Lista instâncias das classes de mapa base
     * @type {{}}
     * @private
     */
    _basemaps = {};

    /**
     * Instância do mapa base atual
     * @type {Object}
     */
    _currentBasemap;

    /**
     * @type {ViewerStore}
     */
    $viewerStore;

    /**
     * @type {MapStore}
     */
    $mapStore;

    _handlers = [];

    _lastFeatureClicked;

    _isSwiped = false;

    _measureControlReference;

    constructor(parent) {
        this.$parent = parent;
        scopeSingletonFactory(this);

        this._handlers.push(
            this.$viewerStore.listen(VIEWER.DELETE_GROUP, this.onDeleteGroups.bind(this)),
            this.$viewerStore.listen(VIEWER.CHANGE_LENGTH_UNIT, this.onChangeLengthUnit.bind(this)),
            this.$mapStore.listen(MAP_CONSTANT.DESELECT_FEATURE, this.onDeselectFeature.bind(this))
        );
    }

    rebuildMap(mapConfig) {
        this.onDeleteGroups();
    }

    rebuildMeasure() {
        this._measureControlReference._container.remove();
        this._measureControlReference._layer.remove();
    }

    destroy() {
        this.onDeleteGroups();
    }

    buildMap(node, mapConfig) {
        return Q.Promise((resolve, reject) => {
            try {
                let center = mapConfig.center;
                this.map = L.map(node, {
                    crs : L.CRS.EPSG3857,
                    zoomSnap: 0.25,
                    zoomDelta: 0.25,
                    maxZoom : mapConfig.maxZoom || 30,
                    minZoom : mapConfig.minZoom || 1,
                    attributionControl : false
                });
                this.map.doubleClickZoom.disable();

                this.map.on('load', () => {
                    resolve();

                    this.map.on("zoom", (e) => {
                        this.$mapAction.mapZoomEnd(e);
                    });
                });

                this.map.on('resize', () => {
                    if (this._onResizeCallbacks.length) {
                        this._onResizeCallbacks.forEach((cb) => {
                            cb();
                        });
                    }
                });

                this.map.setView([center.lat, center.lng], mapConfig.zoom || 5);

                this.setControlsMap();
                this.setRulersToolControls();
                this.setSpacialProgressionCode();
                this.setMousePositionControls();
                this.setScaleControls();
                this.setSpatialReference();

            } catch (error) {
                reject(error);
            }
        });
    }

    setControlsMap() {
        let project = this.$viewerStore.viewer.project;
        let isStaticNavigation = typeof project.mapConfig.dynamicNavigation !== 'undefined' && !project.mapConfig.dynamicNavigation;
        let emptyFunction = () => {};

        if (isStaticNavigation) {
            this.map.dragging.disable();
            this.map.zoomControl.disable();
            this.map.doubleClickZoom.disable();
            this.map.scrollWheelZoom.disable();
            this.map.fitBounds = emptyFunction;
            this.map.flyToBounds = emptyFunction;
            this.map.panTo = emptyFunction;
            this.map.flyTo = emptyFunction;
            this.map.setView = emptyFunction;
        }
    }

    setRulersToolControls () {
        let newUnits = {
            squaredKilometers: {
                factor: 0.001,
                display: 'Km²',
                decimals: 2
            }
        };

        let navigatorLanguage = window.navigator.language.replace("-", "_");
        let primaryLengthUnit = this.$viewerStore.viewer.measureConfig ?
            this.$viewerStore.viewer.measureConfig.lengthUnity : 'kilometers';

        let primaryAreaUnit = this.$viewerStore.viewer.measureConfig ?
            this.$viewerStore.viewer.measureConfig.areaUnity : 'squaredKilometers';

        let options = {
            position: 'topright',
            primaryLengthUnit,
            secondaryLengthUnit : undefined,
            primaryAreaUnit,
            activeColor: '#e61100',
            completedColor: '#e61100',
            localization : navigatorLanguage,
            units : newUnits
        };

        this._measureControlReference = L.control.measure(options).addTo(this.map);

        let interval = setInterval(() => {
            let el = document.getElementsByClassName("leaflet-control-measure leaflet-control")[0];

            if (el) {
                el.classList.add("cmaps-widget-ruler");
                clearInterval(interval);
            }

        }, 1);
    }

    setScaleControls = () => {
        let options = {
            doubleLine: false,
            fill: 'fill',
            showSubunits: false,
            position : "bottomright"
        };

        L.control.graphicScale(options).addTo(this.map);
    };

    setSpacialProgressionCode = () => {
        L.control.attribution({prefix: this.getSpatialReferenceCode()}).addTo(this.map);
    };

    setMousePositionControls = () => {
        let options = {
            position : "bottomright",
            emptyString : locale.getMessage("APP_INFO_07"),
            separator : ', '
        };

        L.control.mousePosition(options).addTo(this.map);
    };

    setSpatialReference = () => {
        L.control.attribution({}).addAttribution(this.getSpatialReferenceCode());
    };

    getMapCenter() {
        return this.map.getCenter();
    }

    getCurrentZoom() {
        return this.map.getZoom();
    }

    getSpatialReferenceCode() {
        return this.$parent.mapAPI.map.options.crs.code;
    }

    isAnalysisLayerBuild(analysisId, type) {

        if (this._analysisLayers[analysisId]) {
            return !!this._analysisLayers[analysisId][type];
        }

        return false;
    }

    centerAtLayer(analysisId, type) {
        let layer = this._analysisLayers[analysisId];

        if (layer) {

            layer = layer[type];

            switch (type) {

                case ANALYSIS_TYPE.THEMATIC:
                case ANALYSIS_TYPE.CHART:
                case ANALYSIS_TYPE.STATIC:
                case ANALYSIS_TYPE.CLUSTER:
                    this.map.fitBounds(layer.getBounds());
                    break;
                case ANALYSIS_TYPE.HEATMAP:
                    break;
            }

        }
    }

    centerAtFeature(leafletLayer){
        let {feature} = leafletLayer;
        let coords;

        if (feature.geometry.type === 'Point') {
            coords = Object.assign([], feature.geometry.coordinates).reverse();
        } else {
            coords = leafletLayer.getCenter();
        }

        this.map.panTo(coords);
    }

    removeAnalysisLayers(analysisId) {
        let layer = this._analysisLayers[analysisId];

        if (layer) {
            for (let type in layer) {
                layer[type].remove();
                // this.map.removeLayer(layer[type]);
                delete layer[type];
            }
        }
    }

    reorderAnalysisLayers() {
        let scope = this;
        let countAnalyses = Object.entries(this.$viewerStore.analyses).length;
        let analysisConfig = this.$viewerStore.analysisConfig;

        reorder(analysisConfig);

        function reorder(arr) {
            arr.forEach((item) => {
                if (item.type === "GROUP") {
                    reorder(item.children);
                } else {
                    let pane = scope.map.getPane(item._id);

                    if (pane) {
                        pane.style.zIndex = 410 + countAnalyses;
                        countAnalyses -= 1;
                    }
                }
            });
        }
    };

    reorderStaticLayers() {
        let count = Object.entries(this.$viewerStore.layers).length;

        let reorder = (items) => {
            items.forEach((item) => {
                if (item.type === "GROUP") {
                    item.children && reorder(item.children);
                } else {
                    let layer = this._staticLayers[item._id];
                    if(layer)
                        layer.setZIndex(count--);
                }
            });
        };

        reorder(this.$viewerStore.layersConfig);
    }

    onDeleteGroups() {
        this._deleteGroupAnalysis();
        this._deleteGroupLayer();
    }

    onChangeLengthUnit() {
        this.rebuildMeasure();
        this.setRulersToolControls();
    }

    _deleteGroupAnalysis(){
        let analysisIds = Object.keys(this.$viewerStore.analyses);
        let layersIds = Object.keys(this._analysisLayers);

        layersIds.forEach((layerId) => {
            if (!analysisIds.includes(layerId)) {
                this.removeAnalysisLayers(layerId);
            }
        });
    }

    _deleteGroupLayer(){
        let ids = Object.keys(this.$viewerStore.layers);
        let layers = Object.keys(this._staticLayers);

        layers.forEach((layerId) => {
            if (!ids.includes(layerId)) {
                this.removeStaticLayer(layerId);
            }
        });
    }

    setLayerOpacity({id, opacity}){
        let layer = this._staticLayers[id];
        if(layer) {
            layer.setOpacity(opacity);
        }
    }

    setPaneOpacity(paneId, opacity) {
        let pane = this.map.getPane(paneId);
        if (pane) {
            pane.style.opacity = opacity;
        }
    }

    registerMoveSwipe(){
        this.map.on('move', this.onMoveSwipe);
    }

    onMoveSwipe = () => {
        setTimeout(() => {
            this.$mapAction.swipeLayer();
        }, 0);
    };

    swipeAnalysisLayer(analysisId, value) {
        let container = this.map.getPane(analysisId);
        let nw = this.map.containerPointToLayerPoint([0, 0]);
        let se = this.map.containerPointToLayerPoint(this.map.getSize());
        let clipX = nw.x + (se.x - nw.x) * value;

        if (!this._isSwiped) {
            this.$viewerAction.changeSwipeValue(0.5);
            clipX = Number(this.map.getContainer().offsetWidth) / 2 - 5;
        }

        if (container) {
            container.style.clip = 'rect(' + [nw.y, clipX, se.y, nw.x].join('px,') + 'px)';
        }

        this._isSwiped = true;
    }

    removeSwipeAnalysisLayer(analysisId) {
        let container = this.map.getPane(analysisId);
        container.style.clip = '';
        this.map.off('move', this.onMoveSwipe);
        this.$viewerAction.disableSwipe();
        this._isSwiped = false;
    }

    resizeMap() {
        this.map.invalidateSize(true);
    }

    /**
     * Método usado para EXIBIR todas a camadas inseridas no mapa a partir do Id de uma análise.
     * @param {String} analysisId
     */
    showLayerByAnalysisId(analysisId, type) {
        if (this._analysisLayers[analysisId]) {
            let layer = this._analysisLayers[analysisId][type];
            this.map.addLayer(layer);
        }
    }

    /**
     * Método usado para REMOVER todas a camadas inseridas no mapa a partir do Id de uma análise.
     * Este método mantém as camadas em memória, ele apenas remove do mapa, sem destruir as camadas.
     * @param {String} analysisId
     */
    hideLayerByAnalysisId(analysisId) {
        if (this._analysisLayers[analysisId]) {
            for (let type in this._analysisLayers[analysisId]) {
                let layer = this._analysisLayers[analysisId][type];
                this.map.removeLayer(layer);
            }
        }
    }

    /**
     *
     * @param {{
     *    name:String,
     *    thumbnail:String,
     *    url:String,
     *    type:String,
     *    options:Object
     * }} basemapConfig
     */
    changeBasemap(basemapConfig) {

        switch(basemapConfig.type) {
            case 'tileLayer':
                if (!this._basemaps[basemapConfig.name]) {
                    this._basemaps[basemapConfig.name] = L.tileLayer(basemapConfig.url, basemapConfig.options);
                }
                this._basemaps[basemapConfig.name].addTo(this.map);
                this._basemaps[basemapConfig.name].setZIndex(0);
                break;

        }

        //TODO: implementar controle de zoom, caso o zoom do mapa esteja maior q o máximo do mapabase, então deverá modificar o zoom do mapa

        if (this._currentBasemap) {
            this.map.removeLayer(this._currentBasemap);
        }

        this._currentBasemap = this._basemaps[basemapConfig.name];
    }


    buildSimpleRendererLayer(analysisId) {
        return this._buildRendererAnalysisLayer(SimpleRendererLayer, ANALYSIS_TYPE.STATIC, analysisId);
    }

    /**
     *
     * @param analysisId
     * @return {ThematicLayer#build}
     */
    buildThematicLayer(analysisId) {
        return this._buildRendererAnalysisLayer(ThematicLayer, ANALYSIS_TYPE.THEMATIC, analysisId);
    }

    buildClusterLayer(analysisId) {
        return this._buildRendererAnalysisLayer(ClusterLayer, ANALYSIS_TYPE.CLUSTER, analysisId);
    }

    buildHeatLayer(analysisId) {
        return this._buildRendererAnalysisLayer(HeatLayer, ANALYSIS_TYPE.HEATMAP, analysisId);
    }

    buildChartLayer(analysisId) {
        return this._buildRendererAnalysisLayer(ChartLayer, ANALYSIS_TYPE.CHART, analysisId);
    }

    /**
     * Método responsável em instanciar e executar a operação de renderização de uma layer.
     * @param {Function} Class
     * @param {String} type 'thematic' | 'cluster' | 'heat' | 'simple' | 'chart'
     * @param {Number} index
     * @private
     */
    _buildRendererAnalysisLayer(Class, type, index) {
        return Q.Promise((resolve, reject) => {
            try {
                let analysis = this.$viewerStore.analyses[index];
                let tooltipConfig = analysis.tooltipConfig;
                let _layer = this._analysisLayers[analysis._id] = this._analysisLayers[analysis._id] || {};

                for (let key in _layer) {
                    this.map.removeLayer(_layer[key]);
                }

                if (_layer[type]) {
                    _layer[type].addTo(this.map);
                    return resolve();
                }

                let rendererMap = new Class(this.$parent, analysis, index);
                rendererMap.build()
                    .then((config) => {
                        try {
                            let pane;

                            if (!this.map.getPane(analysis._id)) {
                                _layer[type] = rendererMap.getLayer();
                                pane = this.map.createPane(analysis._id);
                                pane.style.zIndex = getZIndex.call(this, analysis._id);
                                pane.style.pointerEvents = 'none';
                                _layer[type].addTo(this.map);

                            } else if (!_layer[type]) {
                                pane = this.map.getPane(analysis._id);
                                _layer[type] = rendererMap.getLayer();
                                _layer[type].addTo(this.map);
                            } else {
                                pane = this.map.getPane(analysis._id);
                            }

                            pane.style.display = analysis.outOfVisibleScale ? "none" : "block";

                            setTimeout(() => {
                                //TODO: rever

                                if(tooltipConfig){
                                    _layer[type].on('mouseover', (e) => {
                                        e.layer.bindPopup(e.layer.feature.businessData[tooltipConfig]).openPopup();
                                    });
                                }

                                _layer[type].on('click', (e) => {
                                    let $style = {
                                        stroke: true,
                                        color: '#00ceff',
                                        weight: 4,
                                        opacity: 1
                                    };

                                    let getStyle = (layer) => {
                                        let obj = {};
                                        let properties = Object.keys($style);

                                        properties.forEach((prop) => {
                                            obj[prop] = layer.options[prop];
                                        });

                                        return obj;
                                    };

                                    if (this._lastFeatureClicked && this._lastFeatureClicked.$originalStyle) {
                                        if (this._lastFeatureClicked.setStyle) {
                                            this._lastFeatureClicked.setStyle(this._lastFeatureClicked.$originalStyle);
                                        }

                                        this._lastFeatureClicked.isSelected = false;
                                    }

                                    e.layer.$originalStyle = getStyle(e.layer);

                                    this._lastFeatureClicked = e.layer;

                                    if (e.layer.setStyle) {
                                        e.layer.setStyle($style);
                                    }

                                    if (!e.layer.isSelected) {
                                        this.$mapAction.layerClick(e.layer, analysis);
                                        e.layer.isSelected = true;
                                    }

                                });
                            }, 1000);

                            resolve(config);
                        } catch (error) {
                            reject(error);
                        }
                    })
                    .catch(error => {
                        reject(error);
                    });
            } catch (error) {
                reject(error);
            }

            function getZIndex (analysisId) {
                let analysisIds = Object.assign([], this.$viewerStore.listOfAnalysisIds).reverse();
                return (410 + analysisIds.indexOf(analysisId));
            }
        });
    }

    hasLayer(id){
        let layer = this._staticLayers[id];
        return !!layer;
    }

    buildRendererStaticLayer (layerConfig) {
        let rendererLayer = new StaticLayer(this.$parent, layerConfig);
        let promise = rendererLayer.build();
        let layerId = String(layerConfig._id);

        return promise.then((legend) => {
            this._staticLayers[layerId] = rendererLayer.getLayer();
            return legend;
        });
    }

    showStaticLayerById(layerId){
        let layer = this._staticLayers[layerId];
        layer.setOpacity(this.$viewerStore.layers[layerId].opacity || 0.8);
        if (layer) {
            this.map.addLayer(layer);
            this.reorderStaticLayers();
        }
    }

    hideStaticLayerById (layerId) {
        let layer = this._staticLayers[layerId];
        if (layer) {
            this.map.removeLayer(layer);
        }
    }

    removeStaticLayer(layerId){
        this.hideStaticLayerById(layerId);
        delete this._staticLayers[layerId];
    }

    getMapMeasures() {
        return {
            height : this.map._size.y,
            width : this.map._size.x
        }
    }

    registerResizeCallback(cb) {
        this._onResizeCallbacks.push(cb);
    }

    drawTrackingOnMap (points, colors) {
        let visibleTrack = L.featureGroup();

        let polyline = L.multiOptionsPolyline(points, {
            multiOptions: {
                optionIdxFn: function (latLng, prevLatLng, index) {
                    return index;
                },
                options: colors
            },
            weight: 3,
            lineCap: 'butt',
            opacity: 1,
            smoothFactor: 1
        }).addTo(visibleTrack);

        //Navigate to bounding box of tracking...
        this.map.fitBounds(polyline.getBounds());

        visibleTrack.addTo(this.map);

        return polyline;
    }

    onDeselectFeature ({feature, leafletLayer, richLayer}) {

        if (!leafletLayer) {
            return;
        }

        if (leafletLayer.$originalStyle) {
            leafletLayer.setStyle(leafletLayer.$originalStyle);
            delete leafletLayer.$originalStyle;
        }

        leafletLayer.isSelected = false;

        this.$parent.emit(MAP_CONSTANT.DESELECT_FEATURE, {
            label : richLayer.crossingKeys.resultSetKey,
            value : leafletLayer.feature.businessData[richLayer.crossingKeys.resultSetKey]
        });
    }

}
