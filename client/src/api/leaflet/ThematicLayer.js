import LayerBase from "./_base/LayerBase";
import Q from "q";
import L from "leaflet";
import chroma from "chroma-js";
import WorkerHelper from "../../helper/WorkerHelper";
import ThematicHelper from "../../helper/ThematicHelper";

export default class ThematicMap extends LayerBase {

    /**
     * Instancia de layer do Leaflet
     */
    _geoJSONLayer;

    /**
     * @type {{
     * geometryType : String,
     * features : Array
     * }}
     */
    _geoJSON;
    _getBreaksResult = {};
    _thematicConfig;
    _layer;

    constructor(...args) {
        super(...args);
    }

    getLayer() {
        return this._geoJSONLayer;
    }

    /**
     *
     * @returns {Promise<{
     * fillColors : String[],
     * fillConfig : Object,
     * sizeConfig : Object,
     * fillBreaks : Number[],
     * sizeBreaks : Number[],
     * geoJSON : Object
     * }>}
     */
    build() {
        return Q.Promise((resolve, reject) => {
            try {
                this._layer = this.$viewerStore.getLayerMetaDataByAnalysisId(this.analysisIndex);

                let promise = this.$geoService.query(this._layer._id, {
                    outSR : 'EPSG:4674'
                }, this.analysis);

                this._thematicConfig = this.analysis.configRenderer.thematic;

                promise
                    .then(this._buildGetBreaks)
                    .then(this._bildRenderersByCustomBreaks)
                    .then(this._buildLayerFeatures)
                    .then(resolve)
                    .catch(reject);

            } catch (error) {
                reject(error);
            }
        });
    }

    _buildGetBreaks = (geoJSON) => {
        return Q.Promise((resolve, reject) => {
            try {
                this._geoJSON = geoJSON;
                let promises = {};

                let getOnServer = (type) => {
                    if (this._thematicConfig[type]) {
                        let config = Object.assign({}, this._thematicConfig[type]);
                        if (geoJSON.features.length < config.breakCount) {
                            config.breakCount = geoJSON.features.length;
                        }
                        promises[type] = this.$geoService.getBreaks(this._layer._id, config, this.analysis.filterConfig)
                    }
                };

                if (this._thematicConfig.fill) {
                    getOnServer('fill');
                }

                if (this._thematicConfig.size) {
                    getOnServer('size');
                }

                if (this._thematicConfig.uniqueValues) {
                    promises.uniqueValues = this.$geoService.generateRenderer(
                        this._layer._id,
                        this._thematicConfig.uniqueValues,
                        this.analysis.filterConfig
                    );
                }

                let typeRenderers = Object.keys(promises);

                Q.all(typeRenderers.map(key => promises[key]))
                    .then((responses) => {
                        let results = {};

                        for (let i in responses) {
                            results[typeRenderers[i]] = responses[i];
                        }

                        resolve(results);
                    })
                    .catch(reject);

            } catch (error) {
                reject(error);
            }
        });
    };

    _bildRenderersByCustomBreaks = (resultMappingByTypeRenderer) => {
        let result = {};
        let customBreaks = this._thematicConfig.customBreaks;

        for (let typeRenderer in resultMappingByTypeRenderer) {
            let breaks = ThematicHelper.getConfigThematicRenderer(
                typeRenderer,
                this._geoJSON,
                resultMappingByTypeRenderer[typeRenderer],
                this._thematicConfig
            )[typeRenderer];

            if (customBreaks && typeRenderer !== 'size') {
                for (let customBreak of customBreaks) {
                    if (!customBreak.visible) {
                        if (typeRenderer === 'fill') {
                            breaks[customBreak.breakIdentifier] = void 0;

                        } else {
                            for (let i in breaks) {
                                let iterBreak = breaks[i];
                                if (iterBreak && customBreak.breakIdentifier === iterBreak.value) {
                                    breaks[i] = void 0;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            breaks = breaks.filter(b => b !== void 0);
            result[typeRenderer] = breaks;
        }

        return result;
    };

    _buildLayerFeatures = (renderersResults) => {
        return new Promise((resolve, reject) => {
            let mappingSymbology = {
                borderColor : 'color',
                borderSize : 'weight',
                fillColor : 'fillColor',
                radius : 'radius'
            };

            let getSymbology = (feature) => {
                let symbology;
                let defaultSymbology = {
                    weight : 1,
                    opacity : 1,
                    fillOpacity : 1,
                    fillColor : '#3c7dcb',
                    color : '#000'
                };

                let build = (type, symbology) => {
                    let value = feature.businessData[this._thematicConfig[type][
                        type === 'uniqueValues' ? 'field' : 'classificationField'
                        ]];

                    for (let breakConfig of renderersResults[type]) {
                        if (ThematicHelper.isThematicBreakInside(breakConfig, value)) {
                            symbology = Object.assign({}, symbology || {}, breakConfig.symbology);
                            break;
                        }
                    }

                    return symbology;
                };

                if (renderersResults.fill) {
                    symbology = build('fill', symbology);
                }

                if (renderersResults.size) {
                    symbology = build('size', symbology);
                }

                if (renderersResults.uniqueValues) {
                    symbology = build('uniqueValues', symbology);
                }

                if (symbology) {
                    let oldSymbology = Object.assign({}, symbology);
                    symbology = {};

                    for (let key in oldSymbology) {
                        symbology[mappingSymbology[key]] = oldSymbology[key];
                    }

                    symbology = Object.assign({}, defaultSymbology, symbology);
                }

                return symbology;
            };

            let features = [];

            for (let feature of this._geoJSON.features) {
                let symbology = getSymbology(feature);

                if (symbology) {
                    feature.$style = symbology;
                    features.push(feature);
                }
            }

            this._geoJSON.features = features;

            this._geoJSONLayer = L.geoJson(this._geoJSON, {
                pointToLayer: (feature, latlng) => {
                    if (!this._thematicConfig.size) {
                        feature.$style = Object.assign({}, feature.$style, {
                            radius : this.analysis.configRenderer.simpleRenderer.size || 10
                        });
                    }

                    return L.circleMarker(
                        latlng,
                        Object.assign({}, feature.$style, {pane: this.analysis._id})
                    )
                },
                style : (feature) => Object.assign({}, feature.$style, {pane: this.analysis._id})
            });

            resolve(renderersResults);

        });
    };

    _buildRenderer2 = (renderersResults) => {
        return Q.Promise((resolve, reject) => {
            try {
                this._getBreaksResult = renderersResults;

                let config = {
                    geoJSON : this._geoJSON
                };

                if (renderersResults.fill) {
                    config.fillLegend = {};

                    for (let breakConfig of renderersResults.fill) {
                        config.fillLegend[breakConfig.symbology.fillColor] = breakConfig;
                    }
                }

                if (this._thematicConfig.fill) {
                    config.fillColors = [].concat(
                        chroma
                            .scale(this._thematicConfig.fill.colorRamp)
                            .colors(this._getBreaksResult.fill.length)
                    );
                    config.fillConfig = this._thematicConfig.fill;
                    config.fillBreaks = this._getBreaksResult.fill;
                }

                if (this._thematicConfig.size) {
                    config.sizeConfig = this._thematicConfig.size;
                    config.sizeBreaks = this._getBreaksResult.size;
                }

                if (this._thematicConfig.uniqueValues) {
                    config.uniqueValues = renderersResults["uniqueValues"].uniqueValues;
                    config.uniqueValuesConfig = this._thematicConfig.uniqueValues;
                }

                if (this._thematicConfig.customBreaks) {
                    config.customBreaks = this._thematicConfig.customBreaks;
                }

                this._worker = new WorkerHelper('choropleth-map.js');
                let promise = this._worker.execute(config);

                promise
                    .then(({geoJSON, meters}) => {
                        try {

                            this._geoJSON = geoJSON;
                            config.sizes = meters;

                            this._geoJSONLayer = L.geoJson(this._geoJSON, {
                                pointToLayer: (feature, latlng) => {
                                    if (feature.$style) {
                                        return L.circleMarker(latlng, Object.assign(feature.$style, {pane: this.analysis._id}));
                                    }
                                },
                                style : (feature) => {
                                    if (feature.$style) {
                                        return Object.assign(feature.$style, {pane: this.analysis._id});
                                    }
                                }
                            });

                            resolve(config);

                        } catch (error) {
                            reject(error);
                        }
                    })
                    .catch(reject);

            } catch (error) {
                console.error(error);
                reject(error);
            }
        });
    };

}
