import React from "react";
import Q from "q";
import L from "leaflet";
import "leaflet.markercluster";
import "../../../public/assets/js/MarkerCluster";
import "../../../public/assets/js/MarkerClusterGroup";
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";
import LayerBase from "./_base/LayerBase";
import chroma from 'chroma-js';
import Converter from "../../util/Converter";

export default class ClusterLayer extends LayerBase {

    _layerMarker;

    _preparedConfig = [];

    getLayer() {
        return this._layerMarker;
    }

    build() {
        return Q.Promise((resolve, reject) => {
            try {
                let layer = this.$viewerStore.getLayerMetaDataByAnalysisId(this.analysisIndex);
                let defaultConfig = {
                    uniqueSize: false,
                    size: 30,
                    distance: 80,
                    breakCount: 6,
                    outlineColor: 'black',
                    colorRamp: ['green', 'yellow', 'red']
                };

                this.config = Object.assign({}, defaultConfig, this.analysis.configRenderer.cluster, {
                    breakCount : Number(this.analysis.configRenderer.cluster.breakCount)
                });

                let promise = this.$geoService.query(layer._id, {
                    outSR : 'EPSG:4674'
                }, this.analysis);

                promise.catch(reject);
                promise.then((geoJSON) => {
                    try {
                        this._layerMarker = L.customMarkerClusterGroup({
                        showCoverageOnHover: true,
                        maxClusterRadius: this.config.distance,
                            onEnd : () => {
                                this.setStyleOfGroups();

                                let legend = this.createLegendInfo();

                                this.$mapAction.clusterLegendIsBuilt({
                                    analysisId : this.analysis._id,
                                    type : "cluster",
                                    legend
                                });
                                this._preparedConfig = [];
                            },
                            iconCreateFunction: (cluster) => {
                                let children = cluster.getChildCount();

                                return L.divIcon({ html: `
									<div data-count='${children}'
									     style="border-radius: 50%;
									     width: 100%;
									     display: none;
									     height: 100%;
									     line-height: 20px;
									     box-sizing: content-box;">
										 <div class="cmaps-cluster" 
                                              data-identifier='${String(this.analysis._id)}'
                                              data-lat='${cluster._cLatLng.lat}'
                                              data-lng='${cluster._cLatLng.lng}'
                                              data-count='${children}'>
                                                   ${children}
										 </div>
									</div>
									`,
                                    className: 'cmaps-cluster-container-node-' + this.analysis._id,
                                    iconSize: L.point(40, 40)
                                });
                            }
                        }, {pane: this.analysis._id});

                        for (let feature of geoJSON.features) {
                            if (/Point/i.test(layer.geometryType)) {
                                let marker = L.circleMarker(new L.LatLng(
                                    feature.geometry.coordinates[1],
                                    feature.geometry.coordinates[0]),
                                    {
                                        fillColor : this.config.colorRamp[0],
                                        fillOpacity : 1,
                                        stroke : false,
                                        radius : 5,
                                        pane : this.analysis._id
                                    }
                                );
                                marker.feature = feature;
                                this._layerMarker.addLayer(marker);
                            }
                        }

                        let uniquePointLegend = [{
                            init : 0,
                            color : this.config.colorRamp[0],
                        }];

                        uniquePointLegend.outlineColor = "#000";

                        resolve(uniquePointLegend);
                    } catch (error) {
                        reject(error);
                    }
                });

            } catch (error) {
                reject(error);
            }
        });
    }

    setStyleOfGroups = () => {
        let dataIdentifier = `[data-identifier="${this.analysis._id}"]`;
        let groups = Array.from(
            document.querySelectorAll(dataIdentifier)
        )
            .map(div => {
                if (this.checkVisible(div)) {
                    return Number(div.dataset.count)
                }
            })
            .filter(a => a !== undefined)
            .sort((a, b) => a - b)
            .filter((item, i, arr) => arr.indexOf(item) === i);

        let min = groups[0];
        let max = groups[groups.length - 1];
        let breakCount = this.config.breakCount;
        let divsOfGroups = Array.from(document.querySelectorAll(".cmaps-cluster-container-node-" + this.analysis._id));
        let maxSize = 40;
        let colors;

        if (!groups.length) {
            return;
        }

        if (groups.length <= breakCount) {
            breakCount = groups.length;
            colors = chroma.scale(this.config.colorRamp).colors(breakCount);

            if (breakCount === 1) {
                colors = [this.config.colorRamp[1]];
            }

            let clusterInterval = Math.ceil(max / breakCount);


            for (let index in groups) {
                let groupCount = groups[index];
                index = Number(index);

                this._preparedConfig.push({
                    band : index,
                    color : colors[index],
                    radius : index === clusterInterval ? (((((clusterInterval * (index + 1)) - 1) / max) * maxSize)) : (((clusterInterval * (index + 1)) / max) * maxSize),
                    range : {
                        start : Number(groupCount),
                        end : Number(groupCount)
                    }
                });
            }
        } else {

            let clusterInterval = Math.ceil(max / breakCount);
            let offset = 0;

            colors = chroma.scale(this.config.colorRamp).colors(breakCount);

            while (offset < breakCount) {
                this._preparedConfig.push({
                    band : offset,
                    color : colors[offset],
                    radius : offset === clusterInterval ? (((((clusterInterval * (offset + 1)) - 1) / max) * maxSize)) : (((clusterInterval * (offset + 1)) / max) * maxSize)
                });

                this._preparedConfig[offset].range = {
                    start : offset === 0 ? min : Math.floor(clusterInterval * offset),
                    end : clusterInterval * (offset + 1)
                };

                offset += 1;
            }
        }

        divsOfGroups.forEach((clusterContainerNODE) => {
            let div = Array.from(clusterContainerNODE.querySelectorAll(dataIdentifier))[0];
            let countDiv = clusterContainerNODE.querySelector("[data-count]");
            let count = Number(div.dataset.count);

            for (let key in this._preparedConfig) {
                let config = this._preparedConfig[key];
                let borderColor = Converter.hexToRgb(config.color);

                if ((this._preparedConfig.length === 1 && config.range.end === count) || (count >= config.range.start) && count <= config.range.end) {
                    let pixels = config.radius.toFixed(2) + "px";
                    let radius = (config.radius * 2).toFixed(2) + "px";

                    clusterContainerNODE.style.width = radius;
                    clusterContainerNODE.style.height = radius;
                    clusterContainerNODE.style.marginLeft = "-" + pixels;
                    clusterContainerNODE.style.marginTop = "-" + pixels;

                    div.style.backgroundColor = config.color;
                    div.style.lineHeight = radius;
                    countDiv.style.display = "block";
                    countDiv.style.border = `2px solid rgba(${borderColor.r},${borderColor.g},${borderColor.b}, .4)`;
                    break;
                }
            }
        });
    };

    createLegendInfo () {
        let legendInfo;

        legendInfo = this._preparedConfig.map((band) => {
            return {
                init: band.range.start,
                end: band.range.end,
                color: band.color
            };

        });

        legendInfo.unshift({
            init : 1,
            color : this.config.colorRamp[0]
        });

        legendInfo.outlineColor = this.config.outlineColor;

        return legendInfo;
    }

    checkVisible (elm) {
        let boundingBox = this.$parent.mapAPI.map.getBounds();

        let mapCoords = {
            xMin : boundingBox.getNorthWest().lng,
            yMin : boundingBox.getNorthWest().lat,
            xMax : boundingBox.getSouthEast().lng,
            yMax : boundingBox.getSouthEast().lat
        };

        let groupCoords = {
            x : Number(elm.dataset.lng),
            y : Number(elm.dataset.lat)
        };

        return (
            groupCoords.x >= mapCoords.xMin && groupCoords.x <= mapCoords.xMax &&
            groupCoords.y <= mapCoords.yMin && groupCoords.y >= mapCoords.yMax
        );
    }
}
