import LayerBase from "./_base/LayerBase";
import Q from 'q';
import L from "leaflet";
import '../../../public/assets/js/LeafletDivIcon'

export default class SimpleRendererLayer extends LayerBase {

    _layer;

    getLayer() {
        return this._layer
    }

    build() {
        return Q.Promise((resolve, reject) => {
            try {
                let icon = void 0;
                let layer = this.$viewerStore.getLayerMetaDataByAnalysisId(this.analysisIndex);

                let promise = this.$geoService.query(layer._id, {
                    outSR : 'EPSG:4674'
                }, this.analysis);

                this.config = this.analysis.configRenderer.simpleRenderer || {};

                if (this.config.customSymbology) {
                    if (this.config.customSymbology.base64 || this.config.customSymbology.iconText) {
                        icon = L.icon({
                            iconUrl : this.config.customSymbology.base64,
                            iconSize : [this.config.size, this.config.size],
                            shadowSize : [0, 0]
                        });
                    }
                }

                promise.catch(reject);
                promise.then((res) => {
                    try {
                        let options = {
                            fillColor: this.config.fill || 'blue',
                            color: this.config.outlineColor || 'black',
                            radius: this.config.size || 8,
                            fillOpacity: 1,
                            opacity: 0.3,
                            weight: 2,
                            pane: this.analysis._id
                        };

                        let labelFeatures = [];

                        if (/Polygon/i.test(res.features[0].geometry.type)) {
                            if (this.config.customSymbology) {
                                if (this.config.customSymbology.iconText) {
                                    res.features.forEach((feature) => {
                                        labelFeatures.push({
                                            type: "Feature",
                                            $properties: {
                                                label: feature.businessData[this.config.customSymbology.iconText],
                                                isLabel: true
                                            },
                                            geometry: {
                                                type: "Point",
                                                coordinates: getCentroid(res.features[0].geometry.type, feature.geometry.coordinates)
                                            },
                                            businessData : {}
                                        });
                                    });

                                    function getCentroid (type, coordinates) {

                                        if (/^Polygon$/i.test(type)) {
                                            coordinates = coordinates[0];
                                        } else if (/Multi/i.test(type)) {
                                            coordinates = coordinates[0][0];
                                        }

                                        return coordinates.reduce(function (x,y) {
                                            let lat = x[1] + y[1] / coordinates.length;
                                            let lng = x[0] + y[0] / coordinates.length;

                                            lat = isNaN(lat) ? getNumber(coordinates[0], 0) : lat;
                                            lng = isNaN(lng) ? getNumber(coordinates[0], 1) : lng;
                                            return [lng, lat];
                                        }, [0,0]);

                                        function getNumber(coordinate, index = 0) {
                                            if (coordinate instanceof Array) {
                                                return getNumber(coordinate[index]);
                                            } else {
                                                return coordinate;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        this._layer = L.geoJson(res, {
                            style: () => {
                                return options;
                            },
                            pointToLayer: (feature, latLng) => {
                                if (icon){
                                    let ic = icon;

                                    if (this.config.customSymbology.iconText){
                                        if (feature.$properties && feature.$properties.isLabel) {
                                            ic = L.customDivIcon({
                                                fontSize : this.config.customSymbology.fontSize,
                                                fontColor : this.config.customSymbology.fontColor,
                                                iconText : feature.$properties.label,
                                                iconUrl : this.config.customSymbology.base64,
                                                iconSize : [50, 50],
                                                pointerEvents : 'none',
                                                shadowSize : [0, 0]
                                            });
                                        } else {
                                            ic = L.customDivIcon({
                                                fontSize : this.config.customSymbology.fontSize,
                                                fontColor : this.config.customSymbology.fontColor,
                                                iconText : feature.businessData[this.config.customSymbology.iconText],
                                                iconUrl : this.config.customSymbology.base64,
                                                iconSize : [this.config.size, this.config.size],
                                                shadowSize : [0, 0]
                                            });
                                        }
                                    }

                                    return L.marker(latLng, {icon: ic, pane : options.pane}).setZIndexOffset(999);
                                }
                                return L.circleMarker(latLng, options);
                            }
                        });

                        if (labelFeatures.length) {
                            this._layer.addData(labelFeatures);
                        }

                        let legendInfo = {
                            fill: options.fillColor,
                            outline: options.color,
                            text: this.analysis.title
                        };

                        if (icon) legendInfo.icon = this.config.customSymbology;

                        resolve(legendInfo);

                    } catch (err) {
                        reject(err);
                    }
                });

            } catch (error) {
                reject(error);
            }
        });
    }
}
