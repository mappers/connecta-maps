import {locale} from "../../../i18n/Locale";
import {scopeSingletonFactory} from "../../../helper/scopeSingletonFactory";
import ScopeSingletonBase from "../../../_base/abstract/ScopeSingletonBase";
import AppError from "../../../native/AppErrorI18n";

export default class LayerBase extends ScopeSingletonBase {

    /**
     * @type {IAnalysis}
     */
    analysis;

    /**
     * @type {Number}
     */
    analysisIndex;

    /**
     * @type {*}
     */
    config;

    /**
     *
     * @param {ConnectaMaps} parent
     * @param {IAnalysis} analysis
     * @param {Number} analysisIndex
     */
    constructor(parent, analysis, analysisIndex) {
        super(parent);
        scopeSingletonFactory(this);
        this.analysis = analysis;
        this.analysisIndex = analysisIndex;
    }

    getLayer() {
        throw new AppError('DEV_ERROR_01', ['getLayer']);
    }

    build() {
        throw new AppError('DEV_ERROR_01', ['build']);
    }

}
