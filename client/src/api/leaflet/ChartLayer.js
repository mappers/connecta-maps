import React from "react";
import Q from "q";
import L from "leaflet";
import "leaflet.markercluster";
import "../../../public/assets/js/MarkerCluster";
import "../../../public/assets/js/MarkerClusterGroup";
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";
import LayerBase from "./_base/LayerBase";

const Raphael = require('raphael');

Raphael.fn.pieChart = function (cx, cy, r, values, labels, stroke) {
    let paper = this,
        rad = Math.PI / 180,
        chart = this.set();
    let sector = (cx, cy, r, startAngle, endAngle, params) => {
        let x1 = cx + r * Math.cos(-startAngle * rad),
            x2 = cx + r * Math.cos(-endAngle * rad),
            y1 = cy + r * Math.sin(-startAngle * rad),
            y2 = cy + r * Math.sin(-endAngle * rad);
        return paper.path(["M", cx, cy, "L", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"]).attr(params);
    };
    let angle = 0,
        total = 0,
        start = 0,
        process = function (j) {
            let value = values[j],
                angleplus = 360 * value / total,
                popangle = angle + (angleplus / 2),
                color = Raphael.hsb(start, .75, 1),
                ms = 500,
                delta = 30,
                bcolor = Raphael.hsb(start, 1, 1),
                p = sector(cx, cy, r, angle, angle + angleplus, {fill: "90-" + bcolor + "-" + color, stroke: stroke, "stroke-width": 2}),
                txt = paper.text(cx + (r + delta + 55) * Math.cos(-popangle * rad), cy + (r + delta + 25) * Math.sin(-popangle * rad), labels[j]).attr({fill: bcolor, stroke: "none", opacity: 0, "font-size": 20});
            p.mouseover(function () {
                p.stop().animate({transform: "s1.1 1.1 " + cx + " " + cy}, ms, "elastic");
                txt.stop().animate({opacity: 1}, ms, "elastic");
            }).mouseout(function () {
                p.stop().animate({transform: ""}, ms, "elastic");
                txt.stop().animate({opacity: 0}, ms);
            });
            angle += angleplus;
            chart.push(p);
            chart.push(txt);
            start += .1;
        };

    let i, ii;

    for (i = 0, ii = values.length; i < ii; i++) {
        total += values[i];
    }
    for (i = 0; i < ii; i++) {
        process(i);
    }
    return chart;
};


export default class ChartLayer extends LayerBase {

    _layerMarker;

    /**
     * @type {IChartRendererConfig}
     */
    _chartConfig;

    _rmax = 40;

    getLayer() {
        return this._layerMarker;
    }

    build() {
        return Q.Promise((resolve, reject) => {
            try {
                this._chartConfig = this.analysis.configRenderer.chart;

                let layer = this.$viewerStore.getLayerMetaDataByAnalysisId(this.analysisIndex);

                let promise = this.$geoService.query(layer._id, {
                    outSR : 'EPSG:4674'
                }, this.analysis);

                promise.catch(reject);
                promise.then((geoJSON) => {
                    try {
                        this._layerMarker = L.customMarkerClusterGroup(
                            {
                                maxClusterRadius: 2 * this._rmax,
                                pane: this.analysis._id,
                                iconCreateFunction: this._iconCreateFunction
                            },
                            {
                                pane: this.analysis._id
                            }
                        );

                        let layer = L.geoJson(geoJSON, {
                            pointToLayer : (feature, latLng) =>  L.circleMarker(latLng, {
                                stroke : true,
                                color : '#999',
                                weight : 2,
                                opacity : 0.8,
                                fillOpacity : 0.8,
                                fillColor : '#42495B',
                                pane : this.analysis._id
                            })
                        });
                        this._layerMarker.addLayer(layer);

                        resolve();
                    } catch (error) {
                        reject(error);
                    }
                });

            } catch (error) {
                reject(error);
            }
        });
    }

    _iconCreateFunction = (cluster) => {
        let children = cluster.getAllChildMarkers();
        let n = children.length;
        let strokeWidth = 1;
        let r = this._rmax - 2 * strokeWidth - (n < 10 ? 12 : n < 100 ? 8 : n < 1000 ? 4 :0);
        let iconDim = (r+strokeWidth) * 2;
        let columns = {};
        let total = 0;

        for (let i in children) {
            let child = children[i];
            let originalBusinessData = child.feature.originalBusinessData;

            let dimensions = originalBusinessData[this._chartConfig.dimension];

            for (let k in dimensions) {
                let dimension = String(dimensions[k]);
                if (!columns[dimension]) {
                    columns[dimension] = 0;
                }

                columns[dimension] += originalBusinessData[this._chartConfig.metric][k];
                total += originalBusinessData[this._chartConfig.metric][k];
            }
        }

        let node = document.createElement('div');
        let values = [];
        let labels = [];

        for (let key in columns) {
            labels.push(key);
            values.push(Number(columns[key] / total * 100));
        }

        let html = '';

        if (values.length > 1) {
            Raphael(node, 50, 50).pieChart(25, 25, 25, values, labels, "#fff");
            html = node.innerHTML;

        } else {
            html = `
                <div style="
                    background: #00796b;
                    border-radius: 50%;
                    width: 100%;
                    height: 100%;
                    text-align: center;
                    color: #fff;
                    border:2px solid #fff;]
                    padding-top: 33%;"> ${values[0] || 0} % </div>
            `;
        }

        return new L.DivIcon({
            html : html,
            className: 'maps-leaflet-chart',
            iconSize: new L.Point(iconDim, iconDim)
        });
    };

}
