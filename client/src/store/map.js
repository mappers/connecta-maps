import MapController from "./controller/map";
import StoreBase from "../_base/abstract/StoreBase";
import {MAP_CONSTANT} from "../constant/map";

export default class MapStore extends StoreBase {

    events = {
        [MAP_CONSTANT.BUILD_THEMATIC_LAYER] : {},
        [MAP_CONSTANT.BUILD_SIMPLE_RENDERER_LAYER] : {},
        [MAP_CONSTANT.BUILD_HEAT_LAYER] : {},
        [MAP_CONSTANT.BUILD_CLUSTER_LAYER] : {},
        [MAP_CONSTANT.BUILD_CHART_LAYER] : {},
        [MAP_CONSTANT.REORDER_ANALYSES_LAYERS] : {},
        [MAP_CONSTANT.REORDER_STATIC_LAYERS] : {},
        [MAP_CONSTANT.ANALYSIS_LAYER_IS_BUILDING] : {},
        [MAP_CONSTANT.STATIC_LAYER_IS_BUILDING] : {},
        [MAP_CONSTANT.STATIC_LAYER_BUILD_SUCCESS] : {},
        [MAP_CONSTANT.CLUSTER_LEGEND_IS_BUILT] : {},
        [MAP_CONSTANT.DESELECT_FEATURE] : {},
        [MAP_CONSTANT.STATIC_LAYER_BUILD_ERROR] : {},
        [MAP_CONSTANT.BUILD_SUCCESS] : {},
        [MAP_CONSTANT.BUILD_ERROR] : {},
        [MAP_CONSTANT.CENTER_LAYER] : {},
        [MAP_CONSTANT.BUILD_MAP] : {},
		[MAP_CONSTANT.BUILD_HEAT_LAYER] : {},
        [MAP_CONSTANT.BUILD_STATIC_LAYER]: {},
        [MAP_CONSTANT.LAYER_CLICK] : {},
        [MAP_CONSTANT.REBUILD_MAP] : {},
        [MAP_CONSTANT.MAP_ZOOM_END] : {}
    };

    getControllerClass() {
        return MapController;
    }

}