import StoreBase from "../_base/abstract/StoreBase";
import {ANALYSIS} from "../constant/analysis";
import ToolAnalysisController from "./controller/tool-analysis";
import {APP_CONSTANT} from "../constant/index";

export default class ToolAnalysisStore extends StoreBase {

    events = {
        [APP_CONSTANT.SET_EXTERNAL_FILTER] : {},
        [ANALYSIS.DOWNLOAD_FILE] : {}
    };

    getControllerClass() {
        return ToolAnalysisController;
    }

}