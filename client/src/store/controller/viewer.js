import {VIEWER} from "../../constant/viewer";
import {locale} from "../../i18n/Locale";
import ControllerBase from "../../_base/abstract/ControllerBase";
import update from "react-addons-update";
import {MapsDataLoader} from "../../MapsDataLoader";
import {AnalysisValidator} from "../../helper/AnalysisValidator";
import Converter from "../../util/Converter";
import chroma from "chroma-js";

export default class ViewerController extends ControllerBase {

    /**
     * @type {ViewerStore}
     */
    store;

    prepare() {
        this.$parent.dispatcher.register((action) => {
            let data = action.data;

            switch (action.type) {

                case VIEWER.LOAD :
                    this.getViewer(data);
                    break;

                case VIEWER.UPDATE :
                    this.updateViewer(data);
                    break;

                case VIEWER.REORDER_ANALYSES:
                    this.reorderAnalyses(data);
                    break;

                case VIEWER.REORDER_STATIC_LAYERS:
                    this.reorderStaticLayers(data);
                    break;

                case VIEWER.CHANGE_ANALYSIS_LAYER_OPACITY:
                    this.changeAnalysisLayerOpacity(data);
                    break;

                case VIEWER.CHANGE_STATIC_LAYER_OPACITY:
                    this.changeStaticLayerOpacity(data);
                    break;

                case VIEWER.CHANGE_ANALYSIS_TYPE:
                    this.changeAnalysisType(data);
                    break;

                case VIEWER.ENABLE_ANALYSIS:
                    this.enableAnalysis(data);
                    this.refreshTreeStatusGroups();
                    break;

                case VIEWER.DISABLE_ANALYSIS:
                    this.disableAnalysis(data);
                    this.refreshTreeStatusGroups();
                    break;

                case VIEWER.ENABLE_STATIC_LAYER:
                    this.enableStaticLayer(data);
                    break;

                case VIEWER.DISABLE_STATIC_LAYER:
                    this.disableStaticLayer(data);
                    break;

                case VIEWER.CLICK_ON_SAVE_NEW_ANALYSIS:
                    this.$emitter(VIEWER.CLICK_ON_SAVE_NEW_ANALYSIS, data);
                    // this.emitClickOnSaveNewAnalysis();
                    break;

                case VIEWER.CLICK_ON_SAVE_NEW_LAYER:
                    this.emitClickOnSaveNewLayer();
                    break;

                case VIEWER.SAVE_ANALYSIS:
                    this.saveAnalysis(data);
                    break;

                case VIEWER.SAVE_NEW_LAYER:
                    this.saveNewLayer(data);
                    break;

                case VIEWER.EDIT_ANALYSIS:
                    this.editAnalysis(data);
                    break;

                case VIEWER.DELETE_ANALYSIS:
                    this.deleteAnalysis(data);
                    break;

                case VIEWER.DELETE_STATIC_LAYER:
                    this.deleteStaticLayer(data);
                    break;

                case VIEWER.SAVE_VIEWER:
                    this.saveViewer(data);
                    break;

                case VIEWER.SAVE_PROJECT:
                    this.saveProject(data);
                    break;

                case VIEWER.CHANGE_SWIPE_VALUE:
                    this.changeSwipeValue(data);
                    break;

                case VIEWER.TOGGLE_SWIPE:
                    this.toggleSwipe(data);
                    break;

                case VIEWER.ADD_ANALYSIS_BOX:
                    this.$emitter(VIEWER.ADD_ANALYSIS_BOX, data);
                    break;

                case VIEWER.REMOVE_ANALYSIS_BOX:
                    this.$emitter(VIEWER.REMOVE_ANALYSIS_BOX, data);
                    break;

                case VIEWER.SHOW_ANALYSIS_FORM:
                    this.$emitter(VIEWER.SHOW_ANALYSIS_FORM, data);
                    break;

                case VIEWER.UPDATE_GROUP_CHILDREN:
                    this.updateGroupChildren(data);
                    break;

                case VIEWER.UPDATE_GROUP_ROOT:
                    this.updateGroupRoot(data);
                    break;

                case VIEWER.UPDATE_GROUP_LABEL:
                    this.updateGroupLabel(data);
                    break;

                case VIEWER.DELETE_GROUP:
                    this.deleteGroup(data);
                    break;

                case VIEWER.DISABLE_SWIPE:
                    this.disableSwipe();
                    break;

                case VIEWER.CHECK_GROUP:
                    this.toggleCheckGroup(data);
                    break;

                case VIEWER.ADD_LAYER_BOX:
                    this.$emitter(VIEWER.ADD_LAYER_BOX, data);
                    break;

                case VIEWER.REMOVE_LAYER_BOX:
                    this.$emitter(VIEWER.REMOVE_LAYER_BOX, data);
                    break;

                case VIEWER.ACTIVATE_TRACKING:
                    this.prepareToDrawTrackingOnMap(data);
                    break;

                case VIEWER.DEACTIVATE_TRACKING:
                    this.removeTrackingLayerFromMap();
                    break;
                case VIEWER.CHANGE_LENGTH_UNIT:
                    this.handleChangeLengthUnit(data);
                    break;

                default:
            }
        });
    }

    disableSwipe() {
        this.$emitter(VIEWER.DISABLE_SWIPE);
    }

    reorderAnalyses(){
        setTimeout(() => {
            this.$mapAction.reorderAnalysesLayers();
        }, 0);
    }

    deleteGroup({groupId, isLayers}){
        this.store.deleteGroup(groupId, isLayers);
        this.$emitter(VIEWER.DELETE_GROUP, this.store.viewer);

        setTimeout(() => {
            this.$emitter(isLayers ? VIEWER.GET_LAYER_CONFIG : VIEWER.GET_ANALYSIS_CONFIG, this.store.viewer);
        }, 0);
    }

    updateGroupLabel({groupId, label, isLayers}){
        this.store.updateGroupLabel(groupId, label, isLayers);
    }

    updateGroupRoot({group, isLayers}){
        this.store.replaceGroupRoot(group, isLayers);

        if (isLayers) {
            this.$emitter(VIEWER.GET_LAYER_CONFIG, this.store.viewer);
            this.reorderStaticLayers();

        } else {
            this.$emitter(VIEWER.GET_ANALYSIS_CONFIG, this.store.viewer);
            this.reorderAnalyses();
        }
    }

    updateGroupChildren({groupId, children, isLayers}){
        this.store.replaceGroupChildren(groupId, children, isLayers);

        if(isLayers){
            this.$emitter(VIEWER.GET_LAYER_CONFIG, this.store.viewer);
            this.reorderStaticLayers();
        }else {
            this.$emitter(VIEWER.GET_ANALYSIS_CONFIG, this.store.viewer);
            this.reorderAnalyses();
        }
    }

    toggleSwipe({analysisId, toggle}){
        this.$viewerStore.swipedAnalysisId = toggle ? analysisId : null;
        this.$viewerStore.swipedValue = this.$viewerStore.swipedValue || 0;

        setTimeout(() => {

            this.$emitter(VIEWER.TOGGLE_SWIPE, {analysisId, toggle});

            if(toggle) {
                this.$mapAction.swipeLayer(analysisId, this.$viewerStore.swipedValue || 0);
                setTimeout(() => {
                    this.$mapAction.registerOnMoveSwipe();
                }, 0);
            }else {
                this.$mapAction.removeSwipeLayer(analysisId);
            }
        }, 0);
    }

    changeSwipeValue({value}){
        this.$viewerStore.swipedValue = value;
        let analysisId = this.$viewerStore._swipedAnalysisId;

        setTimeout(() => {
            this.$mapAction.swipeLayer(analysisId, value);
        });
    }

    deleteStaticLayer(id){
        this.$parent.mapAPI.removeStaticLayer(id);
        this.store.deleteLayer(id);
        this.$emitter(VIEWER.DELETE_STATIC_LAYER, this.store.viewer);
    }

    saveProject(project) {
        this.$appService.saveProject(project)
            .then(({data}) => {
                this.$appAction.alertSuccess('APP_SUCCESS_04');
                this.$emitter(VIEWER.SAVE_PROJECT, data);
            })
            .catch(error => this.$appAction.alertError('SERVER_ERROR_02', null, error));
    }

    saveViewer(user) {

        let viewer = update(this.$viewerStore.viewer, {
            project : {
                mapConfig : {
                    center : {
                        $set : this.$parent.mapAPI.getMapCenter()
                    },
                    zoom : {
                        $set : this.$parent.mapAPI.getCurrentZoom()
                    }
                }
            },
            toolsConfig : {
                analysisConfig : {
                    $set : validateFilters.call(this, this.store.viewer.toolsConfig.analysisConfig)
                }
            }
        });

        this.$appService.saveViewer(viewer, user)
            .then(({data}) => {
                this.$emitter(VIEWER.SAVE_VIEWER, data);
            })
            .catch(error => this.$appAction.alertError('SERVER_ERROR_01', null, error));

        function validateFilters (analysisConfig) {
            let arr = [];

            analysisConfig.forEach((item) => {
                let clonedItem = Object.assign({}, item);

                if (item.type === "GROUP") {
                    clonedItem.children = [].concat(validateFilters(item.children));
                } else {
                    clonedItem.filterConfig = clonedItem.filterConfig.filter(filter => !filter.externalFilter);
                }

                arr.push(clonedItem);
            });

            return arr;
        }
    }


    getViewer({viewerId, user}) {
        let exec = ({data}) => {
            try {
                data = this.loadDataViewer(data);
                MapsDataLoader.set(data._id, data);
                this.$emitter(VIEWER.LOAD, data);
            } catch (error) {
                console.error(locale.getMessage('APP_ERROR_01'), error);
            }
        };

        let viewer = MapsDataLoader.get(this.$parent.viewerId);

        if (viewer) {
            exec({
                data : viewer
            });
        } else {
            this.$appService.getViewer(viewerId, user)
                .then(exec)
                .catch(error => this.$appAction.alertError('SERVER_ERROR_01', null, error));
        }
    }

    updateViewer(viewerId){
        //TODO: update viewer if has viewerId
        let viewer = MapsDataLoader.get(this.$parent.viewerId);
        let data = this.loadDataViewer(viewer);
        MapsDataLoader.set(data._id, data);

        setTimeout(() => {
            this.$emitter(VIEWER.UPDATE);
        }, 200)
    }

    loadDataViewer(data){
        if (data &&
            data.toolsConfig &&
            data.toolsConfig.analysisConfig) {

            if (data.toolsConfig.analysisConfig.analyses) {
                data.toolsConfig.analysisConfig = data.toolsConfig.analysisConfig.analyses;
            }

            for (let analysis of data.toolsConfig.analysisConfig) {
                if (!analysis.customBreaks) {
                    analysis.customBreaks = [];
                }
            }
        }
        this.store.viewer = data;
        return data;
    }

    reorderStaticLayers(){
        setTimeout(() => {
            this.$mapAction.reorderStaticLayers();
        }, 0);
    }

    changeAnalysisLayerOpacity(data) {
        let analysis = this.store.analyses[data.id];
        analysis.opacity = data.opacity;
        this.$emitter(VIEWER.CHANGE_ANALYSIS_LAYER_OPACITY, analysis);
    }

    changeStaticLayerOpacity(data){
        let layer = this.store.layers[data.id];
        layer.opacity = data.opacity;
        this.$emitter(VIEWER.CHANGE_STATIC_LAYER_OPACITY, this.store.viewer);
    }

    changeAnalysisType(data) {
        let analysis = this.store.analyses[data.id];
        analysis.currentType = data.analysisType;
        this.$emitter(VIEWER.CHANGE_ANALYSIS_TYPE, analysis);

    }

    enableAnalysis(data) {
        let analysis = this.store.analyses[data.id];
        analysis.enable = true;
        this.$emitter(VIEWER.ENABLE_ANALYSIS, analysis);
    }

    disableAnalysis(data) {
        let analysis = this.store.analyses[data.id];
        analysis.enable = false;
        this.$emitter(VIEWER.DISABLE_ANALYSIS, analysis);
    }

    emitClickOnSaveNewLayer(){
        this.$emitter(VIEWER.CLICK_ON_SAVE_NEW_LAYER);
    }

    emitClickOnSaveNewAnalysis () {
        this.$emitter(VIEWER.CLICK_ON_SAVE_NEW_ANALYSIS);
    }

    saveNewLayer({layer, groupId}){
        let addLayer = false;

        if (!layer._id) {
            addLayer = true;
            layer._id = String(new Date().getTime());
        }

        this.store.saveLayer(groupId, layer, addLayer);
        this.$emitter(VIEWER.SAVE_NEW_LAYER, this.store.viewer);
    }

    saveAnalysis({analysis, groupId}) {
        let geometryType = this.store.getRichLayerMetaData(analysis.richLayerId).layer.geometryType;
        let addAnalysis = false;
        analysis = AnalysisValidator.execute(analysis, geometryType);
        this.$viewerStore.checkIfAnalysisIsOnZoomLevel(analysis);

        if (!analysis._id) {
            addAnalysis = true;
            analysis._id = String(new Date().getTime());
        }

        this.store.saveAnalysis(groupId, analysis, addAnalysis);
        this.$parent.mapAPI.removeAnalysisLayers(analysis._id);

        setTimeout(() => {
            let affectedGroup = this.store.findGroupByAnalysisId(analysis._id);
            analysis.enable = true;

            this.$mapAction.rebuildLayers(analysis._id);

            setTimeout(() => {
                this.$emitter(VIEWER.SAVE_ANALYSIS, {groupId : affectedGroup._id, children : affectedGroup.children});
            }, 100);
        }, 500);
    }

    editAnalysis({analysis, groupId}) {
        this.$emitter(VIEWER.EDIT_ANALYSIS, {analysis, groupId});
    }

    deleteAnalysis(id) {
        let affectedGroup = this.store.findGroupByAnalysisId(id);
        this.$parent.mapAPI.removeAnalysisLayers(id);
        this.$viewerStore.deleteAnalysis(id);
        this.$emitter(VIEWER.DELETE_ANALYSIS, {id, groupId : affectedGroup._id, children : affectedGroup.children});
    }

    enableStaticLayer = (id) => {
        let layer = this.store.layers[id];
        layer.enabled = true;

        this.$emitter(VIEWER.ENABLE_STATIC_LAYER, layer);

        setTimeout(() => {
            this.$mapAction.buildStaticLayer(id);
        }, 0);
    };

    disableStaticLayer = (id) => {
        let layer = this.store.layers[id];
        layer.enabled = false;
        this.$emitter(VIEWER.ENABLE_STATIC_LAYER, layer);
    };

    toggleCheckGroup = ({groupId, checked, isLayers}) => {
        this.store.toggleCheckGroup(groupId, checked, isLayers);
        this.refreshTreeStatusGroups();
    };

    refreshTreeStatusGroups = () => {
        this.store.checkIndeterminateStatusGroup();

        setTimeout(() => {
            this.$emitter(VIEWER.CHECK_GROUP);
        }, 0);
    };

    prepareToDrawTrackingOnMap ({analysis, layerId, filterConfig}) {
        let newAnalysis = Converter.deepClone(analysis);
        newAnalysis.filterConfig = newAnalysis.filterConfig.concat(filterConfig);
        let points = [];

        let promise = this.$geoService.query(layerId, {
            orderByFields : newAnalysis.trackingConfig.temporalDimensionField,
            outSR : "EPSG:4326"
        }, newAnalysis);

        this.$emitter(VIEWER.TRACKING_LAYER_BUILDING);

        promise.catch(() => {
            this.$emitter(VIEWER.TRACKING_LAYER_BUILD_ERROR);
        });

        promise.then((geoJson) => {
            geoJson.features.forEach((feature) => {
                points.push({
                    lng : feature.geometry.coordinates[0],
                    lat : feature.geometry.coordinates[1]
                });
            });

            points.reverse();

            if (points.length > 1) {
                let colors = chroma.scale([newAnalysis.trackingConfig.colors[0], newAnalysis.trackingConfig.colors[1]]).colors(points.length).map((color) => {return {color}});
                this.store.trackingLayer.layer = this.$parent.mapAPI.drawTrackingOnMap(points, colors);
                this.store.trackingLayer.analysisId = analysis._id;
                this.store.trackingLayer.points = points;
                this.$viewerAction.changeAnalysisLayerOpacity(analysis._id, 0.1);
            } else {
                this.$appAction.alertWarn("APP_WARN_02");
            }

            this.$emitter(VIEWER.TRACKING_LAYER_BUILD_SUCCESS);
        });
    }

    removeTrackingLayerFromMap () {
        let trackingLayer = this.$viewerStore.trackingLayer.layer;
        let analysisId = this.$viewerStore.trackingLayer.analysisId;

        if (trackingLayer) {
            this.$parent.mapAPI.map.removeLayer(trackingLayer);
            this.$viewerAction.changeAnalysisLayerOpacity(analysisId, 1);
            this.$viewerStore.trackingLayer = {};
        }

        this.$emitter(VIEWER.DEACTIVATE_TRACKING);
    }

    handleChangeLengthUnit (measureConfig){
        let viewer = Object.assign({}, this.$viewerStore.viewer);
        viewer.measureConfig = Object.assign({}, measureConfig);

        this.$viewerStore.viewer = viewer;
        this.$emitter(VIEWER.CHANGE_LENGTH_UNIT);
    }

}