import {MAP_CONSTANT} from "../../constant/map";
import ControllerBase from "../../_base/abstract/ControllerBase";
import {VIEWER} from "../../constant/viewer";
import {ANALYSIS_TYPE} from "../../constant/analysis";
import {APP_CONSTANT} from "../../constant/index";

export default class MapController extends ControllerBase {

    prepare() {
        this.$parent.dispatcher.register((action) => {
            let data = action.data;

            switch (action.type) {

                case MAP_CONSTANT.BUILD_MAP:
                    this.$parent.mapAPI.buildMap(data.node, data.config)
                        .then(() => {
                            this.$emitter(MAP_CONSTANT.BUILD_MAP, true);
                        })
                        .catch(error => this.$appAction.alertError('MAP_ERROR_06', null, error));
                    break;

                case MAP_CONSTANT.BUILD_THEMATIC_LAYER :
                    this.buildThematicLayer(data);
                    break;

                case MAP_CONSTANT.BUILD_CLUSTER_LAYER :
                    this.buildClusterLayer(data);
                    break;

                case MAP_CONSTANT.BUILD_SIMPLE_RENDERER_LAYER :
                    this.buildSimpleRendererLayer(data);
                    break;

                case MAP_CONSTANT.BUILD_CHART_LAYER :
                    this.buildChartLayer(data);
                    break;

                case MAP_CONSTANT.BUILD_HEAT_LAYER :
                    this.buildHeatLayer(data);
                    break;

                case MAP_CONSTANT.BUILD_STATIC_LAYER:
                    this.buildStaticLayer(data);
                    break;

                case VIEWER.ENABLE_ANALYSIS:
                    this.enableAnalysis(data);
                    break;

                case VIEWER.DISABLE_ANALYSIS:
                    this.disableAnalysis(data);
                    break;

                case VIEWER.ENABLE_STATIC_LAYER:
                    this.enableStaticLayer(data);
                    break;

                case VIEWER.DISABLE_STATIC_LAYER:
                    this.disableStaticLayer(data);
                    break;

                case VIEWER.CHANGE_ANALYSIS_TYPE:
                    this.changeAnalysisType(data);
                    break;

                case VIEWER.CHANGE_ANALYSIS_LAYER_OPACITY:
                    this.changeAnalysisLayerOpacity(data);
                    break;

                case VIEWER.CHANGE_STATIC_LAYER_OPACITY:
                    this.$parent.mapAPI.setLayerOpacity(data);
                    break;

                case MAP_CONSTANT.CENTER_LAYER:
                    this.centerAtLayer(data);
                    break;

                case MAP_CONSTANT.REORDER_ANALYSES_LAYERS:
                    this.$parent.mapAPI.reorderAnalysisLayers();
                    break;

                case MAP_CONSTANT.REORDER_STATIC_LAYERS:
                    this.$parent.mapAPI.reorderStaticLayers();
                    break;

                case MAP_CONSTANT.LAYER_CLICK:
                    this.$emitter(MAP_CONSTANT.LAYER_CLICK, data);
                    this.emitSelectedFeature(data);
                    break;

                case MAP_CONSTANT.CENTER_AT_FEATURE:
                    this.centerAtFeature(data);
                    break;

                case MAP_CONSTANT.REBUILD_LAYERS:
                    this.rebuildAnalysisLayers(data);
                    break;

                case MAP_CONSTANT.REBUILD_STATIC_LAYER:
                    this.rebuildStaticLayer(data);
                    break;

                case MAP_CONSTANT.SWIPE_LAYER:
                    this.swipeAnalysisLayer();
                    break;

                case MAP_CONSTANT.REMOVE_SWIPE_LAYER:
                    this.$parent.mapAPI.removeSwipeAnalysisLayer(data);
                    break;

                case MAP_CONSTANT.REGISTER_MOVE_SWIPE:
                    this.$parent.mapAPI.registerMoveSwipe();
                    break;

                case MAP_CONSTANT.REBUILD_MAP:
                    this.$parent.mapAPI.rebuildMap(data);
                    break;

                case MAP_CONSTANT.DESTROY:
                    this.$parent.mapAPI.destroy();
                    break;

                case MAP_CONSTANT.DESELECT_FEATURE:
                    this.deselectFeature(data);
                    break;

                case MAP_CONSTANT.CLUSTER_LEGEND_IS_BUILT:
                    this.clusterLegendIsBuilt(data);
                    break;

                case MAP_CONSTANT.MAP_ZOOM_END:
                    this.mapZoomEnd(data);
                    break;
            }
        });
    }

    emitSelectedFeature ({feature, analysis}) {
        let richLayer = this.$viewerStore.getRichLayerByAnalysisId(analysis._id);
        let value = feature.businessData[richLayer.crossingKeys.resultSetKey];
        this.$parent.emit(APP_CONSTANT.SELECTED_FEATURE, {value, label : richLayer.crossingKeys.resultSetKey});
    }

    swipeAnalysisLayer(){
        let analysisId = this.$viewerStore.swipedAnalysisId;
        let value = this.$viewerStore.swipedValue;
        this.$parent.mapAPI.swipeAnalysisLayer(analysisId, value);
    }

    centerAtFeature(leafletLayer){
        this.$parent.mapAPI.centerAtFeature(leafletLayer);
        this.$emitter(MAP_CONSTANT.CENTER_AT_FEATURE, leafletLayer);
    }

    buildThematicLayer(data) {
        this.$emitter(MAP_CONSTANT.ANALYSIS_LAYER_IS_BUILDING, {id: data.id});
        this.$parent.mapAPI.buildThematicLayer(data.id)
            .then((config) => {
                this.$viewerStore.analyses[data.id]._buildError = false;
                this.$emitter(MAP_CONSTANT.BUILD_SUCCESS, {id: data.id});
                if (config) {
                    this.$viewerStore.addAnalysisLegend(data.id, config, ANALYSIS_TYPE.THEMATIC);
                    this.$emitter(MAP_CONSTANT.BUILD_THEMATIC_LAYER, {id: data.id, config});
                }
            })
            .catch(error => {
                this.$viewerStore.analyses[data.id]._buildError = true;
                this.$emitter(MAP_CONSTANT.BUILD_ERROR, {id: data.id});
                this.$appAction.alertError('MAP_ERROR_01', null, error)
            });
    }

    buildClusterLayer(data) {
        this.$emitter(MAP_CONSTANT.ANALYSIS_LAYER_IS_BUILDING, {id: data.id});
        this.$parent.mapAPI.buildClusterLayer(data.id)
            .then((config) => {
                this.$viewerStore.analyses[data.id]._buildError = false;
                this.$emitter(MAP_CONSTANT.BUILD_SUCCESS, {id: data.id});
                if (config) {
                    this.$viewerStore.addAnalysisLegend(data.id, config, ANALYSIS_TYPE.CLUSTER);
                    this.$emitter(MAP_CONSTANT.BUILD_CLUSTER_LAYER, {id: data.id, config});
                }
            })
            .catch(error => {
                this.$viewerStore.analyses[data.id]._buildError = true;
                this.$emitter(MAP_CONSTANT.BUILD_ERROR, {id: data.id});
                this.$appAction.alertError('MAP_ERROR_02', null, error)
            });
    }

    buildSimpleRendererLayer(data) {
        this.$emitter(MAP_CONSTANT.ANALYSIS_LAYER_IS_BUILDING, {id: data.id});
        this.$parent.mapAPI.buildSimpleRendererLayer(data.id)
            .then((config) => {
                this.$viewerStore.analyses[data.id]._buildError = false;
                this.$emitter(MAP_CONSTANT.BUILD_SUCCESS, {id: data.id});
                if (config) {
                    this.$viewerStore.addAnalysisLegend(data.id, config, ANALYSIS_TYPE.STATIC);
                    this.$emitter(MAP_CONSTANT.BUILD_SIMPLE_RENDERER_LAYER, {id: data.id, config});
                }
            })
            .catch(error => {
                this.$viewerStore.analyses[data.id]._buildError = true;
                this.$emitter(MAP_CONSTANT.BUILD_ERROR, {id: data.id});
                this.$appAction.alertError('MAP_ERROR_05', null, error)
            });
    }

    buildChartLayer(data) {
        this.$emitter(MAP_CONSTANT.ANALYSIS_LAYER_IS_BUILDING, {id: data.id});
        this.$parent.mapAPI.buildChartLayer(data.id)
            .then((config) => {
                this.$viewerStore.analyses[data.id]._buildError = false;
                this.$emitter(MAP_CONSTANT.BUILD_SUCCESS, {id: data.id});
                if (config) {
                    this.$viewerStore.addAnalysisLegend(data.id, config, ANALYSIS_TYPE.CHART);
                    this.$emitter(MAP_CONSTANT.BUILD_CHART_LAYER, {id: data.id, config});
                }
            })
            .catch(error => {
                this.$viewerStore.analyses[data.id]._buildError = true;
                this.$emitter(MAP_CONSTANT.BUILD_ERROR, {id: data.id});
                this.$appAction.alertError('MAP_ERROR_04', null, error)
            });
    }

    buildHeatLayer(data) {
        this.$emitter(MAP_CONSTANT.ANALYSIS_LAYER_IS_BUILDING, {id: data.id});
        this.$parent.mapAPI.buildHeatLayer(data.id)
            .then((config) => {
                this.$viewerStore.analyses[data.id]._buildError = false;
                this.$emitter(MAP_CONSTANT.BUILD_SUCCESS, {id: data.id});
                if (config) {
                    this.$viewerStore.addAnalysisLegend(data.id, config, ANALYSIS_TYPE.HEATMAP);
                    this.$emitter(MAP_CONSTANT.BUILD_HEAT_LAYER, {id: data.id, config});
                }
            })
            .catch(error => {
                this.$viewerStore.analyses[data.id]._buildError = true;
                this.$emitter(MAP_CONSTANT.BUILD_ERROR, {id: data.id});
                this.$appAction.alertError('MAP_ERROR_03', null, error)
            });
    }

    buildStaticLayer (id) {
        let layer = this.$viewerStore.layers[id];
        if(this.$parent.mapAPI.hasLayer(id)){
            return;
        }

        this.$emitter(MAP_CONSTANT.STATIC_LAYER_IS_BUILDING, {id});
        this.$parent.mapAPI.buildRendererStaticLayer(layer)
            .then((legend) => {
                this.$emitter(MAP_CONSTANT.STATIC_LAYER_BUILD_SUCCESS, {id, legend})
            })
            .catch((error) => {
                console.log("ERROR", error);
                this.$emitter(MAP_CONSTANT.STATIC_LAYER_BUILD_ERROR, {id});
            });
    }

    enableStaticLayer (id) {
        this.$parent.mapAPI.showStaticLayerById(id);
    }

    disableStaticLayer (id) {
        this.$parent.mapAPI.hideStaticLayerById(id);
    }

    enableAnalysis(data) {
        let analysis = this.$viewerStore.analyses[data.id];
        let currentType = data.analysisType || analysis.currentType;
        let isLayerBuild = this.$parent.mapAPI.isAnalysisLayerBuild(analysis._id, currentType);

        if (!isLayerBuild) {

            switch (currentType) {

                case ANALYSIS_TYPE.THEMATIC:
                    this.buildThematicLayer({id: data.id});
                    break;

                case ANALYSIS_TYPE.CLUSTER:
                    this.buildClusterLayer({id: data.id});
                    break;

                case ANALYSIS_TYPE.CHART:
                    this.buildChartLayer({id: data.id});
                    break;

                case ANALYSIS_TYPE.HEATMAP:
                    this.buildHeatLayer({id: data.id});
                    break;

                case ANALYSIS_TYPE.STATIC:
                    this.buildSimpleRendererLayer({id: data.id});
                    break;

            }

        } else {
            this.disableAnalysis(data);
            this.$parent.mapAPI.showLayerByAnalysisId(analysis._id, currentType);
        }
    }

    disableAnalysis(data) {
        let analysis = this.$viewerStore.analyses[data.id];
        this.$parent.mapAPI.hideLayerByAnalysisId(analysis._id);
    }

    changeAnalysisType(data) {
        let analysis = this.$viewerStore.analyses[data.id];
        if (analysis.enable) {
            this.enableAnalysis(data);
        }
    }

    changeAnalysisLayerOpacity(data) {
        this.$parent.mapAPI.setPaneOpacity(data.id, data.opacity);
    }

    centerAtLayer(data) {
        let analysis = this.$viewerStore.analyses[data.id];
        this.$parent.mapAPI.centerAtLayer(data.id, analysis.currentType);
    }

    rebuildAnalysisLayers(data) {
        this.$parent.mapAPI.removeAnalysisLayers(data.id);
        setTimeout(() => {
            this.enableAnalysis(data);
        }, 0);
    }

    rebuildStaticLayer(id){
        this.$parent.mapAPI.removeStaticLayer(id);
        this.buildStaticLayer(id);
    }

    deselectFeature (data) {
        this.$emitter(MAP_CONSTANT.DESELECT_FEATURE, data);
    }

    clusterLegendIsBuilt ({analysisId, type, legend}) {
        this.$viewerStore.legends[analysisId][type] = legend;
        this.$emitter(MAP_CONSTANT.CLUSTER_LEGEND_IS_BUILT, {analysisId, type, legend});
    }

    mapZoomEnd (data) {
        let zoomLevel = this.$parent.mapAPI.map.getZoom();
        let analyses = this.$viewerStore.analyses;

        this.$viewerStore.listOfAnalysisIds.forEach((analysisId) => {
            let analysis = analyses[analysisId];
            let pane = this.$parent.mapAPI.map.getPane(String(analysisId));

            if (pane && analysis.visibleScaleRange) {
                if (zoomLevel < analysis.visibleScaleRange.min || zoomLevel > analysis.visibleScaleRange.max) {
                    pane.style.display = "none";
                    analysis.outOfVisibleScale = true;

                } else {
                    pane.style.display = "block";
                    analysis.outOfVisibleScale = false;
                }

                this.$emitter(MAP_CONSTANT.MAP_ZOOM_END, {data, analysisId});
            }
        });
    }

}