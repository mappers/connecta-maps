import ControllerBase from "../../_base/abstract/ControllerBase";
import {BASEMAP} from "../../constant/basemap";

export default class BasemapController extends ControllerBase{

    prepare() {
        this.$parent.dispatcher.register(action => {
            let data = action.data;

            switch (action.type) {
                case BASEMAP.LOAD_DEFAULT_BASEMAPS:
                    this.loadDefaultBaseMaps();
                    break;

                case BASEMAP.CHANGE_CURRENT_BASEMAP :
                    this.changeBasemap(data);
                    break;
            }
        });
    }

    changeBasemap(name) {
        let basemaps = this.$basemapStore.get(BASEMAP.LOAD_DEFAULT_BASEMAPS);
        let config;

        for (let basemap of basemaps) {
            if (basemap.name === name) {
                config = basemap;
                break;
            }
        }

        this.$parent.mapAPI.changeBasemap(config);
        this.$emitter(BASEMAP.CHANGE_CURRENT_BASEMAP, name);
    }

    loadDefaultBaseMaps() {
        let promise = this.$appService.getDefaultBaseMaps();
        promise.catch(error => this.$appAction.alertError('', null, error));
        promise.then(({data}) => {
            this.$emitter(BASEMAP.LOAD_DEFAULT_BASEMAPS, data);
        });
    }

}