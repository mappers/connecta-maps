import ControllerBase from "../../_base/abstract/ControllerBase";
import {ANALYSIS} from "../../constant/analysis";
import {APP_CONSTANT} from "../../constant/index";
import {MapsDataLoader} from "../../MapsDataLoader";

export default class ToolAnalysisController extends ControllerBase {

    prepare() {

        this.$parent.dispatcher.register(({type, data}) => {

            switch (type) {

                case ANALYSIS.DOWNLOAD_FILE :
                    this.downloadFile(data);
                    break;

                case APP_CONSTANT.SET_EXTERNAL_FILTER :
                    this.setExternalFilter(data);
                    break;

            }
        });
    }

    downloadFile({analysis, outputFormat}) {
        window.open(
            this.$geoService.getQueryUrl(
                this.$viewerStore.getLayerByAnalysisId(analysis._id)._id,
                {
                    outSR : 'EPSG:4674',
                    f : outputFormat
                },
                analysis,
                true
            ).urlStr
        );
    }

    /**
     *
     * @param filters {{field : String, operator : String, value : String}[]}
     */
    setExternalFilter(filters) {
        let analyses = this.$viewerStore.analyses;
        let businessData;
        let columnsOfAnalysis;
        let analysisIdsMap = {};

        clearOldFilters();
        setNewFilters.call(this);

        Object.keys(analysisIdsMap).forEach((analysisId) => {
            this.$mapAction.rebuildLayers(analysisId);
        });

        this.$appAction.disablePopup();

        function clearOldFilters () {
            for (let analysisId in analyses) {
                let nFilterConfig = [];

                analyses[analysisId].filterConfig.forEach((_filter) => {
                    if (_filter.externalFilter) {
                        analysisIdsMap[analysisId] = true;
                    } else {
                        nFilterConfig.push(_filter);
                    }
                });

                analyses[analysisId].filterConfig = nFilterConfig;
            }
        }

        function setNewFilters () {
            for (let analysisId in analyses) {
                let analysis = analyses[analysisId];
                let richLayer = this.$viewerStore.getRichLayerByAnalysisId(analysis._id);

                businessData = MapsDataLoader.get(richLayer.resultSetId);

                columnsOfAnalysis = Object.keys(businessData[0]);

                filters.forEach((filter) => {
                    if (filter.value.length && columnsOfAnalysis.includes(filter.field)) {
                        analysis.filterConfig.push(filter);
                        analysisIdsMap[analysisId] = true;
                    }
                });
            }
        }
    }
}