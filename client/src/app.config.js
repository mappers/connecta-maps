//Config.json#client será mixado no appConfig
let dynamicConfig = '%DYNAMIC_CONFIG%';

export let appConfig = {

    serviceDomain : '%SERVER_URL%',

    mapAPI : 'leaflet',

    hostApplication : 'connecta',
    hostApplicationDomain : ''

};

dynamicConfig = JSON.parse(dynamicConfig);
Object.assign(appConfig, dynamicConfig);
