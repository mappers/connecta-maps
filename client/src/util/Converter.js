import {createHash} from "crypto";

export default class Converter {

    static createHash(...args) {
        let value = JSON.stringify(args);
        let md5sum = createHash('md5');
        md5sum.update(value);
        return md5sum.digest('hex');
    }

    /**
     * @param str
     * @param params
     */
    static substitute(str, params) {
        let pattern = /\$\{([\w\d_\-.]+)}/g;
        return str.replace(pattern, (a, variable) => {
            return params[variable];
        });
    }

    static verifyNestedValue(obj, path) {
        let parts = path.split('.');

        let verify = (currentObject, index) => {
            if ((parts.length - 1) === index) {
                return Boolean(parts[index] in currentObject);
            } else if (parts[index] in currentObject && typeof currentObject[parts[index]] === 'object') {
                return verify(currentObject[parts[index]], index + 1);
            } else {
                return false;
            }
        };

        return verify(obj, 0);
    }

    static resolvePath(props, path) {
        let keys = path.split('.');
        let resultValue = props;
        let i = 0;
        let total = keys.length;

        while (i < total) {
            let key = keys[i];

            if (typeof resultValue !== 'object') {
                return;
            }

            resultValue = resultValue[key];

            if (resultValue === null) {

                if (i === total-1) {
                    return null;
                }

                return;
            }

            i += 1;
        }
        return resultValue;
    };

    static mixin(...args) {
        if (Object.assign) {
            return Object.assign.apply(Object, args);
        }

        let obj = args[0];
        args.splice(0, 1);

        for (let nObj of args) {
            for (let key in nObj) {
                if (typeof nObj[key] !== 'function') {
                    obj[key] = nObj[key];
                }
            }
        }

        return obj;
    }

    static inputToBase64(input) {
        return new Promise((resolve, reject) => {

            try {
                if (!input.value) {
                    throw new Error('Input is empty');
                }

                let reader = new FileReader();

                reader.onload = (e) => {
                    let base64 = e.target.result;
                    if (/^data:image/.test(base64)) {
                        resolve(base64);
                    }
                };

                reader.readAsDataURL(input.files[0]);

            } catch (error) {
                reject(error);
            }
        });
    }

    static createDeepProperty(obj, deep, value) {
        let deepExec = (index, nObject) => {
            if (index < (deep.length -1)) {
                if (!nObject[deep[index]]) {
                    nObject[deep[index]] = {};
                }
                deepExec(index + 1, nObject[deep[index]]);

            } else {
                nObject[deep[index]] = value;
            }
        };

        deep = deep.split('.');
        deepExec(0, obj);

        return obj;
    }

    static deepClone (obj) {
        if (!obj) {
            return {};
        }
        let newObj = {};
        for (let key in obj) {
            if (typeof obj[key] === 'object') {
                if (Array.isArray(obj[key])) {
                    newObj[key] = Object.assign([], obj[key]);
                } else {
                    obj[key] = Object.assign({}, obj[key]);
                    newObj[key] = Converter.deepClone(obj[key]);
                }
            } else {
                newObj[key] = obj[key];
            }
        }
        return newObj;
    }

    static hexToRgb (hex) {
        let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : {
            r: 255,
            g: 255,
            b : 255
        };
    }
}