import React from "react";
import {Card, CardHeader, CardText} from "material-ui/Card";
import IconButton from "material-ui/IconButton";
import ModuleBase from "../_base/abstract/ModuleBase";
import {MAP_CONSTANT} from "../constant/map";
import Clear from "material-ui/svg-icons/content/clear";
import ArrowDown from "material-ui/svg-icons/hardware/keyboard-arrow-down";
import {locale} from "../i18n/Locale";
import {VIEWER} from "../constant/viewer";
import {APP_CONSTANT} from "../constant/index";
import MyLocation from 'material-ui/svg-icons/maps/my-location';
import {CircularProgress, FlatButton} from "material-ui";
import theme from "../theme.config";
import MaskHelper from "../helper/MaskHelper";

export default class WidgetPopup extends ModuleBase {

    state = {
        visible: false,
        expanded: true,
        feature: {
            businessData: {},
            originalBusinessData : {}
        },

        /**
         * @type {IRichLayer}
         */
        richLayer : null,
        trackingEnabled : false,
        loadingTracking : false
    };

    /**
     * @type {IAnalysis}
     */
    currentAnalysis;

    componentDidMount(){
        this.own(
            this.$viewerStore.listen(VIEWER.DISABLE_ANALYSIS, this.disablePopup),
            this.$viewerStore.listen(VIEWER.DEACTIVATE_TRACKING, this.handleDeactivateTracking),
            this.$viewerStore.listen(VIEWER.TRACKING_LAYER_BUILDING, this.handleBuildingTrackingLayer),
            this.$viewerStore.listen(VIEWER.TRACKING_LAYER_BUILD_SUCCESS, this.handleBuildSuccessTrackingLayer),
            this.$appStore.listen(APP_CONSTANT.DISABLE_POPUP, this.disablePopup),
            this.$mapStore.listen(MAP_CONSTANT.LAYER_CLICK, this.handleLayerClick),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_SUCCESS, this.disablePopup)
        );
    }

    componentDidUpdate () {
        let trackingButtonIsAvailable = (
            this.currentAnalysis &&
            this.currentAnalysis.trackingConfig &&
            this.currentAnalysis.trackingConfig.temporalDimensionField &&
            this.currentAnalysis.trackingConfig.filterField);

        if (trackingButtonIsAvailable !== this.state.trackingButtonIsAvailable) {
            this.setState({
                trackingButtonIsAvailable
            });
        }
    }

    handleBuildingTrackingLayer = () => {
        this.setState({
            loadingTracking : true
        });
    };

    handleBuildSuccessTrackingLayer = () => {
        this.setState({
            loadingTracking : false
        });
    };

    disablePopup = (analysis) => {
        if (this.state.visible &&
            typeof analysis === 'object' &&
            (analysis.id === this.currentAnalysis._id ||
            analysis._id === this.currentAnalysis._id)
        ) {
            this.setState({
                visible: false,
                feature: {
                    businessData: {},
                    originalBusinessData : {}
                }
            });
            this.currentAnalysis = null;
            this.$mapAction.deselectFeature({
                feature : this.state.feature,
                leafletLayer : this.state.leafletLayer,
                richLayer : this.state.richLayer
            });
            this.$viewerAction.deactivateTrackingOnMap();
        }
    };

    onPopupScroll = (e) => {
        let scrollTop = e.currentTarget.scrollTop;
        let clientHeight = e.currentTarget.clientHeight;
        let scrollHeight = e.currentTarget.scrollHeight;

        if ((scrollTop + clientHeight) === scrollHeight) {
            if(e.deltaY > 0){
                e.preventDefault();
            } else if(e.deltaY < 0 && scrollTop === 0) {
                e.preventDefault();
            }
        } else if(scrollTop === 0){
            if(e.deltaY < 0){
                e.preventDefault();
            }
        }
    };

    handleDeactivateTracking = () => {
        this.setState({
            trackingEnabled : false
        });
    };

    handleLayerClick = ({feature, leafletLayer, analysis}) => {
        if (feature.properties.isLabel){
            this.handlerClose();
            return;
        }

        let richLayer = this.$viewerStore.viewer.project.richLayers.filter(rich => rich._id === analysis.richLayerId)[0];

        if (!richLayer) {
            return;
        }

        this.currentAnalysis = analysis;

        this.setState({
            visible: true,
            expanded: true,
            feature: feature,
            richLayer : richLayer,
            leafletLayer
        });

        let coordinate = {
            lat : feature.geometry.coordinates[1],
            lng : feature.geometry.coordinates[0]
        };

        if (!this.$viewerStore.verifyIfPointIsOnTracking(coordinate)) {
            this.$viewerAction.deactivateTrackingOnMap();
        }
    };

    handlerClose = () => {
        let newState = {};

        newState.visible = false;

        if (this.state.trackingEnabled) {
            newState.trackingEnabled = false;
            this.$viewerAction.deactivateTrackingOnMap();
        }

        if (this.state.leafletLayer && this.state.visible){
            this.$mapAction.deselectFeature({
                feature : this.state.feature,
                leafletLayer : this.state.leafletLayer,
                richLayer : this.state.richLayer
            });
        }

        this.setState(newState);
    };

    handleExpandButtonClick = () => {
        this.setState({
            expanded : !this.state.expanded
        });
    };

    buildAllProperties() {
        if (!this.currentAnalysis || (this.currentAnalysis && !this.currentAnalysis.outFieldsConfig)) {
            return;
        }
        let outFieldsConfig = this.currentAnalysis.outFieldsConfig || {};
        let businessData = this.state.feature.businessData;
        let columns = Object.keys(outFieldsConfig).sort((a, b) => {
            let labelA = outFieldsConfig[a].label || a;
            let labelB = outFieldsConfig[b].label || b;
            return labelA.localeCompare(labelB);
        });
        return (
            columns.map((columnName, index) => {
                return ( outFieldsConfig[columnName].checked &&
                    <div key={index} className={columnName} style={{
                        fontSize : '12px'
                    }}>
                        <b> { outFieldsConfig[columnName].label || columnName } : </b>
                        { MaskHelper.getValueMasked(businessData[columnName], outFieldsConfig[columnName]) }
                    </div>
                );
            })
        );
    }

    className(name = '') {
        return this.appClass + '-widget-popup' + (name ? '-' +  name : '');
    }

    buildTrackingFilter = () => {
        let field = this.currentAnalysis.trackingConfig.filterField;
        let value = this.state.feature.businessData[field];
        let filters = [];

        let lessOrEqualsThanField = this.currentAnalysis.trackingConfig.temporalDimensionField;
        let limitValue = this.state.feature.businessData[lessOrEqualsThanField];


        filters.push({
            valueType : typeof limitValue,
            field : lessOrEqualsThanField,
            value : limitValue,
            operator : "LESS_OR_EQUAL"
        });

        filters.push({
            valueType : typeof value,
            field,
            value,
            operator : "EQUAL"
        });

        return filters;
    };

    handleClickToggleTracking = () => {
        if (!this.state.trackingEnabled) {
            let filterConfig = this.buildTrackingFilter();
            this.$viewerAction.activateTrackingOnMap({
                analysis : this.currentAnalysis,
                layerId : this.state.richLayer.layer._id,
                filterConfig
            });

            this.setState({trackingEnabled : true});
        } else {
            this.$viewerAction.deactivateTrackingOnMap();
            this.setState({trackingEnabled : false});
        }
    };

    render() {
        let cardStyle = {
            display: this.state.visible ? 'block' : 'none',
            maxHeight : this.state.expanded ? '340px' : '50px'
        };

        let arrowDownButtonStyle = {
            position: 'absolute',
            top: 1,
            right: '40px',
            padding: 0,
            transition : 'none',
            transform: !this.state.expanded ? 'rotateX(180deg)' : ''
        };

        let hasBusinessData = Boolean(Object.keys(this.state.feature.businessData).length);

        return (
            <Card className={ this.className() } style={cardStyle}>
                <CardHeader title={locale.getLabel("L0064")}>
                    <IconButton style={arrowDownButtonStyle}
                                onClick={this.handleExpandButtonClick}>
                        <ArrowDown />
                    </IconButton>

                    <IconButton style={{position: 'absolute', top: 1, right: 0, padding: 0}}
                                onClick={this.handlerClose}>
                        <Clear />
                    </IconButton>
                </CardHeader>

                <div style={{ padding : '0 10px' }}>
                    <div
                        className="cmaps-button-primary"
                        onClick={() => {
                            if (this.state.leafletLayer) {
                                this.$mapAction.centerAtFeature(this.state.leafletLayer);
                            }
                        }}
                    >
                        {locale.getLabel('L1004')}
                    </div>
                </div>

                <CardText className={this.className('scroll')}
                          ref={() => {
                              document.querySelector('.cmaps-widget-popup-scroll').addEventListener("wheel", this.onPopupScroll);
                          }}>

                    {this.state.trackingButtonIsAvailable && (
                        <div>
                            {this.state.loadingTracking && (
                                <div style={{position : 'absolute', right : 15, top: 49, width : 35, height: 36, zIndex: 2}}>
                                    <CircularProgress size={30} />
                                </div>
                            )}

                            <FlatButton
                                style={{position : 'absolute',
                                    right : 15,
                                    minWidth: 35,
                                    display : !this.state.loadingTracking ? 'block' : 'none',
                                    top: 49,
                                    backgroundColor : this.state.trackingEnabled ? theme.palette.primary1Color : ""}}
                                title={locale.getLabel("L0085")}
                                onTouchTap={this.handleClickToggleTracking}
                                icon={<MyLocation color={this.state.trackingEnabled ? "#fff" : "#000"}/>}
                            />
                        </div>
                    )}

                    { hasBusinessData && this.buildAllProperties() }
                </CardText>
            </Card>
        );
    }
}


