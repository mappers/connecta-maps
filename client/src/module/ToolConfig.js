import React from "react";
import ModuleBase from "../_base/abstract/ModuleBase";
import {locale} from "../i18n/Locale";
import PlayArrow from "material-ui/svg-icons/av/play-arrow";
import Checkbox from 'material-ui/Checkbox';
import MeasureConfig from "./ToolConfig/MeasureConfig";
import update from "react-addons-update";

export default class ToolConfig extends ModuleBase{

    constructor(props, context) {
        super(props, context);

        this.state = {
            widgets: [
                {
                    class: MeasureConfig,
                    label: this.buildLabel(locale.getLabel('L1030')),
                    collapsed: true
                }
            ]
        }
    }

    handleCollapse = (index) => {
        let widget = Object.assign({}, this.state.widgets[index]);
        widget.collapsed = !widget.collapsed;

        let widgets = update(this.state.widgets, {
            $splice: [[index, 1, widget]]
        });

        this.setState({ widgets });
    };

    buildLabel(label){
        return (
            <div className={this.appClass + "-label-widget"}>
                <span>{label}</span>
            </div>
        );
    }

    render() {
        return (
            <div>
                {this.state.widgets.map((widget, index) => {
                    let Component = widget.class;
                    return (
                        <div key={index} className={this.appClass + '-tool-config'}>
                            <Checkbox label={widget.label}
                                      checked={widget.collapsed}
                                      onCheck={() => {this.handleCollapse(index)}}
                                      checkedIcon={<PlayArrow style={{transform: 'rotate(90deg)', margin : '8px 0 0 -1px'}} />}
                                      uncheckedIcon={<PlayArrow style={{margin : '8px 0 0 -1px'}}/>}
                                      style={{width: '10%', display: 'inline-block', marginRight : 0}}
                                      iconStyle={{marginRight : 0}} />

                            <div style={{display : widget.collapsed ? "block" : "none", marginLeft: '15px'}}>
                                <Component/>
                            </div>
                        </div>
                    )
                })}
            </div>
        );
    }

}