import React from "react";
import ModuleBase from "../_base/abstract/ModuleBase";
import {BASEMAP} from "../constant/basemap";

export default class ToolBasemap extends ModuleBase {

    constructor(props, context) {
        super(props, context);
        this.state = {
            basemaps : [],
            currentBasemap : ''
        };
    }

    componentDidMount() {
        this.own(
            this.$basemapStore.listen(BASEMAP.LOAD_DEFAULT_BASEMAPS, this.onLoadDefaultBasemaps),
            this.$basemapStore.listen(BASEMAP.CHANGE_CURRENT_BASEMAP, this.changeCurrentBasemap)
        );

        this.$basemapAction.loadDefaultBasemaps();
    }

    onLoadDefaultBasemaps = (basemaps) => {
        let currentBasemap = basemaps[0].name;
        this.$basemapAction.changeCurrentBasemap(currentBasemap);
        this.setState({
            basemaps : basemaps
        });
    };

    changeCurrentBasemap = (name) => {
        this.setState({
            currentBasemap : name
        });
    };

    render() {
        return (
            <div className="cmaps-ToolBasemap">
                {this.state.basemaps.map((basemap, index) => {
                    let active = basemap.name === this.state.currentBasemap ? ' cmaps-active' : '';
                    return (
                        <div className={'cmaps-basemap' + active}
                             onClick={() => this.$basemapAction.changeCurrentBasemap(basemap.name)}
                             key={index}>
                            <span className="cmaps-label">{basemap.label || basemap.name}</span>
                            <img src={basemap.thumbnail} alt={basemap.name}/>
                        </div>
                    );
                })}
            </div>
        );
    }

}