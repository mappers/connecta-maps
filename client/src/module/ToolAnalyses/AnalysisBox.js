import React from "react";
import PropTypes from "prop-types";
import ModuleBase from "../../_base/abstract/ModuleBase";
import Checkbox from "material-ui/Checkbox";
import PlayArrow from "material-ui/svg-icons/av/play-arrow";
import Slider from "material-ui/Slider";
import VisibilityOff from "material-ui/svg-icons/action/visibility-off";
import Visibility from "material-ui/svg-icons/action/visibility";
import Focus from "material-ui/svg-icons/image/filter-center-focus";
import MoreVert from "material-ui/svg-icons/navigation/more-vert";
import Warning from "material-ui/svg-icons/alert/warning";
import Clear from "material-ui/svg-icons/content/clear";
import {locale} from "../../i18n/Locale";
import IconMenu from "material-ui/IconMenu";
import IconButton from "material-ui/IconButton";
import FlatButton from "material-ui/FlatButton";
import MenuItem from "material-ui/MenuItem";
import {ANALYSIS_TYPE, ANALYSIS_TYPE_CONFIG, GEOMETRY_TYPE} from "../../constant/analysis";
import update from "react-addons-update";
import CircularProgress from "material-ui/CircularProgress";
import {Card, CardHeader, CardText} from "material-ui/Card";
import GeoCacheHelper from "../../helper/GeoCacheHelper";
import Toggle from "material-ui/Toggle";
import ArrowDropRight from "material-ui/svg-icons/navigation-arrow-drop-right";
import TypesRadioButton from "../../component/TypesRadioButton";

class AnalysisBox extends ModuleBase {

    _legendReference = {};

    static propTypes = {
        index: PropTypes.number,
        onLoad : PropTypes.func,
        onRemove : PropTypes.func,
        onClickDeleteAnalysis : PropTypes.func,
        onClickDuplicateAnalysis : PropTypes.func,
    };

    static refTypes = {
        measurement: PropTypes.element
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            /**
             * @type {IAnalysis}
             */
            analysis: this.$viewerStore.analyses[props.id],
            opacity: 0.5,
            shown: !!this.$viewerStore.analyses[props.id].shown,
            swiped: this.$viewerStore.swipedAnalysisId === props.id,
            loading: false,
            error: false,
            showDetails: false,
            outOfVisibleScale : this.$viewerStore.analyses[props.id].outOfVisibleScale
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.$viewerAction.addAnalysisBox(this);

            if (this.state.analysis.enable) {
                setTimeout(() => {
                    this.$viewerAction.enableAnalysis(this.props.id);
                }, 0);

                setTimeout(() => {
                    this.$viewerAction.changeAnalysisLayerOpacity(this.props.id, this.state.analysis.opacity);
                }, 0);
            }
        }, 0);
    }

    componentWillUnmount() {
        super.componentWillUnmount();

        setTimeout(() => {
            this.$viewerAction.removeAnalysisBox(this);
        }, 0);

        if (this.refresh) {
            clearInterval(this.refresh);
        }
    }

    checkForRefresh = () => {
        let refreshTime = this.$viewerStore._analyses[this.props.id].refreshTime;

        if (this.refresh) {
            clearInterval(this.refresh);
        }

        if (refreshTime) {
            this.refresh = setInterval(() => {

                if (this.state.analysis.enable ) {
                    GeoCacheHelper.removeCacheByAnalysis(this.state.analysis._id);
                    this.$appAction.alertInfo("APP_INFO_03");
                    this.$mapAction.rebuildLayers(this.props.id);
                }

            }, refreshTime * 1000 * 60);
        }
    };

    onAnalysisChange = (analysis) => {
        let state = this.state;
        if (state.analysis._id === analysis._id) {
            this.setState({analysis});
            this.detectEnabledTypes();
        }
    };

    handleIsBuilding = (payload) => {
        if (payload.id === this.props.id) {
            this.setState({
                loading: true
            });
        }
    };

    handleClickEditAnalysis = (groupId) => {
        this.$viewerAction.editAnalysis(this.state.analysis, groupId);
        this.$viewerAction.deactivateTrackingOnMap();
        // this.$appAction.disablePopup();
    };

    handleClickDetailsAnalysis = () => {
        let newState = update(this.state, {
            showDetails: {
                $set: !this.state.showDetails
            }
        });

        this.setState(newState);
    };

    handleBuildError = (payload) => {
        if (payload.id === this.props.id) {
            this.setState({
                loading: false,
                error: true,
                shown: false
            });
        }
    };

    handleBuildSuccess = (payload) => {
        if (payload.id === this.props.id) {
            this.$viewerAction.changeAnalysisLayerOpacity(this.props.id, this.state.analysis.opacity);
            this.setState({
                loading: false,
                error : false
            });

            this.checkForRefresh();
        }
    };

    handleLayerBuild = () => {
        this.forceUpdate();
    };

    handleDeleteAnalysis = () => {
        this.props.onRemove(this.props.id);
    };

    detectEnabledTypes = () => {
        let metadata = this.$viewerStore.getLayerMetaDataByAnalysisId(this.props.id);
        let enabledTypes = {};
        let geometryType = metadata.geometryType;

        for (let key in this.state.analysis.configRenderer) {
            enabledTypes[key] = true;
        }

        if (geometryType !== GEOMETRY_TYPE.POINT) {
            enabledTypes.cluster = false;
            enabledTypes.heatmap = false;
        }

        this.setState({
            enabledTypes
        });

    };

    handleToggleShow = () => {
        let newState = update(this.state, {
            shown : {
                $set : !this.state.shown
            }
        });

        this.setState(newState, () => {
            this.$viewerStore.toggleAnalysisShown(this.props.id, newState.shown);
        });
    };

    handleAnalysisCheckbox = (event, checked) => {
        if (checked) {
            this.$viewerAction.enableAnalysis(this.props.id);
        } else {
            this.$viewerAction.disableAnalysis(this.props.id);
        }
    };

    handleOpacityChange = () => {
        this.$viewerAction.changeAnalysisLayerOpacity(this.props.id, this.state.opacity);
    };

    handleAnalysisTypeChange = (analysisType) => {
        this.$viewerAction.changeAnalysisType(this.props.id, analysisType);
    };

    handleCenterLayerClick = () => {
        this.$mapAction.centerAtLayer(this.props.id);
    };

    handleToggleSwipeLayer = (event, toggle) => {
        this.setState({swiped: toggle});
        this.props.onToggleSwipeAnalysis(this.props.id, toggle);
    };

    onToggleSwipeAnalysis = ({toggle, analysisId}) => {
        let swipedAnalysisId = toggle ? analysisId : null;

        this.setState({
            swipedAnalysisId : swipedAnalysisId
        });
    };

    handleMapZoomEnd = () => {
        if (this.state.analysis.outOfVisibleScale !== this.state.outOfVisibleScale) {
            this.setState({
                outOfVisibleScale : this.state.analysis.outOfVisibleScale
            }, () => {
                if (this.state.shown) {
                    this.$viewerStore.toggleAnalysisShown(this.props.id, this.state.shown);
                }
            });
        }
    };

    renderActions = () => {
        return (
            <section>
                {this.state.error && (
                    <div className="cmaps-analysis-box-build-error">
                        <div className="cmaps-analysis-box-text-container">
                            <Warning/>{locale.getMessage("APP_ERROR_02")}
                        </div>
                    </div>
                )}

                {this.state.outOfVisibleScale && (
                    <div className="cmaps-analysis-box-out-of-visible-scale">
                        <div className="block-actions" />
                    </div>
                )}

                <span className="transparency-label">
					{locale.getLabel('L1008')}
				</span>

                <div style={{display: 'flex'}}>

                    <VisibilityOff style={{transform: 'translateY(20px)'}} />

                    <Slider style={{width: '100%', padding: '0 8px'}}
                            sliderStyle={{marginBottom: '20px'}}
                            value={Number(this.state.analysis.opacity)}
                            min={0}
                            max={1}
                            step={0.01}
                            defaultValue={0.5}
                            disabled={!this.state.analysis.enable}
                            onChange={(event, value) => this.setState({opacity: value})}
                            onDragStop={this.handleOpacityChange}
                    />

                    <Visibility style={{transform: 'translateY(20px)'}} />

                </div>

                <div className={this.appClass + '-analysis-box-actions'}>

                    <div className={this.appClass + '-analysis-box-action'} >
                        <div className={this.appClass + '-analysis-box-button left'}>

                            {this.state.analysis.enable && (
                                <Toggle
                                    disabled={this.state.swipedAnalysisId && (this.props.id !== this.state.swipedAnalysisId)}
                                    toggled={this.state.swiped}
                                    label={locale.getLabel('L1003')}
                                    onToggle={this.handleToggleSwipeLayer} />
                            )}
                        </div>
                    </div>

                    <div className={this.appClass + '-analysis-box-action'}>
                        <div className={this.appClass + '-analysis-box-button right'}>
                            <FlatButton
                                label={locale.getLabel('L1004')}
                                labelPosition="before"
                                disabled={this.state.analysis.currentType === ANALYSIS_TYPE.HEATMAP || !this.state.analysis.enable}
                                onClick={this.handleCenterLayerClick}
                                labelStyle={{textTransform: 'none'}}
                                icon={<Focus/>}
                            />
                        </div>
                    </div>

                </div>

            </section>
        )
    };

    onChangeLegendConfig = (payload) => {
        if (this._legendReference[this.state.analysis.currentType] && this._legendReference[this.state.analysis.currentType].onChangeLegendConfig) {
            this._legendReference[this.state.analysis.currentType].onChangeLegendConfig(payload);
        }
    };

    renderLegend = () => {
        let currentType = this.state.analysis.currentType;
        let Legend = ANALYSIS_TYPE_CONFIG.types[currentType].LegendComponent;
        let legendConfig;

        if (this.$viewerStore.legends[this.state.analysis._id]) {
            legendConfig = this.$viewerStore.legends[this.state.analysis._id][currentType];
        }

        return legendConfig && (
            <Legend ref={element => this._legendReference[this.state.analysis.currentType] = element}
                    analysis={this.state.analysis}
                    legend={legendConfig} />
        );
    };

    componentDidUpdate() {
        let height;

        if (this.state.shown && !this.state.outOfVisibleScale) {
            height = this.refs.measurement.clientHeight + 8;
        } else {
            height = 0;
        }

        if (this.state.height !== height) {
            this.setState({
                height
            });
        }
    }

    onCLickDownloadFile = (outputFormat) => {
        let analysis = this.state.analysis;
        this.$toolAnalysisAction.downloadFile({analysis, outputFormat});
    };

    renderContent = () => {

        return (
            <div style={{
                height: this.state.height ? 'auto' : 0,
                padding: this.state.height ? '5px' : 0,
                borderWidth : this.state.height ? 1 : 0
            }}
                 className={this.appClass + '-analysis-box-content'}>
                <div
                    style={{
                        marginTop : this.state.height ? 0 : 10
                    }}
                    ref="measurement">
                    <TypesRadioButton onChangeValue={this.handleAnalysisTypeChange}
                                      {...this.state.enabledTypes}
                                      currentType={this.state.analysis.currentType}
                                      name={"analysis-box-" + this.state.analysis._id}
                    />
                    {this.renderLegend()}
                    {this.renderActions()}
                </div>
            </div>
        )

    };

    render() {
        let arrowStyle = {
            margin: '0 3px',
            color: '#888',
            cursor: 'hand'
        };

        if (this.state.shown && !this.state.outOfVisibleScale) {
            arrowStyle.transform = 'rotate(90deg)';
        }

        return (
            <section className={this.appClass + '-analysis-box'}>
                <div className={this.appClass + '-loading ' + (this.state.loading ? 'active' : 'hidden')}>
                    <CircularProgress size={30} />
                </div>

                <div className={this.appClass + '-header'}>
                    <div onClick={this.handleToggleShow}>
                        <PlayArrow style={arrowStyle} />
                    </div>

                    <div>

                        <Checkbox iconStyle={{transition: 'none'}}
                                  checked={this.state.analysis.enable}
                                  label={this.props.dragHandle(this.state.analysis.title)}
                                  labelStyle={{whiteSpace: 'nowrap'}}
                                  onCheck={this.handleAnalysisCheckbox}
                                  style={{width: '10%', display: 'inline-block'}}>
                        </Checkbox>
                    </div>

                    <div style={{position: 'absolute', right: 0, top: 0}}>
                        <IconMenu iconButtonElement={<IconButton style={{transition: 'none'}}><MoreVert /></IconButton>}
                                  anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                                  targetOrigin={{horizontal: 'left', vertical: 'top'}}
                                  style={{cursor: 'point', zIndex : 3}}>

                            <MenuItem className={this.appClass + "-edit-analysis-menu-item"}
                                      onTouchTap={() => this.handleClickEditAnalysis(this.props.groupId)}
                                      primaryText={locale.getLabel('L0091')} />
                            <MenuItem className={this.appClass + "-delete-analysis-menu-item"}
                                      onTouchTap={this.handleDeleteAnalysis}
                                      primaryText={locale.getLabel('L1006')} />
                            <MenuItem onTouchTap={this.handleClickDetailsAnalysis}
                                      disabled={this.state.error || !this.state.analysis.description}
                                      primaryText={locale.getLabel('L1007')} />

                            <MenuItem primaryText={"Download"}
                                      rightIcon={<ArrowDropRight />}
                                      menuItems={[
                                          <MenuItem primaryText="Shape"
                                                    onTouchTap={() => {this.onCLickDownloadFile("SHAPE")}}/>,
                                          <MenuItem primaryText="GEOJson"
                                                    onTouchTap={() => {this.onCLickDownloadFile("GEO_JSON")}}/>,
                                          <MenuItem primaryText="KML"
                                                    onTouchTap={() => {this.onCLickDownloadFile("KML")}}/>,
                                          <MenuItem primaryText="CSV"
                                                    onTouchTap={() => {this.onCLickDownloadFile("CSV")}}/>
                                      ]}/>

                            {/*<MenuItem onTouchTap={this.onCLickDownloadFile}*/}
                            {/*primaryText={locale.getLabel('L1020')} />*/}

                        </IconMenu>
                    </div>
                </div>

                {this.state.showDetails && (
                    <Card style={{margin: '10px 4px 0'}}>
                        <CardHeader style={{padding : 0}}>
                            <IconButton style={{float : 'right'}}
                                        onTouchTap={this.handleClickDetailsAnalysis}>
                                <Clear />
                            </IconButton>
                        </CardHeader>

                        <CardText>
                            {this.state.analysis.description}
                        </CardText>
                    </Card>
                )}

                {this.renderContent()}
            </section>
        )
    }
}

export default AnalysisBox;
