import React from "react";
import {MenuItem, SelectField, TextField} from "material-ui";
import update from 'react-addons-update';
import Checkbox from "material-ui/Checkbox";
import Visibility from "material-ui/svg-icons/action/visibility";
import VisibilityOff from "material-ui/svg-icons/action/visibility-off";
import {locale} from '../../../../i18n/Locale';
import Converter from "../../../../util/Converter";
import InputRange from "../../../../component/InputRange";
import {DEFAULT_ANALYSIS_CONFIG} from "../../../../constant/analysis";
import BreakCount from "../../../../component/BreakCount";
import TabBase from "../TabBase";
import DoubleInputRange from "../../../../component/DoubleInputRange";

export default class GeneralTab extends TabBase {

    /**
     * @type { { onChangeAnalysis : Function, analysis : IAnalysis } }
     */
    props;

    constructor(props, context) {
        super(props, context);
        this.setClassName('analysis-form-general-tab');

        this.state = {
            richLayers : this.$viewerStore.viewer.project.richLayers,
            outFields : props.outFields || [],
            resultSet : props.resultSet || [],
            analysis : Converter.mixin({}, DEFAULT_ANALYSIS_CONFIG, props.analysis),
            containsError : false,
            applyConfigIsEnabled: !!props.analysis.tooltipConfig,
            currentColumn: 0,
            visibleScale : !!props.analysis.visibleScaleRange.min && !!props.analysis.visibleScaleRange.max
        };
    }

    handleRichLayerChanged = (event, index, richLayerId) => {
        this.props.onChangeAnalysis(
            Converter.mixin(this.state.analysis, {
                richLayerId
            })
        );
    };

    handleClickRefreshTime = (count) => {
        this.props.onChangeAnalysis(
            Converter.mixin(this.state.analysis, {
                refreshTime : count
            })
        );
    };

    handleApplyConfigCheck = (event, isInputChecked) => {
        this.setState({
            applyConfigIsEnabled : isInputChecked
        });

        setTimeout(() => {
            if(!isInputChecked){
                let analysis = Converter.mixin(this.state.analysis);
                delete analysis.tooltipConfig;
                this.props.onChangeAnalysis(analysis);
            }
        }, 100);
    };

    handleFieldChange = (event, key, value) => {
        this.props.onChangeAnalysis(
            Converter.mixin(this.state.analysis, {
                tooltipConfig : value
            })
        );

    };

    renderFieldsApplyConfig = () => {
        return this.state.outFields.map((field, index) =>
            <MenuItem key={index} value={field.name} primaryText={field.alias} />
        );
    };

    handleVisibleScaleCheck = (e, checked) => {
        let newState = update(this.state, {
            visibleScale : {
                $set : checked
            }
        });

        if (checked) {
            newState.analysis.visibleScaleRange = {min : 1, max : 30};
        } else {
            delete newState.analysis.visibleScaleRange;
        }

        this.setState(newState, () => {
            this.props.onChangeAnalysis(this.state.analysis);
        });
    };

    handleChangeVisibleScaleValues = ({initial, final}) => {
        let newState = update(this.state, {
            analysis : {
                visibleScaleRange : {
                    min : {
                        $set : initial
                    },
                    max : {
                        $set : final
                    }
                }
            }
        });

        this.props.onChangeAnalysis(newState.analysis);
    };

    validate() {
        let valid = true;

        if (!this.state.analysis.title) {
            valid = false;
        }

        if (!this.state.analysis.richLayerId) {
            valid = false;
        }

        return valid;
    }

    render() {
        return (
            <section className={this.className()}>

                <div className={this.className('row')}>
                    <div>
                        <TextField
                            style={{width : '100%'}}
                            value={this.state.analysis.title}
                            errorText={
                                this.state.containsError &&
                                !this.state.analysis.title ? locale.getLabel("L0068") : ""
                            }
                            onChange={(event, title) =>
                                this.props.onChangeAnalysis(
                                    Converter.mixin(this.state.analysis, {title})
                                )
                            }
                            floatingLabelText={locale.getLabel('L0003')}/>
                    </div>
                    <div>
                        <InputRange label={locale.getLabel('L0020')}
                                    style={{ width : '100%' }}
                                    value={this.state.analysis.opacity}
                                    onDragStop={(opacity) =>
                                        this.props.onChangeAnalysis(
                                            Converter.mixin(this.state.analysis, {opacity})
                                        )
                                    }/>
                    </div>
                </div>

                <div className={this.className('row')}>
                    <div>
                        <SelectField
                            disabled={Boolean(this.state.analysis._id)}
                            style={{width: '100%'}}
                            value={this.state.analysis.richLayerId}
                            autoWidth={true}
                            errorText={
                                this.state.containsError &&
                                !this.state.analysis.richLayerId ? locale.getLabel("L0068") : ""
                            }
                            onChange={this.handleRichLayerChanged}
                            floatingLabelText={locale.getLabel('L0005')}>

                            {this.state.richLayers.map((richLayer, index) => {
                                return <MenuItem key={index} value={richLayer._id} primaryText={richLayer.title}/>
                            })}
                        </SelectField>
                    </div>
                    <div style={{ width : 'auto' }}>
                        <BreakCount
                            min={0}
                            style={{width : '100%'}}
                            label={locale.getLabel("L0057")}
                            count={this.state.analysis.refreshTime}
                            onChange={this.handleClickRefreshTime}/>
                    </div>
                </div>

                <div className={this.className('row')}>
                    <div style={{ width : '50%' }}>
                        <Checkbox label={locale.getLabel('L2019')}
                                  checked={this.state.applyConfigIsEnabled}
                                  onCheck={this.handleApplyConfigCheck}
                                  checkedIcon={<Visibility />}
                                  uncheckedIcon={<VisibilityOff />}/>
                        {this.state.applyConfigIsEnabled && (
                            <SelectField floatingLabelText={locale.getLabel('L0025')}
                                         onChange={this.handleFieldChange}
                                         value={this.state.analysis.tooltipConfig}>

                                {this.renderFieldsApplyConfig()}

                            </SelectField>
                        )}
                    </div>
                </div>

                <div className={this.className('row')}>
                    <div style={{width : '100%'}}>
                        <Checkbox label={locale.getLabel('L0096')}
                                  checked={this.state.visibleScale}
                                  onCheck={this.handleVisibleScaleCheck}
                                  checkedIcon={<Visibility />}
                                  uncheckedIcon={<VisibilityOff />}/>
                        {this.state.visibleScale && (

                            <DoubleInputRange onChange={this.handleChangeVisibleScaleValues}
                                              firstLabel={locale.getLabel("L0097")}
                                              secondLabel={locale.getLabel("L0098")}
                                              min={1}
                                              max={30}
                                              initial={this.state.analysis.visibleScaleRange.min}
                                              final={this.state.analysis.visibleScaleRange.max}/>
                        )}
                    </div>
                </div>

            </section>
        );
    }

}
