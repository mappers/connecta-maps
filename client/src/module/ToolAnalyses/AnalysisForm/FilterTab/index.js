import React from "react";
import TabBase from "../TabBase";
import Converter from "../../../../util/Converter";
import {DEFAULT_ANALYSIS_CONFIG} from "../../../../constant/analysis";
import FilterFields from "../../../../component/FilterFields";

export default class FilterTab extends TabBase {

    constructor(props, context) {
        super(props, context);
        this.setClassName('analysis-form-filter-tab');

        this.state = {
            outFields : props.outFields || [],
            resultSet : props.resultSet || [],
            analysis : Converter.mixin({}, DEFAULT_ANALYSIS_CONFIG, props.analysis),
        };
    }

    handleOnRequestChangeFilter = (filters) => {
        this.props.onChangeAnalysis(
            Converter.mixin(this.state.analysis, {
                filterConfig : [].concat(filters)
            })
        );
    };

    render() {
        return (
            <div className={this.className()}>

                {
                    this.state.analysis.richLayerId &&
                    <FilterFields filters={this.state.analysis.filterConfig}
                                  fields={this.state.outFields}
                                  resultSet={this.state.resultSet}
                                  onRequestChange={this.handleOnRequestChangeFilter}/>
                }

            </div>
        );
    }

}