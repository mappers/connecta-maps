import React from "react";
import {ANALYSIS_TYPE_CONFIG, DEFAULT_ANALYSIS_CONFIG, GEOMETRY_TYPE} from "../../../../constant/analysis";
import Converter from "../../../../util/Converter";
import AnalysisType from "./AnalysisType";
import update from "react-addons-update";
import TabBase from "../TabBase";

export default class RendererTab extends TabBase {

    /**
     * @type { { onChangeAnalysis : Function } }
     */
    props;

    constructor(props, context) {
        super(props, context);
        this.setClassName('analysis-form-renderer-tab');

        this.state = {
            outFields : props.outFields || [],
            currentGeometryType : props.currentGeometryType,
            analysis : Converter.mixin({}, DEFAULT_ANALYSIS_CONFIG, props.analysis)
        };
    }

    handleCurrentRendererChange = (renderer) => {
        let analysis = update(this.state.analysis, {
                configRenderer : {
                    [renderer.currentRenderer] : {
                        $set : renderer.state
                    }
                }
            }
        );

        this.props.onChangeAnalysis(
            analysis
        );
    };

    setAnalysisType = (type) => {
        this.props.onChangeAnalysis(
            Converter.mixin(this.state.analysis, {
                currentType : type
            })
        );
    };

    validate() {
        let valid = true;

        if (!this.state.analysis.title) {
            valid = false;
        }

        if (!this.state.analysis.richLayerId) {
            valid = false;
        }

        return valid;
    }

    render() {
        let currentType = this.state.analysis.currentType;
        let Type = ANALYSIS_TYPE_CONFIG.types[currentType].Class;

        return (
            <div className={this.className()}>
                <AnalysisType onChangeValue={this.setAnalysisType}
                              initialSelectedIndex={tabsIndexes[this.state.analysis.currentType]}
                              thematic={true}
                              // chart={true}
                              heatmap={this.state.currentGeometryType === GEOMETRY_TYPE.POINT}
                              cluster={this.state.currentGeometryType === GEOMETRY_TYPE.POINT}
                              currentType={this.state.analysis.currentType}
                              name="analysis_form"
                />

                <div style={{ padding : '10px' }}>
                    <Type currentRenderer={
                        Converter.mixin(
                            {},
                            DEFAULT_ANALYSIS_CONFIG.configRenderer[currentType],
                            this.state.analysis.configRenderer[currentType]
                        )}
                          analysis={this.state.analysis}
                          outFields={this.state.outFields}
                          currentGeometryType={this.state.currentGeometryType}
                          onChangeAnalysis={this.props.onChangeAnalysis}
                          onChange={this.handleCurrentRendererChange}/>

                </div>
            </div>
        );
    }
}

const tabsIndexes = {
    simpleRenderer : 0,
    thematic : 1,
    chart : 2,
    cluster : 3,
    heatmap : 4
};