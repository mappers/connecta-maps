import React from "react";
import PropTypes from "prop-types";
import ModuleBase from "../../../../../_base/abstract/ModuleBase";
import {legendDecorators} from "../../../AnalysisBox/decorator/LegendDecorator";

export default class Symbology extends ModuleBase{

    static propTypes = {
        geometryType : PropTypes.string,
        symbology : PropTypes.any,
        onClickGeometrySymbology : PropTypes.func
    };

    constructor(props, context) {
        super(props, context);
        this.setClassName('tool-analysis-analysis-form-symbology');
    }

    render() {
        let GeometrySymbology = legendDecorators[this.props.geometryType];
        let geometrySymbologyNode;

        return (
            <div className={this.className()}>
                <GeometrySymbology
                    onLoad={el => geometrySymbologyNode = el}
                    onClick={() => {
                        if (this.props.onClickGeometrySymbology) {
                            this.props.onClickGeometrySymbology(geometrySymbologyNode);
                        }
                    }}
                    color={this.props.symbology.fillColor}
                />
            </div>
        );
    }

}