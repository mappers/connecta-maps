import React from 'react';
import {locale} from '../../../../i18n/Locale';
import {Tabs, Tab} from 'material-ui/Tabs';
import {ANALYSIS_TYPE} from '../../../../constant/analysis';

class AnalysisType extends React.Component{

    handleClickOnRadioButton = (event) => {
        this.props.onChangeValue(event.props.value);
    };

    render() {
        return (
            <section className="cmaps-analysis-types-container">
                <Tabs className="cmaps-analysis-types-tab"
                      initialSelectedIndex={this.props.initialSelectedIndex}>
                    {/*Analysis Type Static*/}
                    <Tab
                        label={locale.getLabel('L0007')}
                        onActive={this.handleClickOnRadioButton}
                        value={ANALYSIS_TYPE.STATIC}
                    />

                    {/*Analysis Type Thematic*/}
                    {this.props.thematic && (
                        <Tab
                            label={locale.getLabel('L0008')}
                            onActive={this.handleClickOnRadioButton}
                            value={ANALYSIS_TYPE.THEMATIC}
                        />
                    )}

                    {/*Analysis Type Chart*/}
                    {this.props.chart && (
                        <Tab
                            label={locale.getLabel('L0009')}
                            onActive={this.handleClickOnRadioButton}
                            value={ANALYSIS_TYPE.CHART}
                        />
                    )}

                    {/*Analysis Type Cluster*/}
                    {this.props.cluster && (
                        <Tab
                            label={locale.getLabel('L0010')}
                            onActive={this.handleClickOnRadioButton}
                            value={ANALYSIS_TYPE.CLUSTER}
                        />
                    )}

                    {/*Analysis Type Heatmap*/}
                    {this.props.heatmap && (
                        <Tab
                            label={locale.getLabel('L0011')}
                            onActive={this.handleClickOnRadioButton}
                            value={ANALYSIS_TYPE.HEATMAP}
                        />
                    )}
                </Tabs>
            </section>
        );
    }
}

export default AnalysisType;