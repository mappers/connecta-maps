import React from 'react';
import InputRange from '../../../../../component/InputRange';
import InputGradient from '../../../../../component/InputGradient';
import {locale} from '../../../../../i18n/Locale'
import SelectField from 'material-ui/SelectField';
import Checkbox from 'material-ui/Checkbox';
import MenuItem from 'material-ui/MenuItem';
import Visibility from 'material-ui/svg-icons/action/visibility';
import VisibilityOff from 'material-ui/svg-icons/action/visibility-off';
import update from 'react-addons-update';
import {ANALYSIS_TYPE} from "../../../../../constant/analysis";

class TypeHeatmap extends React.Component {

    constructor (props) {
        super(props);

        this.state = {
            applyWeightIsEnabled : !!props.currentRenderer.field
        };
    }

    handleApplyWeightCheck = (event, isInputChecked) => {
        let newProps;

        this.setState({
            applyWeightIsEnabled : isInputChecked
        });

        if (!isInputChecked) {
            newProps = update(this.props, {
                currentRenderer : {
                    field : {
                        $set : ''
                    }
                }
            });

            this.emitHeatmap(newProps);
        }
    };

    handleFieldChange = (event, key, value) => {
        let newProps = update(this.props, {
            currentRenderer : {
                field : {
                    $set : value
                }
            }
        });

        this.emitHeatmap(newProps);
    };

    handleColorsInitialColorChange = (color) => {
        let newProps = update(this.props, {
            currentRenderer : {
                colors : {
                    $splice : [[0, 1, color]]
                }
            }
        });

        this.emitHeatmap(newProps);
    };

    handleColorsFinalColorChange = (color) => {
        let newProps = update(this.props, {
            currentRenderer : {
                colors : {
                    $splice : [[1, 1, color]]
                }
            }
        });

        this.emitHeatmap(newProps);
    };

    renderFieldsApplyWeight = () => {
        let fields = [];

        this.props.outFields.forEach((field, index) => {
            if (field.valueType === "number" || field.valueType === "raw") {
                fields.push(<MenuItem key={index} value={field.name} primaryText={field.alias} />);
            }
        });

        return fields;
    };


    handleBlurRadiusChange = (value) => {
        let newProps = update(this.props, {
            currentRenderer : {
                blurRadius : {
                    $set : value
                }
            }
        });

        this.emitHeatmap(newProps);
    };

    emitHeatmap = (state) => {
        this.props.onChange({
            currentRenderer : ANALYSIS_TYPE.HEATMAP,
            state : state.currentRenderer
        });
    };

    render () {
        return (
            <section className="cmaps-type-heatmap-container">
                <div className="cmaps-type-heatmap-inline-inputs">
                    <div className="cmaps-input-radius-aggregation">
                        <InputRange label={locale.getLabel('L0029')}
                                    step={1}
                                    min={1}
                                    max={100}
                                    onDragStop={this.handleBlurRadiusChange}
                                    value={this.props.currentRenderer.blurRadius}/>
                    </div>
                    <div className="cmaps-input-color-ramp">
                        <InputGradient label={locale.getLabel('L0028')}
                                       onInitialColorChange={this.handleColorsInitialColorChange}
                                       onFinalColorChange={this.handleColorsFinalColorChange}
                                       initialColor={this.props.currentRenderer.colors[0]}
                                       finalColor={this.props.currentRenderer.colors[1]}/>
                    </div>
                </div>

                <Checkbox label={locale.getLabel('L0030')}
                          checked={this.state.applyWeightIsEnabled}
                          onCheck={this.handleApplyWeightCheck}
                          checkedIcon={<Visibility />}
                          uncheckedIcon={<VisibilityOff />}/>
                {this.state.applyWeightIsEnabled && (
                    <SelectField floatingLabelText={locale.getLabel('L0015')}
                                 onChange={this.handleFieldChange}
                                 value={this.props.currentRenderer.field}>

                        {this.renderFieldsApplyWeight()}

                    </SelectField>
                )}
            </section>
        );
    }
}

export default TypeHeatmap;
