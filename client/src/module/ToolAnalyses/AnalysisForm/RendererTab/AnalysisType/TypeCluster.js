import React from "react";
import Checkbox from "material-ui/Checkbox";
import {Card, CardText} from "material-ui/Card";
import {locale} from "../../../../../i18n/Locale";
import Visibility from "material-ui/svg-icons/action/visibility";
import VisibilityOff from "material-ui/svg-icons/action/visibility-off";
import InputRange from "../../../../../component/InputRange";
import ColorPicker from "../../../../../component/ColorPicker";
import InputGradient from "../../../../../component/InputGradient";
import {ANALYSIS_TYPE} from "../../../../../constant/analysis";
import update from "react-addons-update";
import BreakCount from "../../../../../component/BreakCount";
import {DEFAULT_ANALYSIS_CONFIG} from '../../../../../constant/analysis';

class TypeCluster extends React.Component {

    constructor (props) {
        super(props);

        this.state = {
            uniqueSize : props.currentRenderer.uniqueSize
        };
    }

    handleUniqueSizeVisibility = (event, isInputChecked) => {
        this.setState({
            uniqueSize : isInputChecked
        });

        let newProps;

        if (isInputChecked) {
            newProps = update(this.props, {
                currentRenderer : {
                    uniqueSize : {
                        $set : true
                    }
                }
            });
        } else {
            newProps = update(this.props, {
                currentRenderer : {
                    uniqueSize : {
                        $set : DEFAULT_ANALYSIS_CONFIG.configRenderer.cluster.uniqueSize
                    },
                    size : {
                        $set : DEFAULT_ANALYSIS_CONFIG.configRenderer.cluster.size
                    }
                }
            });
        }

        this.emitCluster(newProps);
    };

    handleOutlineColorChange = (color) => {
        let newProps = update(this.props, {
            currentRenderer : {
                outlineColor : {
                    $set : color.hex
                }
            }
        });

        this.emitCluster(newProps);
    };

    handleRadiusAggregationChange = (value) => {
        let newProps = update(this.props, {
            currentRenderer : {
                distance : {
                    $set : value
                }
            }
        });

        this.emitCluster(newProps);
    };

    handleBreakCountChange = (count) => {
        let newProps = update(this.props, {
            currentRenderer : {
                breakCount : {
                    $set : count
                }
            }
        });

        this.emitCluster(newProps);
    };

    handleInitialColorRampChange = (color) => {
        let newProps = update(this.props, {
            currentRenderer : {
                colorRamp : {
                    $splice : [[0, 1, color]]
                }
            }
        });

        this.emitCluster(newProps);
    };

    handleFinalColorRampChange = (color) => {
        let newProps = update(this.props, {
            currentRenderer : {
                colorRamp : {
                    $splice : [[1, 1, color]]
                }
            }
        });

        this.emitCluster(newProps);
    };

    handleSizeChange = (value) => {
        let newProps = update(this.props, {
            currentRenderer : {
                size : {
                    $set : value
                }
            }
        });

        this.emitCluster(newProps);
    };

    emitCluster = (state) => {
        this.props.onChange({
            currentRenderer : ANALYSIS_TYPE.CLUSTER,
            state : state.currentRenderer
        });
    };

    render() {
        return (
            <section className="cmaps-type-cluster-container">
                <Card style={{width : '100%', position : 'relative'}}>
                    <CardText style={{paddingBottom : 0}}>
                        <div className="cmaps-type-cluster-color-ramp-container">
                            <div className="cmaps-align-items-cluster">
                                <ColorPicker label={locale.getLabel('L0013')}
                                             value={this.props.currentRenderer.outlineColor}
                                             onChange={this.handleOutlineColorChange}/>

                                <div style={{ width:"100%", transform: "translateY(-4px)" }}>
                                    <InputGradient label={locale.getLabel('L0028')}
                                                   onInitialColorChange={this.handleInitialColorRampChange}
                                                   onFinalColorChange={this.handleFinalColorRampChange}
                                                   initialColor={this.props.currentRenderer.colorRamp[0]}
                                                   finalColor={this.props.currentRenderer.colorRamp[1]}/>
                                </div>

                                <BreakCount label={locale.getLabel('L0017')}
                                            count={this.props.currentRenderer.breakCount}
                                            onChange={this.handleBreakCountChange}/>
                            </div>
                        </div>
                    </CardText>
                </Card>

                <InputRange label={locale.getLabel('L0029')}
                            step={1}
                            min={1}
                            max={100}
                            value={this.props.currentRenderer.distance}
                            onDragStop={this.handleRadiusAggregationChange}/>

            </section>
        );
    }

}

export default TypeCluster;
