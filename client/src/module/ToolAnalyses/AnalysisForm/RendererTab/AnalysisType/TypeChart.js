import React from "react";
import InputRange from "../../../../../component/InputRange";
import {locale} from "../../../../../i18n/Locale";
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {ANALYSIS_TYPE} from '../../../../../constant/analysis';
import update from 'react-addons-update';

class TypeChart extends React.Component {

    constructor (props) {
        super(props);
    }

    handleMetricFieldChange = (event, key, value) => {
        let newProps = update(this.props, {
            currentRenderer : {
                metric : {
                    $set : value
                }
            }
        });

        this.emitChart(newProps);
    };

    handleDimensionFieldChange = (event, key, value) => {
        let newProps = update(this.props, {
            currentRenderer : {
                dimension : {
                    $set : value
                }
            }
        });

        this.emitChart(newProps);
    };

    handleSizeChange = (value) => {
        let newProps = update(this.props, {
            currentRenderer : {
                size : {
                    $set : value
                }
            }
        });

        this.emitChart(newProps);
    };

    emitChart = (state) => {
        this.props.onChange({
            currentRenderer : ANALYSIS_TYPE.CHART,
            state : state.currentRenderer
        });
    };

    render () {
        let style = {
            width : '32%'
        };
        return (
            <section className="cmaps-type-chart-container">
                <div className="cmaps-type-chart-fields-container">
                    <SelectField floatingLabelText={locale.getLabel('L0054')}
                                 value={this.props.currentRenderer.metric}
                                 onChange={this.handleMetricFieldChange}
                                 style={style}>

                        {this.props.outFields.map((outField, index) => {
                            if (outField.valueType === 'number' || outField.valueType === "raw") {
                                return (
                                    <MenuItem key={index} value={outField.name} primaryText={outField.alias}/>
                                );
                            }
                        })}
                    </SelectField>

                    <SelectField floatingLabelText={locale.getLabel('L0055')}
                                 value={this.props.currentRenderer.dimension}
                                 onChange={this.handleDimensionFieldChange}
                                 style={style}>

                        {this.props.outFields.map((outField, index) => {
                            return (
                                <MenuItem key={index} value={outField.name} primaryText={outField.alias}/>
                            );
                        })}
                    </SelectField>

                    <div style={{style, transform : 'translateY(8px)'}}>
                        <InputRange label={locale.getLabel('L0023')}
                                    onDragStop={this.handleSizeChange}
                                    step={1}
                                    min={40}
                                    max={200}
                                    value={this.props.currentRenderer.size}/>
                    </div>
                </div>
            </section>
        );
    }
}

export default TypeChart;
