import React from 'react'
import {DEFAULT_ANALYSIS_CONFIG} from '../../constant/analysis';
import ModuleBase from '../../_base/abstract/ModuleBase';
import {VIEWER} from "../../constant/viewer";
import Converter from '../../util/Converter';
import {MapsDataLoader} from "../../MapsDataLoader";
import {locale} from '../../i18n/Locale';
import GeneralTab from "./AnalysisForm/GeneralTab/index";
import RendererTab from "./AnalysisForm/RendererTab/index";
import FieldsTab from "./AnalysisForm/FieldsTab/index";
import TrackingTab from "./AnalysisForm/TrackingTab/index";
import FilterTab from "./AnalysisForm/FilterTab/index";

class AnalysisForm extends ModuleBase {

    _refTabs = {};
    _currentTab;

    constructor(props, context) {
        super(props, context);

        this.state = {
            trackingConfigurationIsVisible : props.analysis.trackingConfig && !!props.analysis.trackingConfig.temporalDimensionField,
            filterConfigurationIsVisible : props.analysis.filterConfig && !!props.analysis.filterConfig.length,
            infoWindowConfigurationIsVisible : false,
            richLayers : this.$viewerStore.viewer.project.richLayers,
            outFields : [],
            currentGeometryType : '',
            resultSet : [],
            analysis : Converter.mixin({}, DEFAULT_ANALYSIS_CONFIG, props.analysis),
            currentTabName : 'general'
        };

        this.setClassName("analysis-form-container");

        if (props.analysis._id) {
            let richLayerId = props.analysis.richLayerId;
            this.state.currentGeometryType = this.$viewerStore.getRichLayerMetaData(String(richLayerId)).layer.geometryType;
            this.state.outFields = this.$viewerStore.getOutfieldsByRichLayerId(richLayerId);
        }
    }

    componentWillMount () {
        let value = this.props.analysis.richLayerId;
        if (value) {
            let richLayer = this.$viewerStore.getRichLayerMetaData(value);

            this.setState({
                resultSet : MapsDataLoader.get(richLayer.resultSetId)
            });
        }
    }

    componentDidMount () {
        this.own(
            this.$viewerStore.listen(VIEWER.CLICK_ON_SAVE_NEW_ANALYSIS, this.handlerClickSaveAnalysis)
        );
    };

    handlerClickSaveAnalysis = () => {
        let valid;
        let tabName;

        for (let key in this._refTabs) {
            let instance = this._refTabs[key];

            if (instance.validate) {
                valid = instance.validate();
            }

            if (!valid) {
                tabName = key;
                break;
            } else {
                this._refTabs[key].setState({
                    containsError : false
                });
            }
        }

        if (!valid) {
            this.setState({
                currentTabName : tabName
            });
            this._refTabs[tabName].setState({
                containsError : true
            });
        } else {
            this.$viewerAction.saveAnalysis(this.state.analysis, this.props.groupId);
        }
    };

    onChangeAnalysis = (analysis) => {
        this.setState({ analysis });
    };

    changeCurrentTab = (tabName) => {
        this._currentTab = this._refTabs[tabName];
        this.setState({
            currentTabName : tabName
        });
    };

    render() {
        let defaultProps = {
            analysis : this.state.analysis,
            groupId : this.props.groupId,
            outFields : this.state.outFields,
            currentGeometryType : this.state.currentGeometryType,
            resultSet : this.state.resultSet,
            onChangeAnalysis : this.onChangeAnalysis
        };

        return (
            <section className={this.className()}>
                <div className={this.className('sidebar')}>

                    {sideMenu.map((item, index) => {
                        return (
                            <div key={index}
                                 className={this.className('menu-item') + (item.name === this.state.currentTabName ? " selected" : "")}
                                 onClick={() => {this.changeCurrentTab(item.name)}}>

                                {item.label}
                            </div>
                        )
                    })}
                </div>

                <div className={this.className('content')}>
                    <div style={{
                        display : this.state.currentTabName === "general" ? "block" : "none"
                    }}>
                        <GeneralTab {...defaultProps} ref={e => {
                            this._refTabs['general'] = e;
                            if (!this._currentTab) {
                                this._currentTab = e;
                            }
                        }} />
                    </div>

                    <div style={{
                        display : this.state.currentTabName === "renderer" ? "block" : "none"
                    }}>
                        <RendererTab {...defaultProps} ref={e => this._refTabs['renderer'] = e} />
                    </div>

                    <div style={{
                        display : this.state.currentTabName === "fields" ? "block" : "none"
                    }}>
                        <FieldsTab {...defaultProps} ref={e => this._refTabs['fields'] = e} />
                    </div>

                    {/*<div style={{
                        display : this.state.currentTabName === "tracking" ? "block" : "none"
                    }}>
                        <TrackingTab {...defaultProps} ref={e => this._refTabs['tracking'] = e} />
                    </div>*/}

                    <div style={{
                        display : this.state.currentTabName === "filters" ? "block" : "none"
                    }}>
                        <FilterTab {...defaultProps} ref={e => this._refTabs['filters'] = e} />
                    </div>
                </div>
            </section>
        );
    }
}

export default AnalysisForm;

const sideMenu = [
    {
        label : locale.getLabel("L1021"),
        name : "general"
    },
    {
        label : locale.getLabel("L1022"),
        name : "renderer"
    },
    {
        label : locale.getLabel("L1023"),
        name : "fields"
    },
   /* {
        label : locale.getLabel("L1024"),
        name : "tracking"
    },*/
    {
        label : locale.getLabel("L1029"),
        name : "filters"
    }
];
