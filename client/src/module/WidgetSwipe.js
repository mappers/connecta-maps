import React from 'react';
import ModuleBase from "../_base/abstract/ModuleBase";
import Slider from 'rc-slider';
import "rc-slider/assets/index.css";
import {VIEWER} from "../constant/viewer";
import update from 'react-addons-update';

class WidgetSwipe extends ModuleBase {

    _baseClass = this.appClass + "-widget-swipe";

    constructor (props, context) {
        super(props, context);

        this.state = {
            enable : false,
            sliderValue : 50
        };
    }

    componentDidMount() {
        this.own(
            this.$viewerStore.listen(VIEWER.TOGGLE_SWIPE, this.handleToggleSwipe),
            this.$viewerStore.listen(VIEWER.DISABLE_SWIPE, this.disableSwipe)
        );

        this.context.$parent.mapAPI.registerResizeCallback(this.setHandleHeight);
    }

    disableSwipe = () => {
        this.setState({
            sliderValue : 50
        });
    };

    handleToggleSwipe = ({toggle}) => {
        let newState = update(this.state, {
            enable : {
                $set : toggle
            }
        });

        this.setState(newState);
        this.setHandleHeight();
    };

    setHandleHeight = () => {
        this.setState({
            handleHeight : this.context.$parent.mapAPI.getMapMeasures().height
        });
    };

    handleChangeSlider = (value) => {
        this.$viewerAction.changeSwipeValue(value / 100);
    };

    render () {
        let timelineStyle = {display : this.state.enable ? 'flex' : 'none'};

        return (
            <div className={this._baseClass} style={timelineStyle}>
                <div className={this._baseClass + "-slider-container"}>
                    {this.state.enable &&
                    <Slider className={this._baseClass + "-slider"}
                            defaultValue={this.state.sliderValue}
                            min={0}
                            max={100}
                            handleStyle={{height: this.state.handleHeight}}
                            onChange={this.handleChangeSlider}/>
                    }
                </div>
            </div>
        );
    }
}

export default WidgetSwipe;