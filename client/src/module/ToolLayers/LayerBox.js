import React from 'react';
import PropTypes from "prop-types";
import ModuleBase from "../../_base/abstract/ModuleBase";
import Checkbox from 'material-ui/Checkbox';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Slider from "material-ui/Slider";
import VisibilityOff from "material-ui/svg-icons/action/visibility-off";
import Visibility from "material-ui/svg-icons/action/visibility";
import MoreVert from "material-ui/svg-icons/navigation/more-vert";
import PlayArrow from "material-ui/svg-icons/av/play-arrow";
import Warning from "material-ui/svg-icons/alert/warning";
import {locale} from '../../i18n/Locale';
import update from 'react-addons-update';
import CircularProgress from "material-ui/CircularProgress";

class LayerBox extends ModuleBase {

    static propTypes = {
        onRemoveItem : PropTypes.func,
        dragHandle: PropTypes.func
    };

    constructor(props, context) {
        super(props, context);

        this.state = {
            layer : this.$viewerStore.layers[this.props.id],
            loading: false,
            opacity: 0.8,
            height: 0
        };
    }

    componentDidMount(){
        setTimeout(() => {
            this.$viewerAction.addLayerBox(this);

            setTimeout(()=> {
                this.$mapAction.buildStaticLayer(this.props.id);

                if (this.state.layer.enabled) {
                    this.$viewerAction.changeStaticLayerOpacity(this.props.id, this.state.layer.opacity);
                }
                setTimeout(() => {
                }, 0);
            }, 0);
        }, 0);
    }

    componentWillUnmount() {
        super.componentWillUnmount();

        setTimeout(() => {
            this.$viewerAction.removeLayerBox(this);
        }, 0);
    }

    handleLayerCheckbox = (event, checked) => {
        if (checked) {
            this.$viewerAction.enableStaticLayer(this.props.id);
        } else {
            this.$viewerAction.disableStaticLayer(this.props.id);
        }
    };

    onLayerChange = (layer) => {
        let state = this.state;
        if (state.layer._id === layer._id) this.setState({layer});
    };

    handleStaticLayerIsBuilding = ({id}) => {
        if (this.props.id === id) {
            this.setState(update(this.state, {
                loading : {
                    $set : true
                }
            }));
        }
    };

    handleStaticLayerBuildSuccess = ({id, legend}) => {
        if (this.props.id === id) {
            this.setState(update(this.state, {
                loading : {
                    $set : false
                },
                legend: {
                    $set: legend
                }
            }));

            this.$viewerStore.addStaticLegend(id, legend);

            if(this.state.layer.enabled){
                this.$viewerAction.enableStaticLayer(id);

                if(this.state.layer.opacity) {
                    setTimeout(() => {
                        this.$viewerAction.changeStaticLayerOpacity(this.props.id, this.state.layer.opacity || this.state.opacity);
                    }, 0);
                }
            }
        }
    };

    handleOpacityChange = () => {
        this.$viewerAction.changeStaticLayerOpacity(this.props.id, this.state.opacity);
    };

    handleToggleShow = () => {
        this.$mapAction.buildStaticLayer(this.props.id);
        this.setState({
            shown: !this.state.shown
        });
    };

    componentDidUpdate() {
        let height = this.state.shown ? this.refs.measurement.clientHeight : 0;

        if (this.state.height !== height) {
            this.setState({ height });
        }
    }

    renderActions(){
        return (
            <section>
                {this.state.error && (
                    <div className="cmaps-analysis-box-build-error">
                        <div className="cmaps-analysis-box-text-container">
                            <Warning/>{locale.getMessage("APP_ERROR_02")}
                        </div>
                    </div>
                )}

                <span className="transparency-label">
					{locale.getLabel('L1008')}
				</span>

                <div style={{display: 'flex'}}>
                    <VisibilityOff style={{transform: 'translateY(20px)'}} />
                    <Slider style={{width: '100%', padding: '0 8px'}}
                            sliderStyle={{marginBottom: '20px'}}
                            value={Number(this.state.layer.opacity || this.state.opacity)}
                            min={0}
                            max={1}
                            step={0.01}
                            defaultValue={0.7}
                            onChange={(event, value) => this.setState({opacity: value})}
                            onDragStop={this.handleOpacityChange}
                    />

                    <Visibility style={{transform: 'translateY(20px)'}} />
                </div>

            </section>
        )
    }

    renderContent(){
        return (
            <div style={{height: this.state.height}} className={this.appClass + '-layer-box-content'}>
                <div ref="measurement">
                    <section>
                        <img className="static-legend" src={this.state.legend} />
                    </section>

                    {this.renderActions()}
                </div>
            </div>
        )
    }

    render () {
        let arrowStyle = {
            margin: '0 3px',
            color: '#888',
            cursor: 'hand'
        };

        return (
            <section className={this.appClass + '-layer-box'}>
                <div className={'loading ' + (this.state.loading ? 'active' : 'hidden')}>
                    <CircularProgress size={30} />
                </div>

                <div className={this.appClass + '-header'}>
                    <div>
                        <PlayArrow style={{...arrowStyle, transform: (this.state.shown ? 'rotate(90deg)' : 'none')}}
                                   onClick={this.handleToggleShow}/>
                    </div>

                    <div className={this.appClass + '-layer-box-content'} style={{width: '10%', display: 'inline-block'}}>
                        <Checkbox iconStyle={{transition: 'none'}}
                                  onCheck={this.handleLayerCheckbox}
                                  checked={this.state.layer.enabled}
                                  label={this.props.dragHandle(this.state.layer.title)}
                                  labelStyle={{whiteSpace: 'nowrap'}}
                                  style={{width: '100%', display: 'inline-block'}}>
                        </Checkbox>
                    </div>

                    <div style={{position: 'absolute', right: 0, top: 0}}>
                        <IconMenu
                            iconButtonElement={<IconButton style={{transition: 'none'}}><MoreVert /></IconButton>}
                            anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                            targetOrigin={{horizontal: 'left', vertical: 'top'}}
                            style={{cursor: 'point', zIndex : 3}}>
                            <MenuItem primaryText={locale.getLabel('L1006')} onTouchTap={() => this.props.onRemoveItem(this.props.id)}/>
                        </IconMenu>
                    </div>
                </div>

                {this.renderContent()}
            </section>
        );
    }
}

export default LayerBox;

