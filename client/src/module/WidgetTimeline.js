import React from 'react';
import ModuleBase from "../_base/abstract/ModuleBase";
import Slider from 'rc-slider';
import "rc-slider/assets/index.css";
import {VIEWER} from "../constant/viewer";
import update from 'react-addons-update';

class WidgetTimeline extends ModuleBase {

    _baseClass = this.appClass + "-widget-timeline";

    constructor (props, context) {
        super(props, context);

        this.state = {
            enable : false,
            marks: {}
        };
    }

    componentDidMount() {
        this.own(
            this.$viewerStore.listen(VIEWER.ENABLE_TIME_LINE, this.handleToggleTimeLine)
        );
    }

    handleToggleTimeLine = ({index, checked, metrics}) => {
        let self = this;

        if (checked) {
            build();
        } else {
            destroy();
        }

        function build () {
            let min = metrics[0];

            self.setState(update(self.state, {
                minMark : {
                    $set : 0
                },
                maxMark : {
                    $set : metrics.length - 1
                },
                enable : {
                    $set : checked
                },
                analysisIndex : {
                    $set : index
                },
                marks : {
                    $set: self.handleBuildMarks(metrics)
                }
            }));

            setTimeout(() => {
                self.$viewerAction.changeTimelineValue({index: index, value: min});
            }, 0);

        }

        function destroy() {
            self.setState(update(self.state, {
                enable : {
                    $set : checked
                }
            }));
        }
    };

    handleBuildMarks = (metrics) => {
        let marks = {};

        metrics.forEach((item, index) => {
            marks[index] = item;
        });

        return marks;
    };

    handleChangeSlider = (value) => {
        this.$viewerAction.changeTimelineValue({index: this.state.analysisIndex, value : this.state.marks[value]});
    };

    render () {
        let timelineStyle = {display : this.state.enable ? 'flex' : 'none'};

        return (
            <div className={this._baseClass} style={timelineStyle}>
                <div className={this._baseClass + "-slider-container"}>
                    <Slider className={this._baseClass + "-slider"}
                            min={this.state.minMark}
                            max={this.state.maxMark}
                            step={null}
                            marks={this.state.marks}
                            onChange={this.handleChangeSlider}
                            defaultValue={this.state.minMark}/>
                </div>
            </div>
        );
    }
}

export default WidgetTimeline;