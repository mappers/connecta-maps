import React from "react";
import ModuleBase from "../_base/abstract/ModuleBase";
import {VIEWER} from "../constant/viewer";
import {MAP_CONSTANT} from "../constant/map";
import Dialog from "material-ui/Dialog/Dialog";
import FlatButton from "material-ui/FlatButton/FlatButton";
import AnalysisForm from "./ToolAnalyses/AnalysisForm";
import {locale} from "../i18n/Locale";
import update from "react-addons-update";
import {DEFAULT_ANALYSIS_CONFIG} from "../constant/analysis";
import Converter from "../util/Converter";
import ToolAnalysisGroupContainer from "./SortableTree";
import CustomDialog from "../component/CustomDialog";

class ToolAnalyses extends ModuleBase {

    state = {
        analysisConfig: [],
        analysisFormIsOpened : false,
        groupId: null,
        deleteAnalysisDialog : {
            open : false,
            analysisId : null
        },
        deleteGroupDialog : {
            open : false,
            groupId : null
        },
        swipedAnalysisId: '',
        analysisToEdit: {},
        treeIsBuilt: true
    };

    /**
     * @type {AnalysisBox[]}
     */
    _analysesBoxesInstance = [];

    _treeReference = {};

    componentDidMount() {
        this.own(
            this.$viewerStore.listen(VIEWER.GET_ANALYSIS_CONFIG, this.onGetAnalysisConfig, true),
            this.$viewerStore.listen(VIEWER.REORDER_ANALYSES, this.onGetAnalyses),
            this.$viewerStore.listen(VIEWER.SAVE_ANALYSIS, this.onSuccessSaveAnalysis),
            this.$viewerStore.listen(VIEWER.EDIT_ANALYSIS, this.handleEditAnalysis),
            this.$viewerStore.listen(VIEWER.DELETE_ANALYSIS, this.handleDeleteAnalysis),
            this.$viewerStore.listen(VIEWER.DELETE_GROUP, this.handleDeleteGroup),
            this.$viewerStore.listen(VIEWER.ADD_ANALYSIS_BOX, this.putAnalysisBoxInstance),
            this.$viewerStore.listen(VIEWER.REMOVE_ANALYSIS_BOX, this.removeAnalysisBoxInstance),
            this.$viewerStore.listen(VIEWER.SHOW_ANALYSIS_FORM, this.handleAnalysisFormToggle),
            this.$viewerStore.listen(VIEWER.UPDATE, this.rebuildTree),

            //AnalysisBox
            this.$viewerStore.listen(VIEWER.CHANGE_ANALYSIS_LAYER_OPACITY, this.executeOnAnalysisBox.bind(this, 'onAnalysisChange')),
            this.$viewerStore.listen(VIEWER.CHANGE_ANALYSIS_TYPE, this.executeOnAnalysisBox.bind(this, 'onAnalysisChange')),
            this.$viewerStore.listen(VIEWER.ENABLE_ANALYSIS, this.executeOnAnalysisBox.bind(this, 'onAnalysisChange')),
            this.$viewerStore.listen(VIEWER.DISABLE_ANALYSIS, this.executeOnAnalysisBox.bind(this, 'onAnalysisChange')),
            this.$viewerStore.listen(VIEWER.TOGGLE_SWIPE, this.executeOnAnalysisBox.bind(this, 'onToggleSwipeAnalysis')),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_SIMPLE_RENDERER_LAYER, this.executeOnAnalysisBox.bind(this, 'handleLayerBuild', 'simpleRenderer')),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_THEMATIC_LAYER, this.executeOnAnalysisBox.bind(this, 'handleLayerBuild', 'thematic')),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_HEAT_LAYER, this.executeOnAnalysisBox.bind(this, 'handleLayerBuild', 'heatmap')),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_CLUSTER_LAYER, this.executeOnAnalysisBox.bind(this, 'handleLayerBuild', 'cluster')),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_CHART_LAYER, this.executeOnAnalysisBox.bind(this, 'handleLayerBuild', 'chart')),
            this.$mapStore.listen(MAP_CONSTANT.ANALYSIS_LAYER_IS_BUILDING, this.executeOnAnalysisBox.bind(this, 'handleIsBuilding')),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_ERROR, this.executeOnAnalysisBox.bind(this, 'handleBuildError')),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_SUCCESS, this.executeOnAnalysisBox.bind(this, 'handleBuildSuccess')),
            this.$mapStore.listen(MAP_CONSTANT.CLUSTER_LEGEND_IS_BUILT, this.executeOnAnalysisBox.bind(this, 'onChangeLegendConfig'))
        );
    }

    putAnalysisBoxInstance = (instance) => {
        this._analysesBoxesInstance.push(instance);
    };

    removeAnalysisBoxInstance = (instance) => {
        let index = this._analysesBoxesInstance.indexOf(instance);

        if (index !== -1) {
            this._analysesBoxesInstance.splice(index, 1);
        }
    };

    executeOnAnalysisBox = (...args) => {
        let methodName = args[0];
        args.splice(0, 1);

        for (let instance of this._analysesBoxesInstance) {
            if (instance[methodName]) {
                instance[methodName].apply(instance, args);
            }
        }
    };

    onGetAnalysisConfig = (analysisConfig) => {
        let state = update(this.state, {
            analysisConfig: {
                $set: analysisConfig || []
            }
        });

        this.setState(state, this.rebuildTree);
    };

    onSuccessSaveAnalysis = ({groupId, children}) => {
        this.handleAnalysisFormToggle(null, false);
        this.rebuildAGroupOfChildren({groupId, children});
    };

    onGetAnalyses = (analysisConfig) => {
        let state = update(this.state, {
            analysisConfig: {
                $set: analysisConfig
            }
        });
        this.setState(state);
    };

    onClickAddItemAtGroup = (groupId) => {
        let newState = update(this.state, {
            analysisToEdit : {
                $set: Converter.mixin({}, DEFAULT_ANALYSIS_CONFIG)
            },
            groupId : {
                $set : groupId
            },
            analysisFormIsOpened : {
                $set : !this.state.analysisFormIsOpened,
            }
        });
        this.setState(newState);
    };

    handleAnalysisFormToggle = (groupId, toggle) => {
        this.setState({
            analysisFormIsOpened : typeof toggle !== 'undefined' ? toggle : !this.state.analysisFormIsOpened,
            groupId
        });
    };

    handleClickOnSaveAnalysis = () => {
        this.$viewerAction.clickOnSaveNewAnalysis();
    };

    handleEditAnalysis = ({analysis, groupId})=> {
        this.setState(update(this.state, {
            analysisToEdit:{
                $set: analysis
            }
        }));

        this.handleAnalysisFormToggle(groupId, true);
    };

    handleClickCancelAnalysis = () => {
        let newState = update(this.state, {
            analysisToEdit : {
                $set: Converter.mixin({}, DEFAULT_ANALYSIS_CONFIG)
            }
        });

        this.setState(newState);
        this.handleAnalysisFormToggle(null, false);
    };

    handleDeleteAnalysis = ({groupId, children}) => {
        let newState = update(this.state, {
            deleteAnalysisDialog : {
                open : {
                    $set: false
                }
            }
        });

        this.setState(newState, () => {
            this.rebuildAGroupOfChildren({groupId, children});
            this.$appAction.disablePopup();

            setTimeout(() => {
                this.$appAction.alertInfo("APP_INFO_01");
            },0);
        });
    };

    handleDeleteGroup = () => {
        let newState = update(this.state, {
            deleteGroupDialog :{
                open : {
                    $set: false
                }
            }
        });

        this.setState(newState);

        setTimeout(() => {
            this.$appAction.alertInfo("APP_INFO_05");
        },0);
    };

    handleClickDeleteAnalysis = (id) => {
        let newState = update(this.state, {
            deleteAnalysisDialog : {
                open : {
                    $set: true
                },
                analysisId : {
                    $set : id
                }
            }
        });

        this.setState(newState);
    };

    handleClickDeleteGroup = (groupId) => {
        let newState = update(this.state, {
            deleteGroupDialog :{
                open : {
                    $set: true
                },
                groupId: {
                    $set: groupId
                }
            }
        });

        this.setState(newState);
    };

    onToggleSwipeAnalysis = (analysisId, toggle) => {
        let newState = update(this.state,{
            swipedAnalysisId: {
                $set : toggle ? analysisId : ''
            }
        });

        this.setState(newState);
        this.$viewerAction.toggleSwipe({analysisId, toggle})
    };

    onClickConfirmDeleteAnalysisDialog = (confirm) => {

        if (confirm) {
            this.$viewerAction.deleteAnalysis(this.state.deleteAnalysisDialog.analysisId);
        } else {
            let newState = update(this.state, {
                deleteAnalysisDialog :{
                    open : {
                        $set: false
                    }
                }
            });

            this.setState(newState);
        }
    };

    onClickConfirmDeleteGroupDialog = (confirm) => {

        if (confirm) {
            this.$viewerAction.deleteGroup(this.state.deleteGroupDialog.groupId);
        } else {
            let newState = update(this.state, {
                deleteGroupDialog :{
                    open : {
                        $set: false
                    }
                }
            });

            this.setState(newState);
        }
    };

    rebuildTree = () =>{
        if (this.state.treeIsBuilt) {
            this.setState({
                treeIsBuilt: false
            }, () => {
                this.setState({
                    treeIsBuilt: true
                });
            });
        }
    };

    rebuildAGroupOfChildren = ({groupId, children}) => {
        this._treeReference.rebuildChildren(groupId, children);
    };

    render() {
        return (
            <section className="cmaps-ToolAnalyses">
                {this.state.treeIsBuilt &&
                (<ToolAnalysisGroupContainer
                    ref={(element) => {this._treeReference = element}}
                    items={this.state.analysisConfig}
                    onToggleSwipeAnalysis={this.onToggleSwipeAnalysis}
                    onRemoveItem={this.handleClickDeleteAnalysis}
                    onRemoveGroup={this.handleClickDeleteGroup}
                    onAddItemClick={this.onClickAddItemAtGroup} /> )}

                <CustomDialog title={locale.getLabel("L0002")}
                              open={this.state.analysisFormIsOpened}
                              onCloseClick={this.handleClickCancelAnalysis}
                              height={500}
                              width={700}
                              actions={[
                                  <FlatButton
                                      key="1"
                                      label={locale.getLabel('L0037')}
                                      primary={true}
                                      onTouchTap={this.handleClickCancelAnalysis}>
                                  </FlatButton>,
                                  <FlatButton
                                      key="2"
                                      label={locale.getLabel('L0036')}
                                      primary={true}
                                      keyboardFocused={true}
                                      onTouchTap={this.handleClickOnSaveAnalysis}>
                                  </FlatButton>
                              ]}>
                    <AnalysisForm groupId={this.state.groupId} analysis={this.state.analysisToEdit} />
                </CustomDialog>

                <Dialog title={locale.getLabel('L0058')}
                        modal={true}
                        open={this.state.deleteAnalysisDialog.open}
                        actions={[
                            <FlatButton
                                label={locale.getLabel('L0060')}
                                primary={true}
                                onTouchTap={(event) => this.onClickConfirmDeleteAnalysisDialog(false)}>
                            </FlatButton>,
                            <FlatButton
                                label={locale.getLabel('L0059')}
                                primary={true}
                                keyboardFocused={true}
                                onTouchTap={(event) => this.onClickConfirmDeleteAnalysisDialog(true)}>
                            </FlatButton>
                        ]}>
                    {locale.getMessage('APP_INFO_02')}
                </Dialog>

                <Dialog title={locale.getLabel('L0078')}
                        modal={true}
                        open={this.state.deleteGroupDialog.open}
                        actions={[
                            <FlatButton
                                label={locale.getLabel('L0060')}
                                primary={true}
                                onTouchTap={(event) => this.onClickConfirmDeleteGroupDialog(false)}>
                            </FlatButton>,
                            <FlatButton
                                label={locale.getLabel('L0059')}
                                primary={true}
                                keyboardFocused={true}
                                onTouchTap={(event) => this.onClickConfirmDeleteGroupDialog(true)}>
                            </FlatButton>
                        ]}>
                    {locale.getMessage('APP_INFO_04')}
                </Dialog>

            </section>
        )
    }
}

export default ToolAnalyses;