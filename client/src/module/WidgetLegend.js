import React from "react";
import ModuleBase from "../_base/abstract/ModuleBase";
import Rnd from "react-rnd";
import {ANALYSIS_TYPE_CONFIG} from "../constant/analysis";
import update from 'react-addons-update';
import {MAP_CONSTANT} from "../constant/map";
import {CircularProgress} from "material-ui";
import ListActionIcon from "material-ui/svg-icons/action/list";
import {VIEWER} from "../constant/viewer";
import theme from "../theme.config";

class WidgetLegend extends ModuleBase {

    _draggableNode = {};

    _widgetLocation = {};

    _legendReferences = {};

    state = {
        enabled : false,
        analysesIds : [],
        layersIds : [],
        mapIsBuilt : false
    };

    constructor (props, context) {
        super(props, context);

        this.setClassName('widget-legend-component');
    }

    componentWillMount () {
        this.own(
            this.$mapStore.listen(MAP_CONSTANT.ANALYSIS_LAYER_IS_BUILDING, this.handleBuildingLayer),
            this.$mapStore.listen(MAP_CONSTANT.STATIC_LAYER_IS_BUILDING, this.handleBuildingLayer),
            this.$mapStore.listen(MAP_CONSTANT.STATIC_LAYER_BUILD_ERROR, this.handleLayerIsBuilt),
            this.$mapStore.listen(MAP_CONSTANT.STATIC_LAYER_BUILD_SUCCESS, this.handleLayerIsBuilt),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_SUCCESS, this.handleLayerIsBuilt),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_ERROR, this.handleLayerIsBuilt),
            this.$viewerStore.listen(VIEWER.CHANGE_ANALYSIS_TYPE, this.handleChangeAnalysisType),
            this.$viewerStore.listen(VIEWER.DISABLE_STATIC_LAYER, this.handleUpdateWidget),
            this.$viewerStore.listen(VIEWER.ENABLE_STATIC_LAYER, this.handleUpdateWidget),
            this.$viewerStore.listen(VIEWER.ENABLE_ANALYSIS, this.handleUpdateWidget),
            this.$viewerStore.listen(VIEWER.DISABLE_ANALYSIS, this.handleUpdateWidget),
            this.$mapStore.listen(MAP_CONSTANT.BUILD_MAP, this.setWidgetLocation),
            this.$mapStore.listen(MAP_CONSTANT.CLUSTER_LEGEND_IS_BUILT, this.onChangeLegendConfig),
            this.$mapStore.listen(MAP_CONSTANT.MAP_ZOOM_END, this.handleUpdateWidget),
            this.$viewerStore.listen(VIEWER.DELETE_ANALYSIS, this.handleDeleteAnalysis)
        );
    }

    setWidgetLocation = () => {
        this._widgetLocation.x = this.context.$parent.mapAPI.getMapMeasures().width - 375;
        this.setState({
            mapIsBuilt : true
        });
    };

    handleBuildingLayer = () => {
        this.setState({
            building : true
        }, () => this.forceUpdate());
    };

    handleLayerIsBuilt = () => {
        this.setState({
            building : false
        }, () => this.handleUpdateWidget(() => {
            setTimeout(() => {
                this.forceUpdate();
            }, 0);
        }));
    };

    handleChangeAnalysisType = () => {
        this.handleUpdateWidget(() => {
            this.setState({
                building : false
            });
            setTimeout(() => {
                this.forceUpdate();
            }, 0);
        });
    };

    shouldComponentUpdate (nProps, nState) {
        let update = false;

        if (nState.analysesIds.length !== this.state.analysesIds.length || nState.layersIds.length !== this.state.layersIds.length) {
            return true;
        }

        if (nState.analysesIds.length === this.state.analysesIds.length) {
            nState.analysesIds.forEach((id) => {
                if (!this.state.analysesIds.includes(id)) {
                    update = true;
                }
            });
        }

        if (nState.layersIds.length === this.state.layersIds.length) {
            nState.layersIds.forEach((id) => {
                if (!this.state.layersIds.includes(id)) {
                    update = true;
                }
            });
        }

        if (nState.enabled !== this.state.enabled) {
            update = true;
        }

        return update;
    }

    onChangeLegendConfig = ({analysisId, type, legend}) => {
        if (this._legendReferences[analysisId] && this._legendReferences[analysisId][type] && this._legendReferences[analysisId][type].onChangeLegendConfig) {
            this._legendReferences[analysisId][type].onChangeLegendConfig({analysisId, type, legend});
        }
    };

    handleDeleteAnalysis = (analysis) => {
        let newAnalysisID = this.state.analysesIds.filter((id) => id !== analysis.id);
        let state = update(this.state, {
            analysesIds: {
                $set: newAnalysisID
            },
            enabled: {
                $set: false
            }
        });
        this.setState(state);

    };

    handleUpdateWidget = (cb) => {
        let enabledAnalysisLayers = Object.values(this.$viewerStore.analyses).filter(analysis => analysis.enable && !analysis._buildError && !analysis.outOfVisibleScale);
        let enabledStaticLayers = Object.values(this.$viewerStore.layers).filter(layer => layer.enabled);
        let analysesIds = enabledAnalysisLayers.map(analysis => analysis._id);
        let layersIds = enabledStaticLayers.map(layer => layer._id);
        let allEnabledIds = [].concat(analysesIds, layersIds);

        let state = update(this.state, {
            analysesIds : {
                $set : analysesIds
            },
            layersIds : {
                $set : layersIds
            },
            allEnabledIds : {
                $set : allEnabledIds
            }
        });

        if (allEnabledIds.length === 0) {
            state.enabled = false;
        }

        this.setState(state);

        if (cb && typeof cb === "function") {
            cb();
        }
    };

    buildLegends = () => {
        let nodes = [];
        let fnStyle = (index, total) => {
            let obj = {
                padding: '10px',
                border: '1px solid #ddd',
                background : '#fff'
            };

            if (index % 2 === 0) {
                obj.background = '#f5f5f5';
                obj.borderBottom = 0;
            }

            if (index % 2 === 0 && index !== 0) {
                obj.borderTop = 0;
            }

            if (index === (total-1)) {
                delete obj.borderBottom;
            }

            return obj;
        };

        this.state.analysesIds.forEach((id, index) => {
            let analysis = this.$viewerStore.analyses[id];
            let currentType = analysis.currentType;
            let Legend = ANALYSIS_TYPE_CONFIG.types[currentType].LegendComponent;
            let legendConfig;

            if (this.$viewerStore.legends[analysis._id]) {
                legendConfig = this.$viewerStore.legends[analysis._id][currentType];

                if (legendConfig) {
                    nodes.push(
                        <div key={id}
                             style={fnStyle(index, this.state.analysesIds.length)}
                        >
                            <span>{analysis.title}</span>
                            <Legend ref={element => {
                                if (!this._legendReferences[id]) {
                                    this._legendReferences[id] = {}
                                }

                                this._legendReferences[id][currentType] = element;
                            }}
                                    analysis={analysis}
                                    legend={legendConfig && legendConfig} />
                        </div>
                    );
                }
            }
        });

        this.state.layersIds.forEach((id, index) => {
            let legend = this.$viewerStore.legends[id];
            let layer = this.$viewerStore.layers[id];
            let style = fnStyle(index, this.state.layersIds.length);

            if (index === 0) {
                style.borderTop = 0;
            }

            nodes.push(
                <div key={id} style={style}>
                    <span>{layer.title}</span>
                    <div>
                        <img src={legend} alt={layer.title}/>
                    </div>
                </div>
            );
        });

        return nodes;
    };

    handleToggleWidget = (toggle) => {
        let active = toggle !== undefined ? toggle : !this.state.enabled;

        if (!this.state.enabled) {
            if (this.state.allEnabledIds.length === 0) {
                this.$appAction.alertInfo("APP_INFO_06");
                return;
            }
        }

        this.setState({
            enabled : active
        });
    };

    render () {
        let specificConfigs = {
            maxWidth : 400,
            minWidth : 320,
            minHeight : 250,
            bounds : "parent",
        };

        let defaultConfigs = {
            width: 320,
            height: 250,
            x: this._widgetLocation.x || 100,
            y: 128
        };

        let enabledWidgetButtonStyle = {
            backgroundColor : theme.palette.primary1Color,
            color: "white"
        };

        return (
            this.state.mapIsBuilt ? (
                <div className={this.className()}>
                    <div className={this.className("toggle-button")}
                         onClick={this.state.building ? () => {} : () => this.handleToggleWidget()}
                         style={this.state.enabled ? enabledWidgetButtonStyle : {}}>
                        {this.state.building ? (
                            <div className={this.className('loading')}>
                                <CircularProgress size={20}
                                                  color={this.state.enabled ? "white" : ""}/>
                            </div>
                        ) : (
                            <i style={{ marginTop : 2 }}>
                                <ListActionIcon />
                            </i>
                            // <i className="maps-icon maps-icon-info" />
                        )}
                    </div>
                    <div className={this.className('legend-container')}
                         style={{
                             display : !this.state.enabled ? 'none' : ''
                         }}>
                        <Rnd ref={element => this._draggableNode = element}
                             className={this.className('draggable-legend')}
                             default={defaultConfigs}
                             {...specificConfigs}>

                            <div className={this.className('box-content')}>
                                <div className={this.className('header')}>
                                    <span>Legendas</span>
                                </div>

                                {this.state.building ? (
                                    <div className={this.className('loading')}>
                                        <CircularProgress size={30}/>
                                    </div>
                                ) : (
                                    this.buildLegends()
                                )}
                            </div>
                        </Rnd>
                    </div>
                </div>
            ) : (
                <div />
            )
        );
    }
}

export default WidgetLegend;

