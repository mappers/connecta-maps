/* eslint-disable */


export const messages = {

    'DEV_ERROR_01'    : 'Método ${0} precisa ser implementado',
    'DEV_ERROR_02'    : 'Atributo \'events\' precisa ser registrado na classe',
    'DEV_ERROR_03'    : 'Atributo $parent não existe na implementação',

    'SERVER_ERROR_01' : 'Não foi possível requisitar os dados do visualizador',
    'SERVER_ERROR_02' : 'Não foi possível salvar o projeto',

    'APP_ERROR_01'    : 'Problema com a viewer requisitada, talvez falte atributos em sua estrutura',
    'APP_ERROR_02'    : 'Erro ao construir análise',
    'APP_ERROR_03'    : 'Nenhum dado encontrado',
    'APP_ERROR_04'    : 'Este campo é obrigatório',
    'APP_ERROR_05'    : 'Não foi possível salvar customizações',

    'MAP_ERROR_01'    : 'Erro ao tentar construir a camada temática',
    'MAP_ERROR_02'    : 'Erro ao tentar construir a camada agrupada',
    'MAP_ERROR_03'    : 'Erro ao tentar construir a camada de calor',
    'MAP_ERROR_04'    : 'Erro ao tentar construir a camada com gráfico',
    'MAP_ERROR_05'    : 'Erro ao tentar construir a camada simples',
    'MAP_ERROR_06'    : 'Não foi possível inicializar o mapa',

    'APP_SUCCESS_01'  : 'Análise adicionada com sucesso',
    'APP_SUCCESS_02'  : 'O valor emitido ao Store não é do tipo previsto nas configurações deste',
    'APP_SUCCESS_03'  : 'Os estados e alterações da aplicação foram salvas',
    'APP_SUCCESS_04'  : 'As alterações foram salvas com sucesso',
    'APP_SUCCESS_05'  : 'Fonte de dados espacial salvo com sucesso',
    'APP_SUCCESS_06'  : 'Fonte de dados espacial removido com sucesso',
    'APP_SUCCESS_07'  : 'Customizações configuradas com sucesso',

    'APP_INFO_01'     : 'Análise excluída com sucesso',
    'APP_INFO_02'     : 'Deseja excluir a análise\?',
    'APP_INFO_03'     : 'Tempo de atualização do mapa expirado, recarregando dados',
    'APP_INFO_04'     : 'Deseja excluir o grupo\?',
    'APP_INFO_05'     : 'Grupo excluído com sucesso',
    'APP_INFO_06'     : 'É necessário pelo menos uma análise ou camada visível para usar o Widget de legenda',
    'APP_INFO_07'     : 'Cursor fora do mapa',
    'APP_INFO_08'     : 'Mínimo de um campo selecionado',

    'APP_WARN_01'     : 'A camada selecionada não suporta nenhum tipo de renderização',
    'APP_WARN_02'     : 'Não é possível traçar a rota com apenas um ponto, selecione outra referência para a rota',
    'APP_WARN_03'     : 'O arquivo selecionado não é do tipo imagem',
    'APP_WARN_04'     : 'Não possui feições para esta consulta'

};