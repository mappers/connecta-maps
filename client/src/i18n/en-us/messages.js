/* eslint-disable */

export const messages = {

    'DEV_ERROR_01' : 'Method ${0} must to be implemented',
    'DEV_ERROR_02' : '\'events\' attribute must be registered on store class',
    'DEV_ERROR_03' : '$parent attribute does\'n exists on implementation',

    'SERVER_ERROR_01' : 'It wasn\'t possible to request data from the viewer',
    'SERVER_ERROR_02' : 'The project cannot be saved',

    'APP_ERROR_01' : 'Viewer has a problem, maybe have less properties in your structure',
    'APP_ERROR_02' : 'Error building analysis',
    'APP_ERROR_03' : 'No data found',
    'APP_ERROR_04' : 'That field is must!',
    'APP_ERROR_05' : 'Could not save customizations',

    'MAP_ERROR_01' : 'An error was occurred to try build the thematic layer',
    'MAP_ERROR_02' : 'An error was occurred to try build the grouped layer',
    'MAP_ERROR_03' : 'An error was occurred to try build the heat layer',
    'MAP_ERROR_04' : 'An error was occurred to try build the chart layer',
    'MAP_ERROR_05' : 'An error was occurred to try build the simple layer',
    'MAP_ERROR_06' : 'Can\'t do initialize the map instance',

    'APP_SUCCESS_01' : 'Analysis inserted successfully',
    'APP_SUCCESS_02' : 'The emitted value to the Store isn\'t of type provided by configuration',
    'APP_SUCCESS_03' : 'Application\'s modifications and states has saved',
    'APP_SUCCESS_04' : 'The changes of application has been saved successfully',
    'APP_SUCCESS_05' : 'Spatial data source has been saved successfully',
    'APP_SUCCESS_06' : 'Spatial data source has been removed successfully',
    'APP_SUCCESS_07' : 'Customizations successfully configured',

    'APP_INFO_01' : 'Analysis deleted successfully',
    'APP_INFO_02' : 'Do you want to delete the analysis\?',
    'APP_INFO_03' : 'Map refresh time has expired, reloading data',
    'APP_INFO_04' : 'Do you want to delete the group\?',
    'APP_INFO_05' : 'Group deleted successfully',
    'APP_INFO_06' : 'One or more layer or analysis active is required to use Widget legend',
    'APP_INFO_07' : 'Cursor out of map',
    'APP_INFO_08' : 'Minimum of a selected field',

    'APP_WARN_01' : 'The selected layer does not support any rendering',
    'APP_WARN_02' : 'It\'s not possible to trace the route with just one point, select another reference to the route',
    'APP_WARN_03' : 'The selected file isn\'t image',
    'APP_WARN_04' : 'No have features on this request'
};