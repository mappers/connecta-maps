import React from "react";
import PropTypes from "prop-types";
import update from "react-addons-update";
import ModuleBase from "../../../_base/abstract/ModuleBase";
import TOOLBAR_COMPONENT_CONSTANT from "../../../constant/toolbar-component-constant";
import {locale} from "../../../i18n/Locale";
import RaisedButton from "material-ui/RaisedButton";
import IconContentCreate from "material-ui/svg-icons/content/create";
import IconContentClear from "material-ui/svg-icons/content/clear";
import IconMapsLayers from 'material-ui/svg-icons/maps/layers';
import IconActionSwapHoriz from "material-ui/svg-icons/action/swap-horiz";
import CircularProgress from "material-ui/CircularProgress";
import ReactTooltip from 'react-tooltip';
import "./_style.scss";
import MaintainLayers from "./MaintainLayers";
import SpatialDataSourceForm from "./SpatialDataSourceForm";

export default class MaintainSpatialDataSource extends ModuleBase{

    static propTypes = {
        openSubPage : PropTypes.func
    };

    constructor(props, context) {
        super(props, context);
        this.setClassName('toolbar-maintain-spatial-data-source');

        this.state = {
            loading : true,
            spatialDataSources : []
        };
    }

    componentDidMount() {
        this.own(
            this.$toolbarComponentStore.listen(TOOLBAR_COMPONENT_CONSTANT.GET_SPATIAL_DATA_SOURCES, this.onGetSpatialDataSources)
        );
        this.$toolbarComponentAction.getSpatialDataSources();
    }

    $onSave() {

    }

    onGetSpatialDataSources = (spatialDataSources) => {
        this.setState({
            loading : false,
            spatialDataSources
        });
    };

    render() {
        return (
            <div className={this.className()}>

                {this.state.loading &&
                <div style={{ padding: 25 }}>
                    <CircularProgress/>
                </div>}

                {!this.state.loading &&
                <div style={{ width : '100%' }}>
                    <div className={this.className("actions")}>
                        <RaisedButton
                            label={locale.getLabel('L2006')}
                            onTouchTap={() => {
                                this.props.openSubPage(
                                    locale.getLabel('L2006'),
                                    SpatialDataSourceForm,
                                    {
                                        currentSpatialDataSource : null
                                    }
                                );
                            }}
                            primary={true} />
                    </div>

                    {this.state.spatialDataSources.length &&
                    <ReactTooltip/>
                    }

                    <div className={'cmaps-table1'}>
                        <div className={'cmaps-row'}>

                            <div className={'cmaps-cell'}> {locale.getLabel('L2004')} </div>

                            <div className={'cmaps-cell'}
                                 style={{width: 150}}> {locale.getLabel('L2005')} </div>

                            <div className={'cmaps-cell'}> DSN </div>

                            <div className={'cmaps-cell'}
                                 style={{width: 1}}> &nbsp; </div>

                        </div>
                        {this.state.spatialDataSources.map((spatialDataSource, index) => {
                            return (
                                <div key={index}
                                     className={'cmaps-row'}
                                >
                                    <div className={'cmaps-cell'}> {spatialDataSource.title} </div>
                                    <div className={'cmaps-cell'}> {spatialDataSource.serverType} </div>
                                    <div className={'cmaps-cell'}> {spatialDataSource.dsn} </div>
                                    <div className={'cmaps-cell cmaps-actions'}>
                                        <div
                                            onClick={() => {
                                                this.props.openSubPage(
                                                    locale.getLabel('L2017') + ': ' + spatialDataSource.title,
                                                    SpatialDataSourceForm,
                                                    {
                                                        currentSpatialDataSource : spatialDataSource
                                                    }
                                                );
                                            }}
                                            data-tip={locale.getLabel('L1005')}><IconContentCreate/></div>

                                        <div data-tip={locale.getLabel('L1006')}
                                             onClick={() => {
                                                 let handle = this.$toolbarComponentStore.listen(TOOLBAR_COMPONENT_CONSTANT.REMOVE_SPATIAL_DATA_SOURCE, () => {
                                                     handle.remove();
                                                     this.setState({
                                                         spatialDataSources : update(this.state.spatialDataSources, {
                                                             $splice : [[index, 1]]
                                                         })
                                                     });
                                                 });
                                                 this.$toolbarComponentAction.removeSpatialDataSource(spatialDataSource._id);
                                             }}
                                             className={this.className('red-icon')}><IconContentClear/></div>

                                        <div
                                            onClick={() => {
                                                this.props.openSubPage(
                                                    locale.getLabel('L2013') + ': ' + spatialDataSource.title,
                                                    MaintainLayers,
                                                    {
                                                        currentSpatialDataSource : spatialDataSource
                                                    }
                                                );
                                            }}
                                            data-tip={locale.getLabel('L2009')}><IconMapsLayers /></div>

                                        <div
                                            onClick={() => {
                                                this.$toolbarComponentAction.saveSpatialDataSource(spatialDataSource);
                                            }}
                                            data-tip={locale.getLabel('L2008')}><IconActionSwapHoriz/></div>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
                }
            </div>
        );
    }
}