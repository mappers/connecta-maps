import React from "react";
import PropTypes from "prop-types";
import update from "react-addons-update";
import ModuleBase from "../../../_base/abstract/ModuleBase";
import "./_SpatialDataSourceForm.scss";
import {CircularProgress, RaisedButton, TextField} from "material-ui";
import ISpatialDataSource from "../../../_base/interface/ISpatialDataSource";
import {locale} from "../../../i18n/Locale";
import TOOLBAR_COMPONENT_CONSTANT from "../../../constant/toolbar-component-constant";

export default class SpatialDataSourceForm extends ModuleBase {

    static propTypes = {
        currentSpatialDataSource : PropTypes.any
    };

    _serviceTypes = {
        OGC : {
            label : 'GeoServer',
            name : 'ogc'
        },
        ArcGIS : {
            label : 'ArcGIS Server',
            name : 'arcgis'
        },
        MapViewer : {
            label : 'Map Viewer',
            name : 'mapviewer'
        }
    };

    constructor(props, context) {
        super(props, context);
        this.setClassName('toolbar-spatial-data-source-FORM');
        this.state = {
            /** @type {ISpatialDataSource} */
            currentSpatialDataSource : props.currentSpatialDataSource || ISpatialDataSource.getDefault(),
            /** @type {ValidateError} */
            error : null,
            msgError : ''
        };
    }

    onChangeTexTField(attributeName, event, value) {
        let currentSpatialDataSource = update(this.state.currentSpatialDataSource, {
            [attributeName] : {
                $set : value
            }
        });
        this.setState({ currentSpatialDataSource });
    }

    onSave = () => {
        try {
            if (!this.state.currentSpatialDataSource.title) {
                throw new ValidateError('title', locale.getMessage('APP_ERROR_04'));
            }
            if (!this.state.currentSpatialDataSource.dsn) {
                throw new ValidateError('dsn', locale.getMessage('APP_ERROR_04'));
            }
            // ADAPTER[this.state.currentSpatialDataSource.serverType].validate(this.state.currentSpatialDataSource);

            let handle = this.$toolbarComponentStore.listen(TOOLBAR_COMPONENT_CONSTANT.SAVE_SPATIAL_DATA_SOURCE, ([error, spatialDatasource]) => {
                handle.remove();
                this.setState({
                    loading : false
                });
                if (!error) {
                    this.props.back();
                }
            });
            this.setState({
                loading : true
            });
            this.$toolbarComponentAction.saveSpatialDataSource(this.state.currentSpatialDataSource);
        } catch (error) {
            if (error instanceof ValidateError) {
                this.setState({ error });

            } else {
                this.setState({
                    msgError : error.message
                });
            }
        }
    };

    render() {
        let serverTypes = Object.keys(this._serviceTypes);
        let getError = (fieldName) => {
            return this.state.error &&
            this.state.error.fieldName === fieldName ? this.state.error.msgError : ''
        };
        return (
            <div className={this.className()}>
                <div className={this.className('server-types')}>
                    { serverTypes.map((serverType, index) => {
                        let isCurrentServerType = this.state.currentSpatialDataSource.serverType === serverType;
                        let label = this._serviceTypes[serverType].label;
                        return (
                            <div
                                key={index}
                                onClick={() => {
                                    this.setState({
                                        currentSpatialDataSource : update(this.state.currentSpatialDataSource, {
                                            serverType : {
                                                $set : serverType
                                            }
                                        })
                                    });
                                }}
                                className={this.className("server-type") + ' ' +  (
                                    isCurrentServerType ? this.className('current') : ''
                                )}>
                                {label}
                            </div>
                        );
                    }) }
                </div>
                {this.state.currentSpatialDataSource.serverType &&
                <div>
                    <div className={'cmaps-row'}>

                        <div className="cmaps-col-4">
                            <TextField
                                style={{ width : '100%' }}
                                floatingLabelText={locale.getLabel('L1027')}
                                onChange={this.onChangeTexTField.bind(this, 'title')}
                                value={this.state.currentSpatialDataSource.title}
                                errorText={getError('title')}
                            />
                        </div>

                        <div className="cmaps-col-8">
                            <TextField
                                style={{ width : '100%' }}
                                floatingLabelText={'DSN'}
                                errorText={getError('dsn')}
                                onChange={this.onChangeTexTField.bind(this, 'dsn')}
                                value={this.state.currentSpatialDataSource.dsn}
                            />
                        </div>

                    </div>
                    <div className={'cmaps-row'}>

                        <div className="cmaps-col-4">
                            <TextField
                                style={{ width : '100%' }}
                                errorText={getError('user')}
                                floatingLabelText={locale.getLabel('L2016')}
                                onChange={this.onChangeTexTField.bind(this, 'user')}
                                value={this.state.currentSpatialDataSource.user}
                            />
                        </div>

                        <div className="cmaps-col-4">
                            <TextField
                                style={{ width : '100%' }}
                                floatingLabelText={'Password'}
                                errorText={getError('password')}
                                onChange={this.onChangeTexTField.bind(this, 'password')}
                                type={'password'}
                                value={this.state.currentSpatialDataSource.password}
                            />
                        </div>
                    </div>

                    <div className="cmaps-row">
                        {this.state.loading ?
                            <CircularProgress size={30}/>
                            :
                            <RaisedButton
                                primary={true}
                                label={locale.getLabel('L0036')}
                                onTouchTap={this.onSave}
                            />
                        }
                    </div>

                </div>
                }
            </div>
        );
    }

}

class ValidateError {
    constructor(fieldName, msgError) {
        this.fieldName = fieldName;
        this.msgError = msgError;
    }
}