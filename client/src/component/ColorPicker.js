import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField/TextField';
import Dialog from 'material-ui/Dialog/Dialog';
import {SketchPicker} from 'react-color';

class ColorPicker extends React.Component {

    static propTypes = {
        value : PropTypes.string,
        onChange : PropTypes.func,
        label : PropTypes.string,
        style : PropTypes.any,
    };

    constructor(props) {
        super(props);

        this.state = {
            displayColorPicker : false
        };
    }

    handleClickColorPicker = () => {
        this.setState({
            displayColorPicker : !this.state.displayColorPicker
        });
    };

    render() {
        return(
            <section className="cmaps-color-picker-container" style={this.props.style}>
                <div className="cmaps-color-picker-input-container" >
                    <div className="cmaps-color-picker-circle"
                         style={{background : this.props.value}}
                         onClick={this.handleClickColorPicker}/>

                    <TextField className="cmaps-color-picker-input"
                               value={this.props.value}
                               style={{
                                   width : '100%'
                               }}
                               inputStyle={{
                                   width : '100%'
                               }}
                               onClick={this.handleClickColorPicker}
                               floatingLabelText={this.props.label}/>
                </div>

                <Dialog open={this.state.displayColorPicker}
                        modal={false}
                        onRequestClose={this.handleClickColorPicker}
                        bodyStyle={{padding : 0}}
                        contentStyle={{width : 220}}>

                    <SketchPicker color={this.props.value}
                                  onChangeComplete={this.props.onChange}/>
                </Dialog>
            </section>
        );
    }
}

export default ColorPicker;
