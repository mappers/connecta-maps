import React from 'react';
import Slider from 'material-ui/Slider';
import update from 'react-addons-update';
import ModuleBase from "../_base/abstract/ModuleBase";
import PropTypes from 'prop-types';
import {locale} from '../i18n/Locale';

class DoubleInputRange extends ModuleBase {

    static propTypes = {
        initial : PropTypes.number ,
        final : PropTypes.number,
        firstLabel : PropTypes.string,
        secondLabel : PropTypes.string,
        onChange : PropTypes.func,
        min : PropTypes.number,
        max :PropTypes.number
    };

    _limits = {};

    constructor (props, context) {
        super(props, context);

        this.setClassName("double-input-range-component");

        this._limits = {
            min : !!props.min ? props.min : 1,
            max : !!props.max ? props.max : 25,
        };

        this.state = {
            initial : !!this.props.initial ? this.props.initial : 1,
            final : !!this.props.final ? this.props.final : 10
        };
    }

    handleChangeFirstSlider = (event, value) => {

        /**
         * @type{{initial:number, final:number}}
         */
        let newState = update(this.state, {
            initial : {
                $set : value
            }
        });

        if (value >= this.state.final) {
            newState.final = value + 1;
        }

        this.setState(newState);
    };

    handleChangeSecondSlider = (event, value) => {

        /**
         * @type{{initial:number, final:number}}
         */
        let newState = update(this.state, {
            final : {
                $set : value
            }
        });

        if (value <= this.state.initial) {
            newState.initial = value - 1;
        }

        this.setState(newState);
    };

    emitValues = () => {
        let values = {
            initial : this.state.initial,
            final : this.state.final
        };

        this.props.onChange(values);
    };

    render () {
        return (
            <div className={this.className()}>
                <div className={this.className("first-slider-container")}>
                    <p className={this.className("first-slider-label")}>{this.props.firstLabel}</p>

                    <Slider onChange={this.handleChangeFirstSlider}
                            onDragStop={this.emitValues}
                            min={this._limits.min}
                            max={this._limits.max - 1}
                            value={this.state.initial}
                            step={1}
                    />

                    <div className={this.className("first-slider-tip tip")} >
                        {this.state.initial}
                    </div>
                </div>

                <span className={this.className("to")}>{locale.getLabel("L0099")}</span>

                <div className={this.className("second-slider-container")}>
                    <p className={this.className("second-slider-label")}>{this.props.secondLabel}</p>
                    <div className={this.className("second-slider-tip tip")} >
                        {this.state.final}
                    </div>

                    <Slider onChange={this.handleChangeSecondSlider}
                            onDragStop={this.emitValues}
                            min={this._limits.min + 1}
                            max={this._limits.max}
                            value={this.state.final}
                            step={1}
                    />
                </div>
            </div>
        );
    }
}

export default DoubleInputRange;