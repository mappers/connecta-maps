import React from 'react';
import {locale} from '../i18n/Locale';
import {ANALYSIS_TYPE} from '../constant/analysis';

class TypesRadioButton extends React.Component{

    handleClickOnRadioButton = (event) => {
        this.props.onChangeValue(event.target.value);
    };

    render() {
        return (
            <section className="cmaps-analysis-types-radio-button-container">
                {/*Analysis Type Static*/}
                <label className="analysis-type-icon">
                    <input name={this.props.name}
                           type="radio"
                           checked={this.props.currentType === ANALYSIS_TYPE.STATIC}
                           onChange={this.handleClickOnRadioButton}
                           value={ANALYSIS_TYPE.STATIC}
                           className="hidden-input"/>
                    <div className="item">
                        <svg version="1.2" xmlns="http://www.w3.org/2000/svg"  x="0px"
                             y="0px" viewBox="0 0 34.955 36">
                            <circle cx="9.651" cy="10.382" r="4.582"/>
                            <circle cx="14.233" cy="23.928" r="4.582"/>
                            <circle cx="23.396" cy="12.772" r="4.582"/>
                        </svg>
                    </div>
                    <div className="subtitle">{locale.getLabel('L0007')}</div>
                </label>

                {/*Analysis Type Thematic*/}
                {this.props.thematic && <label className="analysis-type-icon" >
                    <input name={this.props.name}
                           type="radio"
                           checked={this.props.currentType === ANALYSIS_TYPE.THEMATIC}
                           onChange={this.handleClickOnRadioButton}
                           value={ANALYSIS_TYPE.THEMATIC}
                           className="hidden-input"/>
                    <div className="item">
                        <svg version="1.2" xmlns="http://www.w3.org/2000/svg"  x="0px"
                             y="0px" viewBox="0 0 34.955 36">
                            <g id="Layer_2">
                                <path className="path1" d="M19.914 22.043c0 5.499-4.458 9.957-9.957 9.957s-9.957-4.458-9.957-9.957c0-5.499 4.458-9.957 9.957-9.957s9.957 4.458 9.957 9.957z"></path>
                                <path className="path2" d="M23.419 5.44c0 3.005-2.436 5.441-5.441 5.441s-5.441-2.436-5.441-5.441c0-3.005 2.436-5.441 5.441-5.441s5.441 2.436 5.441 5.441z"></path>
                                <path className="path3" d="M32 17.332c0 2.032-1.647 3.679-3.679 3.679s-3.679-1.647-3.679-3.679c0-2.032 1.647-3.679 3.679-3.679s3.679 1.647 3.679 3.679z"></path>
                            </g>
                        </svg>
                    </div>
                    <div className="subtitle">{locale.getLabel('L0008')}</div>
                </label>}

                {/*Analysis Type Chart*/}
                {this.props.chart && <label className="analysis-type-icon">
                    <input name={this.props.name}
                           type="radio"
                           checked={this.props.currentType === ANALYSIS_TYPE.CHART}
                           onChange={this.handleClickOnRadioButton}
                           value={ANALYSIS_TYPE.CHART}
                           className="hidden-input"/>
                    <div className="item">
                        <svg version="1.2" xmlns="http://www.w3.org/2000/svg"  x="0px"
                             y="0px" viewBox="0 0 34.955 36">
                            <path className="path1" d="M14.42 17.893l3.808-14.212c-1.214-0.332-2.487-0.525-3.807-0.525-7.965 0-14.421 6.457-14.421 14.422s6.456 14.422 14.421 14.422c7.964 0 14.42-6.457 14.42-14.422 0-1.187-0.159-2.333-0.429-3.436l-13.992 3.751z"></path>
                            <path className="path2" d="M18.072 14.196l13.992-3.749c-1.242-5.076-5.162-9.088-10.184-10.462l-3.808 14.211z"></path>
                        </svg>
                    </div>
                    <div className="subtitle">{locale.getLabel('L0009')}</div>
                </label>}

                {/*Analysis Type Cluster*/}
                {this.props.cluster && <label className="analysis-type-icon">
                    <input name={this.props.name}
                           onChange={this.handleClickOnRadioButton}
                           value={ANALYSIS_TYPE.CLUSTER}
                           type="radio"
                           checked={this.props.currentType === ANALYSIS_TYPE.CLUSTER}
                           className="hidden-input"/>
                    <div className="item">
                        <svg version="1.2" xmlns="http://www.w3.org/2000/svg"  x="0px"
                             y="0px" viewBox="0 0 34.955 36">
                            <path className="path1" d="M16-0.125c-8.905 0-16.125 7.219-16.125 16.125s7.22 16.125 16.125 16.125 16.125-7.22 16.125-16.125c0-8.906-7.22-16.125-16.125-16.125zM12.602 21.485h-2.75v-8.753l-2.701 0.789v-2.083l5.198-1.798h0.252v11.845zM24.438 21.485h-8.265v-1.79l3.808-4.002c0.938-1.068 1.406-1.917 1.406-2.546 0-0.51-0.111-0.897-0.332-1.164-0.223-0.266-0.547-0.398-0.969-0.398-0.418 0-0.756 0.178-1.018 0.533-0.26 0.355-0.391 0.799-0.391 1.33h-2.749c0-0.727 0.182-1.397 0.545-2.013s0.868-1.097 1.513-1.444c0.646-0.347 1.367-0.521 2.164-0.521 1.281 0 2.266 0.296 2.957 0.887s1.037 1.44 1.037 2.546c0 0.466-0.086 0.921-0.26 1.363s-0.443 0.906-0.809 1.391c-0.367 0.485-0.957 1.134-1.77 1.948l-1.529 1.766h4.66v2.114z"></path>
                        </svg>
                    </div>
                    <div className="subtitle">{locale.getLabel('L0010')}</div>
                </label>}

                {/*Analysis Type Heatmap*/}
                {this.props.heatmap && <label className="analysis-type-icon">
                    <input name={this.props.name}
                           type="radio"
                           checked={this.props.currentType === ANALYSIS_TYPE.HEATMAP}
                           onChange={this.handleClickOnRadioButton}
                           value={ANALYSIS_TYPE.HEATMAP}
                           className="hidden-input"/>
                    <div className="item">
                        <svg version="1.2" xmlns="http://www.w3.org/2000/svg"  x="0px"
                             y="0px" viewBox="0 0 34.955 36">
                            <path className="path1" d="M23.121 7.037c-0.487-0.783-1.451-1.125-2.323-0.824-0.872 0.3-1.421 1.163-1.323 2.081 0.184 1.724-0.111 3-0.969 4.035-1.828-2.091-2.458-5.027-1.555-7.73 0.158-0.478 0.368-0.942 0.641-1.422 0.386-0.677 0.301-1.569-0.158-2.198-0.373-0.509-0.966-0.81-1.597-0.811-0.001 0-0.001 0-0.001 0-0.63 0-1.223 0.301-1.595 0.809-0.985 1.342-9.619 13.308-9.619 20.060 0 5.953 5.030 10.795 11.214 10.795 6.257 0 11.544-4.943 11.544-10.795-0.001-4.424-1.633-9.788-4.259-14v0zM15.894 29.854c-3.269 0-5.918-2.115-5.918-4.723 0-2.801 4.077-7.924 5.463-9.584 0.135-0.161 0.432-0.077 0.428 0.118-0.017 0.758 0 2.062 0.23 3.304 0.228 1.229 0.665 2.4 1.489 2.924 1.312-0.76 1.891-1.73 1.939-2.922 0.008-0.201 0.323-0.273 0.438-0.1 1.37 2.105 2.063 4.477 2.063 6.26-0.001 2.607-2.863 4.723-6.132 4.723v0z"></path>
                        </svg>
                    </div>
                    <div className="subtitle">{locale.getLabel('L0011')}</div>
                </label>}
            </section>
        );
    }
}

export default TypesRadioButton;