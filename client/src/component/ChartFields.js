import React from 'react';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import Card from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import DeleteForever from 'material-ui/svg-icons/action/delete-forever';
import {locale} from '../i18n/Locale';

class ChartFields extends React.Component {

   generateRandomColor () {

        function randomNumber() {
            return Math.ceil(Math.random() * 255).toString();
        }

        return 'rgb(' + randomNumber() + "," + randomNumber() + "," + randomNumber() + ')';
    }


    handleClickAddButton = () => {

    };

    handleClickRemoveButton = () => {

    };

    render() {
        return (
            <section className="cmaps-chart-fields-container">
                {this.props.fields.map((field, index) => {
                    return(
                        <Card className="cmaps-chart-fields-card" key={index}>
                            <IconButton tooltip={locale.getLabel('L0031')} tooltipPosition="top-center"
                                        style={{position : 'absolute', right : 0, top : 0}}>
                                <DeleteForever />
                            </IconButton>
                            <SelectField floatingLabelText={locale.getLabel('L0025')}
                                         style={{width : '80%'}}/>
                            <TextField floatingLabelText={locale.getLabel('L0033')}
                                       style={{width : '80%'}}/>
                            <div className="cmaps-chart-fields-color-circle"
                                 style={{backgroundColor : this.generateRandomColor()}}></div>
                        </Card>
                    )
                })}
            </section>
        );
    }
}

export default ChartFields;