import React from "react";
import {APP_CONSTANT} from "../constant/index";
import ModuleBase from "../_base/abstract/ModuleBase";
import Converter from "../util/Converter";
import {scope} from "../scope";
import Snackbar from "material-ui/Snackbar";

export default class MessageComponent extends React.Component {

    handlers = [];

    /**
     * @type {AppStore}
     */
    $appStore;

    constructor(props) {
        super(props);

        this.$appStore = this.props.$parent.inject(scope.AppStore);


        this.state = {
            alert : {
                open : false,
                message : '',
                color : "#333"
            }
        };
    }

    componentDidMount() {
        ModuleBase.own(
            this.$appStore.listen(APP_CONSTANT.ALERT_ERROR, this.onAlertError),
            this.$appStore.listen(APP_CONSTANT.ALERT_WARN, this.onAlertWarn),
            this.$appStore.listen(APP_CONSTANT.ALERT_SUCCESS, this.onAlertSuccess),
            this.$appStore.listen(APP_CONSTANT.ALERT_INFO, this.onAlertInfo)
        );
    }

    componentWillUnmount() {
        for (let handler of this.handlers) {
            handler.remove();
        }
    }

    onAlertError = (message) => {
        this.setState({ alert : { open : true, message, color: "#E82C0C" } });
    };

    onAlertWarn = (message) => {
        this.setState({ alert : { open : true, message, color: "#e8900f" } });
    };

    onAlertSuccess = (message) => {
        this.setState({ alert : { open : true, message, color: "#0b9819" } });
    };

    onAlertInfo = (message) => {
        this.setState({ alert : { open: true, message, color: "#4c4cff" }});
    };

    render() {
        return (
            <div>
                <Snackbar
                    open={this.state.alert.open}
                    message={this.state.alert.message}
                    bodyStyle={{
                        background: this.state.alert.color
                    }}
                    onRequestClose={() => {
                        let alert = Converter.mixin({}, this.state.alert);
                        alert.open = false;
                        this.setState({
                            alert : alert
                        });
                    }}
                    autoHideDuration={3000}
                />
            </div>
        );
    }

}