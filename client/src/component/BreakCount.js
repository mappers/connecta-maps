import React from 'react';
import PropTypes from "prop-types";
import IconButton from 'material-ui/IconButton';
import SocialPlusOne from 'material-ui/svg-icons/social/plus-one';
import ImageExposureNeg1 from 'material-ui/svg-icons/image/exposure-neg-1';

class BreakCount extends React.Component {

    static propTypes = {
        onChange : PropTypes.func,
        label : PropTypes.string,
        count : PropTypes.number,
        min : PropTypes.number,
        max : PropTypes.number
    };

    constructor (props) {
        super(props);
    }

    handleClickAddButton = () => {

        this.props.onChange((Number(this.props.count) + 1) || 1);
    };

    handleClickRemoveButton = () => {

        this.props.onChange((Number(this.props.count) - 1));
    };

    render () {

        return (
            <section className="cmaps-break-count-field-container">
                <span className="cmaps-break-count-label">{this.props.label}</span>

                <div className="cmaps-break-count-buttons-container">
                    <IconButton onMouseDown={this.handleClickRemoveButton}
                                disabled={this.props.count === this.props.min}>
                        <ImageExposureNeg1 />
                    </IconButton>

                    <div className="cmaps-break-count-display">
                        <div className="cmaps-break-count-display-text">{this.props.count}</div>
                    </div>

                    <IconButton onMouseDown={this.handleClickAddButton}
                                disabled={this.props.count === this.props.max}>
                        <SocialPlusOne />
                    </IconButton>
                </div>
            </section>
        );
    }
}

export default BreakCount;