import React, {Component} from 'react';
import PropTypes from "prop-types";

class EditableInput extends Component{

    static propTypes = {
        isEditing: PropTypes.bool,
        onFinishEdit: PropTypes.func
    };

    state = {
        isEditing: false,
        value: ''
    };

    componentDidMount(){
        this.setState({
            isEditing: !!this.props.isEditing,
            value: this.props.value
        });
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            isEditing: nextProps.isEditing
        });
    }

    handleFinishEdit = () => {
        if(this.props.onFinishEdit){
            this.props.onFinishEdit(this.state.value);
        }
    };

    render(){
        return (
            <div style={{display : 'flex', alignItems : 'center', transform: 'translateX(10px)'}}>
                {!this.state.isEditing && this.props.children}
                {this.state.isEditing && (
                    <div style={{display: 'flex'}}>
                        <input value={this.state.value}
                               onChange={(event) => { this.setState({value: event.target.value}) }}/>
                        <button onClick={this.handleFinishEdit}>OK</button>
                    </div>
                )}
            </div>
        )
    }
}

export default EditableInput;

