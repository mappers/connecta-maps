import {APP_CONSTANT} from "../constant/index";
import ActionBase from "../_base/abstract/ActionBase";

export default class AppAction extends ActionBase{

    toggleTools(open) {
        this.dispatch({
            type : APP_CONSTANT.TOGGLE_TOOLS,
            data : open
        });
    }

    disablePopup() {
        this.dispatch({
            type : APP_CONSTANT.DISABLE_POPUP,
            data : true
        });
    }

    /**
     *
     * @param {String} codeMessage
     * @param {Object|Array} params
     * @param {Error} [error]
     */
    alertError(codeMessage, params = [], error) {
        if (error) {
            console.error('MAPS APPLICATION', error);
        }
        this.dispatch({
            type : APP_CONSTANT.ALERT_ERROR,
            data : {
                codeMessage : codeMessage,
                params : params,
                error
            }
        });
    }

    /**
     *
     * @param {String} codeMessage
     * @param {Object|Array} params
     */
    alertWarn(codeMessage, params = []) {
        this.dispatch({
            type : APP_CONSTANT.ALERT_WARN,
            data : {
                codeMessage : codeMessage,
                params : params
            }
        });
    }

    /**
     *
     * @param {String} codeMessage
     * @param {Object|Array} params
     */
    alertSuccess(codeMessage, params = []) {
        this.dispatch({
            type : APP_CONSTANT.ALERT_SUCCESS,
            data : {
                codeMessage : codeMessage,
                params : params
            }
        });
    }

    /**
     *
     * @param {String} codeMessage
     * @param {Object|Array} params
     */
    alertInfo(codeMessage, params = []) {
        this.dispatch({
            type : APP_CONSTANT.ALERT_INFO,
            data : {
                codeMessage : codeMessage,
                params : params
            }
        });
    }

    setExternalFilter (data) {
        this.dispatch({
            type : APP_CONSTANT.SET_EXTERNAL_FILTER,
            data : data
        });
    }


}