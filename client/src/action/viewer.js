import {VIEWER} from "../constant/viewer";
import ActionBase from "../_base/abstract/ActionBase";

export default class ViewerAction extends ActionBase {

    getViewer(viewerId, user) {
        this.dispatch({
            type : VIEWER.LOAD,
            data : {viewerId, user}
        });
    }

    updateViewer(viewerId){
        this.dispatch({
            type : VIEWER.UPDATE,
            data : viewerId
        });
    }

    changeAnalysisLayerOpacity(id, opacity) {
    	this.dispatch({
    		type : VIEWER.CHANGE_ANALYSIS_LAYER_OPACITY,
			data : {id, opacity}
		});
	}

    changeStaticLayerOpacity(id, opacity) {
        this.dispatch({
            type : VIEWER.CHANGE_STATIC_LAYER_OPACITY,
            data : {id, opacity}
        });
    }

	changeAnalysisType(id, analysisType) {
    	this.dispatch({
    		type : VIEWER.CHANGE_ANALYSIS_TYPE,
			data : {id, analysisType}
		});
	}

	enableAnalysis(id) {
		this.dispatch({
			type : VIEWER.ENABLE_ANALYSIS,
			data : {id}
		});
	}

	disableAnalysis(id) {
		this.dispatch({
			type : VIEWER.DISABLE_ANALYSIS,
			data : {id}
		});
	}

	duplicateAnalysis(index){
        this.dispatch({
            type : VIEWER.DUPLICATE_ANALYSIS,
            data : index
        });
    }

	clickOnSaveNewAnalysis() {
        this.dispatch({
            type : VIEWER.CLICK_ON_SAVE_NEW_ANALYSIS
        });
    }

    clickOnSaveNewLayer() {
        this.dispatch({
            type : VIEWER.CLICK_ON_SAVE_NEW_LAYER
        });
    }

    saveNewLayer(layer, groupId){
        this.dispatch({
            type : VIEWER.SAVE_NEW_LAYER,
            data : {layer, groupId}
        });
    }

    saveAnalysis(analysis, groupId) {
        this.dispatch({
            type : VIEWER.SAVE_ANALYSIS,
            data : {analysis, groupId}
        });
    }

    editAnalysis(analysis, groupId) {
        this.dispatch({
            type: VIEWER.EDIT_ANALYSIS,
            data: {analysis, groupId}
        });
    }

    deleteAnalysis(id) {
        this.dispatch({
           type: VIEWER.DELETE_ANALYSIS,
           data: id
        });
    }

    saveViewer(user) {
        this.dispatch({
            type : VIEWER.SAVE_VIEWER,
            data: user
        });
    }

    saveProject(project) {
        this.dispatch({
            type : VIEWER.SAVE_PROJECT,
            data: project
        });
    }

    enableStaticLayer(id) {
        this.dispatch({
            type : VIEWER.ENABLE_STATIC_LAYER,
            data: id
        });
    }

    disableStaticLayer = (id) => {
        this.dispatch({
            type : VIEWER.DISABLE_STATIC_LAYER,
            data: id
        });
    };

    deleteStaticLayer = (id) => {
        this.dispatch({
            type : VIEWER.DELETE_STATIC_LAYER,
            data: id
        });
    };

    toggleTimeLine = (index, checked, disableAnalysis) => {
        this.dispatch({
            type : VIEWER.ENABLE_TIME_LINE,
            data: {index, checked, disableAnalysis}
        });
    };

    changeTimelineValue = (data) => {
        this.dispatch({
            type : VIEWER.CHANGE_TIMELINE_VALUE,
            data : data
        });
    };

    changeSwipeValue(data){
        this.dispatch({
            type : VIEWER.CHANGE_SWIPE_VALUE,
            data : {value: data}
        });
    }

    toggleSwipe(data){
        this.dispatch({
            type: VIEWER.TOGGLE_SWIPE,
            data: data
        });
    }

    addAnalysisBox(analysisBox){
        this.dispatch({
            type: VIEWER.ADD_ANALYSIS_BOX,
            data: analysisBox
        });
    }

    addLayerBox(layerBox){
        this.dispatch({
            type: VIEWER.ADD_LAYER_BOX,
            data: layerBox
        });
    }

    removeLayerBox(layerBox){
        this.dispatch({
            type: VIEWER.REMOVE_LAYER_BOX,
            data: layerBox
        });
    }

    removeAnalysisBox(analysisBox){
        this.dispatch({
            type: VIEWER.REMOVE_ANALYSIS_BOX,
            data: analysisBox
        });
    }

    showAnalysisForm(groupId){
        this.dispatch({
            type: VIEWER.SHOW_ANALYSIS_FORM,
            data: groupId
        });
    }

    updateGroupChildren(groupId, children, isLayers){
        this.dispatch({
            type: VIEWER.UPDATE_GROUP_CHILDREN,
            data: {groupId, children, isLayers}
        });
    }

    updateGroupRoot(group, isLayers = false){
        this.dispatch({
            type: VIEWER.UPDATE_GROUP_ROOT,
            data: {group, isLayers}
        });
    }

    updateGroupLabel(groupId, label, isLayers){
        this.dispatch({
            type: VIEWER.UPDATE_GROUP_LABEL,
            data: {groupId, label, isLayers}
        });
    }

    deleteGroup(groupId, isLayers){
        this.dispatch({
            type: VIEWER.DELETE_GROUP,
            data: {groupId, isLayers}
        });
    }

    disableSwipe() {
        this.dispatch({
            type: VIEWER.DISABLE_SWIPE
        });
    }

    handleCheckGroup (groupId, checked, isLayers) {
        this.dispatch({
            type: VIEWER.CHECK_GROUP,
            data: {groupId, checked, isLayers}
        });
    }

    activateTrackingOnMap(data){
        this.dispatch({
            type : VIEWER.ACTIVATE_TRACKING,
            data
        });
    }

    deactivateTrackingOnMap () {
        this.dispatch({
            type : VIEWER.DEACTIVATE_TRACKING,
        });
    }

    changeLengthUnit (measureConfig) {
        this.dispatch({
            type : VIEWER.CHANGE_LENGTH_UNIT,
            data: measureConfig
        });
    }
}