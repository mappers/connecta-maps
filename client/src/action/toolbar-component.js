import ActionBase from "../_base/abstract/ActionBase";
import TOOLBAR_COMPONENT_CONSTANT from "../constant/toolbar-component-constant";

export default class ToolbarComponentAction extends ActionBase {

    getSpatialDataSources() {
        this.dispatch({
            type : TOOLBAR_COMPONENT_CONSTANT.GET_SPATIAL_DATA_SOURCES
        });
    }

    removeSpatialDataSource(spatialDataSourceId) {
        this.dispatch({
            type : TOOLBAR_COMPONENT_CONSTANT.REMOVE_SPATIAL_DATA_SOURCE,
            data : spatialDataSourceId
        });
    }

    getResultSetsMetadata() {
        this.dispatch({
            type : TOOLBAR_COMPONENT_CONSTANT.GET_RESULT_SETS_METADATA
        });
    }

    getLayers(spatialDataSourceId) {
        this.dispatch({
            type : TOOLBAR_COMPONENT_CONSTANT.GET_LAYERS,
            data : spatialDataSourceId
        });
    }

    saveLayer(layer) {
        this.dispatch({
            type : TOOLBAR_COMPONENT_CONSTANT.SAVE_LAYER,
            data : layer
        });
    }

    saveSpatialDataSource(spatialDataSource) {
        this.dispatch({
            type : TOOLBAR_COMPONENT_CONSTANT.SAVE_SPATIAL_DATA_SOURCE,
            data : spatialDataSource
        });
    }

}