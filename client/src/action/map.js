import {MAP_CONSTANT} from "../constant/map";
import ActionBase from "../_base/abstract/ActionBase";

export default class MapAction extends ActionBase {

    /**
     *
     * @param {HTMLElement} node
     * @param {Object} config
     */
    buildMap(node, config) {
        this.dispatch({
            type : MAP_CONSTANT.BUILD_MAP,
            data : { node, config }
        });
    }

    buildStaticLayer(id) {
        this.dispatch({
            type : MAP_CONSTANT.BUILD_STATIC_LAYER,
            data : id
        });
    }

    rebuildStaticLayer(index) {
        this.dispatch({
            type: MAP_CONSTANT.REBUILD_STATIC_LAYER,
            data: index
        });
    }

    centerAtLayer(id) {
        this.dispatch({
            type : MAP_CONSTANT.CENTER_LAYER,
            data : {id}
        });
    }

    reorderAnalysesLayers() {
        this.dispatch({
            type : MAP_CONSTANT.REORDER_ANALYSES_LAYERS
        });
    }

    reorderStaticLayers() {
        this.dispatch({
            type : MAP_CONSTANT.REORDER_STATIC_LAYERS
        });
    }

    rebuildLayers(analysisId) {
        this.dispatch({
            type: MAP_CONSTANT.REBUILD_LAYERS,
            data: {id: analysisId}
        });
    }

    centerAtFeature(layer){
        this.dispatch({
            type : MAP_CONSTANT.CENTER_AT_FEATURE,
            data : layer
        });
    }

    layerClick(leafletLayer, analysis){
        this.dispatch({
            type : MAP_CONSTANT.LAYER_CLICK,
            data : {feature: leafletLayer.feature, leafletLayer: leafletLayer, analysis}
        });
    }

    swipeLayer() {
        this.dispatch({
            type : MAP_CONSTANT.SWIPE_LAYER
        });
    }

    removeSwipeLayer(analysisId) {
        this.dispatch({
            type : MAP_CONSTANT.REMOVE_SWIPE_LAYER,
            data : analysisId
        });
    }

    registerOnMoveSwipe(){
        this.dispatch({
            type : MAP_CONSTANT.REGISTER_MOVE_SWIPE,
        });
    }

    destroy() {
        this.dispatch({
            type : MAP_CONSTANT.DESTROY
        });
    }

    deselectFeature(data) {
        this.dispatch({
            type : MAP_CONSTANT.DESELECT_FEATURE,
            data
        });
    }

    clusterLegendIsBuilt(data) {
        this.dispatch({
            type : MAP_CONSTANT.CLUSTER_LEGEND_IS_BUILT,
            data
        });
    }

    mapZoomEnd (data) {
        this.dispatch({
            type : MAP_CONSTANT.MAP_ZOOM_END,
            data
        });
    }

}