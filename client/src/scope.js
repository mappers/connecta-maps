import AppService from "./service/AppService";
import GeoService from "./service/GeoService";

import AppStore from "./store/index";
import MapStore from "./store/map";
import ViewerStore from "./store/viewer";
import BasemapStore from "./store/basemap";

import AppAction from "./action/index";
import ViewerAction from "./action/viewer";
import MapAction from "./action/map";
import BasemapAction from "./action/basemap";
import RequestHelper from "./helper/RequestHelper";
import ToolAnalysisAction from "./action/tool-analysis";
import ToolAnalysisStore from "./store/tool-analysis";
import ToolbarComponentAction from "./action/toolbar-component";
import ToolbarComponentStore from "./store/toolbar-component-store";

export const scope = {

    //SERVICES
    AppService : {
        namespace : 'service.AppService',
        Class : AppService
    },

    GeoService : {
        namespace : 'service.GeoService',
        Class : GeoService
    },




    //STORES

    AppStore : {
        namespace : 'store.AppStore',
        Class : AppStore
    },

    MapStore : {
        namespace : 'store.MapStore',
        Class : MapStore
    },

    ViewerStore : {
        namespace : 'store.ViewerStore',
        Class : ViewerStore
    },

    BasemapStore : {
        namespace : 'store.BasemapStore',
        Class : BasemapStore
    },

    ToolAnalysisStore : {
        namespace : 'store.ToolAnalysisStore',
        Class : ToolAnalysisStore
    },

    ToolbarComponentStore : {
        namespace : 'action.ToolbarComponentStore',
        Class : ToolbarComponentStore
    },


    //ACTIONS

    AppAction : {
        namespace : 'action.AppAction',
        Class : AppAction
    },

    MapAction : {
        namespace : 'action.MapAction',
        Class : MapAction
    },

    ViewerAction : {
        namespace : 'action.ViewerAction',
        Class : ViewerAction
    },

    BasemapAction : {
        namespace : 'action.BasemapAction',
        Class : BasemapAction
    },

    ToolAnalysisAction : {
        namespace : 'action.ToolAnalysisAction',
        Class : ToolAnalysisAction
    },

    ToolbarComponentAction : {
        namespace : 'action.ToolbarComponentAction',
        Class : ToolbarComponentAction
    },

    //HELPER

    RequestHelper : {
        namespace : 'helper.RequestHelper',
        Class : RequestHelper
    }

};