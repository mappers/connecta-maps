
class ILayer {

    /**
     * @type {String[]}
     */
    capabilities;

    /** @type {String} */
    serviceType;

    /**
     * @type {String}
     */
    domainId;

    geoCache = {
        queryCache : Boolean,
        getBreaksCache : Boolean
    };

    /**
     * @type {String}
     */
    geometryField;

    /**
     * @type {String}
     */
    geometryType;

    /**
     * @type {Boolean}
     */
    isVector;

    /**
     * @type {{
     * alias : String,
     * name : String,
     * type : String
     * }}[]
     */
    layerFields;

    /**
     * @type {String}
     */
    layerIdentifier;

    /**
     * @type {String}
     */
    spatialDataSourceId;

    /**
     * @type {String}
     */
    title;

    /**
     * @type {String}
     */
    _id;

}