
import Converter from "../../util/Converter";

const getDefault = (layer) => {
    return {
        "_id": layer._id,
        "title" : layer.title,
        "layer" : layer,
        "crossingKeys" : {},
        "domainId" : layer.domainId,
        // "dataSourceIdentifier" : layer._id.toString(),
        "resultSetId" : Converter.createHash(layer.title, layer._id, Date.now()).slice(0, 10)
    }
};

export default class IRichLayer {

    /**
     *
     * @type {Function}
     * @return {IRichLayer}
     */
    static getDefault = getDefault;

    /**
     * @type {String}
     */
    _id;

    /**
     * @type {ILayer}
     */
    layer;

    /**
     * @type {String}
     */
    dataSourceIdentifier;

    /**
     * @type {String}
     */
    title;

    /**
     * @type {String}
     */
    resultSetId;

    /**
     * @type {Object}
     */
    crossingKeys = {
        /**
         * @type {String}
         */
        geoKey:String,

        /**
         * @type {String}
         */
        resultSetKey:String
    };

}