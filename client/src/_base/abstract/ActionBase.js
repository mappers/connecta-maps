import ScopeSingletonBase from "./ScopeSingletonBase";
import {scopeSingletonFactory} from '../../helper/scopeSingletonFactory';

export default class ActionBase extends ScopeSingletonBase {

    $prepare() {
        scopeSingletonFactory(this);
    }

    dispatch(payload) {
        setTimeout(() => {
            this.$parent.dispatcher.dispatch(payload);
        }, 10);
    }

}