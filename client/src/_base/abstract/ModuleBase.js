import React from "react";
import PropTypes from "prop-types";
import {scope} from "../../scope";
import {scopeSingletonFactory} from "../../helper/scopeSingletonFactory";

export default class ModuleBase extends React.Component {

    static contextTypes = {
        $parent: PropTypes.any
    };

    static propTypes = {
        $parent : PropTypes.any
    };

    static appClass = 'cmaps';

    appClass = ModuleBase.appClass;

    /**
     * @type { { $parent : ConnectaMaps } }
     */
    context;

    /**
     * @type {AppStore}
     */
    $appStore;

    /**
     * @type {AppAction}
     */
    $appAction;

    /**
     * @type {MapStore}
     */
    $mapStore;

    /**
     * @type {MapAction}
     */
    $mapAction;

    /**
     * @type {ViewerStore}
     */
    $viewerStore;

    /**
     * @type {ViewerAction}
     */
    $viewerAction;

    /**
     * @type {BasemapAction}
     */
    $basemapAction;

    /**
     * @type {BasemapStore}
     */
    $basemapStore;

    /**
     * @type {ToolAnalysisStore}
     */
    $toolAnalysisStore;

    /**
     * @type {ToolAnalysisAction}
     */
    $toolAnalysisAction;

    /** @type {ToolbarComponentStore} */
    $toolbarComponentStore;

    /** @type {ToolbarComponentAction} */
    $toolbarComponentAction;

    $$handlers = [];

    constructor(props, context) {
        super(props, context);
        scopeSingletonFactory(this);
    }

    own(...args) {
        this.$$handlers = ModuleBase.own.apply(null, Array.prototype.slice.call(args));
    }

    static own(...args) {
        let handlers = [];
        for (let handler of args) {
            if (handler && handler.remove) {
                handlers.push(handler);
            }
        }
        return handlers;
    }

    componentWillUnmount() {
        for (let handler of this.$$handlers) {
            handler.remove();
        }
    }

    className(name = '') {
        return this.appClass + (name ? '-' +  name : '');
    }

    setClassName(name) {
        this.appClass = this.appClass + '-' + name;
    }

}