import {EventEmitter} from "events";
import {locale} from "../../i18n/Locale";
import Converter from "../../util/Converter";
import {scopeSingletonFactory} from "../../helper/scopeSingletonFactory";
import AppError from "../../native/AppErrorI18n";

export default class StoreBase extends EventEmitter {

    _data = {};

    /**
     * @type {ConnectaMaps}
     */
    $parent;

    constructor(parent) {
        super();
        this.$parent = parent;
    }

    getControllerClass() {
        throw new AppError('DEV_ERROR_01', ['getControllerClass']);
    }

    $prepare() {
        if (!this.events) {
            throw new Error(locale.getMessage('DEV_ERROR_02'));
        }

        scopeSingletonFactory(this);

        this._id = String(Math.floor(Math.random() * 10000));

        let emitter = this.buildEmitter();

        let Class = this.getControllerClass();
        this.controller = new Class(this.$parent, this, emitter);
        this.controller.$prepare();
        this.controller.prepare();
    }

    listen(eventName, cb, immediate = false) {
        if (!this.events[eventName]) {
            return;
        }

        let id = eventName + this._id;

        if (immediate) {
            cb(this._getData(eventName));
        }

        this.on(id, cb);
        return {
            remove : () => {
                this.removeListener(id, cb);
            }
        };
    }

    get(eventName) {
        return this._getData(eventName);
    }

    _setData(eventName, value) {
        let path = this.events[eventName].path;
        if (path && value !== undefined) {
            this._data[this.getIdentifierData(eventName)] = value;
        }
    }

    _getData(eventName) {
        let path = this.events[eventName].path;
        if (this.events[eventName].path) {
            let identifier = this.getIdentifierData(eventName);
            let data = this._data[identifier];
            return Converter.resolvePath({ [identifier] : data }, path);
        }
    }

    getIdentifierData(eventName) {
        let pattern = /([\w\d\-]+)\.?/;
        let path = this.events[eventName].path;
        if (path) {
            return path.match(pattern)[1];
        }
    }

    buildEmitter() {
        return (eventName, value) => {
            if (!this.events[eventName]) {
                return;
            }
            let type = this.events[eventName].type;
            let old;
            let path = this.events[eventName].path;

            if (path) {
                old = this._getData(eventName);
                this._setData(eventName, value);

                if (type) {
                    if (typeof type === "string") {
                        if (typeof value !== type) {
                            console.warn(locale.getMessage('APP_SUCCESS_02'));
                        }
                    } else if (!(value instanceof type)) {
                        console.warn(locale.getMessage('APP_SUCCESS_02'));
                    }
                }
            }

            if (value !== old) {
                if (path) {
                    for (let event in this.events) {
                        let eventPath = this.events[event].path;
                        if (eventPath && eventPath.startsWith(path)) {
                            let data = this._getData(event);

                            if (eventName === event) {
                                this.emit(event + this._id, data);
                            }
                        }
                    }
                } else {
                    this.emit(eventName + this._id, value);
                }

            } else if (value === undefined && old === undefined) {
                this.emit(eventName + this._id);
            }
        }
    }

    registryEvent(eventName, cb) {
        let scope = this;
        this.on(eventName, cb);
        return {
            remove : () => {
                scope.removeListener(eventName, cb);
            }
        };
    }
}