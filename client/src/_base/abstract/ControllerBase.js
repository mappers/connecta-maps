import {locale} from "../../i18n/Locale";
import {scopeSingletonFactory} from "../../helper/scopeSingletonFactory";
import ScopeSingletonBase from "./ScopeSingletonBase";
import AppError from "../../native/AppErrorI18n";

export default class ControllerBase extends ScopeSingletonBase {

    constructor(parent, store, emitter) {
        super(parent);
        this.$emitter = emitter;
        this.store = store;
    }

    $prepare() {
        scopeSingletonFactory(this);
    }

    prepare() {
        throw new AppError('DEV_ERROR_01', ['prepare']);
    }

}