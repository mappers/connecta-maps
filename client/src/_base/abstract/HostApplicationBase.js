
export default class HostApplicationBase {

    constructor($parent) {
        this.$parent = $parent;
    }

    /**
     * @returns {Promise<{id:number, name:string, outFields : {}}[]>}
     */
    $getResultSetsMetadata() {
        //Todos os métodos com o prefixo:$ devem ser considerados como um método contratual
        throw new Error('$getResultSetsMetadata method must be implemented!');
    }

}

class IResultSetMetaData {

    /** @type {Number} */
    id;
    /** @type {String} */
    name;
    /** @type {{name:string, alias:string, valueType:string}[]} */
    outFields;
}