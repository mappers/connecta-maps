import {scope} from "../scope";
import {locale} from "../i18n/Locale";
import AppError from "../native/AppErrorI18n";

/**
 * Função utilitária que injeta todos os scopeSingletons na instancia parametrizada.
 * Esta função cria uma propriedade prefixada com um '$' seguido do nome do escopo(com a primeira letra em minúsculo);
 * @example {lang:javascript}
 *
 * var instance = new MyClass();
 * instance.$geoService.query() // Uncaught TypeError: Cannot read property 'query' of undefined
 *
 * scopeSingletonFactory(instance);
 * instance.$geoService.query() // true
 *
 *
 * @param {*} instance
 */
export const scopeSingletonFactory = (instance) => {
    let parent = instance.$parent ?
        instance.$parent
        : instance.context ? instance.context.$parent : null;

    if (!parent) {
        throw new AppError('DEV_ERROR_03');
    }


    for (let key in scope) {
        let singletonConstant = scope[key];

        if (parent.inject) {
            instance['$' + firstToLowerCase(key)] = parent.inject(singletonConstant);
        }
    }
};

function firstToLowerCase(str) {
    return str.trim().replace(/^(.)/, (a, first) => {
        return first.toLowerCase();
    });
}