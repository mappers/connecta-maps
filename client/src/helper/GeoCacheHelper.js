import {MapsDataLoader} from '../MapsDataLoader';
import Converter from "../util/Converter";

export default class GeoCacheHelper {

    static mappingCacheByAnalysisId = {};

    _hash;

    _analysisId;

    /**
     *
     * @param {String} analysisId
     * @param {String} url
     * @param {Object} queryParams
     */
    constructor(analysisId, url, queryParams) {
        this._analysisId = analysisId;
        this._hash = Converter.createHash(url, queryParams);
    }

    /**
     *
     * @param {GeoJSON} geoJSON
     */
    setData(geoJSON) {
        MapsDataLoader.set(this._hash, geoJSON);
        GeoCacheHelper.mappingCacheByAnalysisId[this._analysisId] = this._hash;
    }

    /**
     *
     * @returns {GeoJSON}
     */
    getDataFromCache() {
        return MapsDataLoader.get(this._hash);
    }

    /**
     *
     * @param {String} analysisId
     */
    static removeCacheByAnalysis(analysisId) {
        let hash = GeoCacheHelper.mappingCacheByAnalysisId[analysisId];
        if (hash) {
            delete GeoCacheHelper.mappingCacheByAnalysisId[analysisId];
            MapsDataLoader.remove(hash);
        }
    }

}