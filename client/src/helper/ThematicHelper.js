import chroma from "chroma-js";

export default class ThematicHelper {

    static isThematicBreakInside = (breakConfig, value) => {

        if (typeof breakConfig.value !== 'object') {
            return value === breakConfig.value;
        }

        let start = breakConfig.value.start;
        let ends = breakConfig.value.ends;
        let contained = false;

        ends = ends !== void 0 ? ends : start;

        if ((value === ends && value === start) ||
            (value >= start && value < ends)) {
            contained = true;
        }

        return contained;
    };

    static getConfigThematicRenderer(typeRenderer, queryResponse, breaksResponse, thematicConfig) {
        let response = {};
        let customBreaks = thematicConfig.customBreaks;

        let buildBreaks = (type, config) => {
            if (config) {

                let symbologies = [];
                let uniqueValuesStr = [];

                response[type] = {};

                if (type === 'fill') {
                    symbologies = [].concat(
                        chroma
                            .scale(config.colorRamp)
                            .colors(breaksResponse.length)
                    );

                } else if (type === 'size') {
                    symbologies = ((meter) => {
                        let j = 0,
                            arr = [];

                        while (j <= config.breakCount) {
                            arr.push(config.minSize + (meter * j));
                            j += 1;
                        }

                        return arr;
                    })((config.maxSize - config.minSize) / config.breakCount);

                } else {
                    breaksResponse.uniqueValues.forEach(({value}) => {
                        uniqueValuesStr.push(value);
                    });

                }

                if (breaksResponse.uniqueValues) {
                    response[type] = breaksResponse.uniqueValues;

                    if (customBreaks) {

                        for (let customBreak of customBreaks) {
                            let index = uniqueValuesStr.indexOf(customBreak.breakIdentifier);

                            if (index >= 0 && customBreak.symbology) {
                                response[type][index].symbology = customBreak.symbology;
                            }

                        }

                    }

                } else {
                    for (let {businessData} of queryResponse.features) {
                        let value = businessData[config.classificationField];
                        let i = 0;
                        while (i < breaksResponse.length) {
                            let ends =  breaksResponse[i + 1];
                            let start = breaksResponse[i];

                            ends = ends !== void 0 ? ends : start;

                            if (!response[type][symbologies[i]]) {
                                response[type][symbologies[i]] = {
                                    count : 0,
                                    value : {
                                        start,
                                        ends
                                    },
                                    symbology : {
                                        [type === 'fill' ? 'fillColor' : 'radius'] : symbologies[i]
                                    }
                                };
                            }

                            let breakConfig = response[type][symbologies[i]];

                            if (ThematicHelper.isThematicBreakInside(breakConfig, value)) {
                                breakConfig.count += 1;
                                i += 1;
                                break;
                            }

                            i += 1;
                        }
                    }

                    response[type] = Object.keys(response[type]).map((symbol) => response[type][symbol]);

                    if (customBreaks && type !== 'size') {
                        for (let i in customBreaks) {
                            let customBreak = customBreaks[i];

                            if (customBreak.symbology) {
                                response[type][i].symbology = customBreak.symbology;
                            }
                        }
                    }
                }

            }
        };

        buildBreaks(typeRenderer, thematicConfig[typeRenderer]);

        return response;
    }

}