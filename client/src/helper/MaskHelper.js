
export default class MaskHelper {

    /**
     *
     * @param {*} value
     * @param {IPopupFieldConfig} outFieldConfig
     */
    static getValueMasked(value, outFieldConfig) {
        let strValue = value;

        if (!outFieldConfig ||
            (outFieldConfig && outFieldConfig.typeMask === 'none') ||
            (outFieldConfig && !outFieldConfig.config)
        ) {
            return strValue;
        }

        // numbers
        if (outFieldConfig.typeMask !== 'date') {
            strValue = String(value);
            let toFixed = outFieldConfig.config.toFixed !== void 0 ? outFieldConfig.config.toFixed : 2;
            let decimalSeparator = outFieldConfig.config.decimalSeparator || ',';
            let millerSeparator = outFieldConfig.config.millerSeparator || '.';
            let prefix = outFieldConfig.config.prefix ? outFieldConfig.config.prefix + ' ' : '';
            let suffix = outFieldConfig.config.suffix ?  + ' ' + outFieldConfig.config.suffix : '';
            let parts = '';
            let decimalPlaces = '';

            strValue = strValue
                .replace(/[^\d^.]+/g, '');

            if (/\d\.\d/.test(strValue)) {
                decimalPlaces = strValue
                    .replace(/\.([\d]+)$/, (full, matched) => decimalSeparator + matched);
                parts = decimalPlaces.split(decimalSeparator);
                decimalPlaces = parts[1].substr(0, toFixed);
            } else if (toFixed > 0) {
                let i = 0;
                decimalPlaces = '';
                while (i < toFixed) {
                    decimalPlaces += '0';
                    i += 1;
                }
            } else {
                decimalSeparator = '';
                decimalPlaces = '';
            }

            let integerValue = (parts ? parts[0] : strValue)
                .reverse()
                .replace(/(\d{3})/g, (full, a) => a  + millerSeparator)
                .reverse()
                .replace(/^[^\d]/, '');

            strValue = integerValue + decimalSeparator +  decimalPlaces;
            strValue = prefix + strValue + suffix;

        } else if (outFieldConfig.config.pattern) {
            //date
            let dateInstance = new Date(strValue);
            let date = String(dateInstance.getDate()).replace(/^(.)$/, '0$1');
            let month = String(dateInstance.getMonth() + 1).replace(/^(.)$/, '0$1');
            let year = String(dateInstance.getFullYear());

            strValue = outFieldConfig.config.pattern
                .replace(/day/, date)
                .replace(/month/, month)
                .replace(/year/, year);
        }

        return strValue;
    }

}