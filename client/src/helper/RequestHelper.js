import axios from "axios";
import queryString from "query-string";

export default class RequestHelper {

    /**
     *
     * @param {ConnectaMaps} parent
     */
    constructor(parent) {
        this.$parent = parent;
    }

    get(route, params, header = {}) {
        return this._ajax('get', (
            params && Object.keys(params).length ? ( route + '?' + queryString.stringify(params) ) : route
        ), {headers: Object.assign({}, header, this.$parent.requestHeader)});
    }

    post(route, params, header = {}) {
        return this._ajax('post', route, params, {headers: Object.assign({}, header, this.$parent.requestHeader)});
    }

    del(route, params, header = {}) {
        return this._ajax('delete', route, params, {headers: Object.assign({}, header, this.$parent.requestHeader)});
    }

    _ajax(verb, route, params, header = {}) {
        route = route.replace(/([^:])\/\//g, '$1/');
        return axios[verb](route, params, header);
    }

}