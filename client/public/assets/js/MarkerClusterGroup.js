// import "leaflet.markercluster";

L.CustomMarkerClusterGroup = L.MarkerClusterGroup.extend({
	options: {
		maxClusterRadius: 80, //A cluster will cover at most this many pixels from its center
		iconCreateFunction: null,
        onEnd : null,

		spiderfyOnMaxZoom: true,
		showCoverageOnHover: true,
		zoomToBoundsOnClick: true,
		singleMarkerMode: false,

		disableClusteringAtZoom: null,

		// Setting this to false prevents the removal of any clusters outside of the viewpoint, which
		// is the default behaviour for performance reasons.
		removeOutsideVisibleBounds: true,

		// Set to false to disable all animations (zoom and spiderfy).
		// If false, option animateAddingMarkers below has no effect.
		// If L.DomUtil.TRANSITION is falsy, this option has no effect.
		animate: true,

		//Whether to animate adding markers after adding the MarkerClusterGroup to the map
		// If you are adding individual markers set to true, if adding bulk markers leave false for massive performance gains.
		animateAddingMarkers: false,

		//Increase to increase the distance away that spiderfied markers appear from the center
		spiderfyDistanceMultiplier: 1,

		// Make it possible to specify a polyline options on a spider leg
		spiderLegPolylineOptions: { weight: 1.5, color: '#222', opacity: 0.5 },

		// When bulk adding layers, adds markers in chunks. Means addLayers may not add all the layers in the call, others will be loaded during setTimeouts
		chunkedLoading: false,
		chunkInterval: 200, // process markers for a maximum of ~ n milliseconds (then trigger the chunkProgress callback)
		chunkDelay: 50, // at the end of each interval, give n milliseconds back to system/browser
		chunkProgress: null, // progress callback: function(processed, total, elapsed) (e.g. for a progress indicator)

		//Options to pass to the L.Polygon constructor
		polygonOptions: {}
	},

	initialize: function (options, customOptions) {
		L.Util.setOptions(this, options);
		if (!this.options.iconCreateFunction) {
			this.options.iconCreateFunction = this._defaultIconCreateFunction;
		}

		this._featureGroup = L.featureGroup();
		this._featureGroup.addEventParent(this);

		this._nonPointGroup = L.featureGroup();
		this._nonPointGroup.addEventParent(this);

		this._inZoomAnimation = 0;
		this._needsClustering = [];
		this._needsRemoving = []; //Markers removed while we aren't on the map need to be kept track of
		//The bounds of the currently shown area (from _getExpandedVisibleBounds) Updated on zoom/move
		this._currentShownBounds = null;

		this._queue = [];

		// Hook the appropriate animation methods.
		var animate = L.DomUtil.TRANSITION && this.options.animate;
		L.extend(this, animate ? this._withAnimation : this._noAnimation);
		// Remember which MarkerCluster class to instantiate (animated or not).
		this._customOptions = customOptions;
		this._markerCluster = L.CustomMarkerCluster;
		this._markerCluster.prototype._customOptions = this._customOptions;
	},

    onAdd: function (map) {
        this._map = map;
        var i, l, layer;

        if (!isFinite(this._map.getMaxZoom())) {
            throw "Map has no maxZoom specified";
        }

        this._featureGroup.addTo(map);
        this._nonPointGroup.addTo(map);

        if (!this._gridClusters) {
            this._generateInitialClusters();
        }

        this._maxLat = map.options.crs.projection.MAX_LATITUDE;

        for (i = 0, l = this._needsRemoving.length; i < l; i++) {
            layer = this._needsRemoving[i];
            this._removeLayer(layer, true);
        }
        this._needsRemoving = [];

        //Remember the current zoom level and bounds
        this._zoom = Math.round(this._map._zoom);
        this._currentShownBounds = this._getExpandedVisibleBounds();

        this._map.on('zoomend', this._zoomEnd, this);
        this._map.on('moveend', this._moveEnd, this);

        if (this._spiderfierOnAdd) { //TODO FIXME: Not sure how to have spiderfier add something on here nicely
            this._spiderfierOnAdd();
        }

        this._bindEvents();

        //Actually add our markers to the map:
        l = this._needsClustering;
        this._needsClustering = [];
        this.addLayers(l);

        setTimeout(function () {
            this.options.onEnd(this._featureGroup);
        }.bind(this), 500);
    },

    _mergeSplitClusters: function () {
        var mapZoom = Math.round(this._map._zoom);

        //In case we are starting to split before the animation finished
        this._processQueue();

        if (this._zoom < mapZoom && this._currentShownBounds.intersects(this._getExpandedVisibleBounds())) { //Zoom in, split
            this._animationStart();
            //Remove clusters now off screen
            this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, this._zoom, this._getExpandedVisibleBounds());

            this._animationZoomIn(this._zoom, mapZoom);

        } else if (this._zoom > mapZoom) { //Zoom out, merge
            this._animationStart();

            this._animationZoomOut(this._zoom, mapZoom);
        } else {
            this._moveEnd();
        }

        setTimeout(function () {
            this.options.onEnd(this._featureGroup);
        }.bind(this), 500);
    },

    _moveEnd: function () {
        if (this._inZoomAnimation) {
            return;
        }

        var newBounds = this._getExpandedVisibleBounds();

        this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, this._zoom, newBounds);
        this._topClusterLevel._recursivelyAddChildrenToMap(null, Math.round(this._map._zoom), newBounds);

        this._currentShownBounds = newBounds;

        setTimeout(function () {
            this.options.onEnd(this._featureGroup);
        }.bind(this), 500);

        return;
    },
	_generateInitialClusters: function () {
		var maxZoom = this._map.getMaxZoom(),
			radius = this.options.maxClusterRadius,
			radiusFn = radius;

		//If we just set maxClusterRadius to a single number, we need to create
		//a simple function to return that number. Otherwise, we just have to
		//use the function we've passed in.
		if (typeof radius !== "function") {
			radiusFn = function () { return radius; };
		}

		if (this.options.disableClusteringAtZoom) {
			maxZoom = this.options.disableClusteringAtZoom - 1;
		}
		this._maxZoom = maxZoom;
		this._gridClusters = {};
		this._gridUnclustered = {};

		//Set up DistanceGrids for each zoom
		for (var zoom = maxZoom; zoom >= 0; zoom--) {
			this._gridClusters[zoom] = new L.DistanceGrid(radiusFn(zoom));
			this._gridUnclustered[zoom] = new L.DistanceGrid(radiusFn(zoom));
		}

		// Instantiate the appropriate L.MarkerCluster class (animated or not).
		this._topClusterLevel = new this._markerCluster(this, -1, null, null, this._customOptions);
	}
});

L.customMarkerClusterGroup = function (options, customOptions) {
	return new L.CustomMarkerClusterGroup(options, customOptions);
};
