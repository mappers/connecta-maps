L.CustomMarkerCluster = L.MarkerCluster.extend({
	_addChild: function (new1, isNotificationFromChild) {
		this._iconNeedsUpdate = true;

		this._boundsNeedUpdate = true;
		this._setClusterCenter(new1);

		if (new1 instanceof L.CustomMarkerCluster) {
			if (!isNotificationFromChild) {
				new1.options = Object.assign(new1.options, this._customOptions);
				this._childClusters.push(new1);
				new1.__parent = this;
			}
			this._childCount += new1._childCount;
		} else {
			if (!isNotificationFromChild) {
				this._markers.push(new1);
			}
			this._childCount++;
		}

		if (this.__parent) {
			this.__parent._addChild(new1, true);
		}
	}
});