
export = {

    //==== Erros usados para notificar o usuário quando houver requisição

    "RES_ERROR_01" : "Erro desconhecido",
    "RES_ERROR_02" : "Operação não disponível",
    "RES_ERROR_03" : "Operação não executada",
    "RES_ERROR_04" : "Operação não existe",
    "RES_ERROR_05" : "Tipo de servidor não suportado",
    "RES_ERROR_06" : "Parâmetro ${0} é obrigatório",
    "RES_ERROR_07" : "Origem de dados espacial não encontrado",
    "RES_ERROR_08" : "Não foi possível concluir a requisição",
    "RES_ERROR_09" : "Atributo ${1} do parâmetro ${0} é inválido",
    "RES_ERROR_10" : "Parâmetro ${0} inválido",
    "RES_ERROR_11" : "Atributo ${1} do parâmetro ${0} é obrigatório",
    "RES_ERROR_12" : "Documento de id ${0}, não existe",
    "RES_ERROR_13" : "Parâmetros inválidos",
    "RES_ERROR_14" : "Não foi possível requisitar informações na origem de dados de dsn: ${0}",
    "RES_ERROR_15" : "Não foi possível salvar a Fonte de Dados Espaciais",
    "RES_ERROR_16" : "Não é possível excluir este(a) ${0} porque ele(a) está sendo usado(a) em um(a) ${1}",
    "RES_ERROR_17" : "Parâmetro(s) faltando: ${0}",
	"RES_ERROR_18" : "O tipo de serviço: ${0} não existe",
    "RES_ERROR_19" : "O documento de ID ${0} tem vinculo com a coleção ${1}",
    "RES_ERROR_20" : "É necessário ter autenticidade para esta requisição!",
    "RES_ERROR_21" : "Nenhum dado foi removido",

    //==== Erros usados para notificar o desenvolvedor sobre implementações erradas

    "DEV_ERROR_01" : "As obrigatoriedades de implementação do módulo devem ser seguidas",
    "DEV_ERROR_02" : "Rotas estão erradas!",
    "DEV_ERROR_03" : "Os middlewares foram registrados",

    //==== Erros do servidor que são disparados no tempo de build do Server

    'SERVER_ERROR_01' : 'Conexão com o banco de dados não foi estabelecida',
    'SERVER_ERROR_02' : 'Não foi possível executar a operação segundo a padronização OGC, problemas com parâmetros',

    //==== Informações do servidor usados para log

    "SERVER_INFO_01" : "Teste"

};
