
export = {

    //==== Erros usados para notificar o usuário quando houver requisição

    "RES_ERROR_01" : "Unknown error",
    "RES_ERROR_02" : "PreOperationData not available",
    "RES_ERROR_03" : "PreOperationData not executed",
    "RES_ERROR_04" : "PreOperationData doesn\'t exist",
    "RES_ERROR_05" : "Server type not supported",
    "RES_ERROR_06" : "Parameter ${0} is required",
    "RES_ERROR_07" : "SpatialDataSource was not found",
    "RES_ERROR_08" : "Could not complete the request",
    "RES_ERROR_09" : "Atribute ${1} from parameter ${0} is invalid",
    "RES_ERROR_10" : "Parameter ${0} invalid",
    "RES_ERROR_11" : "Atribute ${1} from parameter ${0} is required",
    "RES_ERROR_12" : "The document with id ${0} doesn't exist",
    "RES_ERROR_13" : "Invalid parameters",
    "RES_ERROR_14" : "Don't possible require information in DSN ${0}",
    "RES_ERROR_15" : "Don't possible to save spatial data source",
    "RES_ERROR_17" : "${0} service type doesn\'t exists",
    "RES_ERROR_18" : "Service type ${0} does not exists",
    "RES_ERROR_19" : "The document of ID ${0} have bond with the collection ${1}",
    "RES_ERROR_20" : "It must to have authentication to request it",
    "RES_ERROR_21" : "No data has been removed",

    //==== Erros usados para notificar o desenvolvedor sobre implementações erradas

    "DEV_ERROR_01" : "Module deployment settings must be followed",
    "DEV_ERROR_02" : "Routes its wrongs!",
    "DEV_ERROR_03" : "The middlewares were recorded",

    //==== Erros do servidor que são disparados no tempo de build do Server

    "SERVER_ERROR_01" : "Connection to the database has not been established",
    "SERVER_ERROR_02" : "Unable to perform the operation according to the OGC standardization, problems with parameters",

    //==== Informações do servidor usados para log

    "SERVER_INFO_01" : "test"

};