/**
 * Método de classificação
 */
export enum ClassificationMethod {

    /**
     * Intervalos iguais
     * @type {any}
     */
    EQUAL_INTERVAL = <any>"EqInterval",

    /**
     * Progressão aritmética
     * @type {any}
     */
    ARITHMETIC_PROGRESSION = <any>"ArithmeticProgression",

    /**
     * Progressão geométrica
     * @type {any}
     */
    GEOMETRIC_PROGRESSION = <any>"GeometricProgression",

    /**
     * Quebras naturais
     * @type {any}
     */
    NATURAL_BREAKS = <any>"Jenks",

    /**
     * Frequências iguais
     * @type {any}
     */
    QUANTILE = <any>"Quantile",

    /***
     * Desvio padrão
     * @type {any}
     */
    STANDARD_DEVIATION = <any>"StdDeviation"

}