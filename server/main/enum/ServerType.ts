
export enum ServerType {

    ArcGIS = <any>'ArcGIS',
    MapViewer = <any>'MapViewer',
    Oracle = <any>'Oracle',
    PostGIS = <any>'PostGIS',
    MapServer = <any>'MapServer',
    GeoServer = <any>'GeoServer',
    OGC = <any>'OGC'

}


