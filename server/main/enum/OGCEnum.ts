
export enum OGCServiceType {
    WMS = <any>"wms",
    WFS = <any>"wfs",
    WCS = <any>"wcs"
}

export enum OGCOperationName {
    GET_MAP = <any>"GetMap",
    GET_FEATURE = <any>"GetFeature",
    DESCRIBE_FEATURE_TYPE = <any>"DescribeFeatureType",
    GET_CAPABILITIES = <any>"GetCapabilities",
    GET_LEGEND_GRAPHIC = <any>"GetLegendGraphic"
}

export enum WMSVersion {
    v1_0_0 = <any>"1.0.0",
    v1_1_0 = <any>"1.1.0",
    v1_3_0 = <any>"1.3.0",
    v2_0_0 = <any>"2.0.0"
}

export enum WFSVersion {
    v1_0_0 = <any>"1.0.0",
    v1_1_0 = <any>"1.1.0",
    v2_0_0 = <any>"2.0.0"
}

export enum OGCSpatialRel {
    INTERSECT = <any>"INTERSECTS",
    CONTAIN = <any>"CONTAINS",
    CROSSES = <any>"CROSSES",
    ENVELOPE = <any>"BBOX",
    OVERLAP = <any>"OVERLAPS",
    TOUCH = <any>"TOUCHES",
    WITHIN = <any>"WITHIN"
}