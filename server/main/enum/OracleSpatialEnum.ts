export enum OracleSpatialRel {

	INTERSECT = <any>"SDO_ANYINTERACT",
	CONTAIN = <any>"SDO_CONTAINS",
	OVERLAP = <any>"SDO_OVERLAPS",
	TOUCH = <any>"SDO_TOUCH",
	WITHIN = <any>"SDO_INSIDE"

	// https://docs.oracle.com/cd/B19306_01/appdev.102/b14255/sdo_operat.htm#BGEBCFBE
}

export enum OracleSpatialGeometryType {

	POINT = <any>"2001",

	MULTIPOINT = <any>"2005",

	POLYLINE = <any>"2006",

	POLYGON = <any>"2003"

	// https://docs.oracle.com/cd/B19306_01/appdev.102/b14255/sdo_objrelschema.htm#g1013735

}
