

export enum ArcGISGeometryType {

    POINT = <any>"esriGeometryPoint",

    MULTIPOINT = <any>"esriGeometryMultipoint",

    POLYLINE = <any>"esriGeometryPolyline",

    POLYGON = <any>"esriGeometryPolygon"

}

export enum ArcGISSpatialRel {

    INTERSECT = <any>"esriSpatialRelIntersects",
    CONTAIN = <any>"esriSpatialRelContains",
    CROSSES = <any>"esriSpatialRelCrosses",
    ENVELOPE = <any>"esriSpatialRelEnvelopeIntersects",
    OVERLAP = <any>"esriSpatialRelOverlaps",
    TOUCH = <any>"esriSpatialRelTouches",
    WITHIN = <any>"esriSpatialRelWithin"
}

export enum ArcGISTypeField {

    INTEGER = <any>'esriFieldTypeInteger',
    SMALL_INTEGER = <any>'esriFieldTypeSmallInteger',
    DOUBLE = <any>'esriFieldTypeDouble',
    STRING = <any>'esriFieldTypeString',
    SINGLE = <any>'esriFieldTypeSingle',
    DATE = <any>'esriFieldTypeDate',
    GEOMETRY = <any>'esriFieldTypeGeometry',
    OID = <any>'esriFieldTypeOID',
    BLOB = <any>'esriFieldTypeBlob',
    GLOBAL_ID = <any>'esriFieldTypeGlobalID',
    RASTER = <any>'esriFieldTypeRaster'

}

export enum ArcGISVersion{

    v10_31 = <any>"10.31"

}