
export enum ResponseFormat{

    IMAGE = <any>'image',
    JSON = <any>'json',
    SHAPE = <any>"SHAPE",
    GEO_JSON = <any>"GEO_JSON",
    KML = <any>"KML",
    CSV = <any>"CSV",

}
