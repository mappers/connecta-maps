import {IDataSourceService, IFilterConfig} from "../_base/IDataSourceService";
import {ViewerSchema} from "../../../../data/schema/Viewer";
import {ProjectSchema, RichLayer} from "../../../../data/schema/Project";
import {GeoJSON} from "../../../../mapping/GeoJSON";
import {AdapterHelper} from "../../../geo/helper/AdapterHelper";
import WorkerHelper from "../../../../helper/WorkerHelper";
import {Converter} from "../../../../util/Converter";
import * as mongoose from "mongoose";
import {requestFileInterface} from "restify";

export class GeoAdapter implements IDataSourceService {

    buildDataSource(params: any): Promise<any> {
        return null;
    }

    saveViewer(operationFunction: Function): Promise<ViewerSchema> {
        return null;
    }

    deleteViewer(operationFunction: Function): Promise<ViewerSchema> {
        return null;
    }

    updateViewer(operationFunction: Function): Promise<ViewerSchema> {
        return null;
    }

    getResultSet(params: {
        project:ProjectSchema,
        richLayer:RichLayer,
        geoJSON?:GeoJSON,
        filterConfig?:{ operator:string; field:string; value?:any; values?:any; }[]
    }): Promise<any> {
        return new Promise((resolve, reject) => {
            if (params.geoJSON) {
                return this._prepareResultSet(params).then(resolve, reject);
            }

            let whereClause = Converter.buildWhereClause(params.filterConfig) || '1=1';

            return  AdapterHelper.executeWithLayer(params.richLayer.layer, 'query', { where : whereClause })
                .then((geoJSON) => {
                    let businessData = geoJSON.features.map(feature => feature.properties);
                    let worker = new WorkerHelper('filter-result-set.js');
                    return worker.execute({
                        filterConfig : params.filterConfig,
                        businessData,
                        richLayer : params.richLayer
                    })
                        .then(({businessData}) => {
                            try {
                                params.geoJSON = {
                                    features : businessData.map(featureSet => {
                                        return {
                                            properties : featureSet
                                        }
                                    })
                                } as any;
                                this._prepareResultSet(params)
                                    .then(resolve, reject);
                            } catch (error) {
                                reject(error);
                            }
                        })
                        .catch(reject);
                });
        });
    }

    private _prepareResultSet(params: {
        project:ProjectSchema,
        richLayer:RichLayer,
        geoJSON?:GeoJSON,
        filterConfig?:{ operator:string; field:string; value?:any; values?:any; }[]
    }): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                let businessData = [];
                let where = ' IN (';

                if (params.geoJSON) {
                    for (let feature of params.geoJSON.features) {
                        for (let columnName in feature.properties) {
                            if (columnName === params.richLayer.crossingKeys.resultSetKey) {
                                let quote = '';
                                if (typeof feature.properties[columnName] === 'string') {
                                    quote = '\'';
                                }
                                where += quote + feature.properties[columnName] + quote + ',';
                            }
                        }

                        feature.properties['_id'] = String(mongoose.Types.ObjectId());
                        businessData.push(feature.properties);
                    }
                }

                where = where.replace(/,$/, '') + ')';

                resolve({
                    geoJSON : params.geoJSON,
                    businessData,
                    where
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    getDistinctValues({richLayer, domainId, field, dataSourceIdentifier, filterConfig }) {
        return AdapterHelper.executeWithLayer(
            richLayer.layer._id,
            'query',
            {
                outFields : [field],
                where : '1=1',
                returnGeometry : false,
                filterConfig
            }
        )
            .then(geoJSON => {
                let mapping = {};
                let aa = [];

                for (let feature of geoJSON.features) {
                    if (!mapping[feature.properties[field]]) {
                        let value = feature.properties[field];
                        mapping[value] = 1;
                        aa.push({ [field] : value});
                    }
                }

                return aa;
            })
    }
}