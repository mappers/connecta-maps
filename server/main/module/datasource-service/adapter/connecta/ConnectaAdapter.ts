import {IDataSourceService} from "../_base/IDataSourceService";
import {ServerException} from "../../../../global/ServerException";
import {ViewerSchema} from "../../../../data/schema/Viewer";
import Config from "../../../../Config";
import {ProjectSchema, RichLayer} from "../../../../data/schema/Project";
import * as request from "request";
import WorkerHelper from "../../../../helper/WorkerHelper";

let solr = require('solr-client');

export class ConnectaAdapter implements IDataSourceService {

    solr: any;

    getSolr() {
        if (!this.solr) {
            this.solr = solr.createClient(Config.solr);
        }
        return this.solr;
    }

    buildDataSource(params: any): Promise<any> {
        return new Promise((resolve, reject) => {

            try {

                let pattern = /()/;

                if (!pattern.test(params.dsn)) {
                    throw new ServerException('RES_ERROR_10', ['dsn']);
                }

                let dataSource: any = {};
                dataSource = params;

                resolve(dataSource);
            } catch (error) {
                reject(error);
            }

        });


    }

    /**
     * Método responsável por construir, salvar e indexar no Solr o Viewer do Connecta.
     * @param {Function} operationFunction
     * @returns {Promise}
     */
    saveViewer(operationFunction: Function): Promise<ViewerSchema> {
        return new Promise<ViewerSchema>((resolve, reject) => {
            try {
                let promise = operationFunction();
                promise.catch(reject);
                promise.then((viewer) => {
                    let client = this.getSolr();
                    client.add({
                        module: 'maps',
                        text: viewer.title,
                        domain: viewer.domainId,
                        name: viewer.title,
                        id: viewer._id
                    }, AddCallback);

                    function AddCallback(err) {
                        try {
                            if (err) {
                                throw err;
                            }
                            client.commit(CommitCallback);
                        } catch (err) {
                            reject(err);
                        }
                    }

                    function CommitCallback(err) {
                        try {
                            if (err) {
                                throw err;
                            }
                            resolve(viewer);
                        } catch (err) {
                            reject(err);
                        }
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Método responsável por deletar e desindexar no Solr o Viewer do Connecta.
     * @param {Function} operationFunction
     * @returns {Promise}
     */
    deleteViewer(operationFunction: Function): Promise<ViewerSchema> {
        return new Promise<ViewerSchema>((resolve, reject) => {
            try {
                let promise = operationFunction();
                promise.catch(reject);
                promise.then((viewer) => {

                    let client = this.getSolr();
                    client.delete('id', viewer._id.toString(), DeleteCallback);

                    function DeleteCallback(err) {
                        try {
                            if (err) {
                                throw err;
                            }
                            client.commit(CommitCallback);
                        } catch (err) {
                            reject(err);
                        }
                    }

                    function CommitCallback(err) {
                        try {
                            if (err) {
                                throw err;
                            }
                            resolve(viewer);
                        } catch (err) {
                            reject(err);
                        }
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Método responsável por atualizar no Solr/Mongodb o Viewer do Connecta.
     * @param {Function} operationFunction
     * @returns {Promise}
     */
    updateViewer(operationFunction: Function): Promise<ViewerSchema> {
        return new Promise<ViewerSchema>((resolve, reject) => {
            try {
                let promise = operationFunction();
                promise.catch(reject);
                promise.then((viewer) => {

                    let updatedViewer = {
                        module: 'maps',
                        text: viewer.title,
                        domain: viewer.domainId,
                        name: viewer.title,
                        id: viewer._id.toString()
                    };

                    let client = this.getSolr();
                    client.add(updatedViewer, UpdateCallback);

                    function UpdateCallback(err, obj) {
                        try {
                            if (err) {
                                throw err;
                            }
                            client.commit(CommitCallback);
                        } catch (err) {
                            reject(err);
                        }
                    }

                    function CommitCallback(err) {
                        try {
                            if (err) {
                                throw err;
                            }
                            resolve(viewer);
                        } catch (err) {
                            reject(err);
                        }
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    getResultSet(params: { project: ProjectSchema; richLayer: RichLayer, filterConfig?:any[] }): Promise<any> {
        return new Promise((resolve, reject) => {
            let {filterConfig, richLayer, project} = params;

            try {
                let headers = {
                    'Domain' : project.domainId
                };

                console.log('Request metadata on Connecta!');

                let url = String(Config.connecta.serviceDomain + '/connecta-presenter/analysis/' + richLayer.dataSourceIdentifier);

                request({
                        url,
                        method: "GET",
                        headers
                    },
                    (error, response, analysis) => {
                        try {
                            if (error) {
                                throw error;
                            }

                            analysis = JSON.parse(analysis);

                            if (analysis.type && analysis.type === 'ERROR') {
                                throw new Error('Connecta: ' + analysis.message);
                            }

                            console.log('Request resultSet on Connecta!');

                            request({
                                url: Config.connecta.serviceDomain + '/connecta-presenter/analysis/result',
                                method: "POST",
                                headers: {
                                    'Domain' : project.domainId,
                                    'portal.dashboard.validated':true,
                                    "content-type": "application/json",  // <--Very important!!!
                                },
                                json: true,   // <--Very important!!!
                                body: {
                                    analysis,
                                    filters : this._getFilter(filterConfig),
                                    drill : {
                                        columnToDrill : richLayer.crossingKeys.resultSetKey,
                                        columnsToSum : [],
                                        listPreviousColumns : []
                                    }
                                }
                            }, (error, response, businessData) => {
                                try {
                                    if (error) {
                                        throw error;
                                    }

                                    let promise:any = Promise.resolve({ businessData , where : '1=1' });

                                    if (filterConfig && filterConfig.length) {
                                        let worker = new WorkerHelper('filter-result-set.js');
                                        promise = worker.execute({
                                            filterConfig,
                                            businessData,
                                            richLayer
                                        });
                                    }

                                    promise.then(({businessData, where}) => {
                                        new WorkerHelper('aggregate-business-data.js')
                                            .execute({
                                                businessData,
                                                richLayer,
                                                href : Config.connecta.host
                                            })
                                            .then(({businessData}) => {
                                                resolve({
                                                    businessData,
                                                    where
                                                });
                                            })
                                            .catch(reject);
                                    });
                                } catch (error) {
                                    reject(error);
                                }
                            });
                        } catch (error) {
                            reject(error);
                        }
                    }
                )
            } catch (error) {
                reject(error);
            }
        });
    }

    private _getFilter (filterConfig) {
        let mapping = {
            'EQUAL' : 'EQUAL',
            'DIFFERENT' : 'NOT_EQUAL',
            'LESS' : 'LESS_THAN',
            'LESS_OR_EQUAL' : 'LESS_THAN_EQUAL',
            'GREATER' : 'GREATER_THAN',
            'GREATER_OR_EQUAL' : 'GREATER_THAN_EQUAL',
            'BETWEEN' : 'BETWEEN',
            'IN' : 'IN',
            'LIKE' : 'CONTAINS'
        };

        return filterConfig.map(({operator, field, value}) => {
            value = operator === 'IN' ? { 'in' : value } : (
                operator === 'BETWEEN'? { 'between' : value } : { value }
            );
            operator = mapping[operator];
            return {
                operator,
                columnName: field,
                value
            };
        })
    }

    getDistinctValues ({domainId, field, dataSourceIdentifier, filterConfig, richLayer}) {

        let url = String(Config.connecta.serviceDomain + '/connecta-presenter/analysis/' + dataSourceIdentifier);
        let analysis:any = {};

        return new Promise((resolve, reject) => {
            request({
                url,
                method: "GET",
                headers : {
                    'Domain' : domainId
                }
            }, (error, response, _analysis) => {
                analysis = JSON.parse(_analysis);

                if (analysis && analysis.type && analysis.type === 'ERROR') {
                    throw new Error(analysis.message);
                }

                analysis.analysisColumns = analysis.analysisColumns.filter((column) => {
                    return column.name === field
                });

                request({
                    url: Config.connecta.serviceDomain + '/connecta-presenter/analysis/result',
                    method: "POST",
                    headers: {
                        'Domain' : domainId,
                        'portal.dashboard.validated' : true,
                        "content-type": "application/json",
                    },
                    json: true,
                    body: {
                        analysis : analysis,
                        filters : this._getFilter(filterConfig),
                        customConfigs : [
                            {
                                column : {
                                    label : richLayer.crossingKeys.resultSetKey
                                },
                                customAlias : "COUNT",
                                "function" : "DISTINCT_COUNT",
                                order : "ASC"
                            },
                            {
                                column : {
                                    label : field
                                },
                                customAlias : "",
                                "function" : "GROUP",
                                order : "ASC"
                            }
                        ],
                        drill : {
                            columnToDrill : field,
                            columnsToSum : [],
                            listPreviousColumns : []
                        }
                    }
                }, (error, response, result) => {
                    if (error) {
                        throw error;
                    }

                    resolve(result);
                });
            });
        });
    }
}

