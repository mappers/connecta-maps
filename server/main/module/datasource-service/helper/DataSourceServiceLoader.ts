import * as path from "path";
import * as fs from "fs";
import * as fse from "fs-extra";
import * as Q from "q";
import {ServerException} from "../../../global/ServerException";
import {DataSourceServiceType} from "../../../enum/DataSourceServiceType";
import {Converter} from "../../../util/Converter";
import {ResultSetCache} from "../../../data/schema/ResultSetCache";

let pathData = path.resolve(__dirname, 'data');

let creatingCacheMapping = {};

export class DataSourceServiceLoader {

    /**
     * @param type {DataSourceServiceType}
     * @param operationName
     * @param params
     * @param generateCache
     * @returns {Promise<any>}
     */
    static loadAndExecute(type:DataSourceServiceType, operationName:string, params?:any, generateCache = false):Q.Promise<any> {
        return Q.Promise<any>((resolve, reject, notify) => {
            try {
                let hash = Converter.createHash({type, operationName, params});

                if (!(hash in creatingCacheMapping)) {
                    creatingCacheMapping[hash] = true;
                }

                DataSourceServiceLoader
                    ._verifyInCache(generateCache, hash)
                    .then(cache => {
                        try {
                            if (cache) {
                                resolve(cache);
                                return;
                            }

                            let pathAdapters:string = path.resolve(serverPath, 'module', 'datasource-service', 'adapter', type);

                            fs.readdir(pathAdapters, (err, files) => {
                                try {
                                    if (err) {
                                        throw err;
                                    }

                                    let adapterName:string;

                                    for (let file of files) {
                                        if (/Adapter\.js$/.test(file)) {
                                            let pattern = /(.+Adapter)(\.js)?(\.map)?/;
                                            adapterName = file.match(pattern)[1];
                                            break;
                                        }
                                    }

                                    if (!adapterName) {
                                        throw new ServerException('RES_ERROR_18', [adapterName]);
                                    }

                                    let pathAdapter = path.resolve(pathAdapters, adapterName);

                                    fs.exists(path.resolve(pathAdapter + '.js'), (exists) => {
                                        try {
                                            if (!exists) {
                                                throw new ServerException('RES_ERROR_18', [type]);
                                            }

                                            let Adapter = require(pathAdapter);

                                            for (let key in Adapter) {
                                                if (/Adapter/.test(key)) {
                                                    Adapter = Adapter[key];
                                                    break;
                                                }
                                            }

                                            let adapter = new Adapter();

                                            if (!adapter[operationName]) {
                                                throw new ServerException('RES_ERROR_18', [type]);
                                            }

                                            return adapter[operationName](params)
                                                .then(result => {
                                                    if (generateCache && creatingCacheMapping[hash]) {
                                                        creatingCacheMapping[hash] = false;
                                                        DataSourceServiceLoader._createCache(hash, result)
                                                            .then(() => {
                                                                delete creatingCacheMapping[hash];
                                                                resolve(result);
                                                            })
                                                            .catch(reject);
                                                    } else {
                                                        resolve(result);
                                                    }
                                                })
                                                .catch(reject);
                                        } catch (error) {
                                            reject(error);
                                        }
                                    });

                                } catch (error) {
                                    reject(error);
                                }
                            });

                        } catch (error) {
                            reject(error);
                        }

                    }, reject, notify);
            } catch (error) {
                reject(error);
            }
        });
    }

    static _createCache(hash, result) {
        return new Promise((resolve, reject) => {

            try {
                ResultSetCache.Model.create({
                    hash : hash,
                    info : result
                }, (error) => {
                    if (error) {
                        console.error(error);
                        _saveOnFileSystem();
                    }
                    resolve();
                });

            } catch (error) {
                console.log(error);
                reject(error);
            }


            function _saveOnFileSystem () {
                fse.ensureDir(pathData, (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        fs.writeFile(path.resolve(pathData, hash), JSON.stringify(result), error => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve();
                            }
                        });
                    }
                });
            }
        });
    }

    static _verifyInCache(generateCache, hash):Q.Promise<any> {
        return Q.Promise<boolean>((resolve, reject, notify) => {
            try {

                if (!generateCache) {
                    resolve('' as any);
                    return;
                }

                function _verifyOnFileSystem () {
                    let pathFileCache = path.resolve(pathData, hash);

                    if (!fs.existsSync(pathFileCache)) {
                        resolve('' as any);
                        return;
                    }

                    let readableStream = fs.createReadStream(pathFileCache);
                    let data:any = '';

                    readableStream.on('error', function() {
                        resolve(null);
                    });
                    readableStream.on('data', function(chunk) {
                        data += chunk;
                    });

                    readableStream.on('end', function() {
                        try {
                            resolve(JSON.parse(data));
                        } catch (error) {
                            reject(error);
                        }
                    });
                }

                try {
                    ResultSetCache.Model.findOne({
                        hash : hash
                    },(error, doc) => {
                        if (error || !doc) {
                            _verifyOnFileSystem();
                        } else {
                            resolve(doc.info);
                        }
                    });

                } catch (e) {
                    console.log(e);
                    reject(e);
                }

            } catch (error) {
                reject(error);
            }

        });
    }

}

