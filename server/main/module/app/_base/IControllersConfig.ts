export interface IControllersConfig {
    get? : IControllerConfig[];
    post? : IControllerConfig[];
    del? : IControllerConfig[];
    put? : IControllerConfig[];
    patch? : IControllerConfig[];
}

interface IControllerConfig {
    route : string,
    execute : Function
}