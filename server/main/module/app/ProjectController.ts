import {Project} from "../../data/schema/Project";
import * as mongoose from "mongoose";
import {ServerException} from "../../global/ServerException";
import {Sender} from "../../middleware/response/Sender";
import {EndPoint} from "../../decorator/EndPoint";
import {HttpVerb} from "../../enum/HttpVerb";

export class ProjectController {

    @EndPoint({
        verb : HttpVerb.GET,
        route : '/app/project/:id',
        response : {}
    })
    getProjectById = (params) => {
        return new Promise((resolve, reject) => {
            try {
                Project.Model.findById(params.id, (error, doc) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(doc);
                    }
                });
            } catch (error) {
                reject(error);
            }
        });
    };

}