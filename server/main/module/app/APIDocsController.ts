import {EndPoint, getConfigurations} from "../../decorator/EndPoint";
import {HttpVerb} from "../../enum/HttpVerb";
import * as FS from "fs";
import * as PATH from "path";

const Config = JSON.parse( FS.readFileSync(PATH.resolve(__dirname, '../../../../', 'Config.json')).toString() );
const packageJSON = JSON.parse( FS.readFileSync(PATH.resolve(__dirname, '../../', 'package.json')).toString() );

export class APIDocsController {

    @EndPoint({
        verb : HttpVerb.GET,
        route : '/v2/api-docs',
        response : {}
    })
    getAPIDescriptor(params) {
        return new Promise((resolve, reject) => {
            try {
                let schemas = FS.readdirSync(PATH.resolve(__dirname, '../../', 'data', 'schema'));
                let definitions = {};

                let getAttributesBySchema = (schemaInstance) => {
                    let attributes = {};
                    for (let attributeName in schemaInstance) {
                        if (schemaInstance.hasOwnProperty(attributeName)) {
                            attributes[attributeName] = {
                                type : ((type) => {
                                    switch (type) {
                                        case String:
                                            return 'String';
                                        case Number:
                                            return 'Number';
                                        case Boolean:
                                            return 'Boolean';
                                        default:
                                            return type ? typeof type : 'Any';
                                    }
                                })(schemaInstance[attributeName].type),
                                required : Boolean(schemaInstance[attributeName].type)
                            };
                            if (schemaInstance[attributeName].type && schemaInstance[attributeName].type.obj) {
                                attributes[attributeName] = {
                                    type : 'Object',
                                    properties : getAttributesBySchema(schemaInstance[attributeName].type.obj)
                                };
                            } else {
                                if (schemaInstance[attributeName].enum) {
                                    attributes[attributeName].enum = schemaInstance[attributeName].enum;
                                }
                            }
                        }
                    }
                    return attributes;
                };

                for (let fileName of schemas) {
                    let fileImported = require('../../data/schema/' + fileName);

                    for (let className in fileImported) {
                        if (!fileImported[className].Model) {
                            definitions[/Schema$/.test(className) ? className : (className + 'Schema')] = {
                                type : 'Object',
                                properties : getAttributesBySchema(new fileImported[className]())
                            };
                        }
                    }
                }

                let paths = getConfigurations();

                resolve({
                    "swagger": "2.0",
                    "info": {
                        "description": packageJSON.description,
                        "version": packageJSON.version,
                        "title": "Connecta Maps",
                        "host": Config.client.PROD.serviceDomain,
                        "basePath": "/",
                        "license": {
                            "name": "GNU GPLv3",
                            "url": "http://git.cds.com.br/connecta/connecta-portal/blob/master/LICENSE"
                        }
                    },
                    paths,
                    definitions
                });
            } catch (error) {
                reject(error);
            }
        });
    }

}