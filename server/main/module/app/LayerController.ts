import {Layer} from "../../data/schema/Layer";
import {ServerException} from "../../global/ServerException";
import {EndPoint} from "../../decorator/EndPoint";
import {HttpVerb} from "../../enum/HttpVerb";

export class LayerController {

    @EndPoint({
        verb : HttpVerb.GET,
        route : '/app/layer/:id',
        response : {},
        params : {
            id : {
                type : 'string',
                cast : true,
                required : true
            }
        }
    })
    getOne = (params) => {
        return new Promise((resolve, reject) => {
            try {
                Layer.Model.findById(params.id, {info : 0}, (error, doc) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(doc);
                    }
                });
            } catch (error) {
                reject(error);
            }
        });
    };

    @EndPoint({
        verb : HttpVerb.POST,
        route : '/app/layer',
        response : {},
        params : {
            layer : {
                type : 'object',
                required : true
            }
        }
    })
    saveLayer = (params) => {
        return new Promise((resolve, reject) => {
            try {
                let {layer} = params;

                let id = layer._id;
                delete layer._id;

                Layer.Model.findByIdAndUpdate(id, { $set : layer }, (error) => {
                    try {
                        if (error) {
                            throw error;
                        }

                        Layer.Model.findById(id, (error, doc) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(doc);
                            }
                        });
                    } catch (error) {
                        reject(error);
                    }
                });
            } catch (error) {
                reject(error);
            }
        });
    };

}