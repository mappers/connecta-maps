import {Viewer} from "../../data/schema/Viewer";
import {ServerException} from "../../global/ServerException";
import {Project} from "../../data/schema/Project";
import {EndPoint} from "../../decorator/EndPoint";
import {HttpVerb} from "../../enum/HttpVerb";
import INDECrowler from "../../helper/INDECrowler";
import * as mongoose from "mongoose";

export class ViewerController {

    @EndPoint({
        verb : HttpVerb.POST,
        route : '/app/viewer',
        response : {},
        params : {
            viewer : {
                type : 'object',
                required : true
            }
        }
    })
    save = (params) => {
        return new Promise((resolve, reject) => {
            try {
                if (!params.viewer['_id']) {
                    throw new ServerException('RES_ERROR_06', ['_id']);
                }

                if (params.viewer &&
                    params.viewer.toolsConfig &&
                    params.viewer.toolsConfig.layersConfig) {
                    let arr = [];
                    for (let layerConfig of params.viewer.toolsConfig.layersConfig) {
                        if (!layerConfig.$INDE && layerConfig.title !== 'INDE') {
                            arr.push(layerConfig);
                        }
                    }
                    params.viewer.toolsConfig.layersConfig = arr;
                }

                if (params.user && params.user.id && params.user.application) {
                    this.saveViewerUser(params.viewer, params.user).then((doc) => {
                        resolve(doc);
                    }, (error) => {
                        reject(error);
                    });
                } else {
                    let callback = (error, doc) => {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(doc);
                        }
                    };

                    params.viewer.projectId = params.viewer.project._id;
                    let project = params.viewer.project;
                    let id = project._id;
                    delete params.viewer.project;
                    delete project._id;

                    Project.Model.update({_id: id}, project).exec((error) => {
                        if (error) {
                            reject(error);
                        } else {
                            let document = new Viewer.Model(params.viewer);
                            document.validate((error) => {
                                try {
                                    if (error) {
                                        throw error;
                                    }

                                    Viewer.Model.findByIdAndRemove(params.viewer['_id'], (error, viewer) => {
                                        try {
                                            if (error) {
                                                throw error;
                                            }
                                            document.save(callback);
                                        } catch (err) {
                                            reject(err);
                                        }
                                    });

                                } catch (error) {
                                    reject(error);
                                }
                            });
                        }
                    });
                }
            } catch (error) {
                reject(error);
            }
        });
    };

    @EndPoint({
        verb : HttpVerb.POST,
        route : '/app/project',
        response : {},
        params : {
            project : {
                type : 'object',
                required : true
            }
        }
    })
    saveProject = (params) => {
        return new Promise((resolve, reject) => {
            try {
                let {project} = params;
                let id = project._id;
                delete project._id;

                Project.Model.update({_id: id}, project).exec((error) => {
                    if (error) {
                        reject(error);
                    } else {
                        Project.Model.findById(id, (error, doc) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(doc);
                            }
                        });
                    }
                });
            } catch (error) {
                reject(error);
            }
        });
    };

    @EndPoint({
        verb : HttpVerb.GET,
        route : '/app/viewer/:id',
        response : {}
    })
    getOneViewer = (params) => {
        return new Promise((resolve, reject) => {
            try {
                Viewer.Model.findById(params.id, (error, viewer) => {
                    try {
                        if (error) {
                            throw error;
                        }

                        if (!viewer) {
                            throw new ServerException('RES_ERROR_12', [params.id]);
                        }

                        Project.Model.findById(viewer.projectId, (error, project) => {
                            try {
                                if (error) {
                                    throw error;
                                }

                                if (!project) {
                                    throw new ServerException('RES_ERROR_12', [viewer.projectId]);
                                }

                                let viewerResult:any = viewer.toObject();
                                delete viewerResult.projectId;
                                viewerResult.project = project.toObject();

                                if (viewerResult.useINDE) {
                                    let crowler:any = new INDECrowler();
                                    viewerResult.toolsConfig.layersConfig = viewerResult.toolsConfig.layersConfig.concat(
                                        [
                                            {
                                                "_id": mongoose.Types.ObjectId(),
                                                "type":"GROUP",
                                                "title":"INDE",
                                                "$INDE" : 1,
                                                "collapsed":false,
                                                "children":crowler.getStructure(),
                                                "shown":true
                                            }
                                        ]
                                    );
                                }

                                if (viewerResult &&
                                    viewerResult.toolsConfig &&
                                    viewerResult.toolsConfig.analysisConfig) {

                                    if (viewerResult.toolsConfig.analysisConfig.length > 0 && viewerResult.toolsConfig.analysisConfig[0].analyses) {
                                        viewerResult.toolsConfig.analysisConfig = [{
                                            type: "GROUP",
                                            title: "Grupo",
                                            children: viewerResult.toolsConfig.analysisConfig[0].analyses
                                        }];
                                    }
                                }

                                if(params.userId){
                                    let p = {user_id: params.userId, old_id: viewer._id};

                                    Viewer.Model.findOne(p, (err, viewer) => {
                                        if(err){
                                            reject(error);
                                            return;
                                        }

                                        if (viewer &&
                                            viewer.toolsConfig &&
                                            viewer.toolsConfig.analysisConfig) {
                                            viewerResult.toolsConfig.analysisConfig = viewer.toolsConfig.analysisConfig;
                                        } else if (viewerResult.toolsConfig.analysisConfig.length > 0 &&
                                            viewerResult.toolsConfig.analysisConfig[0].analyses) {
                                            viewerResult.toolsConfig.analysisConfig = [{
                                                type: "GROUP",
                                                title: "Grupo",
                                                children: viewerResult.toolsConfig.analysisConfig[0].analyses
                                            }];
                                        }
                                        resolve(viewerResult);
                                    });
                                }else {
                                    resolve(viewerResult);
                                }
                            } catch (error) {
                                reject(error);
                            }
                        });

                    } catch (error) {
                        reject(error);
                    }
                });

            } catch (error) {
                reject(error);
            }
        });
    };

    @EndPoint({
        verb : HttpVerb.GET,
        route : '/viewer/:id',
        response : {}
    })
    getOneViewer2(params) {
        return this.getOneViewer(params);
    }

    private saveViewerUser(viewer, user){
        return new Promise((resolve, reject) => {
            if(!viewer.user_id){
                viewer.old_id = viewer._id;
                viewer.user_id = user.id;
                delete viewer._id;
            }

            Viewer.Model.update(
                {old_id: viewer['old_id'], user_id: viewer['user_id']},
                {$set: viewer},
                { upsert: true },
                (err) => {

                    if(err){
                        reject(err);
                        return;
                    }

                    resolve(viewer);
                });
        });
    }

}