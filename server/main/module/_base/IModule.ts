import {HttpVerb} from "../../enum/HttpVerb";
import * as Q from 'q';

/**
 * Estabele o contrato de todas as classes principais dos módulos de serviço.
 * @interface
 */
export interface IModule{

    /**
     * Método responsável em iniciar o carregamento do módulo.
     * Este método é executado no carregamento do Server.
     */
    prepare():void;

    /**
     * Método responsável em executar os carregamentos do módulo.
     * Este método é executado para cada requisição feita.
     * @param {String} operationName
     * @param {http.ClientRequest} request
     * @param {http.ServerResponse} response
     * @param {Function} next
     * @returns {Promise}
     */
    execute?(operationName:string, request:Object, response:Object, next?:Function):Q.Promise<any>;

}

export interface IModuleRoute {

    route:string;

    verb:HttpVerb;

}
