
export abstract class OGCParamsVersionBase {

    static toObject(param):any {
        let keys = Object.keys(param);
        let obj:any = {};
        let pattern = /^\_/;

        for (let key of keys) {
            if (typeof param[key] !== 'function' && param[key] !== null && param[key] !== void (0)) {
                obj[key.replace(pattern, '')] = param[key];
            }
        }

        return obj;
    }

}
