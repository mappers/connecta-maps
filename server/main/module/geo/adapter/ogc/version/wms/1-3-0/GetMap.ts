
import {GetMap as GetMap1_1_1, IGetMap as IGetMap1_1_0} from "../1-1-1/GetMap";

export class GetMap extends GetMap1_1_1 {

    version:string = '1.3.0';

    toParams(spatialDataSource, params) {
        let parameters:any = super.toParams(spatialDataSource, params);
        parameters.crs = parameters.srs;
        delete parameters.srs;
        return parameters;
    }

}

export interface IGetMap extends IGetMap1_1_0 {

}
