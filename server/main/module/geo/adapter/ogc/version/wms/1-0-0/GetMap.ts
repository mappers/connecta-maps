import {IVersion} from "../../_base/IVersion";
import {FormatImage} from "../../../../../../../enum/FormatImage";
import {LayerSchema} from "../../../../../../../data/schema/Layer";

export class GetMap implements IVersion {

    version:string = '1.0.0';
    
    toParams(params:IGetMap, layer:LayerSchema) {
        let parameters:any = {};
        parameters.request = 'GetMap';
        parameters.service = 'WMS';
        parameters.version = this.version;
        parameters.format = 'image/' + FormatImage.PNG;
        parameters.bbox = params.bbox;
        parameters.bbox = params.bbox;
        parameters.srs = params.srs;
        parameters.width = params.width;
        parameters.height = params.height;
        parameters.layers = layer.layerIdentifier;
        parameters.transparent = params.transparent;

        return parameters;
    }

}

export interface IGetMap {

    request:string;
    version:string;
    service:string;

    transparent:boolean;

    layers:string;

    format:string;

    bbox : string;

    bboxSR : string;

    srs : string;
    size: string;
    width: any;
    height: any;
}
