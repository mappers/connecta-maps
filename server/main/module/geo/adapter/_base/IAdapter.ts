import * as Q from 'q';
import {LayerSchema} from "../../../../data/schema/Layer";
import {SpatialDataSourceSchema} from "../../../../data/schema/SpatialDataSource";
import {ISpatialDataSourceParams} from "../../../admin/controller/SpatialDataSourceController";
import {QueryParams} from "../../operation/QueryOperation";

/**
 * @class
 * @classdesc Estabele os métodos contratuais de todos os Adapters.
 */
export interface IAdapter{

    /**
     * Método responsável por construir o Layer do servidor GIS em que o Adapter representar.
     * @param {Object} params
     * @returns {Promise<LayerSchema>}
     */
    buildSpatialDataSource(params:ISpatialDataSourceParams):Promise<{
        spatialDataSource: SpatialDataSourceSchema,
        layers : LayerSchema[]
    }>;

    /**
     * Testa a conexão com os dados informados pelo usuário
     * @param spatialDataSource
     * @returns {Promise<boolean>}
     */
    testConnection(spatialDataSource:SpatialDataSourceSchema):Promise<boolean>;

    /**
     * Método responsável por retornar as camadas existentes no spatialDataSource parametrizado.
     * @param spatialDataSource
     */
    getLayers(spatialDataSource: SpatialDataSourceSchema): Promise<LayerSchema[]>;

    /**
     * Método responsável por construir a Layer.
     * @param spatialDataSource
     * @returns {Promise}
     */
    buildLayer(spatialDataSource:SpatialDataSourceSchema, params:LayerSchema):Promise<any>;

    /**
     * Método responsável por resolver um GEOJSON a partir de uma consulta a um serviço externo.
     * @param {LayerSchema} spatialDataSource
     * @param {Object} params
     * @returns {Q.Promise<T>}
     */
    query(spatialDataSource:SpatialDataSourceSchema, layer:LayerSchema, params:QueryParams):Q.Promise<any>;

}
