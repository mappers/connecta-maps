
import {OGCGetMapParams1_0_0} from "../1-0-0/OGCGetMapVersion";

/**
 * @class
 * @classdesc Classe responsável pelo mapeamento dos parâmetros de requisição da operação GetMap do WMS-OGC
 * na versão 1.1.0
 */
export class OGCGetMapParams1_1_0 extends OGCGetMapParams1_0_0 {

    crs:string;

}