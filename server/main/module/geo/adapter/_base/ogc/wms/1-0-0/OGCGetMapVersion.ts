/**
 * @class
 * @classdesc Classe responsável pelo mapeamento dos parâmetros de requisição da operação GetMap do WMS-OGC
 * na versão 1.0.0.
 */
export class OGCGetMapParams1_0_0 {

    request:string = 'GetMap';
    version:string = '1.0.0';
    service:string = 'WMS';

    transparent:boolean;

    layers:string;

    format:string;

    bbox : string;

    bboxSR : string;

    srs : string;

    width : string;

    height : string;

}

