import {OperationBase} from "./_base/OperationBase";
import {IOperationParams} from "./_base/OperationBase";
import {ServerException} from "../../../global/ServerException";
import {Converter} from "../../../util/Converter";
import {customValidator} from "../../../util/Validator";
import {Request} from "../../../helper/Request";
import {IModuleRoute} from "../../_base/IModule";
import {HttpVerb} from "../../../enum/HttpVerb";
import Q = require('q');
import queryString = require('querystring');

export class GetImage extends OperationBase {

    /**
     * Define sua própria rota(EndPoint).
     * @returns {{route: string, verb: HttpVerb}}
     */
    static defineRoute():IModuleRoute {
        return {
            route : '/geo/image/href',
            verb : HttpVerb.GET
        };
    }

    /**
     *
     * @param params
     * @param dataSourceId
     * @returns {Promise<T>}
     * @override
     */
    execute(params:GetImageParams, dataSourceId?:string):Q.Promise<any> {
        let deferred = Q.defer();

        try {
            let url = Converter.decryptCipher(params.n);

            if (!customValidator.isURL(url)) {
                console.error('URL decriptada não um endereço válido. URL: %s', url);
                throw new ServerException('RES_ERROR_08');
            }

            let patternDomain = /(https?\:\/\/\w.+)\?/;

            if (patternDomain.test(url)) {
                let domain = url.match(patternDomain)[0];
                domain = domain.substr(0, domain.length - 1);
                let strParams = url.replace(patternDomain, '');
                let objParams = queryString.parse(strParams);

                let promise = Request.post(domain, objParams, {useNotify : true});
                promise.then(deferred.resolve, deferred.reject, deferred.notify);

            } else {
                let promise = Request.post(url, {}, {useNotify : true});
                promise.then(deferred.resolve, deferred.reject, deferred.notify);

            }

        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }
}

export class GetImageParams implements IOperationParams {

    /**
     * Endereço cifrado que será usado para requisitar a imagem.
     * @type {String}
     */
    n:string = '';

    /**
     *
     * @param values
     * @override
     */
    validate(values:GetImageParams):void {
        if (!values.n) {
            throw new ServerException('RES_ERROR_06', ['n']);
        }
    }
}

export class GetImageResponse {

}