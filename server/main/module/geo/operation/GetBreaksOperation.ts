import {OperationBase} from "./_base/OperationBase";
import {DataSourceServiceLoader} from "../../datasource-service/helper/DataSourceServiceLoader";
import WorkerHelper from "../../../helper/WorkerHelper";
import * as Q from "q";
import {Converter} from "../../../util/Converter";
import {GeoCache} from "../../../data/schema/GeoCache";
import {EndPoint} from "../../../decorator/EndPoint";
import {HttpVerb} from "../../../enum/HttpVerb";

interface Request {
    params:any;
}

/**
 * @class
 * @classdesc Classe responsável por prover detalhes do SpatialDataSource requerido.
 */
export class GetBreaks extends OperationBase {

    private _hash:string;

    @EndPoint({
        verb : [HttpVerb.GET, HttpVerb.POST],
        route : '/geo/:layerId/get-breaks',
        params : {
            classificationMethod:{
                type : 'string',
                required : true
            },
            classificationField:{
                type : 'string',
                required : true
            },
            breakCount:{
                type : 'number',
                required : true
            },
            project:{
                type : 'object',
                required : true
            },
            filterConfig:{
                type : 'array',
                required : false
            },
        },
        response : {

        }
    })
    execute(params:GetBreaksParams):Q.Promise<any> {
        return Q.Promise((resolve, reject, notify) => {
            try {
                let {
                    project,
                    classificationMethod,
                    classificationField,
                    breakCount,
                    filterConfig,
                    layerId
                } = params as any;

                this._hash = Converter.createHash({
                    project,
                    classificationMethod,
                    classificationField,
                    breakCount,
                    filterConfig
                });

                let richLayer = project.richLayers.filter(richLayer => String(richLayer.layer._id) === String(layerId)).pop();
                let generateResultSetCache = false;

                if (richLayer.layer.geoCache.queryCache) {
                    generateResultSetCache = true;
                }

                let getBreaks = () => {
                    let serviceType = richLayer.layer.serviceType || project.serviceType;
                    DataSourceServiceLoader.loadAndExecute(
                        serviceType as any, 'getResultSet',
                        {richLayer, project, filterConfig},
                        'getBreaksCache' in richLayer.layer.geoCache ? Boolean(richLayer.layer.geoCache.getBreaksCache) : true
                    )
                        .then(({businessData}) => {
                            try {
                                let worker = new WorkerHelper('get-breaks.js');
                                worker
                                    .execute({
                                        businessData,
                                        classificationMethod,
                                        classificationField,
                                        breakCount
                                    })
                                    .then(breaks => {
                                        if (!generateResultSetCache) {
                                            resolve(breaks);
                                        } else {
                                            resolve(breaks);
                                            this.createCache(params, breaks);
                                        }
                                    })
                                    .catch(reject);
                            } catch (error) {
                                reject(error);
                            }
                        })
                        .catch(reject);
                };

                if (generateResultSetCache) {
                    GeoCache.Model.findOne({hash:this._hash}, (error, doc) => {
                        if (error) {
                            getBreaks.call(this);
                        } else if (!doc) {
                            getBreaks.call(this);
                        } else {
                            resolve(doc.toObject().info);
                        }
                    });
                } else {
                    getBreaks.call(this);
                }
            } catch (error) {
                reject(error);
            }
        });
    }

    createCache(params, breaks) {
        GeoCache.Model.findOne({hash:this._hash}, (error, doc) => {
            try {
                if (error) {
                    throw error;
                }

                if (doc) {
                    return;
                }

                new GeoCache.Model({
                    hash : this._hash,
                    projectId : params.project._id,
                    classificationMethod : params.classificationMethod,
                    classificationField : params.classificationField,
                    breakCount : params.breakCount,
                    info : breaks
                }).save((error) => {
                    if (error) {
                        console.error('GetBreaks', error);
                    }
                });

            } catch (error) {
                console.error('GetBreaks', error);
            }
        });
    }
}


export interface GetBreaksParams {

    classificationMethod:string;
    classificationField:string;
    breakCount:number;
    project:any;
    filterConfig:any[];

}
