import {IOperationParams, OperationBase} from "./_base/OperationBase";
import {DataSourceServiceLoader} from "../../datasource-service/helper/DataSourceServiceLoader";
import {ServerException} from "../../../global/ServerException";
import * as chroma from "chroma-js";
import {EndPoint} from "../../../decorator/EndPoint";
import {HttpVerb} from "../../../enum/HttpVerb";

export class GenerateRenderer extends OperationBase {

    @EndPoint({
        verb : [HttpVerb.POST, HttpVerb.GET],
        route : '/geo/:layerId/generate-renderer',
        params : {
            field:{
                type : 'string',
                required : true,
                cast : true
            },
            project:{
                type : 'object',
                required : true
            },
            colorRamp:{
                type : 'array',
                required : true
            },
            filterConfig:{
                type : 'object',
                required : false
            }
        },
        response : {
            uniqueValues : {
                type : 'array'
            }
        }
    })
    execute(params: GenerateRendererParams): Q.Promise<any> | Promise<any> {
        return new Promise((resolve, reject) => {

            try {
                let {layerId} = params as any;
                let project = typeof params.project === "string" ? JSON.parse(params.project) : params.project;
                let richLayer = project.richLayers.filter(richLayer => richLayer.layer._id === layerId).pop();

                if (!richLayer) {
                    throw new ServerException('RES_ERROR_10', ['richLayer']);
                }

                let dataSourceIdentifier = richLayer.dataSourceIdentifier;
                let serviceType = richLayer.layer.serviceType || project.serviceType;

                return DataSourceServiceLoader.loadAndExecute(serviceType, "getDistinctValues", {
                    domainId : project.domainId,
                    field : params.field,
                    dataSourceIdentifier,
                    filterConfig : params.filterConfig,
                    richLayer
                })
                    .then((uniqueValues) => {
                        let response:GenerateRendererResponse = { uniqueValues : [] } as any;
                        let distinctObject = {};

                        for (let featureSet of uniqueValues) {
                            if (!distinctObject[featureSet[params.field]]) {
                                distinctObject[featureSet[params.field]] = 0;
                            }
                            distinctObject[featureSet[params.field]] += featureSet.COUNT || 0;
                        }

                        uniqueValues = [];

                        for (let distinctValue in distinctObject) {
                            uniqueValues.push({
                                [params.field] : distinctValue ,
                                COUNT : distinctObject[distinctValue]
                            });
                        }

                        let colors = uniqueValues.length > 1 ? chroma.scale(params.colorRamp).colors(uniqueValues.length) : [params.colorRamp[0]];

                        uniqueValues.forEach((uniqueValue, index) => {
                            response.uniqueValues.push({
                                value : uniqueValue[params.field],
                                count : uniqueValue.COUNT,
                                symbology : {
                                    fillColor : colors[index]
                                }
                            });
                        });
                        resolve(response);
                    })
                    .catch(reject);

            } catch (e) {
                reject(e);
            }
        });
    }

}

export interface GenerateRendererResponse {
    uniqueValues:{
        value : string,
        count : number,
        symbology : {
            fillColor:string
        }
    }[];
}

export interface GenerateRendererParams {
    field:string;
    project:any;
    colorRamp:string[];
    filterConfig:any;
}