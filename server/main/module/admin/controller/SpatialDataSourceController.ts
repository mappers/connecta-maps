import {ControllerBase} from "../_base/ControllerBase";
import {SpatialDataSource} from "../../../data/schema/SpatialDataSource";
import {HttpVerb} from "../../../enum/HttpVerb";
import {ServerException} from "../../../global/ServerException";
import {ServerType} from "../../../enum/ServerType";
import {AdapterHelper} from "../../geo/helper/AdapterHelper";
import {AdapterLoader} from "../../../loader/AdapterLoader";
import {Layer} from "../../../data/schema/Layer";
import {EndPoint} from "../../../decorator/EndPoint";

export class SpatialDataSourceController extends ControllerBase {

    @EndPoint({
        verb: HttpVerb.GET,
        route : '/admin/spatial-data-source',
        response : {},
        params : {
            page: {
                type : 'number',
                required : false,
                cast : true
            },
            size: {
                type : 'number',
                required : false,
                cast : true
            },
            filter: {
                type : 'array',
                required : false
            }
        }
    })
    getSpatialDataSources(params: {page: number, size: number, filter: any}): Promise<any> {
        return this._getDocumentsPaged({
            model: SpatialDataSource.Model,
            page: params.page,
            size: params.size,
            filter: params.filter
        });
    }

    @EndPoint({
        verb: HttpVerb.GET,
        route: '/admin/spatial-data-source/listAll',
        response : {}
    })
    listAll(): any {
        return SpatialDataSource.Model.find({});
    }

    @EndPoint({
        verb: HttpVerb.GET,
        route: '/admin/spatial-data-source/:_id/layers',
        response : {}
    })
    getSpatialDataSourceLayers(params: {_id: string}): Promise<any> {
        return new Promise((resolve, reject) => {
            let promise: any = SpatialDataSource.Model.findOne({_id: params._id}, {serverType: 1, dsn: 1});
            promise.catch(reject);
            promise.then(spatialDataSource => {
                let promise = new AdapterLoader(spatialDataSource.serverType).load();
                promise.catch(reject);
                promise.then(AdapterClass => {
                    let adapter = new AdapterClass();
                    let promise = adapter.getLayers(spatialDataSource);
                    promise.catch(reject);
                    promise.then(layers => {
                        resolve(layers);
                    });
                });
            });
        });
    }

    @EndPoint({
        verb: HttpVerb.GET,
        route: '/admin/spatial-data-source/:_id',
        response : {}
    })
    getSpatialDataSourceById(params: {_id: string}): Promise<any> {
        return this._getDocument(params._id, SpatialDataSource.Model);
    }

    @EndPoint({
        verb: HttpVerb.POST,
        route : '/admin/spatial-data-source',
        response : {}
    })
    insertSpatialDataSource(spatialDataSource: ISpatialDataSourceParams): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                let missingFields = [];
                if (!spatialDataSource.title) {
                    missingFields.push('title');
                }
                if (!spatialDataSource.dsn) {
                    missingFields.push('dsn');
                }
                if (!spatialDataSource.serverType) {
                    missingFields.push('serverType');
                }
                if (missingFields.length) {
                    throw new ServerException('RES_ERROR_17', [missingFields.join(', ')]);
                }
                SpatialDataSourceController.saveAllLayersFromSpatialDataSource(spatialDataSource, this._request)
                    .then(resolve, reject)
                    .catch(reject);

            } catch (error) {
                reject(error);
            }
        });
    }

    @EndPoint({
        verb: HttpVerb.DELETE,
        route: '/admin/spatial-data-source/:_id',
        response : {}
    })
    deleteSpatialDataSource(params: {_id: string}): Promise<any> {
        return new Promise((resolve, reject) => {
            Layer.Model.remove({spatialDataSourceId: params._id}, (error) => {
                try {

                    if (error) {
                        throw error;
                    }

                    this._deleteDocument(params._id, SpatialDataSource.Model)
                        .then(resolve, reject);
                } catch (error) {
                    reject(error);
                }
            });
        });

    }

    @EndPoint({
        verb: HttpVerb.PUT,
        route : '/admin/spatial-data-source/:_id',
        response : {}
    })
    updateSpatialDataSource(params: any): Promise<any> {
        return new Promise((resolve, reject) => {

            try {
                let id = params._id;
                let spatialDataSource = params;
                delete spatialDataSource._id;

                let promise:any = AdapterHelper.executeWithSpatialDataSource(spatialDataSource, "testConnection", this._request);
                promise.catch(reject);
                promise.then(() => {
                    let promise = this._updateDocument(id, spatialDataSource, SpatialDataSource.Model);
                    promise.catch(reject);
                    promise.then(resolve);
                });

            } catch (error) {
                reject(error);
            }
        });
    }

    static saveAllLayersFromSpatialDataSource(spatialDataSource, request) {
        return new Promise((resolve, reject) => {
            return AdapterHelper.executeWithSpatialDataSource(spatialDataSource, "testConnection", request)
                .then(() => {
                    return AdapterHelper.executeWithSpatialDataSource(
                        spatialDataSource,
                        'buildSpatialDataSource',
                        request
                    )
                        .then(({spatialDataSource, layers}) => {
                            let promises = [];
                            let domainId = request.headers['c-maps-domain-id'];
                            let spatialDataSourceID = spatialDataSource._id;

                            spatialDataSource.domainId = domainId;

                            if (spatialDataSource.isNew) {
                                promises.push(
                                    new SpatialDataSource.Model(spatialDataSource).save()
                                );
                            } else {
                                promises.push(new Promise((resolve, reject) => {
                                    delete spatialDataSource._id;
                                    SpatialDataSource.Model.update(
                                        {_id : spatialDataSourceID},
                                        { $set : spatialDataSource },
                                        { upsert : true },
                                        (error, doc) => {
                                            if (error) return reject(error);
                                            SpatialDataSource.Model.findOne({_id : spatialDataSourceID}, (error, spatialDataSource) => {
                                                if (error) return reject(error);
                                                resolve(spatialDataSource);
                                            });
                                        })
                                }))
                            }

                            for (let layer of layers) {
                                promises.push(
                                    updateOrSaveDocument.call(this, {
                                        spatialDataSourceId : spatialDataSourceID,
                                        layerIdentifier : layer.layerIdentifier
                                    }, layer, Layer.Model)
                                );
                            }

                            Promise.all(promises)
                                .then(() => {
                                    spatialDataSource._id = spatialDataSourceID;
                                    resolve({ layers, spatialDataSource });
                                })
                                .catch(reject);

                            function updateOrSaveDocument(conditional, layer, Model) {
                                return new Promise((resolve, reject) => {
                                    let cb = (error, result) => {
                                        if (error) {
                                            reject(error);
                                        } else {
                                            resolve(result);
                                        }
                                    };

                                    if (layer['domainId'] === domainId) {
                                        conditional['domainId'] = domainId;
                                        if (layer.toObject) {
                                            layer = layer.toObject();
                                        }
                                        delete layer._id;
                                        delete layer.__v;
                                        Model.update(conditional, layer, {upsert:true}, cb);
                                    } else {
                                        layer['domainId'] = domainId;
                                        Model.create(layer, cb);
                                    }
                                });
                            }
                        });
                })
                .catch(reject);
        });
    }
}

export interface ISpatialDataSourceParams {

    serverType?: ServerType;

    title?: string;

    description?: string;

    dsn?: string;

    user?: string;

    password?: string;

    info?: any;

}