import {ControllerBase} from "../_base/ControllerBase";
import {Viewer} from "../../../data/schema/Viewer";
import {HttpVerb} from "../../../enum/HttpVerb";
import {ServerException} from "../../../global/ServerException";
import {DataSourceServiceLoader} from "../../datasource-service/helper/DataSourceServiceLoader";
import {Project} from "../../../data/schema/Project";
import {DataSourceServiceType} from "../../../enum/DataSourceServiceType";
import {EndPoint} from "../../../decorator/EndPoint";

export class ViewerController extends ControllerBase {

    @EndPoint({
        verb : HttpVerb.GET,
        route : '/admin/viewer',
        response : {},
        params : {
            page: {
                type : 'number',
                required : false,
                cast : true
            },
            size: {
                type : 'number',
                required : false,
                cast : true
            },
            filter: {
                type : 'array',
                required : false
            }
        }
    })
    getViewers(params: {page: number, size: number, filter: any}): Promise<any> {
        return this._getDocumentsPaged({
            model: Viewer.Model,
            page: params.page,
            size: params.size,
            filter: params.filter
        });
    }

    @EndPoint({
        verb: HttpVerb.GET,
        route: '/admin/viewer/:_id',
        response : {}
    })
    getViewer(params: IViewerParams): Promise<any> {
        return this._getDocument(params._id, Viewer.Model);
    }

    @EndPoint({
        verb: HttpVerb.POST,
        route : '/admin/viewer',
        response : {}
    })
    saveViewer(params: IViewerParams): Promise<any> {
        return new Promise((resolve, reject) => {
            try {

                let missingFields = [];
                if (!params.title) {
                    missingFields.push('title');
                }

                if (missingFields.length) {
                    throw new ServerException('RES_ERROR_17', [missingFields.join(', ')]);
                }

                let promise:any;

                if (params.projectId) {
                    promise = Project.Model.findById(params.projectId);
                } else {
                    promise = Promise.resolve(new Project.Model({
                        title : params.title,
                        domainId : this._request.header('c-maps-domain-id'),
                        serviceType : params.serviceType
                    }).save());
                }

                promise.catch(reject);
                promise.then((project: any) => {
                    try {
                        if (!project) {
                            throw new ServerException('RES_ERROR_12', [params.projectId]);
                        }

                        DataSourceServiceLoader.loadAndExecute(
                            project.serviceType,
                            "saveViewer",
                            this._saveDocument.bind(
                                this,
                                Object.assign({}, params, {
                                    toolsConfig : {},
                                    projectId : project._id.toString()
                                }),
                                Viewer.Model
                            ),
                            false)
                            .then(resolve)
                            .catch(reject);
                    } catch (err) {
                        reject(err);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    @EndPoint({
        verb: HttpVerb.PUT,
        route: '/admin/viewer/:_id',
        response : {}
    })
    updateViewer(params: IViewerParams): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                let promise: any = Project.Model.findById(params.projectId);
                promise.catch(reject);
                promise.then((project: any) => {
                    try {
                        if (!project) {
                            throw new ServerException('RES_ERROR_12', [params.projectId]);
                        }

                        let promise: any = DataSourceServiceLoader.loadAndExecute(project.serviceType, "updateViewer", this._updateDocument.bind(this, params._id, params, Viewer.Model));
                        promise.catch(reject);
                        promise.then(resolve);

                    } catch (err) {
                        reject(err);
                    }
                });

            } catch (err) {
                reject(err);
            }

        });
    }

    @EndPoint({
        verb: HttpVerb.DELETE,
        route: '/admin/viewer/:_id',
        response : {}
    })
    deleteViewer(params: IViewerParams): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                let promise: any = Viewer.Model.findById(params._id);
                promise.catch(reject);
                promise.then((viewer: any) => {

                    try {
                        if (!viewer) {
                            throw new ServerException('RES_ERROR_12', [params._id]);
                        }

                        let promise: any = Project.Model.findById(viewer.projectId);
                        promise.catch(reject);
                        promise.then((project: any) => {

                            try {
                                if (!project) {
                                    throw new ServerException('RES_ERROR_12', [params.projectId]);
                                }

                                let promise: any = DataSourceServiceLoader.loadAndExecute(project.serviceType, "deleteViewer", this._deleteDocument.bind(this, params._id, Viewer.Model));
                                promise.catch(reject);
                                promise.then(resolve);

                            } catch (err) {
                                reject(err);
                            }
                        });

                    } catch (err) {
                        reject(err);
                    }

                });

            } catch (err) {
                reject(err);
            }
        });
    }

}

export interface IViewerParams {
    _id?: string;
    title: string;
    serviceType: DataSourceServiceType;
    projectId:string;
}