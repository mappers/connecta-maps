import {ServerException} from "../global/ServerException";
import * as fs from "fs";
import * as path from "path";

export class SchemaLoader{

    load():Promise<any> {
        return new Promise<any>((resolve, reject) => {
            let pathModels;

            try {
                pathModels = path.resolve(serverPath, 'data', 'schema');
                fs.readdir(pathModels, (err, files) => {
                    try {
                        if (err) {
                            throw err;
                        }

                        let i = files.length - 1;
                        let promises = [];

                        while (i >= 0) {
                            if (/\.js$/.test(files[i])) {
                                let className = files[i].replace('.js', '');
                                let Class = require(pathModels + '/' + className)[className];

                                if (!Class.load) {
                                    console.info('Todos os Schemas devem possuir o método \'load\' estático');
                                    throw new ServerException('DEV_ERROR_01');
                                }

                                promises.push(Class.load());
                            }
                            i -= 1;
                        }

                        if (!promises.length) {
                            resolve();
                        } else {
                            Promise.all<any>(promises).then(resolve, reject);
                        }

                    } catch (error) {
                        reject(err);
                    }
                });

            } catch (error) {
                reject(error);
            }
        });
    }

}
