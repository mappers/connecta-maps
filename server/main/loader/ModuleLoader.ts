import {IModule} from "../module/_base/IModule";
import {Converter} from "../util/Converter";
import * as fs from "fs";
import * as pathModule from "path";
import * as path from "path";

export class ModuleLoader {

    getPath():string {
        return pathModule.resolve(serverPath, 'module');
    }

    load() {
        return new Promise((resolve, reject) => {
            try {
                let allControllersPaths = [];
                let pathModule = path.resolve(__dirname, '../', 'module');

                let findControllers = (currentPath) => {
                    let files = fs.readdirSync(currentPath);

                    for (let fileName of files) {
                        let currentPathFile = path.resolve(currentPath, fileName);
                        if (/((Controller|Operation)\.js)$/.test(fileName)) {
                            allControllersPaths.push(currentPathFile);
                        } else if (fs.statSync(currentPathFile).isDirectory()) {
                            findControllers(currentPathFile);
                        }
                    }
                };

                findControllers(pathModule);

                for (let controllerPath of allControllersPaths) {
                    controllerPath = controllerPath.replace(serverPath, '').replace(/^\//, '../');
                    require(controllerPath);
                }

                resolve();
            } catch (error) {
                reject(error);
            }
        });
    }

    load2():Promise<Function> {
        return new Promise((resolve, reject) => {

            let path = this.getPath();

            fs.readdir(path, (error, files) => {
                try {
                    let ModulesClass = [];

                    if (error) {
                        throw error;
                    }

                    let i = 0;
                    while (i < files.length) {
                        if (!(/\./.test(files[i]))) {

                            let moduleClassName = Converter.traceCaseToCamelCase(files[i], true);
                            let moduleClassPath = pathModule.resolve(path, files[i], moduleClassName + '.js');

                            if (fs.existsSync(moduleClassPath)) {
                                ModulesClass.push(require(moduleClassPath)[moduleClassName]);
                            }
                        }
                        i += 1;
                    }

                    if (ModulesClass.length) {
                        let i = ModulesClass.length - 1;
                        while (i >= 0) {
                            let instance:IModule = new ModulesClass[i]();
                            instance.prepare();
                            i -= 1;
                        }
                    }

                    resolve();

                } catch (error) {
                    reject(error);
                }
            });

        });
    }
}
