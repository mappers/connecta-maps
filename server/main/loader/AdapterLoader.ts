import {ServerException} from "../global/ServerException";
import * as fs from "fs";
import * as path from "path";
import * as Q from "q";
import {IAdapter} from "../module/geo/adapter/_base/IAdapter";
import {ServerType} from "../enum/ServerType";

/**
 * @class
 * @classdesc Classe responsável por carregar um Adapter de um servidor parametrizado.
 */
export class AdapterLoader {

    /**
     * Nome do servidor a ser carregado.
     * @param {String} _serverName
     */
    constructor(private _serverName:ServerType) {}

    /**
     * Método responsável por carregar o Adapter.
     * @returns {Promise<IAdapter>}
     */
    load():Q.Promise<any>{
        let deferred = Q.defer(),
            adapterName,
            pathAdapter;

        try {

            adapterName = this._serverName + 'Adapter';
            pathAdapter = path.resolve(serverPath, 'module', 'geo', 'adapter', String(this._serverName).toLowerCase(), adapterName);
            fs.exists(pathAdapter + '.js', exists => {
                try {
                    if (!exists) {
                        throw new ServerException('RES_ERROR_05');
                    }

                    let AdapterClass:IAdapter = require(pathAdapter)[adapterName];
                    deferred.resolve(AdapterClass);

                } catch (error) {
                    deferred.reject(error);
                }
            });

        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }

}
