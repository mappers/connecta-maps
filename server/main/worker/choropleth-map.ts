
/**
 * Emite um GeoJSON alterado, em cada feature possui uma propriedade $style para que seja aplicado em sua tematização
 * @param event
 */
process.on('message', (config) => {
    let geoJSON = config.geoJSON,
        fillColors = config.fillColors,
        fillConfig = config.fillConfig,
        fillBreaks = config.fillBreaks,
        sizeConfig = config.sizeConfig,
        sizeBreaks = config.sizeBreaks,
        style = {
            weight: 0.5,
            opacity: 1,
            color: 'white',
            fillOpacity: 1
        },
        meters,
        i = 0;

    try {

        if (sizeConfig && sizeBreaks) {

            meters = ((meter) => {
                let j = 0,
                    arr = [];

                while (j <= sizeConfig.breakCount) {
                    arr.push(sizeConfig.minSize + (meter * j));
                    j += 1;
                }

                return arr;
            })((sizeConfig.maxSize - sizeConfig.minSize) / sizeConfig.breakCount);

        }

        while (i < geoJSON.features.length) {
            let feature = geoJSON.features[i];

            if (!feature.businessData) {
                i += 1;
                continue;
            }

            if (fillConfig && fillBreaks) {
                let fillColor = doFill(fillBreaks, fillConfig, fillColors, feature);

                if (fillColor !== undefined) {
                    feature.$style = Object.assign({}, style,
                        {fillColor: fillColor}
                    );
                }
            }

            if (/Point/.test(geoJSON.geometryType)) {
                if (sizeBreaks && sizeConfig) {
                    let radius = doSize(sizeBreaks, sizeConfig, feature, meters);

                    if (radius !== undefined) {
                        feature.$style = Object.assign({}, feature.$style,
                            {radius: radius}
                        );
                    }
                }
            }

            if (!feature.$style) {
                feature.$style = {
                    opacity: 0,
                    fillOpacity: 0
                };
            }

            i += 1;
        }

        process.send({geoJSON, meters});

    } catch (error) {
        console.log('ERROR: worker/cloropleth-map', error.message);
        throw error;
    }

    function doSize(sizeBreaks, sizeConfig, feature, meters) {
        let value = Number(feature.businessData[sizeConfig.classificationField]);
        let radius;

        if (sizeBreaks && value) {
            let i = 0;
            while (i < sizeBreaks.length) {
                if (value >= sizeBreaks[i] && value < sizeBreaks[i + 1]) {
                    radius = meters[i];
                    break;
                }
                i += 1;
            }

            // FIXME I'm scared about this shit
            if (i === meters.length) {
                radius = meters[i - 1];
            }
        }

        return radius;
    }

    function doFill(fillBreaks, fillConfig, fillColors, feature) {
        let value = Number(feature.businessData[fillConfig.classificationField]);
        let fillColor;

        if (fillBreaks && value) {
            let i = 0;
            while (i < fillBreaks.length) {
                if (value >= fillBreaks[i] && value < fillBreaks[i + 1]) {
                    fillColor = fillColors[i];
                    break;
                }
                i += 1;
            }
            // FIXME I'm scared about this shit
            if (i === fillBreaks.length) {
                fillColor = fillColors[i - 1];
            }
        }

        return fillColor;
    }
});
