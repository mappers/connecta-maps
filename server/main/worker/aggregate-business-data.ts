
process.on('message', ({richLayer, href, businessData}) => {
    let resultSetKey = richLayer.crossingKeys.resultSetKey;
    let businessDataAggregated = [];
    let mappingByKey = {};
    let mappingByKeyAll = {};
    let columns;
    let globals = {};

    // Média Global : R$ 392.009,44
    // Valor Mediana Global : R$ 387.364,55
    // Valor Médio : R$ 392.309,42
    // Valor Mínimo : R$ 392.309,42
    // Valor Máximo : R$ 392.309,42
    // Mediana do Valor : R$ 392.309,42
    // Valor Mínimo Global : R$ 371.676,63
    // Município : GAUCHA DO NORTE
    // Valor Máximo Global : R$ 436.431,38

    try {

        if (!businessData.length) {
            throw new Error('No have featuresSet!');
        }

        columns = Object.keys(businessData[0]);

        //TODO: implementar rotina prevendo campos único

        for (let featureSet of businessData) {
            let resultSetValueKey = featureSet[resultSetKey];

            if (!mappingByKey[resultSetValueKey]) {
                mappingByKey[resultSetValueKey] = {};
                mappingByKeyAll[resultSetValueKey] = {};
            }

            for (let columnName of columns) {
                if (typeof featureSet[columnName] === 'number' && columnName !== resultSetKey) {
                    if (!mappingByKey[resultSetValueKey][columnName]) {
                        mappingByKey[resultSetValueKey][columnName] = 0;
                        mappingByKeyAll[resultSetValueKey][columnName] = [];
                    }

                    mappingByKeyAll[resultSetValueKey][columnName].push(featureSet[columnName]);

                    let valuesFromAll = mappingByKeyAll[resultSetValueKey][columnName];
                    let value = mappingByKey[resultSetValueKey][columnName];

                    if (/connectad|connecta2|connecta01|172|\.tce/.test(href)) {

                        if (columnName === 'Média Global' ||
                            columnName === 'Valor Mediana Global' ||
                            columnName === 'Valor Mínimo Global' ||
                            columnName === 'Valor Máximo Global'
                        ) {
                            if (!globals[columnName]) globals[columnName] = { count : 0, result : 0 };
                            globals[columnName].count += 1;
                            globals[columnName].result += featureSet[columnName];

                        } else if (columnName === 'Valor Médio') {
                            value = 0;
                            valuesFromAll.forEach(v => value += v);
                            value = value / valuesFromAll.length;

                        } else if (columnName === 'Valor Mínimo') {
                            value = 0;
                            valuesFromAll.forEach((_value, j) => {
                                if (Number(j) === 0) {
                                    value = _value;
                                }
                                if (_value < value) {
                                    value = _value;
                                }
                            });

                        } else if (columnName === 'Mediana do Valor') {
                            value = 0;
                            let values = valuesFromAll.sort((a, b) => a - b);

                            if (values.length === 1) {
                                value = values.pop();
                            } else {
                                if (values.length % 2 !== 0) {
                                    value = values[Math.floor(values.length / 2) - 1];
                                } else {
                                    let index = values.length / 2;
                                    let calc;
                                    calc = values[index - 1] + values[index];
                                    value = calc / 2;
                                }
                            }

                        } else if (columnName === 'Valor Máximo') {
                            value = 0;
                            valuesFromAll.forEach((a, j) => {
                                if (Number(j) === 0) {
                                    value = valuesFromAll[j];
                                }
                                if (valuesFromAll[j] > value) {
                                    value = valuesFromAll[j];
                                }
                            });

                        } else {
                            value += featureSet[columnName];
                        }

                    } else {
                        value += featureSet[columnName];
                    }

                    mappingByKey[resultSetValueKey][columnName] = value;

                } else {
                    mappingByKey[resultSetValueKey][columnName] = featureSet[columnName];
                }
            }
        }

        for (let resultSetKeyValue in mappingByKey) {
            if (/connectad|connecta2|connecta01|172|\.tce/.test(href)) {
                for (let columnName in mappingByKey[resultSetKeyValue]) {
                    if (columnName === 'Média Global' ||
                        columnName === 'Valor Mediana Global' ||
                        columnName === 'Valor Mínimo Global' ||
                        columnName === 'Valor Máximo Global'
                    ) {
                        mappingByKey[resultSetKeyValue][columnName] = globals[columnName].result / globals[columnName].count;
                    }
                }
            }

            businessDataAggregated.push(mappingByKey[resultSetKeyValue]);
        }

        process.send({
            businessData : businessDataAggregated
        });

    } catch ({message}) {
        process.send({
            error : { message }
        });
    }
});