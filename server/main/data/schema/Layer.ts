import {SchemaBuilder} from "../../helper/SchemaBuilder";
import * as fs from "fs";
import * as path from "path";
import {ArcGISGeometryType, ArcGISTypeField} from "../../enum/ArcGISEnum";
import * as mongoose from 'mongoose';
import {ServerException} from "../../global/ServerException";

let Project;

export interface IField {
    alias:string;
    name:string;
    type:ArcGISTypeField;
}

/**
 * @class
 * @classdesc Classe responsável por representar os atributos dos documentos da coleção: Layer.
 */
export class LayerSchema {

    serviceType:any = { type : String, 'default' : 'connecta' };

    /**
     * Identificação da Layer em sua fonte de dados espaciais.
     * @type {String}
     */
    layerIdentifier: any = {
        type: String,
        required: true
    };

    /**
     * Título da Layer, esta será usado para exibição em aplicações clientes.
     * @type {String}
     */
    title: any = {
        type : String,
        required : true
    };

    /**
     * Descrição da Layer, esta será usado para exibição em aplicações clientes.
     * @type {String}
     */
    description: any = {
        type : String
    };

    /**
     * Configuração de cache da Layer.
     * @type {Object}
     */
    geoCache:any = {
        type : Object
    };

    /**
     * Identificação do Spatial DataSource dono da Layer.
     * @type {String}
     */
    spatialDataSourceId: any = {
        type: String,
        required: true
    };

    /**
     * Propriedade que indica se a camada é vetorial.
     * @type {Boolean}
     */
    isVector: any = {
        type: Boolean,
        required: true
    };

    /**
     * Campo geométrico da Layer.
     * @type {String}
     */
    geometryField: any = {
        type : Object
    };

    /**
     * Tipo geométrico da Layer.
     * @type {{type: String}}
     */
    geometryType:any = {
        type : String,
        enum: [
            ArcGISGeometryType.POLYLINE,
            ArcGISGeometryType.POLYGON,
            ArcGISGeometryType.POINT,
            ArcGISGeometryType.MULTIPOINT
        ],
        required: true
    };

    /**
     * Campos da Layer.
     * @type {Array}
     */
    layerFields: any = {
        type : Array
    };


    /**
     *  Informações específicas da Layer.
     * @type {Object}
     */
    info:any = {
        type : Object
    };

    /**
     *
     * @type {{type: StringConstructor; required: boolean}}
     */
    domainId: any = {
        type: String,
        required: true
    };

    capabilities: any = {
        type: Array,
        required: true
    };

}

/**
 * @class
 * @classdesc Classse responsável por registrar as construções
 */
export abstract class Layer {

    static Model: mongoose.Model<any>;
    static Schema;

    static load():Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                fs.readdir(path.resolve(serverPath, 'module', 'geo', 'operation'), (error, files) => {
                    let geoCache = {
                        type : Object
                    };

                    for (let file of files) {
                        if (/\.js$/.test(file)) {
                            geoCache[file.replace(/\.js$/, '').replace(/^\w/, (value:string) => {
                                return value.toLowerCase();
                            })] = Boolean;
                        }
                    }

                    LayerSchema.prototype.geoCache = geoCache;

                    let schemaBuilder:SchemaBuilder = new SchemaBuilder();
                    schemaBuilder.buildSchema(LayerSchema, {
                        collection : 'layer'
                    });
                    Layer.Schema = schemaBuilder.schema;

                    schemaBuilder.buildPre('remove', function (next) {
                        try {
                            Project = require('./Project').Project;

                            Project.Model.find({richLayers: { $elemMatch: {'layer._id': this._id}}}, (err, docs) => {
                                try {
                                    if (docs.length) {
                                        throw new ServerException('RES_ERROR_16', ['layer', 'projeto']);
                                    }
                                    next();
                                } catch (err) {
                                    next(err);
                                }
                            });
                        } catch (err) {
                            next(err);
                        }
                    });

                    schemaBuilder.buildModel('layer');

                    Layer.Model = schemaBuilder.getModel();
                    resolve();
                });
            } catch (err) {
                reject(err);
            }

        });
    }

}