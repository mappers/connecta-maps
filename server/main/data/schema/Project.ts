import {DataSourceServiceType} from "../../enum/DataSourceServiceType";
import {SchemaBuilder} from "../../helper/SchemaBuilder";
import * as mongoose from 'mongoose';
import {LayerSchema} from "./Layer";
import {ServerException} from "../../global/ServerException";
import {Viewer} from './Viewer'

/**
 * @class
 * @classdesc Classe responsável por representar os atributos do esquema: ToolConfig.
 */
export class ToolConfig {

    /**
     * Nome da ferramenta.
     * @type {String}
     */
    name: any = {
        type: String
    };

    /**
     * Indica se uma ferramenta está habilitada.
     * @type {Boolean}
     */
    active: any = {
        type: Boolean
    };

}

const _LayerSchema = new mongoose.Schema(new LayerSchema() as any);

/**
 * @class
 * @classdesc Classe responsável por representar os atributos do esquema: RichLayer.
 */
export class RichLayer {

    /**
     * Camada espacial vinculada à camada enriquecida.
     * @type {Array}
     */
    layer: any = _LayerSchema;

    /**
     * Datasource  vinculada a RichLayer
     * @type String
     */
    //TODO renomear este atributo
    dataSourceIdentifier:any = {
        type: String,
        required: true
    };

    dataSourceId: any = {
        type: String
        //TODO colocar como required
    };

    /**
     * Título da camada enriquecida.
     * @type {String}
     */
    title: any = {
        type: String,
        required: true
    };

    /**
     * Rótulos dos botões de navegação/drill.
     * @type {Object}
     */
    drillLabels: any = {
        up: {
            type: String
        },
        down: {
            type: String
        }
    };


    /**
     * Referência da origem de dados tabulares.
     * @type {String}
     */
    resultSetId: any = {
        required: true,
        type: String
    };

    /**
     * Configuração das chaves de cruzamento dos dados tabulares com dados espaciais.
     * @type {Object}
     */
    crossingKeys: any = {
        type: Object,
        required: true,
        geoKey: {
            type: String,
            required: true
        },
        resultSetKey: {
            type: String,
            required: true
        }
    };

    /**
     * Informações específicas de cada tipo de camada enriquecida.
     * @type {Object}
     */
    info: any = {
        type: Object
    };

}

const RichLayerSchema = new mongoose.Schema(new RichLayer() as any);
const ToolConfigSchema = new mongoose.Schema(new ToolConfig() as any);

/**
 * @class
 * @classdesc Classe responsável por representar os atributos dos documentos da coleção: Project.
 */
export class ProjectSchema {

    /**
     * Título do projeto.
     * @type {String}
     */
    title: any = {
        type: String,
        required: true
    };

    /**
     * Tipo de serviço que proverá os dados tabulares
     * @type {String}
     */
    serviceType: any = {
        type: String,
        "enum": [DataSourceServiceType.OBIEE, DataSourceServiceType.CONNECTA],
        required: true
    };

    /**
     * Descrição do projeto.
     * @type {String}
     */
    description: any = {
        type: String
    };

    /**
     * Configurações do mapa que será usado no projeto.
     @type {{initialExtent: Object, zoomMin: Number, zoomMax: Number}}
     */
    mapConfig: any = {
        type: Object,
        center: {
            type: Object
        },
        zoom: {
            type: Number
        },
        maxZoom: {
            type: Number
        },
        minZoom: {
            type: Number
        },
        'default' : () => {
            return {
                "minZoom": 3,
                "maxZoom": 20,
                "dynamicNavigation": true,
                "center": {
                    "lng": -60.5334610153028,
                    "lat": -13.8143929492345
                },
                "zoom": 5.75
            };
        }
    };

    /**
     * Configurações das ferramentas definidas no projeto.
     * @type {Object}
     */
    tools: any = {
        type: [ToolConfigSchema]
    };

    /**
     * Configurações dos Widgets definidas no projeto.
     * @type {Object}
     */
    widgets: any = {
        type: Object
    };

    /**
     * Lista da MapaBases definidos para o projeto.
     * @type {Array}
     */
    basemaps: any = {
        type: Array
    };

    /**
     * Lista de configurações das camadas enriquecidas.
     * @type {RichLayerSchema}
     */
    richLayers: any = {
        type: [RichLayerSchema]
    };

    /**
     *
     * @type {{type: StringConstructor; required: boolean}}
     */
    domainId: any = {
        type: String,
        required: true
    };

}


/**
 * @class
 * @classdesc Classe responsável por registrar as construções
 */
export abstract class Project {
    static Model: mongoose.Model<any>;

    static load(): Promise<any> {
        return new Promise((resolve, reject) => {
            let schemaBuilder: SchemaBuilder = new SchemaBuilder();
            schemaBuilder.buildSchema(ProjectSchema, {
                collection: 'project'
            });

            schemaBuilder.buildPre('remove', function (next) {
                try {
                    Viewer.Model.find({projectId: this._id}, (err, docs) => {
                        try {
                            if (docs.length) {
                                throw new ServerException('RES_ERROR_16', ['projeto', 'análise']);
                            }
                            next();
                        } catch (err) {
                            next(err);
                        }
                    });
                } catch (err) {
                    next(err);
                }
            });

            schemaBuilder.buildModel('project');
            Project.Model = schemaBuilder.getModel();
            resolve();
        });

    }
}