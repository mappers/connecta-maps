import {SchemaBuilder} from "../../helper/SchemaBuilder";
import * as mongoose from "mongoose";

export class ResultSetCacheSchema {

    hash:any = {
        type : String,
        unique : true
    };

    params:any = {
        type : Object
    };

    info:any = {
        type : mongoose.Schema.Types.Mixed
    };

}

export class ResultSetCache {

    static Model:mongoose.Model<any>;

    static load():Promise<any> {
        return new Promise((resolve) => {
            let schemaBuilder:SchemaBuilder = new SchemaBuilder();
            schemaBuilder.buildSchema(ResultSetCacheSchema, {
                collection : 'result_set_cache'
            });

            schemaBuilder.buildModel('result_set_cache');
            ResultSetCache.Model = schemaBuilder.getModel();

            resolve();
        });
    }

}
