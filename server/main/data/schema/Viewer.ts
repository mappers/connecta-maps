import * as mongoose from 'mongoose';
import {SchemaBuilder} from "../../helper/SchemaBuilder";

class PopupFieldConfigSchema {

    /**
     * @type {Boolean}
     */
    checked:any = { type : Boolean };

    /**
     * @type {String}
     */
    label:any = { type : String };

    /**
     * @type {string}
     * @enum 'number' | 'date'
     */
    typeMask:any = { type : String, enum : ['number', 'date'] };

    /**
     * Configuração específica de acordo com o tipo de máscara
     */
    config:any = {type : Object};

}

const PopupFieldConfig = new mongoose.Schema(new PopupFieldConfigSchema() as any, { _id: false });

/**
 * @class
 * @classdesc Classe responsável por representar os atributos do esquema: Legend.
 */
export class AnalysisSchema {

    /**
     * Título da análise.
     * @type {String}
     */
    title: any = {
        type: String,
        required: true
    };

    /**
     * Indica a visibilidade da análise.
     * @type {Boolean}
     */
    visible: any = {
        type: Boolean
    };

    shown:any = { type : Boolean };

    /**
     * Indica se a análise inicia-se habilitada.
     * @type {Boolean}
     */
    enable: any = {
        type: Boolean
    };

    /**
     * Indica a opacidade das camadas da análise.
     * @type {Number}
     */
    opacity: any = {
        type: Number
    };

    /**
     * Tipo atual de visualização da camada.
     * @type {String}
     */
    currentType: any = {
        type: String,
        required: true
    };

    /**
     * Configurações das visualizaçãos da camada da análise.
     * @type {Object}
     */
    configRenderer: any = {
        type: Object
    };

    /**
     *
     * @type {String}
     */
    richLayerId:any = {
        type: String,
        required : true
    };

    /**
     *Campos do filtro do Tracking
     * @type {{Array}}
     */
    trackingConfig:any = {
        type: Object,
        filter: Array,
        orderBy: String,
        uniqueValue: String
    };

    /**
     *Campos de filtro
     * @type {{Array}}
     */
    filterConfig:any = {
        type: Array
    };

    /**
     * Definição de como deverá ser tratado cada campo quanto a sua máscara e visibilidade.
     */
    outFieldsConfig:any = {
        type : [PopupFieldConfig]
    };
}

const Analysis = new mongoose.Schema(new AnalysisSchema() as any, { _id: false });

export class ToolsConfigSchema {

    layersConfig:any = {
        type: Object
    };

    analysisConfig: any = {
        type: Array
    };

}

const ToolsConfig = new mongoose.Schema(new ToolsConfigSchema() as any, { _id: false });

export class MeasureConfigSchema {

    lengthUnity:any = {
        type: String,
        default : function () {
            return 'kilometers';
        }
    };

    areaUnity:any = {
        type: String,
        default : function () {
            return 'squaredKilometers';
        }
    };

}

const MeasureConfig = new mongoose.Schema(new MeasureConfigSchema() as any, { _id: false });

/**
 * Schema de Análise.
 */
export class ViewerSchema {

    _id:any = {
        type:String,
        default : function () {
            return String(Math.round(Math.random() * 1000 + Date.now())).substr(-5);
        }
    };

    old_id:any = {
        type:Number
    };

    user_id:any ={
        type: Number
    };

	/**
	 * Projeto detentor das informações da análise.
	 * @type {Object}
	 */
	projectId: any = {
		type: String
	};

	/**
	 * Título do mapa.
	 * @type {String}
	 */
	title: any = {
		type: String,
		required: true
	};

    /**
     *
     * @type {{type: "mongoose".Schema}}
     */
    toolsConfig: any = {
        type: ToolsConfig
    };

    /**
     *
     * @type {String}
     */
    domainId: any = {
        type: String,
        required: true
    };


    /**
     *
     * @type {String}
     */
    type : any = {
        type: String,
        default : function () {
            return 'MAP';
        }
    };

    useINDE : any = {
        type: Boolean
    };

    measureConfig:any = {
        type : MeasureConfig
    };
}

/**
 * Classe responsável por contruir o modelo do schema: Analysis.
 */
export abstract class Viewer {
	static Model: mongoose.Model<any>;

	static load(): Promise<any> {
		return new Promise((resolve, reject) => {
			let schemaBuilder: SchemaBuilder = new SchemaBuilder();
			schemaBuilder.buildSchema(ViewerSchema, {
				collection: 'viewer'
			});
			schemaBuilder.buildModel('viewer');
			Viewer.Model = schemaBuilder.getModel();
			resolve();
		});

	}
}
