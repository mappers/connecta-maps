type ICallback<Result> = (error:Error, docs:Result) => void;

export abstract class ModelBase {

    abstract save(callback?:ICallback<any>):Promise<any>;

    abstract delete(callback?:ICallback<any>):Promise<any>;

    static find(conditional:Object, outFields?:Object|ICallback<any[]>, callback?:ICallback<any[]>){}

    static findOne(conditional:Object, outFields?:Object|ICallback<any>, callback?:ICallback<any>){}

    static findById(id:string|number, outFields?:Object|ICallback<any>, callback?:ICallback<any>){}

    static update(contional:Object, values:Object, operators?:Object|Function, callback?:ICallback<any>) {}

    static create() {}

    static remove() {}

    static findByIdAndRemove(){}

    static findByIdAndUpdate(){}

}