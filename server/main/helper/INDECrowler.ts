import {Converter} from "../util/Converter";

const fs = require('fs');
const path = require('path');
import {Layer} from "../data/schema/Layer";
import {SpatialDataSource} from "../data/schema/SpatialDataSource";
import * as mongoose from "mongoose";
import {CacheFileSystem} from "./CacheFileSystem";

function clError(error, message = '') {
    console.error('IBAMA ERROR => INDECrowler: ' + message, error || '');
}

let fileAlreadyCreated = false;

class INDECrowler {

    _spatialDatasourceModel:mongoose.Model<any> = SpatialDataSource.Model;

    _layerModel:mongoose.Model<any> = Layer.Model;

    RequestHelper;

    hash;

    constructor() {
        this.hash = Converter.createHash({INDE : '1', path : this.getStructureFileName()});
    }

    init() {
        Layer.Model.findOne({
            'info.INDE' : 1
        }, (error) => {
            if (error) {
                clError(error);
            } else {
                this.build();
            }
        });
    }

    build() {
        let pageSTR = '';

        pageSTR = fs.readFileSync(path.resolve(__dirname, 'data', 'INDE-BKP.html')).toString();

        //TODO: remover aqui embaixo
        let layers = this.getLayersfromPageINDE(pageSTR);
        this.saveSpatialDataSource()
            .then(spatialDataSource => {
                return this.saveLayers(spatialDataSource, layers)
                    .then(() => {
                        console.log('INDE IBAMA: its done!');
                    })
            })
            .catch(error => {
                console.error(error);
            });
    }

    saveSpatialDataSource() {
        return new Promise((resolve, reject) => {
            let conditional = {
                title : 'Visualizador INDE'
            };

            this._spatialDatasourceModel.findOne(conditional, (error, doc) => {
                try {
                    if (error) {
                        throw error;
                    }

                    if (doc) {
                        resolve(doc);
                    } else {
                        let instance = new SpatialDataSource.Model(Object.assign({}, conditional, {
                            serverType : 'OGC',
                            dsn : 'fake',
                            domainId : 'ibama'
                        }));
                        instance.save((error, doc) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(doc);
                            }
                        });
                    }

                } catch (error) {
                    reject(error);
                }
            });
        });
    }

    /**
     *
     * @param spatialDataSource
     * @param {ILayerINDE[]} layersINDE
     */
    saveLayers(spatialDataSource, layersINDE) {
        return new Promise((resolve, reject) => {
            let layersStructured = {};
            let layersFoundINDEMap = {};
            let $in = [];

            for (let layerINDE of layersINDE) {
                let layer = {
                    layerIdentifier: layerINDE.getMapParams.layers,
                    title: layerINDE.layerName,
                    name: layerINDE.layerName,
                    spatialDataSourceId: spatialDataSource._id,
                    info : {
                        INDE : 1,
                        dsn : layerINDE.dsn,
                        serverType : /arcgis/.test(layerINDE.dsn) ? 'arcgis' : 'geoserver',
                        wfs : {
                            currentVersion : '2.0.0'
                        }
                    },
                    geometryType : 'esriGeometryPolygon',
                    isVector : false,
                    capabilities : ['getMap'],
                    geoCache : {
                        queryCache : false
                    },
                    domainId : 'ibama'
                };

                let orderArray = layerINDE.groupPath.split('/');
                orderArray.pop();
                doObject(layersStructured, orderArray, 0, layerINDE.layerName);
                layersFoundINDEMap[layerINDE.layerName] = layer;
            }

            for (let layerName in layersFoundINDEMap) {
                $in.push(layerName);
            }

            this._layerModel.find({
                spatialDataSourceId : spatialDataSource._id,
                title : {
                    $in
                }
            }, (error, docs) => {
                try {
                    if (error) {
                        throw error;
                    }

                    let newLayers = [];

                    let layersNamesSaved = docs.map(doc => doc.title);

                    Object.keys(layersFoundINDEMap).forEach((layerName) => {
                        if (layersNamesSaved.indexOf(layerName) === -1) {
                            newLayers.push(layersFoundINDEMap[layerName]);
                        }
                    });

                    let buildStructure = (layersDocs) => {
                        try {
                            for (let layerDoc of layersDocs) {
                                layersFoundINDEMap[layerDoc.title] = layerDoc.toObject();
                            }

                            let doRemapping = (groupName, childrenObject) => {
                                let parentGroup = createGroup(groupName);

                                for (let subGroupName in childrenObject) {
                                    if (childrenObject[subGroupName] === 1) {
                                        parentGroup.put({
                                            "opacity" : 0.7,
                                            "enabled" : false,
                                            "_id" : mongoose.Types.ObjectId(),
                                            "title" : subGroupName,
                                            "spatialDataSourceId" : layersFoundINDEMap[subGroupName].spatialDataSourceId,
                                            "operationName" : "getMap",
                                            "layerId" : layersFoundINDEMap[subGroupName]._id
                                        });
                                    } else {
                                        parentGroup.put(
                                            doRemapping(subGroupName, childrenObject[subGroupName])
                                        );
                                    }
                                }

                                return parentGroup;
                            };

                            let structure = [];

                            for (let groupName in layersStructured) {
                                structure.push(doRemapping(groupName, layersStructured[groupName]));
                            }

                            let sort = (arr) => {
                                return arr.sort((a, b) => {
                                    if (a.children && a.children.length) {
                                        a.children = sort(a.children);
                                    }
                                    return a.title.localeCompare(b.title);
                                });
                            };

                            structure = sort(structure);

                            fileAlreadyCreated = true;

                            CacheFileSystem.createCacheSync(this.hash, structure);

                            resolve();


                        } catch (error) {
                            reject(error);
                        }
                    };

                    if (!newLayers.length) {
                        buildStructure(docs);
                    } else {
                        this._layerModel.create(newLayers, (error, docs) => {
                            if (error) {
                                reject(error);
                            } else {
                                buildStructure(docs);
                            }
                        });
                    }

                } catch (error) {
                    reject(error);
                }
            });

            function doObject(layersStructured, orderArray, i, layerName) {
                if (i < orderArray.length) {
                    if (!layersStructured[orderArray[i]]) {
                        layersStructured[orderArray[i]] = {};
                    }
                    doObject(layersStructured[orderArray[i]], orderArray, i + 1, layerName);
                } else {
                    layersStructured[layerName] = 1;
                }
            }

            function createGroup(title, children?, options = {}) {
                return Object.assign({
                    title : title,
                    _id : mongoose.Types.ObjectId(),
                    children : children || [],
                    type : "GROUP",
                    shown : false,
                    put(element) {
                        this.children.push(element);
                    }
                }, options);
            }
        });
    }

    getStructure() {
        return CacheFileSystem.getCacheSync(this.hash);
    }

    getStructureFileName() {
        return path.resolve(__dirname, 'data', 'INDE_structure.json');
    }

    /**
     * @param {String} pageSTR
     * @returns {ILayerINDE[]}
     */
    getLayersfromPageINDE(pageSTR) {
        let regex = /(new OpenLayers\.Layer\.WMS\(.+\}\))/g;

        let OpenLayers = {
            init : () => {},
            Layer : {
                WMS : function (layerName, dsn, getMapParams, {group}) {
                    return {layerName, dsn : dsn.replace(/\/wms/, '/wfs'), getMapParams, groupPath : group};
                }
            },
            Bounds : function () {}
        };

        let layersSTR = pageSTR.match(regex);
        let instances = [];

        for (let layerINDE of layersSTR) {
            OpenLayers.init();
            instances.push(eval(layerINDE));
        }

        return instances;
    }

}

export default INDECrowler;

class ILayerINDE {

    /**
     * @type {String}
     */
    get layerName() { return ''; }


    get dsn() { return ''; }

    /**
     * @type {String}
     */
    get getMapParams() { return ''; }

    /**
     * @type {String}
     */
    get groupPath() { return ''; }

}