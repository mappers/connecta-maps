import {ServerException} from "../global/ServerException";
import * as Q from "q";
import * as http from "http";
import * as queryString from "querystring";
import * as urlParse from "url-parse";

/**
 * @class
 * @classdesc Classe responsável por prover a possibilidade de requisitar de forma padronizada utilizando o protolo HTTP.
 */
export abstract class Request {

    /**
     * Método responsável por fazer requisição usando o protocolo POST.
     * @param {String} urlOrigin Endereço onde será feito a requisição.
     * @param {Object} params Parâmetros da requisição.
     * @param {RequestOptions} options Parâmetros da requisição.
     * @param {Object} headers
     * @returns {Promise<T>}
     */
    static post(urlOrigin:string, params:Object = {},
                options:RequestOptions = {useNotify : false, returnJSON : true, isString : false}, headers:any = {}):Q.Promise<any> {
        let deferred = Q.defer();

        try {
            let url = urlParse(urlOrigin, true);
            let paramsRequest = queryString.stringify(params);

            let _options = {
                hostname: url.hostname,
                port : url.port || 80,
                path: url.pathname,
                method: 'POST',
                headers: Object.assign({
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': paramsRequest.length
                }, headers)
            };

            let req = http.request(_options, (res) => {
                let promise = Request.chunkRequest(options, res);
                promise.then(deferred.resolve, deferred.reject, deferred.notify);
            });
            req.on('aborted', function (error) {
                deferred.reject(error);
            });
            req.on('abort', function (error) {
                deferred.reject(error);
            });
            req.on('error', function (error) {
                deferred.reject(error);
            });

            req.write(paramsRequest);
            req.end();

        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }

    /**
     * Método responsável por fazer uma requisição GET parametrizando pela URL
     * @param {String} urlOrigin
     * @param {Object} params
     * @param {Object} headers
     * @param {Object} parameters
     * @returns {Promise<T>}
     */
    static get(urlOrigin: string, params?: any, headers:any = {},
               parameters:{useNotify?:boolean, returnJSON?:boolean, isString?:boolean} = {}): Q.Promise<any> {

        let deferred = Q.defer();

        try {
            urlOrigin = urlOrigin.replace(/\?.*/, '');
            let url: any = urlParse(urlOrigin, true);
            let queryParams = queryString.stringify(params);
            let options = {
                hostname: url.hostname,
                port : url.port || 80,
                path: url.pathname + '?' + queryParams,
                method: 'GET',
                headers: headers
            };

            let req = http.request(options, onRequest);
            req.on('aborted', function (error) {
                deferred.reject(error);
            });
            req.on('abort', function (error) {
                deferred.reject(error);
            });
            req.on('error', function (error) {
                deferred.reject(error);
            });

            function onRequest(res) {
                let promise = Request.chunkRequest(parameters, res);
                promise.then(deferred.resolve, deferred.reject, deferred.notify);
            }

            req.end();

        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }

    static chunkRequest(options: any = {}, response): Q.Promise<any> {
        let deferred = Q.defer();
        let {statusCode} = response;
        let data = [];

        try {

            response.on('data', (chunk) => {
                try {
                    if (statusCode !== 200 && statusCode !== 302) {
                        console.error(chunk.toString());
                        throw new ServerException('RES_ERROR_08');
                    }

                    if (options.useNotify) {
                        deferred.notify([chunk, response]);
                    } else {
                        data.push((options.returnJSON || options.isString) ? chunk.toString() : chunk);
                    }
                } catch (error) {
                    deferred.reject(error);
                }
            });

            response.on('end', () => {
                try {
                    if ((!options.useNotify && options.returnJSON) || options.isString) {
                        deferred.resolve([data.join(""), response]);
                    } else if (options.useNotify) {
                        deferred.resolve([null, response]);

                    } else {
                        let buff = Buffer.concat(data, data.length);
                        deferred.resolve([buff, response]);
                    }
                } catch (error) {
                    deferred.reject(error);
                }
            });

        } catch (error) {
            deferred.reject(error);
        }

        return deferred.promise;
    }

}

export interface RequestOptions{

    useNotify?:boolean;

    returnJSON?:boolean;

    isString?: boolean;

}
