
import {Converter} from "../util/Converter";
import Config from "../Config";

/**
 * @class
 * @classdesc Classe responsável por prover como recuperar mensagens internacionalizadas.
 */
export abstract class Locale{

    /**
     * Método responsável por retornar a mensagem internacionalizada requerida.
     * @param {String} code
     * @param {Object} params
     * @param {String} forceLanguage Força em qual idioma a mensagem deve ser retornada.
     * @returns {string}
     */
    static getMessage(code:string, params:Array<String> = [], forceLanguage?:string):string {
        let message = '';

        params = params || [];

        if (!forceLanguage) {
            message = getMessage(code, Config.messages[Config.languageServer]);

        } else if (Config.messages[forceLanguage]) {
            message = getMessage(code, Config.messages[forceLanguage]);

        }

        return message;

        function getMessage(code:string, map:Object) {
            let str = '';
            if (map[code]) {

                if (params.length) {
                    str = Converter.replaceVariableInStr(map[code], params);

                } else {
                    str = map[code];
                }
            }

            return str;
        }
    }

}