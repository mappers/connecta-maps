import {Connector} from "../data/Connector";
import * as mongoose from "mongoose";

export type PreOperationData = 'save' | 'remove';

/**
 * @class
 * @classdesc Classe responsável por prover o Build das Models e Schemas das classes que representam as coleções da base
 * de dados.
 */
export class SchemaBuilder {

    /**
     * Representa a instância do Esquema da coleção a ser construída.
     * @type {mongoose.Schema}
     */
    public schema:mongoose.Schema;

    /**
     * Representa a instância da Model da coleção a ser construída.
     * @type {mongoose.Model}
     */
    private model:mongoose.Model<any>;

    /**
     * Representa a instância da conexão com a base de dados.
     * @type {mongoose.Connection}
     */
    private conn:mongoose.Connection;

    /**
     * Método responsável em chamar a conexão(base de dados) usada na aplicação.
     */
    constructor() {
        this.conn = Connector.getConnection();
    }

    /**
     *
     * @returns {mongoose.Model}
     */
    getModel():mongoose.Model<any> {
        return this.model;
    }
    
    getSchema():mongoose.Schema {
        return this.schema;
    }

    /**
     * Método responsável por construir o esquema da coleção.
     * @param {Function} Schema
     * @param {Object} [options]
     */
    buildSchema(Schema:any, options:Object = {}):void {
        if (!this.schema) {
            this.schema = new mongoose.Schema(new Schema(), options);
        }
    }

    /**
     * Método responsável por construir a Model da coleção.
     * @param {String} modelName
     */
    buildModel(modelName:string) {
        this.model = this.conn.model(modelName, this.schema);
    }

    /**
     * Método responsável por prover a possibilidade de inserir funções que serão executadas ANTES de uma
     * execução persistida.
     * @param {String} operation Pode ser: 'save' ou 'remove'
     * @param {Function} callback
     */
    buildPre(operation:PreOperationData, callback: any):void {
        this.schema.pre(operation, callback);
    }

    /**
     * Método responsável por prover a possibilidade de inserir funções que serão executadas DEPOIS de uma
     * execução persistida.
     * @param {String} operation Pode ser: 'save' ou 'remove'
     * @param {Function} callback
     */
    buildPost(operation:PreOperationData, callback: any):void {
        this.schema.post(operation, callback);
    }

}