//<reference path="./d/node.d.ts" />
///<reference path="./typings/restify-cookies.d.ts"/>
///<reference path="./typings/index.d.ts"/>
///<reference path="./typings/serve-static.d.ts"/>
///<reference path="./typings/Server.d.ts"/>
///<reference path="./typings/url-parse.d.ts"/>
///<reference path="./typings/xml2json.d.ts"/>
///<reference path="./typings/arcgis-to-geojson-utils.d.ts"/>

import {Server} from "./Server";
import * as cluster from 'cluster';
import * as OS from 'os';
import Config from "./Config";

buildServer(Config.cluster);

function buildServer(fork:boolean = false) {
    let instanceServer;

    if (fork) {
        if (cluster.isMaster) {
            let cpuCount:number = OS.cpus().length;
            let i = 0;

            while (i < cpuCount) {
                cluster.fork();
                i += 1;
            }

        } else {
            instanceServer = new Server(Config.port);
        }

    } else {
        instanceServer = new Server(Config.port);
    }

    exports.server = instanceServer;
    exports.serverPath = __dirname;

    global['server'] = instanceServer;
    global['serverPath'] = __dirname;
}
