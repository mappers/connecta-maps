declare class Error {
    static captureStackTrace(scope:any, n:any);
    constructor(message?:string);
}

/**
 * @class
 * @classdesc Classe responsável por representar Exceptions da aplicação Server.
 */
export class ServerException extends Error {

    public stack;

    __proto__:any;

    /**
     *
     * @param {String} code Código da internacionalização.
     * @param {Array} [params] Lista de valores a serem substituídos na mensagem(caso houver)
     * @param {String} [message]
     */
    constructor(public code:string, public params:Array<any> = [], public message:string = '') {
        super((message || code));

        if (typeof code !== 'string') {
            throw new Error('Código precisa ser uma string');
        }

        Error.call(this);
        Error.captureStackTrace(this, ServerException);
    }

}