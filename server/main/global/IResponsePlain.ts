/**
 * @interface
 * responsável em mapear os atributos de todos os objetos que serão enviados
 * como resposta às requisições.
 */
export interface IResponsePlain{

    /**
     * Dados da requisição.
     * @type {Object}
     */
    data:Object;

    /**
     * Nome do Command que atendeu à requisição.
     * @type {String}
     */
    commandName:string;

    /**
     * Mensagem à requisição em caso de falha.
     * @type {String}
     */
    message:string;

    /**
     * Rota de acesso ao Command de requisição.
     * @type {String}
     */
    href:string;

    /**
     * Lista de rotas possíveis ao módulo do Command envolvido.
     * @type {Array}
     */
    hrefs:Array<String>;
}