import Config from "../Config";
import * as crypto from "crypto";
let algorithm = 'aes-256-ctr';

/**
 * @class
 * @classdesc Classe abstrata responsável por prover métodos de conversão reutilizáveis.
 */
export abstract class Converter {

    /**
     * Método responsável por transformar uma string no padrão trace-case para o padrão CamelCase.
     * @param {String} str
     * @param {Boolean} firstUppercase Força primeiro caractere para que fique maísculo.
     * @returns {string}
     */
    static traceCaseToCamelCase(str:string, firstUppercase?:boolean):string {
        var value:string = str.replace(/\-./g, function(pattern, indexInStr, str) {
            return str[indexInStr + 1].toUpperCase();
        });
        value = firstUppercase ? value[0].toUpperCase() + value.substr(1) : value;
        return value;
    }

    static toObject(instance):any {
        return generateObject(instance);
        function generateObject(param) {
            let Obj = {};
            for (let key in param) {
                if(typeof param[key] !== 'function' && param[key] !== null && param[key] !== '') {
                    let keyOk = key.replace(/^\_/, '');
                    if (typeof param[key] === 'object' && !Array.isArray(param[key])) {
                        Obj[keyOk] = generateObject(param[key]);
                    } else {
                        Obj[keyOk] = param[key]
                    }
                }
            }
            return Obj;
        }
    }

    /**
     * Substitui variáveis de template pelos valores parametrizados.
     * @example:
     *
     * var str = Converter.replace("teste: ${meuValor}", { meuValor : 123 });
     * // teste: 123
     *
     * @param {String} str
     * @param {Object|Array} [values]
     * @returns {string}
     */
    static replaceVariableInStr(str:string, values:Object) {
        return str.replace(/\$\{(\w+)\}/g, function (a, b) {
            return values[b];
        })
    }

    /**
     *
     * @param {*} value
     * @returns {any}
     */
    static createHash(value:any):string {
        if (typeof value === 'object') {
            value = JSON.stringify(value);
        }
        let md5sum = crypto.createHash('md5');
        md5sum.update(value as string);
        return md5sum.digest('hex');
    }

    /**
     *
     * @param {{ operator:string, field:string, [value]:any, [values]:any }[]} filterConfig
     */
    static buildWhereClause(filterConfig:any):string {
        let operator;
        let where = '';

        if (filterConfig.length) {
            filterConfig = filterConfig.sort(reorderFilters);

            filterConfig.forEach((filter, index) => {
                operator = filter.operator;
                if (index !== 0) where += " AND ";
                where += scope()[operator]().filter(filter);
            });
        }

        return  where;

        function scope() {
            return {
                EQUAL : EQUAL,
                DIFFERENT : DIFFERENT,
                GREATER : GREATER,
                GREATER_OR_EQUAL : GREATER_OR_EQUAL,
                LESS : LESS,
                LESS_OR_EQUAL : LESS_OR_EQUAL,
                LIKE : LIKE,
                BETWEEN : BETWEEN,
                NOT_BETWEEN : NOT_BETWEEN,
                DISTINCT : DISTINCT,
                IN : IN
            };

            function EQUAL () {
                return {
                    filter : (filter) => {
                        let value = typeof filter.value === 'number' ? filter.value : '\'' + filter.value + '\'';
                        return ` ${filter.field} = ${value} `;
                    }
                }
            }

            function DIFFERENT () {
                return {
                    filter : (filter) => {
                        let value = typeof filter.value === 'number' ? filter.value : '\'' + filter.value + '\'';
                        return ` ${filter.field} <> ${value} `;
                    }
                }
            }

            function GREATER () {
                return {
                    filter : (filter) => {
                        let value = typeof filter.value === 'number' ? filter.value : '\'' + filter.value + '\'';
                        return ` ${filter.field} > ${filter.value} `;
                    }
                }
            }

            function GREATER_OR_EQUAL () {
                return {
                    filter : (filter) => {
                        let value = typeof filter.value === 'number' ? filter.value : '\'' + filter.value + '\'';
                        return ` ${filter.field} >= ${filter.value} `;
                    }
                }
            }

            function LESS () {
                return {
                    filter : (filter) => {
                        let value = typeof filter.value === 'number' ? filter.value : '\'' + filter.value + '\'';
                        return ` ${filter.field} < ${filter.value} `;
                    }
                }
            }

            function LESS_OR_EQUAL () {
                return {
                    filter : (filter) => {
                        let value = typeof filter.value === 'number' ? filter.value : '\'' + filter.value + '\'';
                        return ` ${filter.field} <= ${filter.value} `;
                    }
                }
            }

            function LIKE () {
                return {
                    filter : (filter) => {
                        return ` ${filter.field} LIKE ${filter.value} `;
                    }
                }
            }

            function BETWEEN () {
                return {
                    filter : (filter) => {
                        let initialValue = filter.initialValue;
                        let finalValue = filter.finalValue;
                        return ` ${filter.field} >= ${initialValue} AND ${filter.field} <= ${finalValue} `;
                    }
                }
            }

            function NOT_BETWEEN () {
                return {
                    filter : (filter) => {
                        let initialValue = filter.initialValue;
                        let finalValue = filter.finalValue;
                        return ` ${filter.field} < ${initialValue} OR ${filter.field} > ${finalValue} `;
                    }
                }
            }

            function DISTINCT () {
                //TODO: rever se realmente vai precisar de um DISTINCT
                return {
                    filter : (filter, resultSet) => ''
                }
            }

            function IN () {
                return {
                    filter : (filter) => {
                        let newResultSet = filter.value.map((item) =>{
                            return typeof item === 'number' ? item : '\'' + item + '\'';
                        });
                        return ` ${filter.field} IN (${newResultSet.join(',')}) `;
                    }
                }
            }
        }

        function reorderFilters (a) {
            if (a.operator === 'DISTINCT') {
                return 1;
            }
            return -1;
        }
    }

    /**
     * Método usado para encriptar um valor de forma que este possa ser decriptado posteriormente.
     * @param {String} value
     * @returns {string}
     */
    static createCipher(value:string) {
        var cipher = crypto.createCipher(algorithm, Config.keyCipher);
        var crypted = cipher.update(value,'utf8','hex');
        crypted += cipher.final('hex');
        return crypted;
    }

    /**
     * Método usado para decriptar uma cifra gerada pelo método: createCipher.
     * @param {String} cipher
     * @returns {string}
     */
    static decryptCipher(cipher:string) {
        var decipher = crypto.createDecipher(algorithm, Config.keyCipher);
        var dec = decipher.update(cipher,'hex','utf8');
        dec += decipher.final('utf8');
        return dec;
    }
}
