import * as validator from 'validator';
import * as fs from 'fs';
import * as path from 'path';
import {ServerException} from "../global/ServerException";

/**
 * Objeto que disponibiliza métodos reutilizáveis de validação.
 * @type {Object}
 */
class CustomValidator {
    
    constructor() {
        //mixin
        for (let key in validator) {
            this[key] = validator[key];
        }
    }

    /**
     * Método que provê a possibilidade de verificar se um arquivo existe, e no caso de existir este método retorna o módulo
     * exportado no arquivo encontrado.
     * @param {String} pathFile O caminho do arquivo será concatenado com o caminho do servidor, logo o parâmetro já deve
     * considerar o início a partir do /target/
     * @param {String|RegExp} fileName Nome do arquivo a ser pesquisado, pode ser utilizado uma Regex para encontrá-lo ou
     * pode ser usado o nome real do arquivo
     * @param {String|RegExp} [moduleName] Nome do módulo exportado no arquivo encontrado, pode ser usado uma Regex para
     * encontrar o nome do módulo exportado.
     * @returns {Promise<any>}
     */
    verifyExistsNReturn(pathFile:string, fileName:string|RegExp, moduleName?:string|RegExp):Promise<any> {
        return new Promise<any>((resolve, reject) => {
            try {
                if (typeof fileName !== 'string' && !(fileName instanceof RegExp)) {
                    throw new ServerException('RES_ERROR_10' , ['fileName']);
                }

                if (typeof fileName === 'string') {
                    pathFile = path.resolve(serverPath, pathFile, fileName);

                } else {
                    let files = fs.readdirSync(path.resolve(serverPath, pathFile));

                    for (let file of files) {
                        if (fileName.test(file)) {
                            pathFile = path.resolve(serverPath, pathFile, file);
                            break;
                        }
                    }
                }

                fs.exists(pathFile, (exists) => {
                    try {
                        if (!exists) {
                            throw new ServerException('RES_ERROR_10' , ['pathValue']);
                        }

                        if (moduleName && typeof moduleName !== 'string' && !(moduleName instanceof RegExp)) {
                            throw new ServerException('RES_ERROR_10' , ['moduleName']);
                        }

                        pathFile = pathFile.replace(/\..+$/, '');

                        if (!moduleName) {
                            resolve(require(pathFile));

                        } else if (typeof moduleName === 'string') {
                            resolve(require(pathFile)[moduleName]);

                        } else {
                            let modules = require(pathFile);
                            let module;

                            for (let key in modules) {
                                if (moduleName.test(key)) {
                                    module = modules[key];
                                    break;
                                }
                            }

                            resolve(module);
                        }
                    } catch (error) {
                        reject(error);
                    }
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    isValidObject(value:string):boolean {
        let isValid = false;
        try {
            JSON.parse(value);
            isValid = true;
        } catch (err) {
            
        }
        return isValid;
    }
    
    /**
     * Método responsável por validar se um valor é um objeto primitivo.
     * @param {*} value valor a ser validado.
     * @returns {Boolean}
     */
    isObject(value:any):boolean {
        var isObject = false;
        if (value && value.__proto__ == Object.prototype) {
            isObject = true;
        }
        return isObject;
    }
    
    isURL(url:string):boolean {
        return false;
    }
    
}

export let customValidator:any = new CustomValidator();