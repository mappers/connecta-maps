import {Field} from "./Field";
import {Feature} from "./Feature";
import {ArcGISGeometryType} from "../enum/ArcGISEnum";

export interface AppJSON {
    fields: Field[];
    features:Feature[];
    spatialReference:{
        wkid : number;
        latestWkid: number;
    };
    geometryType?:ArcGISGeometryType;
    objectIdFieldName?:string;
}
