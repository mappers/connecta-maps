export enum AnalyseType {

    TOC = <any>"toc",
    CHART = <any>"chart"

}

export interface Analyse {

    _id:string;

    title:string;

    type:AnalyseType;

    spatialDataSourceId:string;

    enable:boolean;

    info:AnalyseInfo|any;

    style?:AnalyseStyle;
}

export interface AnalyseInfo {

    type:string;

    config:any;

}

export interface AnalyseStyle {
    opacity?:number;
    borderSize?:number;
    borderColor?:string;
    fill?:string;
}