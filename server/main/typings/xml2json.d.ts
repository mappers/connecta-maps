
declare module "xml2js" {

    export let parseString:(value:string, callback:(error:Error, json:any) => void) => void;

}