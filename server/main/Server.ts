import {HttpVerb} from "./enum/HttpVerb";
import {ModuleLoader} from "./loader/ModuleLoader";
import {SchemaLoader} from "./loader/SchemaLoader";
import {Connector} from "./data/Connector";
import * as restify from "restify";
import Config from "./Config";
import {Sender} from "./middleware/response/Sender";
import {Getter} from "./middleware/request/Getter";
import {Authenticator} from "./middleware/request/Authenticator";
import * as CookieParser from "restify-cookies";

/**
 * @classdesc Classe responsável em definir o servidor, além de abstrair as implementações de definição de rotas, middleware,
 * e serviços essenciais do servidor.
 * @class
 */
export class Server {

    /**
     * Instância do servidor HTTP da aplicação.
     * @type {http.Server}
     * @private
     */
    private _app;

    db:any;
    geoDB:any;

    get app() {
        return this._app;
    }

    static requestMiddleware = [];

    /**
     * Método responsável em iniciar a instância do Servidor.
     * @param {Number} port
     */
    constructor(public port:Number) {
        this._app = restify.createServer({
            name : 'connecta-maps'
        });
        this._app.head(/.*/, (req, res, next) => {
            res.send('ok');
        });

        let modulesInstances = [];

        for (let Class of Config.externalModule) {
            if (Class.load) {
                Class.load(Server);
            }
            modulesInstances.push( new Class() );
        }

        let requestMiddleware:any = ([
            restify.queryParser(),
            restify.bodyParser(),
            CookieParser.parse,
            Server.registryMidCorsOrigin,
            Server.registryMidSender,
            Server.registryMidGetter,
            Server.registryMidAuthenticator
        ]).concat(Server.requestMiddleware);

        for (let Mid of requestMiddleware) {
            let fn = Mid;
            if (Server.requestMiddleware.indexOf(Mid) !== -1) {
                let instance:any = new Mid(this);
                fn = instance.build.bind(instance);
            }
            this.putMiddleware(fn);
        }

        this._app.listen(port, Config.host, () => {

            let onError = (error) => {
                console.error(error);
                process.exit();
            };

            Promise.all([
                Connector.load('mainDB'),
                Connector.load('geoDB')
            ]).then(([dbConnector, geoDBConnector]) => {
                    this.db = dbConnector;
                    this.geoDB = geoDBConnector;
                    let moduleLoader:ModuleLoader = new ModuleLoader();
                    let promise = moduleLoader.load();
                    promise.catch((error) => {
                        console.error('Banco de Dados não connectado!');
                        onError(error);
                    });
                    promise.then(() => {
                        let modelLoader:SchemaLoader = new SchemaLoader();
                        let promise = modelLoader.load();
                        promise
                            .then(() => {
                                Connector.updateDB();
                                for (let instance of modulesInstances) {
                                    instance.build(this);
                                }
                                console.log(`Servidor ConnectaMaps iniciado, rodando na porta: ${port}`);
                            })
                            .catch(onError);
                    });
                })
                .catch(onError);
        });
    }

    require(str) {
        return require(str);
    }

    /**
     * Adiciona um novo middleware à aplicação.
     * <i>Esta implementação é feita para que seja abstraído o restify, pondendo ser alterado futuramente(caso necessário)</i>.
     * @param {Function} mid
     */
    putMiddleware(mid:any) {
        this._app.use(mid);
    }

    static registryMidCorsOrigin(req, res, next) {
        res.header("Access-Control-Allow-Origin", req.header("Origin"));
        res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        res.header("Access-Control-Max-Age", "3600");
        res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
        res.header("Access-Control-Allow-Credentials", "true");
        next();
    }

    static registryMidSender(req, res:any, next) {
        res['sender'] = new Sender(req, res);
        next();
    }

    static registryMidGetter(req, res, next) {
        req.getter = new Getter(req);
        next();
    }

    static registryMidAuthenticator(req, res, next) {
        new Authenticator().execute(req, res, next);
        next();
    }

    /**
     * Adiciona uma nova rota.
     * <i>Esta implementação é feita para que seja abstraído o restify, pondendo ser alterado futuramente(caso necessário)</i>.
     * @param {HttpVerb} verb Verbo HTTP
     * @param {String} path rota de acesso
     * @param {Array|Arguments} callback Lista de argumentos das camadas a serem executadas para a rota parametrizada.
     */
    putRoute(verb:HttpVerb|string, path:string, ...callback:Array<any>) {
        let args:Array<any> = [path];
        for (let cb of callback) {
            args.push(cb);
        }
        this._app[HttpVerb.OPTIONS](path, (req, res) => {
            res.end();
        });
        this._app[verb].apply(this._app, args);
    }
}