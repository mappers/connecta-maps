const ValidarTopologia = require('./controller/ValidarTopologia');
let Router = require('./helper/Router');
const fs = require('fs');
const path = require('path');
const exec = require('child_process').exec;

class TCE_MT {

    static load(Server) {}

    build(server) {
        this.server = server;
        this.createMunicipios();
        new Router(server);
        new ValidarTopologia(server);
    }

    createMunicipios() {
        let collection = this.server.geoDB.db.collection('tce_features');
        let cursor = collection.find().limit(1);

        cursor.toArray().then(
            (docs) => {
                if (!docs.length) {
                    console.log('TCE-MT -> Importando dados de municípios...');

                    let featuresPath = path.resolve(__dirname, '../', 'data', 'municipios.geojson');
                    let command = `mongoimport --db ${collection.s.dbName} -c ${collection.s.name} --file "${featuresPath}" --jsonArray`;
                    exec(command, (error, stdout, stderr) => {
                        if (error) {
                            console.error(error);
                        }

                        if (stdout) {
                            console.log('TSE-MT -> INFO = ', stdout);
                        }

                        if (stderr) {
                            console.info('TSE-MT -> WARN = ', stderr);
                        }

                        console.log('TCE-MT -> DONE!');
                    });
                } else {
                    console.log('TCE-MT -> DONE!');
                }
            },
            error => {
                throw new Error('NÃO CONSEGUIU GERAR DADOS DO TCE_MT | STACK: ' + error.message);
            }
        );
    }

}

module.exports = TCE_MT;