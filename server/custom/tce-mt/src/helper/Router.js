
let instance;

class Router {

    constructor(server) {
        if (instance) {
            return instance;
        }
        instance = this;
        this.server = server;
    }

    get(route, _interface, cb) {
        this.server.putRoute('get', route, this._sandBox.bind(this, cb, _interface));
    }

    post(route, _interface, cb) {
        this.server.putRoute('post', route, this._sandBox.bind(this, cb, _interface));
    }

    _sandBox(cb, _interface = [], req, res, next){
        try {
            let params = {};

            for (let i in _interface) {
                params[_interface[i]] = req.params[_interface[i]];
            }

            let promise = cb(params, req, res, next);

            if (promise) {
                promise
                    .then(result => res.sender.end(result))
                    .catch(error => {
                        res.sender.error(error);
                    });

            }

        } catch (error) {
            res.sender.error(error);
        }
    }

}

module.exports = Router;