const Q = require('q');
let Router = require('../helper/Router');
const geo = require('mt-geo');

let SpatialDataSourceModel;
let LayerModel;

class ValidarTopologia {

    constructor(server) {
        this.server = server;

        SpatialDataSourceModel = server.require('./data/schema/SpatialDataSource').SpatialDataSource.Model;
        LayerModel = server.require('./data/schema/Layer').Layer.Model;

        this.init();
    }

    init() {
        new Router().get('/validaPontosNoMunicipio', [
            'geocodigo',
            'listaDecimal',
            'listaDMS'
        ], this.$get.bind(this));
    }

    /**
     *
     * @param {{
     *      geocodigo:String,
     *      listaDecimal : IPonto[],
     *      listaDMS : IGrau[]
     * }} params
     * @returns {Promise}
     */
    $get(params) {
        return new Promise((resolve, reject) => {
            let collection;

            try {
                let {listaDecimal, geocodigo, listaDMS} = params;

                if (!geocodigo) {
                    throw new Error('Parâmetro: geocodigo é obrigatório!');
                }

                if (listaDecimal) {
                    listaDecimal = typeof listaDecimal === 'string' ? JSON.parse(listaDecimal) : listaDecimal;

                    if (!listaDecimal || (listaDecimal && !listaDecimal.length)) {
                        throw new Error('Parâmetro: listaDecimal é obrigatório!');
                    }
                }

                listaDecimal = listaDecimal || [];
                
                for (let index in listaDecimal) {
                    let ponto = listaDecimal[index];
                    listaDecimal[index] = {
                        decimal : ponto,
                        dms : {
                            lat : geo.toDMS(String(ponto.lat)).replace(/([^\d])/g, (str, a) => (a + ' ')).trim(),
                            lng : geo.toDMS(String(ponto.lng)).replace(/([^\d])/g, (str, a) => (a + ' ')).trim()
                        }
                    };
                }

                if (listaDMS) {
                    listaDMS = typeof listaDMS === 'string' ? JSON.parse(listaDMS) : listaDMS;

                    let propertiesName = [
                        'grau',
                        'minuto',
                        'segundo',
                    ];

                    listaDMS.map(({lat, lng}) => {
                        let validate = (value) => {
                            for (let propertyName of propertiesName) {
                                if (!value[propertyName]) {
                                    throw new Error('Parâmetro: ' + propertyName + ' é obrigatório!');
                                }
                            }
                        };

                        let convert = (value, hemisferio) => {
                            let dms = value.grau + "º " + value.minuto + "' " + value.segundo + '" ' + hemisferio;
                            return {
                                dms,
                                decimal : geo.parseDMS(dms)
                            }
                        };

                        if (!lat) {
                            throw new Error('Parâmetro: ' + lat + ' é obrigatório!');
                        }

                        if (!lng) {
                            throw new Error('Parâmetro: ' + lng + ' é obrigatório!');
                        }

                        validate(lat);
                        validate(lng);

                        lat = convert(lat, 'S');
                        lng = convert(lng, 'W');

                        listaDecimal.push({
                            decimal : {
                                lat : lat.decimal,
                                lng : lng.decimal
                            },
                            dms : {
                                lat : lat.dms,
                                lng : lng.dms
                            }
                        });
                    });
                }

                collection = this.server.geoDB.db.collection('tce_features');

                // consulta cada ponto geograficamente para saber se o ponto está contido ou não
                Promise.all(
                    listaDecimal.map((ponto) =>
                        execQuery.call(this, Object.assign({geocodigo}, ponto))
                    )
                )
                    .then(results => {
                        return collection.find({
                            'properties.GEOCODIGO' : String(geocodigo)
                        })
                            .toArray()
                            .then(([municipio]) => {

                                if (!municipio) {
                                    throw new Error('Não existe um município com este código!');
                                }

                                let {properties} = municipio;
                                return [results, properties];
                            })
                    })
                    .then(([results, municipio]) => {
                        let response = {
                            geocodigo,
                            contidos : [],
                            naoContidos : [],
                            nome : municipio["NOME"]
                        };

                        for (let ponto of results) {
                            let {contido} = ponto;
                            delete ponto.contido;
                            if (contido) {
                                response.contidos.push(ponto);
                            } else {
                                response.naoContidos.push(ponto);
                            }
                        }

                        resolve(response);
                    })
                    .catch(reject);
            } catch (error) {
                reject(error);
            }

            function execQuery (params) {
                return new Promise((resolve, reject) => {
                    try {
                        for (let key in params) {
                            if (params[key] === undefined) {
                                throw new Error('Parâmetro: ' + key + ' é obrigatório!');
                            }
                        }

                        let {decimal, dms, geocodigo} = params;
                        let {lng, lat} = decimal;

                        collection.find({
                            $and : [
                                {
                                    'geometry': {
                                        $geoIntersects: {
                                            $geometry: {
                                                type: "Point", coordinates: [
                                                    Number(lng),
                                                    Number(lat)
                                                ]
                                            }
                                        }
                                    }
                                }
                            ]
                        }, {
                            'properties.GEOCODIGO' : 1,
                            'properties.NOME' : 1
                        })
                            .toArray()
                            .then((docs) => {
                                try {
                                    let distinctValues = {};

                                    docs.forEach((doc) => {
                                        distinctValues[doc.properties.GEOCODIGO] = doc;
                                    });

                                    docs = [];

                                    for (let key in distinctValues) {
                                        docs.push(distinctValues[key].properties);
                                    }

                                    resolve({
                                        decimal : {lat, lng},
                                        dms,
                                        interseccao : docs,
                                        contido : Boolean(
                                            docs.filter(({GEOCODIGO}) => GEOCODIGO === geocodigo).length
                                        )
                                    });
                                } catch (error) {
                                    reject(error);
                                }
                            })
                            .catch(reject);
                    } catch (error) {
                        reject(error);
                    }
                });
            };
        });
    }

}

module.exports = ValidarTopologia;

// class IGrau {
//
//     /**
//      * @type {Number}
//      */
//     grau;
//
//     /**
//      * @type {Number}
//      */
//     minuto;
//
//     /**
//      * @type {Number}
//      */
//     segundo;
//
//     /**
//      * @type {String}
//      * @enum 'lat', 'lng'
//      */
//     type;
//
// }
//
// class IPonto {
//     /**
//      * @type {Number}
//      */
//     lng;
//
//     /**
//      * @ytype {Number}
//      */
//     lat;
// }