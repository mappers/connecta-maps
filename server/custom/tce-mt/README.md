## Import GeoJSON to MongoDB

Download jq (it's sed-like program but for JSON)

Then run:

jq --compact-output ".features" input.geojson > output.geojson

then

mongoimport --db dbname -c collectionname --file "output.geojson" --jsonArray