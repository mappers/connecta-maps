const oracledb = require('oracledb');
const connector = require('../helper/Connector');
const Router = require('../helper/Router');
const EstruturasService = require('./EstruturasService');
const MapsModel = require('../helper/MapsModel');
const INDECrowler = require("../../../../target/helper/INDECrowler");

let mongoose;
let q;

class ViewerService {

    constructor(server) {
        this.server = server;
        mongoose = this.server.require('mongoose');
        q = this.server.require('q');

        this.LayerModel = this.server.require('./data/schema/Layer').Layer.Model;
        this.ViewerModel = this.server.require('./data/schema/Viewer').Viewer.Model;
        let projectModule = this.server.require('./data/schema/Project');
        this.ProjectModel = projectModule.Project.Model;
        this.Converter = this.server.require('./util/Converter').Converter;
        this.AdapterHelper = this.server.require('./module/geo/helper/AdapterHelper').AdapterHelper;
    }

    /**
     *
     * @param {String[]} processIds
     * @param {Number[]} tipologiaIds
     */
    createViewer(processIds, tipologiaIds = []) {
        return new Promise((resolve, reject) => {
            let promises = [];

            for (let processId of processIds) {
                promises.push(
                    this._getEmpreendimentoByProcessId(processId)
                        .then(empreendimento => {
                            return this._buildEstruturas(processId, tipologiaIds)
                                .then(estruturasMapByProcessId => {
                                    return this._buildLayersAndBusinessData(estruturasMapByProcessId)
                                        .then(({layers, businessData, layersMapByEstruturaName}) => {
                                            return this._buildAnalyses(empreendimento, layers, layersMapByEstruturaName, businessData, estruturasMapByProcessId);
                                        })
                                })
                        })
                );
            }

            return Promise.all(promises)
                .then((results) => {
                    let _layersIds = {};
                    let _richLayersIds = {};

                    let groupProcessos = ViewerService.createGroupAnalysis("Processos", null, { shown : true });
                    let n_businessData = {},
                        n_layers = [],
                        n_richLayers = [];

                    for (let {businessData, layers, groupProcesso, richLayers} of results) {

                        for (let key in businessData) {
                            if (!n_businessData[key]) {
                                n_businessData[key] = [];
                            }

                            n_businessData[key] = n_businessData[key].concat(businessData[key]);
                        }

                        layers.forEach(layer => {
                            if (!_layersIds[layer._id]) {
                                n_layers.push(layer);
                            }
                            _layersIds[layer._id] = true;
                        });

                        richLayers.forEach(richLayer => {
                            if (!_richLayersIds[richLayer._id]) {
                                n_richLayers.push(richLayer);
                            }
                            _richLayersIds[richLayer._id] = true;
                        });

                        if (groupProcesso) {
                            groupProcessos.put(groupProcesso);
                        }
                    }

                    this._buildViewer(
                        n_businessData,
                        n_layers,
                        groupProcessos,
                        n_richLayers
                    ).then(resolve)
                        .catch(reject);
                })
                .catch(reject);
        });
    }

    _getEmpreendimentoByProcessId(processId) {
        return new Promise((resolve, reject) => {
            connector.execute(`
            SELECT NO_EMPREENDEDOR, CD_EMPREENDIMENTO, NO_EMPREENDIMENTO FROM SISSIGA.VW_PROCESSO
            WHERE NU_PROCESSO_IBAMA = ${processId}
            `, [], { outFormat: oracledb.OBJECT })
                .then(({rows}) => {
                    resolve(rows[0]);
                })
                .catch(reject);
        });
    }

    _buildLayersAndBusinessData(estruturasMapByProcessId) {
        return new Promise((resolve, reject) => {
            let layerModel = new MapsModel(this.LayerModel);
            let layersMapByEstruturaName = {};
            let allEstruturasArray = [];
            let $inLayers = [];
            let $inQuery = {};

            for (let processId in estruturasMapByProcessId) {
                let estruturas = estruturasMapByProcessId[processId];
                allEstruturasArray = allEstruturasArray.concat(estruturas);
            }

            if (!allEstruturasArray.length) {
                throw new Error('Não existem estruturas para este processo');
            }

            allEstruturasArray.forEach((value) => {
                $inLayers.push(value['NO_TABELA_ESTRUTURA']);
                $inQuery[value['CD_PROGRESSAO_EMPREENDIMENTO']] = true;
            });

            return layerModel.find({
                title : {
                    $in : $inLayers
                }
            })
                .then(layers => {
                    let promisesQuery = [];
                    let $in_NO_TABELA = [];

                    for (let layer of layers) {
                        let estrutura = allEstruturasArray.filter((e) => e['NO_TABELA_ESTRUTURA'] === layer.title)[0];
                        layer.$hash = layer.title + ':' + estrutura['CD_PROGRESSAO_EMPREENDIMENTO'];
                        promisesQuery.push(
                            this.AdapterHelper.executeWithLayer(layer._id.toString(), 'query', {
                                where : '1=1 AND ' + (
                                    'CD_PROGRESSAO_EMPREENDIMENTO IN(' + Object.keys($inQuery).map(key => '\'' + key + '\'').join(',') + ')'
                                ),
                                returnGeometry : false
                            })
                        );
                        $in_NO_TABELA.push('\'V' + layer.title + '\'');
                    }

                    if (!layers.length) {
                        throw new Error('Nenhuma camada encontrada');
                    }

                    return q.allSettled([
                        q.allSettled(promisesQuery),
                        EstruturasService.getEstruturaFields(null, `NO_TABELA IN (${$in_NO_TABELA.join(',')})`)
                    ])
                        .then(([resultsGeoJSONs, allOutFields]) => {
                            let geoJSONs = [];
                            let aliasesIBAMA = allOutFields.value.rows;
                            let result = [
                                geoJSONs,
                                aliasesIBAMA
                            ];
                            let oldLayers = [].concat(layers.map(l => Object.assign({}, l.toObject(), {$hash : l.$hash})));
                            layers = [];

                            for (let i in resultsGeoJSONs.value) {
                                let promiseResult = resultsGeoJSONs.value[i];
                                let layerPlainObject = oldLayers[i];

                                if (promiseResult.state !== 'rejected' &&
                                    promiseResult.value.features &&
                                    promiseResult.value.features.length) {
                                    geoJSONs.push(promiseResult.value);
                                    layerPlainObject.domainId = 'ibama';
                                    layers.push(layerPlainObject);
                                    layersMapByEstruturaName[layerPlainObject.title] = layerPlainObject;
                                } else {
                                    console.log(
                                        'Não conseguiu montar a layer: ',
                                        layerPlainObject.title,
                                        promiseResult
                                    );
                                }
                            }

                            return result;
                        })
                        .then(([resultsGeoJSONs, aliasesIBAMA]) => {
                            let businessData = {};

                            resultsGeoJSONs.forEach((geoJSON, index) => {
                                let fields = {};

                                geoJSON.fields.forEach((field) => {
                                    for (let value of aliasesIBAMA) {
                                        if (field.name === value['NO_COLUNA']) {
                                            fields[value['NO_COLUNA']] = {
                                                name : value['NO_COLUNA'],
                                                alias : value['NO_ALIAS'],
                                                type : field.type
                                            };

                                        } else if (field.name === 'CD_ESTRUTURA' ||
                                            field.name === 'CD_PROGRESSAO_EMPREENDIMENTO') {
                                            fields[field.name] = {
                                                name : field.name,
                                                alias : field.name,
                                                type : field.type
                                            };
                                        }
                                    }
                                });

                                geoJSON.fields = Object.keys(fields).map(nameColumn => fields[nameColumn]);
                                layers[index].layerFields = geoJSON.fields;

                                businessData[layers[index].title] =
                                    geoJSON.features.map(
                                        feature => feature.properties
                                    );
                            });

                            resolve({
                                layers, businessData, layersMapByEstruturaName
                            });
                        })
                        .catch(reject)
                })
        });
    }

    _buildAnalyses(empreendimento, layers, layersMapByEstruturaName, businessData, estruturasMapByProcessId) {
        return new Promise((resolve, reject) => {
            /**
             * TODO:
             * - iterar nas fases do processo -> dado será consumido ainda
             * -- Para cada fase deverá conter uma grupo de análises, por exemplo: FCA, requerimento de licenciamento
             * */

                // Processos -> Processo 1 -> FCA -> Elementos do Projeto -> [CI_SEILA_1, CI_SEILA_2]

            let richLayers = [];

            let mappingRichLayersByLayerId = {};

            for (let layer of layers) {
                let richLayer = {
                    "_id": layer._id, //REVER
                    "title" : layer.title,
                    "layer" : layer,
                    "crossingKeys" : {
                        "resultSetKey" : "CD_ESTRUTURA",
                        "geoKey" : "CD_ESTRUTURA"
                    },
                    "domainId" : "ibama",
                    "dataSourceIdentifier" : layer._id.toString(),
                    "resultSetId" : layer.title
                };
                mappingRichLayersByLayerId[layer._id.toString()] = richLayer;
                richLayers.push(richLayer);
            }

            let groupProcesso;

            for (let processId in estruturasMapByProcessId) {
                groupProcesso = ViewerService.createGroupAnalysis(
                    empreendimento.NO_EMPREENDIMENTO + ` (Nº ${processId})`,
                    null,
                    {
                        id: processId,  shown : true
                    }
                );

                let groupFCA = ViewerService.createGroupAnalysis("FCA", null);
                groupProcesso.put(groupFCA);

                for (let estrutura of estruturasMapByProcessId[processId]) {
                    let NOME_ESTRUTURA = estrutura['NO_TABELA_ESTRUTURA'];
                    let LABEL_ESTRUTURA = estrutura['NO_TP_ESTRUTURA'];
                    let CD_PROGRESSAO_EMPREENDIMENTO = estrutura['CD_PROGRESSAO_EMPREENDIMENTO'];
                    let layer = layersMapByEstruturaName[NOME_ESTRUTURA];

                    if (!layer) {
                        continue;
                    }

                    let richLayer = mappingRichLayersByLayerId[layer._id.toString()];

                    let color = ViewerService.generateColor();

                    let outFieldsConfig = {};

                    layer.layerFields.forEach((outField) => {
                        outFieldsConfig[outField.name] = {
                            label: outField.alias,
                            typeMask : 'none'
                        };
                    });

                    let analysis = {
                        "_id": mongoose.Types.ObjectId(),
                        "title" : LABEL_ESTRUTURA,
                        "enable": false,
                        "description": "...",
                        "richLayerId": richLayer._id.toString(),
                        "tags": [],
                        "refreshTime": 0,
                        "opacity": 0.8,
                        "currentType": "simpleRenderer",
                        "infoWindowConfig": [],
                        "timeLineIsActive": false,
                        "type" : "ANALYSIS",
                        "filterConfig" : [
                            {
                                "operator" : "EQUAL",
                                "value" : CD_PROGRESSAO_EMPREENDIMENTO,
                                "field" : "CD_PROGRESSAO_EMPREENDIMENTO",
                                "valueType" : "string"
                            }
                        ],
                        outFieldsConfig,
                        "configRenderer": {
                            "simpleRenderer": {
                                "fill": color,
                                "outlineColor": color,
                                "size": 5,
                                "weight": 4,
                                "stroke" : true,
                                "customSymbology": {"fileName": "", "base64": "", "iconText": ""}
                            },
                            "cluster" : {
                                "uniqueSize": false,
                                "outlineColor": "#000",
                                "colorRamp": [ViewerService.generateColor(), ViewerService.generateColor()],
                                "breakCount": 4,
                                "size": 12,
                                "distance": 12
                            },
                            "heatmap" : {
                                "blurRadius": 5,
                                "colors": [ViewerService.generateColor(), ViewerService.generateColor()],
                                "field": ""
                            }
                        }
                    };
                    groupFCA.put(analysis);
                }

                if (!groupFCA.children.length) {
                    groupProcesso = null;
                }else{
                    groupFCA = ViewerService.ordenarPorGeometryType(groupFCA, richLayers);
                }
            }

            if (!richLayers || (richLayers && !richLayers.length)) {
                throw new Error('Não possui layers');
            }

            resolve({businessData, layers, groupProcesso, richLayers});
        });
    }

    _buildViewer(businessData, layers, groupProcessos, richLayers) {
        return new Promise((resolve, reject) => {

            let richLayersInfo = [];

            let project = new this.ProjectModel({
                "widgets" : {
                    "zoomHistory" : true,
                    "zoom" : true,
                    "northScale" : true,
                    "miniature" : true,
                    "localization" : true,
                    "geoSearch" : true,
                    "areaZoom" : true
                },
                "serviceType" : "geo",
                "mapConfig" : {
                    "zoom" : 4,
                    "center" : {
                        "lng" : -49.6142578125,
                        "lat" : -15.6230368315283
                    },
                    "maxZoom" : 20,
                    "minZoom" : 3
                },
                "title" : "Visualizador",
                "domainId" : "ibama",
                "richLayers" : richLayers,
                "basemaps" : [
                    "topo"
                ],
                "tools" : [
                    {
                        "name" : "buffer"
                    },
                    {
                        "name" : "geoProcessing"
                    },
                    {
                        "name" : "fileUpload"
                    },
                    {
                        "name" : "legend"
                    },
                    {
                        "name" : "timeline"
                    },
                    {
                        "name" : "spatialFilter"
                    },
                    {
                        "name" : "comparison"
                    }
                ]
            });

            for (let richLayer of project.richLayers) {
                richLayersInfo.push({
                    "richLayerId" : richLayer._id.toString(),
                    "outFields" : richLayer.layer.layerFields.map((field) => (
                        {
                            "name" : field.name,
                            "alias" : field.alias,
                            "valueType" : 'string'
                        }
                    ))
                });
            }

            let viewer = new this.ViewerModel({
                "projectId" : project._id.toString(),
                "viewContext" : "geo",
                "title" : "SIGA",
                "initialRichLayerId" : project.richLayers[0]._id,
                "allowDrill" : true,
                "domainId" : "ibama",
                "toolsConfig" : {
                    "analysisConfig" : [groupProcessos],
                    "layersConfig" : new INDECrowler().getStructure()
                },
                "type" : "MAP"
            });
            let result = viewer.toObject();
            result.project = project;
            resolve({viewer : result, businessData});
        });
    }

    /**
     *
     * @param processId
     * @param tipologiaIds
     * @returns {Promise}
     * @private
     */
    _buildEstruturas(processId, tipologiaIds = []) {
        return new Promise((resolve, reject) => {
            let tipologias = [];

            if (tipologiaIds.length) {
                return EstruturasService.getEstruturasByProcessId(processId)
                    .then((resultEstruturas) => {
                        return EstruturasService.getTipologiasById(tipologiaIds)
                            .then(({rows}) => {
                                //preciso saber as estruturas
                                //richLayers
                                //richLayersInfo
                                //analysis
                                //businessData
                                //chave de cruzamento
                                tipologias = rows;
                                return resultEstruturas;
                            });
                    });
            } else {
                return EstruturasService.getEstruturasByProcessId(processId)
                    .then(resultEstruturas => {
                        resolve({
                            [processId] : resultEstruturas.rows
                        });
                    });
            }
        });
    }

    static ordenarPorGeometryType(groupFCA, richLayers){
        return groupFCA.children.sort((analysisA, analysisB) => {
            let layerA = richLayers.filter((item) => item._id.toString() === analysisA.richLayerId)[0].layer;
            let layerB = richLayers.filter((item) => item._id.toString() === analysisB.richLayerId)[0].layer;

            if(layerA.title === 'CI_AREA_ESTUDO'){
                return 1;
            }

            if(layerA.geometryType === layerB.geometryType){
                return 0;
            }

            if((layerA.geometryType === 'esriGeometryPoint' &&
                (layerB.geometryType === 'esriGeometryPolyline' || layerB.geometryType === 'esriGeometryPolygon')) ||
                (layerA.geometryType === 'esriGeometryPolyline' && layerB.geometryType === 'esriGeometryPolygon')){
                return -1;
            }

            return 1;
        });
    }

    static createGroupAnalysis(title, children, options = {}) {
        return Object.assign({
            title : title,
            _id : options.id || mongoose.Types.ObjectId(),
            children : children || [],
            type : "GROUP",
            shown : false,
            put(element) {
                this.children.push(element);
                return this.children.length - 1;
            }
        }, options);
    }

    static generateColor() {
        let color = '#' + Math.floor(Math.random() * 16777215).toString(16);
        if (/\#ffffff/i.test(color)) {
            color = generateColor();
        }
        return color;
    }
}

module.exports = ViewerService;
