const oracledb = require('oracledb');
const connector = require('../helper/Connector');
const SQLHelper = require('../helper/SQLHelper');
const fs = require('fs');
const path = require('path');

class EstruturasService {

    static getEstruturasByProcessId(processId) {
        return EstruturasService._all('SQL_SELECT_ESTRUTURAS_BY_NUM_PROCESSO.sql', { processId });
    }

    static getSetor() {
        return EstruturasService._all('SQL_SETOR.sql');
    }

    /**
     *
     * @param {Number[]} tipologiasIds
     */
    static getTipologiasById(tipologiasIds) {
        return EstruturasService._all('SQL_SELECT_TIPOLOGIAS.sql', { where : 'ID_TIPOLOGIA IN(' + tipologiasIds.join(',') + ')' });
    }

    static getTipologiasBySetor(setorId) {
        let where = setorId ? `TI.CD_SETOR_TIPOLOGIA=${setorId}` : '1=1';
        return EstruturasService._all('SQL_SELECT_TIPOLOGIAS.sql', { where });
    }

    static getEstruturaFields(estruturaName, where) {

        if (!where) {
            where = `NO_TABELA = '${estruturaName}'`;
        }

        return EstruturasService._all('SQL_TB_ALIAS_LABELS.sql', {where});
    }

    static _all(sqlFileName, params = {}) {
        return new Promise((resolve, reject) => {
            let str = SQLHelper.getSql(sqlFileName, params);
            connector.execute(str, [], { outFormat: oracledb.OBJECT })
                .then(resolve)
                .catch(reject);
        });
    }

}

module.exports = EstruturasService;