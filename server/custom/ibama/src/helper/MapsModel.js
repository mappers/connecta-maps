
class MapsModel {

    constructor(Model) {
        this.Model = Model;
    }

    find(conditional = {}, outFields = {}) {
        return new Promise((resolve, reject) => {
            this.Model.find(conditional, outFields, (error, docs) => {
                if (error) {
                    reject(error);
                } else  {
                    resolve(docs);
                }
            });
        });
    }

    findOne(conditional, outFields) {
        return new Promise((resolve, reject) => {
            this.Model.findOne(conditional, outFields, (error, docs) => {
                if (error) {
                    reject(error);
                } else  {
                    resolve(docs);
                }
            });
        });
    }

    update(conditional, value, modifiers = {}) {
        return new Promise((resolve, reject) => {
            this.Model.update(conditional, value, modifiers, (error, docs) => {
                if (error) {
                    reject(error);
                } else  {
                    resolve(docs);
                }
            });
        });
    }

}

module.exports = MapsModel;