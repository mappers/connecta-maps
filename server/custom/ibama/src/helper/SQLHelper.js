const config = require('../config');
const fs = require('fs');
const path = require('path');

let filesCache = {};

class SQLHelper {

    static getSql(sqlFileName, params) {
        if (!filesCache[sqlFileName]) {
            filesCache[sqlFileName] = fs.readFileSync(path.resolve(__dirname, '../service/sql', sqlFileName)).toString();
        }
        return filesCache[sqlFileName]
            .replace(/\$\{([\w\d]+)\}/g, (str, key) => {
                return params[key] || '';
            })
            .replace(/[\n]+/g, ' ');
    }

}

module.exports = SQLHelper;