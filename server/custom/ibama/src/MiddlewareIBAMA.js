
let pattern = /\/geo\/([\w\d]+)\/([\w\d]+)/;

class MiddlewareIBAMA {

    constructor(server) {
        this.server = server;
    }

    invoke(name) {
        return this.server.require(name);
    }

    build (request, response, next) {
        try {
            let match = request.url.match(pattern);

            if (match && match.length) {
                let layerId = match[1];
                let operationName = match[2];
                let LayerModel = this.invoke('./data/schema/Layer').Layer.Model;
                let SpatialDataSourceModel = this.invoke('./data/schema/SpatialDataSource').SpatialDataSource.Model;
                let OGCAdapter = this.invoke('./module/geo/adapter/ogc/OGCAdapter').OGCAdapter;
                let GeoModule = this.invoke('./module/geo/Geo').Geo;

                LayerModel.findOne({
                    _id : layerId
                }, (error, layerDoc) => {
                    try {
                        if (error) {
                            throw error;
                        }

                        if (!layerDoc) {
                            throw new Error('No layers found! ID not identified: ' +  layerId);
                        }

                        if (!layerDoc.info || (layerDoc.info && !layerDoc.info.INDE)) {
                            next();
                            return;
                        }

                        SpatialDataSourceModel.findOne({
                            _id : layerDoc.spatialDataSourceId
                        }, (error, spatialDataSourceDoc) => {
                            try {
                                if (error) {
                                    throw error;
                                }

                                if (!spatialDataSourceDoc) {
                                    throw new Error('No spatialDataSources found!');
                                }

                                let adapter = new OGCAdapter(request);

                                if (!adapter[operationName]) {
                                    throw new Error('Operation name no exists!');
                                }

                                let operations = GeoModule.instance.getOperations()[operationName.replace(/(.)/, (a, b)=> b.toUpperCase() )];
                                let paramsInstance = new operations.Params(request);
                                let paramsRequest = GeoModule.getParams(paramsInstance, request);
                                paramsInstance.validate(paramsRequest);

                                adapter[operationName]({
                                    dsn : layerDoc.info.dsn
                                }, layerDoc, paramsInstance)
                                    .then(
                                        response.sender.end.bind(response.sender),
                                        response.sender.error.bind(response.sender),
                                        response.sender.chunk.bind(response.sender)
                                    )
                                    .catch(
                                        response.sender.error.bind(response.sender)
                                    );

                            } catch (error) {
                                response.sender.error(error);
                            }
                        });

                    } catch (error) {
                        response.sender.error(error);
                    }
                });
            } else {
                next();
            }
        } catch (error) {
            response.sender.error(error);
        }
    }

}

module.exports = MiddlewareIBAMA;