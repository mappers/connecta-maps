const Conversor = {

    /**
     *
     * @param {{ field:string, operator:string, [value]:* }[]} params
     */
    getSQLString(tbName, params = []) {
        let outFields = params.map(({field}) => field);
        let mainStr = `SELECT ${outFields.join(', ')} FROM ${tbName}`;

        let putStr = (str, isAnd = true) => {
            mainStr += ` ${ mainStr.length ? (isAnd ? ' AND ' : ' OR ') : '' } ${str} `;
        };

        let mappingOperator = {
            'in' : ({field, value}) => {
                return ` ${field} IN(${value.map((v) => `'${v}'` ).join(', ')}) `;
            },
            'distinct' : ({field}) => {
                mainStr = mainStr.replace(/SELECT.+FROM/, `SELECT DISTINCT ${field} FROM`);
                return '';
            }
        };

        for (let param of params) {
            let str = mappingOperator[param.operator](param);
            if (str) putStr(str);
        }

        return mainStr;
    }

};

module.exports = Conversor;